#!/bin/sh
# Lancement de Fudaa-LSPIV

# A modifier suivant l'installation du JDK.
JAVA=%{JAVA_HOME}

# A modifier suivant la version de Fudaa-LSPIV
JAR=fudaa-lspiv.jar

if [ "%{ISO3_LANG}" = "fra" ] ; then PIV_LANG="fr"; fi
if [ "%{ISO3_LANG}" = "eng" ] ; then PIV_LANG="fr"; fi

# Pour retrouver la librairie libgfortran
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:"%{INSTALL_PATH}/exes"

# Verification que le java existe bien, sinon message.
if [ ! -f $JAVA/bin/java ]
then

  if [ $PIV_LANG = "fr" ]
  then
    echo "Java a probablement ete modifie (ou mis a jour)" > /tmp/error_mess
    echo "et Fudaa-LSPIV ne fonctionnera plus." >> /tmp/error_mess
    echo "Merci de reinstaller Fudaa-LSPIV" >> /tmp/error_mess
  else
    echo "Java has probably been modified (or updated)" > /tmp/error_mess
    echo "and Fudaa-LSPIV will no longer work." >> /tmp/error_mess
    echo "Please reinstall Fudaa-LSPIV" >> /tmp/error_mess
  fi

  xmessage -center -buttons ok -file /tmp/error_mess
  rm /tmp/error_mess
  exit 1
fi

$JAVA/bin/java -Xmx2048m -Dpiv.lang="$PIV_LANG" -Dpiv.exe.path="%{INSTALL_PATH}/exes" -Dpiv.doc.path="%{INSTALL_PATH}/doc" -Dpiv.templates.path="%{INSTALL_PATH}/templates" -cp "%{INSTALL_PATH}/lib/$JAR:%{INSTALL_PATH}/lib/jai_imageio.jar" org.fudaa.fudaa.piv.Piv $1 $2 $3 $4 $5 $6 $7 $8 $9

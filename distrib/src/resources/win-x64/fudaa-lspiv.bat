@echo off
:: Lancement de Fudaa-LSPIV

:: Pour retrouver le chemin absolu vers l'application
set APP_PATH=%~dp0

::$JAVA/bin/java -Xmx2048m -Dpiv.lang="$PIV_LANG" -Dpiv.exe.path="/home/bm/Fudaa-LSPIV 1.9.2/exes" -Dpiv.doc.path="/home/bm/Fudaa-LSPIV 1.9.2/doc" -Dpiv.templates.path="/home/bm/Fudaa-LSPIV 1.9.2/templates" -cp "/home/bm/Fudaa-LSPIV 1.9.2/lib/$JAR:/home/bm/Fudaa-LSPIV 1.9.2/lib/jai_imageio.jar" org.fudaa.fudaa.piv.Piv $1 $2 $3 $4 $5 $6 $7 $8 $9
"%APP_PATH%\appli\appli.exe" --exepath "%APP_PATH%exes" --docpath "%APP_PATH%doc" --templatepath "%APP_PATH%templates"

package org.fudaa.fudaa.piv;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.table.AbstractTableModel;

import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteraction;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteractionTargetI;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.commun.EbliSelectedChangeListener;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.piv.action.PivEnterGeometryAction;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;
import org.fudaa.fudaa.piv.metier.PivProject;

import com.memoire.fu.FuLog;

/**
 * Un panneau de saisie des param�tres de transformation pour la mise � l'�chelle.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivScalingTransformationSubPanel extends JPanel {
  
  class CoupleRow {
    /** Peut �tre null si modifi� par l'utilisateur. */
    Integer i1;
    /** Peut �tre null si modifi� par l'utilisateur. */
    Integer j1;
    /** Peut �tre null si modifi� par l'utilisateur. */
    Integer i2;
    /** Peut �tre null si modifi� par l'utilisateur. */
    Integer j2;
    /** Peut �tre null quand pas saisi. */
    Double x1;
    /** Peut �tre null quand pas saisi. */
    Double y1;
    /** Peut �tre null quand pas saisi. */
    Double x2;
    /** Peut �tre null quand pas saisi. */
    Double y2;
    
    /**
     * @return True si tous les champs de coordonn�es image sont remplis
     */
    public boolean isImgPointsFilled() {
      return i1 != null && i2 != null && j1 != null && j2 != null;
    }
    
    /**
     * @return True si tous les champs de coordonn�es reelles sont remplis
     */
    public boolean isRealPointsFilled() {
      return x1 != null && x2 != null && y1 != null && y2 != null;
    }
  }
  
  /**
   * Un modele pour les couples / (distance ou coordonn�es)
   * @author Bertrand Marchand (marchand@detacad.fr)
   */
  class CoupleTableModel extends AbstractTableModel {
    int COL_I1 = 0;
    int COL_J1 = 1;
    int COL_X1 = 2;
    int COL_Y1 = 3;
    int COL_I2 = 4;
    int COL_J2 = 5;
    int COL_X2 = 6;
    int COL_Y2 = 7;
    
    List<CoupleRow> rows = new ArrayList<>();
    
    @Override
    public int getRowCount() {
      return rows.size();
    }

    @Override
    public int getColumnCount() {
      return 8;
    }

    @Override
    public Object getValueAt(int row, int column) {
      if (column == COL_I1) {
        return rows.get(row).i1;
      }
      else if (column == COL_J1) {
        return rows.get(row).j1;
      }
      else if (column == COL_I2) {
        return rows.get(row).i2;
      }
      else if (column == COL_J2) {
        return rows.get(row).j2;
      }
      else if (column == COL_X1) {
        return rows.get(row).x1;
      }
      else if (column == COL_Y1) {
        return rows.get(row).y1;
      }
      else if (column == COL_X2) {
        return rows.get(row).x2;
      }
      else {
        return rows.get(row).y2;
      }
    }

    @Override
    public String getColumnName(int column) {
      if (column == COL_I1) {
        return PivResource.getS("I1");
      }
      else if (column == COL_J1) {
        return PivResource.getS("J1");
      }
      else if (column == COL_I2) {
        return PivResource.getS("I2");
      }
      else if (column == COL_J2) {
        return PivResource.getS("J2");
      }
      else if (column == COL_X1) {
        return PivResource.getS("X1") + " (m)";
      }
      else if (column == COL_Y1) {
        return PivResource.getS("Y1") + " (m)";
      }
      else if (column == COL_X2) {
        return PivResource.getS("X2") + " (m)";
      }
      else {
        return PivResource.getS("Y2") + " (m)";
      }
    }
    
    @Override
    public boolean isCellEditable(int row, int column) {
        return column>=0 && column<=7;
    }

    private Double getDoubleValue(Object aValue) {
      if (!aValue.toString().trim().isEmpty()) {
        // Double attendu
        try {
          return Double.parseDouble(aValue.toString().trim());
        }
        catch (NumberFormatException _exc) {
        }
      }
      
      return null;
    }

    private Integer getIntegerValue(Object aValue) {
      if (!aValue.toString().trim().isEmpty()) {
        // Double attendu
        try {
          return Integer.parseInt(aValue.toString().trim());
        }
        catch (NumberFormatException _exc) {
        }
      }
      
      return null;
    }
    
    @Override
    public void setValueAt(Object aValue, int row, int column) {
      if (column == COL_I1) {
        rows.get(row).i1=getIntegerValue(aValue);
      }
      else if (column == COL_J1) {
        rows.get(row).j1=getIntegerValue(aValue);
      }
      else if (column == COL_I2) {
        rows.get(row).i2=getIntegerValue(aValue);
      }
      else if (column == COL_J2) {
        rows.get(row).j2=getIntegerValue(aValue);
      }
      else if (column == COL_X1) {
        rows.get(row).x1=getDoubleValue(aValue);
      }
      else if (column == COL_Y1) {
        rows.get(row).y1=getDoubleValue(aValue);
      }
      else if (column == COL_X2) {
        rows.get(row).x2=getDoubleValue(aValue);
      }
      else if (column == COL_Y2) {
        rows.get(row).y2=getDoubleValue(aValue);
      }

      fireTableDataChanged();
      
      pnParams_.fireParametersChanged();
    }
      
    public void setCouple(CoupleRow _cpl) {
      rows.clear();
      rows.add(_cpl);
      fireTableDataChanged();
      
      pnParams_.fireParametersChanged();
    }
  }
  
  
  /**
   * Un editeur qui r�agit � la saisie d'une forme pour la grille.
   *
   * @author Bertrand Marchand (marchand@deltacad.fr)
   * @version $Id$
   */
  class EditionController implements ZCalqueEditionInteractionTargetI {

    /** Non utilis� */
    public void atomicChanged() {
      // On notifie la palette que la forme a chang�, pour activer ou non le
      // bouton fin d'edition.
      GrPolyligne pl=(GrPolyligne) cqEdition_.getFormeEnCours().getFormeEnCours();
      if (pl.sommets_.nombre() == 2) {
        cqEdition_.endEdition();

        CoupleRow couple=new CoupleRow();
        couple.i1=(int)pl.sommet(0).x_;
        couple.j1=(int)pl.sommet(0).y_;
        couple.i2=(int)pl.sommet(1).x_;
        couple.j2=(int)pl.sommet(1).y_;
        mdlPoints.setCouple(couple);
        
        actEnterLine_.hideWindow();
      }
    }

    /** Non utilis� */
    public boolean addNewPolygone(GrPolygone _pg, ZEditionAttributesDataI _data) {
      return true;
    }

    /** 
     * Remet a jour la position du point centre.
     */
    public boolean addNewPoint(GrPoint _point, ZEditionAttributesDataI _data) {
//      actEnterDistance_.changeAll();
      return true;
    }

    /** Non utilis� */
    public boolean addNewRectangle(GrPoint _pt, GrPoint _pt2, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public boolean addNewPolyligne(GrPolyligne _pt, ZEditionAttributesDataI _data) {
      FuLog.debug("Nombre de points "+_pt.nombre());
      return true;
    }

    /** Non utilis� */
    public boolean addNewMultiPoint(GrPolyligne _pt, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public void setMessage(String _s) {
    }

    /** Non utilis� */
    public void unsetMessage() {
    }

    /** Non utilis� */
    public void pointMove(double _x, double _y) {
    }
  }

  private PivProject prj_;
  private PivVisuPanel pnLayers_;
  private ZCalqueEditionInteractionTargetI ctrl_=new EditionController();
  private PivEnterGeometryAction actEnterLine_;
  private ZCalqueEditionInteraction cqEdition_;
  private CtuluTable tbPoints=new CtuluTable();
  private CoupleTableModel mdlPoints;
  private PivScalingParamPanel pnParams_;
  
  /**
   * Constructeur.
   */
  public PivScalingTransformationSubPanel(PivVisuPanel _pn, PivProject _prj, PivScalingParamPanel _pnParams) {
    pnLayers_=_pn;
    prj_=_prj;
    pnParams_=_pnParams;
    
    initComponents();
    customize();
    
    setParams(_prj.getOrthoParameters());
    
    cqEdition_=_pn.getEditionLayer();
  }
  
  private void customize() {
    
    rbNoTransf.setText(PivResource.getS("Pas de translation / rotation"));
    rbTransf.setText(PivResource.getS("Saisie des 2 points pour la translation / rotation"));
    
    actEnterLine_=new PivEnterGeometryAction(pnLayers_, PivResource.getS("Saisir"), PivResource.getS("Saisie des 2 points pour la transformation"), true);
    actEnterLine_.setEditionController(ctrl_);
    btClick.setAction(actEnterLine_);
    actEnterLine_.addPropertyChangeListener(new EbliSelectedChangeListener(btClick));
    
    spPoints.setViewportView(tbPoints);
    
    rbNoTransf.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        rbTransfChanged();
      }
    });
    rbTransf.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        rbTransfChanged();
      }
    });
    
    rbNoTransf.setSelected(true);
    
    mdlPoints= new CoupleTableModel();
    tbPoints.setModel(mdlPoints);
    // #5754 : Blocage de fenetre si l'edition n'est pas termin�e et qu'on essai de supprimer la ligne s�lectionn�e.
    tbPoints.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    
    rbNoTransf.doClick();
  }
  
  protected void rbTransfChanged() {
    tbPoints.setEnabled(rbTransf.isSelected());
    btClick.setEnabled((rbTransf.isSelected()));
  }
  
  public boolean isParamsTransf() {
    return rbTransf.isSelected();
  }

  public void setParams(PivOrthoParameters _params) {
    if (_params==null)
      return;
    
    GrPoint[] imgPoints = _params.getScalingTranformationImgPoints();
    GrPoint[] realPoints = _params.getScalingTranformationRealPoints();
    
    if (imgPoints.length!=2 || realPoints.length!=2) {
      rbNoTransf.doClick();
    }
    else {
      rbTransf.doClick();
      CoupleRow cpl = new CoupleRow();
      cpl.i1 = (int)imgPoints[0].x_;
      cpl.j1 = (int)imgPoints[0].y_;
      cpl.i2 = (int)imgPoints[1].x_;
      cpl.j2 = (int)imgPoints[1].y_;
      cpl.x1 = realPoints[0].x_;
      cpl.y1 = realPoints[0].y_;
      cpl.x2 = realPoints[1].x_;
      cpl.y2 = realPoints[1].y_;
      mdlPoints.setCouple(cpl);
    }
  }
  
  /**
   * Met a jour les parametres depuis l'interface. On suppose que la validit� des donn�es de la fenetre ont �t� test�es avant.
   * 
   * ATTENTION : Il existe un mode d�grad�, ou les donn�es ne sont pas necessairement toutes valid�es (remplies), pour l'affichage
   * interactif des couples de points pendant la saisie.
   * 
   * @param _params Les parametres.
   */
  public void retrieveParams(PivOrthoParameters _params) {
    GrPoint[] imgPoints;
    GrPoint[] realPoints;
    
    if (rbNoTransf.isSelected() || mdlPoints.rows.size() == 0 || !mdlPoints.rows.get(0).isImgPointsFilled()) {
      imgPoints=new GrPoint[0];
      realPoints=new GrPoint[0];
    }
    else {
      CoupleRow cpl=mdlPoints.rows.get(0);
      
      imgPoints=new GrPoint[2];
      realPoints=new GrPoint[2];
      imgPoints[0]=new GrPoint(cpl.i1, cpl.j1, 0);
      imgPoints[1]=new GrPoint(cpl.i2, cpl.j2, 0);
      realPoints[0]=new GrPoint(cpl.x1 == null ? 0:cpl.x1, cpl.y1 == null ? 0:cpl.y1, 0);
      realPoints[1]=new GrPoint(cpl.x2 == null ? 0:cpl.x2, cpl.y2 == null ? 0:cpl.y2, 0);
    }
    _params.setScalingTranformationImgPoints(imgPoints);
    _params.setScalingTranformationRealPoints(realPoints);
  }
  
  /**
   * @return Les points images. Les champs sont suppos�s avoir �t� control�s avant l'appel � cette m�thode. 
   */
  public GrPoint[] getImgPoints() {
    GrPoint[] imgPoints = new GrPoint[0];
    
    if (rbNoTransf.isSelected() || mdlPoints.rows.size() == 0 || !mdlPoints.rows.get(0).isImgPointsFilled()) {
      imgPoints=new GrPoint[0];
    }
    else {
      CoupleRow cpl=mdlPoints.rows.get(0);
      
      imgPoints=new GrPoint[2];
      imgPoints[0]=new GrPoint(cpl.i1, cpl.j1, 0);
      imgPoints[1]=new GrPoint(cpl.i2, cpl.j2, 0);
    }
    
    return imgPoints;
  }
  
  /**
   * @return Les points reels. Les champs sont suppos�s avoir �t� control�s avant l'appel � cette m�thode. 
   */
  public GrPoint[] getRealPoints() {
    GrPoint[] realPoints = new GrPoint[0];
    
    if (rbNoTransf.isSelected() || mdlPoints.rows.size() == 0 || !mdlPoints.rows.get(0).isImgPointsFilled()) {
      realPoints=new GrPoint[0];
    }
    else {
      CoupleRow cpl=mdlPoints.rows.get(0);
      
      realPoints=new GrPoint[2];
      realPoints[0]=new GrPoint(cpl.x1 == null ? 0:cpl.x1, cpl.y1 == null ? 0:cpl.y1, 0);
      realPoints[1]=new GrPoint(cpl.x2 == null ? 0:cpl.x2, cpl.y2 == null ? 0:cpl.y2, 0);
    }
    
    return realPoints;
  }
  
  public boolean isDataValid(boolean _withMes) {
    if (rbNoTransf.isSelected()) {
      return true;
    }
    else {
      boolean bok=false;
      if (mdlPoints.rows.size()!=0) {
        CoupleRow cpl=mdlPoints.rows.get(0);
        if (cpl.i1!=null && cpl.j1!=null && cpl.i2!=null && cpl.j2!=null && 
            cpl.x1!=null && cpl.y1!=null && cpl.x2!=null && cpl.y2!=null ) {
          bok = true;
        }
      }
      if (!bok) {
        if (_withMes)
          setErrorText(PivResource.getS("Les coordonn�es pour les points de transformation doivent �tre renseign�s"));
        else
          setErrorText("");
        return false;
      }
    }
    
    setErrorText("");
    return true;
  }
  
  private void setErrorText(String _error) {
    if (_error.isEmpty()) {
      pnParams_.setErrorText("");
    }
    else {
      pnParams_.setErrorText(PivResource.getS("Transformation")+" : "+_error);
    }
  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgTransf = new javax.swing.ButtonGroup();
        rbNoTransf = new javax.swing.JRadioButton();
        rbTransf = new javax.swing.JRadioButton();
        spPoints = new javax.swing.JScrollPane();
        btClick = new javax.swing.JToggleButton();

        bgTransf.add(rbNoTransf);
        rbNoTransf.setText("Pas de translation / rotation");

        bgTransf.add(rbTransf);
        rbTransf.setText("Saisie des 2 points pour la translation / rotation");
        rbTransf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbTransfActionPerformed(evt);
            }
        });

        btClick.setText("Saisie");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(rbNoTransf)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(spPoints)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(rbTransf)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 134, Short.MAX_VALUE)
                        .addComponent(btClick)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(rbNoTransf)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rbTransf, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btClick))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(spPoints, javax.swing.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void rbTransfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbTransfActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbTransfActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgTransf;
    private javax.swing.JToggleButton btClick;
    private javax.swing.JRadioButton rbNoTransf;
    private javax.swing.JRadioButton rbTransf;
    private javax.swing.JScrollPane spPoints;
    // End of variables declaration//GEN-END:variables
}

package org.fudaa.fudaa.piv;

import java.util.Map;

import javax.swing.JComponent;

import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.commun.EbliActionAbstract;
import org.fudaa.ebli.commun.EbliCoordinateDefinition;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivProjectStateListener;

/**
 * Une interface pour l'affichage d'une vue particuli�re dans la fille
 * des calques.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public interface PivViewI extends PivProjectStateListener {

  /**
   * @return Le titre de la vue.
   */
  public String getTitle();
  
  /**
   * @return Les tools sp�cifiques, affich�s dans la barre d'outils.
   */
  public JComponent[] getSpecificTools();
  
  /**
   * @return Le syst�me de coordonn�es pour la vue.
   */
  public EbliCoordinateDefinition[] getCoordinateDefinitions();
  
  /**
   * Definit le projet. Les calques sont r�initialis�s.
   * @param _prj Le projet.
   */
  public void setProject(PivProject _prj);
  
  /**
   * Restore les propri�t�s graphique des calques.
   * @param _props Les propri�t�s.
   */
  public void restoreLayerProperties(Map<String,EbliUIProperties> _props);
  
  /**
   * @return Les propri�t�s graphiques de calques pour �tre sauv�es sur le fichier projet.
   */
  public Map<String,EbliUIProperties> saveLayerProperties();
  
  /**
   * @return Les calques pour la vue.
   */
  public BCalque[] getLayers();
  
  /**
   * @return L'action permettant d'activer la vue.
   */
  public EbliActionAbstract getActivationAction();
  
  /**
   * Affiche l'image suivante
   */
  public void nextImage();
  
  /**
   * Affiche l'image pr�c�dente
   */
  public void previousImage();
}

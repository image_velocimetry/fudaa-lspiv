package org.fudaa.fudaa.piv.utils;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.piv.PivResource;

import com.memoire.bu.BuFileFilter;

/**
 * Une classe d'utilitaires et de valeurs utiles.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivUtils {

  /** Valeur min pour un double support�e par FORTRAN */
  public final static double FORTRAN_DOUBLE_MIN=-1.e30;
  /** Valeur max pour un double support�e par FORTRAN */
  public final static double FORTRAN_DOUBLE_MAX=1.e30;

  /** Type de trac� pour tous les trac�s temporaires */
  public final static TraceLigneModel TMP_LINE_MODEL=new TraceLigneModel(TraceLigne.MIXTE, 1, Color.RED);
  
  /** Un formatter pour l'affichage standard des double */
  public final static DecimalFormat DECIMAL_FORMATTER = CtuluLib.getDecimalFormat(3);
  
  /** Une couleur blanche transparente, pour le trac� du fond des labels. */
  public final static Color TRANPARENT_WHITE_COLOR = new Color(255, 255, 255, 120);
  
  /**
   * Un filtre des fichiers bathy de transect
   */
  public static BuFileFilter FILE_FLT_TRANS_BTH=new BuFileFilter("bth_dat");
  /**
   * Un filtre des fichiers resultats de transect
   */
  public static BuFileFilter FILE_FLT_TRANS_RES=new BuFileFilter("res_dat");
  /**
   * Un filtre des fichiers parametres de transect
   */
  public static BuFileFilter FILE_FLT_TRANS_PAR=new BuFileFilter("par_dat");

  /**
   * Un filtre des fichiers bathy.dat, utilis�e lors de l'import
   * par boite de dialogue.
   */
  public static BuFileFilter FILE_FLT_BATHY;
  /**
   * Un filtre des fichiers transect en abscisse/cote, utilis�e lors de l'import
   * par boite de dialogue.
   */
  public static BuFileFilter FILE_FLT_TRANS_ABS_Z;
  /**
   * Un filtre des fichiers GRP.dat, utilis�e lors de l'import
   * par boite de dialogue.
   */
  public static BuFileFilter FILE_FLT_GRP;
  /**
   * Un filtre des fichiers flow_area.dat, utilis�e lors de l'import
   * par boite de dialogue.
   */
  public static BuFileFilter FILE_FLT_USER_EXTRACT;
  /**
   * Un filtre des fichiers user_extract.dat, utilis�e lors de l'import
   * par boite de dialogue.
   */
  public static BuFileFilter FILE_FLT_STAB_AREAS;
  /**
   * Un filtre des fichiers grid.dat, utilis�e lors de l'import
   * par boite de dialogue.
   */
  public static BuFileFilter FILE_FLT_GRID;
  /**
   * Un filtre des fichiers img_ref.dat, utilis�e lors de l'import
   * par boite de dialogue.
   */
  public static BuFileFilter FILE_FLT_TRANSF_PARAMS;
  /**
   * Un filtre des fichiers PIV_param.dat, utilis�e lors de l'import
   * par boite de dialogue.
   */
  public static BuFileFilter FILE_FLT_COMPUTE_PARAMS;
  /**
   * Un filtre des fichiers images, utilis�e lors de la selection
   * des fichiers par boite de dialogue.
   */
  public static BuFileFilter FILE_FLT_IMAGES;
  /**
   * Un filtre des fichiers images de type pgm.
   */
  public static BuFileFilter FILE_FLT_PGM;
  /**
   * Un filtre des fichiers projets, utilis�e lors d'ouverture
   * d'un projet par boite de dialogue.
   */
  public static BuFileFilter FILE_FLT_PROJ;
  /**
   * Un filtre des fichiers excel export�s
   * d'un projet par boite de dialogue.
   */
  public static BuFileFilter FILE_FLT_XLSX;
  
  /**
   * Un filtre des fichiers Seraphin export�s
   * d'un projet par boite de dialogue.
   */
  public static BuFileFilter FILE_FLT_SERA;
  
  
  /**
   * Initialise les champs statiques d�pendants du language.
   */
  static {
    FILE_FLT_PROJ=new BuFileFilter(new String[]{"lspiv.zip", "lspiv.7z"},PivResource.getS("Fichier projet"));
    FILE_FLT_PGM=new BuFileFilter("pgm",PivResource.getS("Fichiers images en niveaux de gris"));
    FILE_FLT_IMAGES=new BuFileFilter(new String[]{"jpg","jpeg","gif","png","pgm","tif","tiff","bmp"},PivResource.getS("Fichiers images"));
    FILE_FLT_GRID=new BuFileFilter("grid.dat",PivResource.getS("Fichier grille (grid.dat)"));
    FILE_FLT_GRID.setExtensionListInDescription(false);
    FILE_FLT_GRP=new BuFileFilter("GRP.dat",PivResource.getS("Fichier points (GRP.dat)"));
    FILE_FLT_GRP.setExtensionListInDescription(false);
    FILE_FLT_STAB_AREAS=new BuFileFilter("stab_areas.dat",PivResource.getS("Fichier des zones de stabilisation (stab_areas.dat)"));
    FILE_FLT_STAB_AREAS.setExtensionListInDescription(false);
    FILE_FLT_USER_EXTRACT=new BuFileFilter("user_extract.dat", PivResource.getS("Fichier des param�tres vid�o (user_extract.dat)"));
    FILE_FLT_USER_EXTRACT.setExtensionListInDescription(false);
    FILE_FLT_BATHY=new BuFileFilter(new String[]{"dat",FILE_FLT_TRANS_BTH.getFirstExt()},PivResource.getS("Fichier bathy"));
    FILE_FLT_TRANS_ABS_Z=new BuFileFilter(new String[]{"dat"},PivResource.getS("Fichier transect abscisse/z"));
    FILE_FLT_TRANSF_PARAMS=new BuFileFilter("img_ref.dat",PivResource.getS("Fichier transformation (img_ref.dat)"));
    FILE_FLT_TRANSF_PARAMS.setExtensionListInDescription(false);
    FILE_FLT_COMPUTE_PARAMS=new BuFileFilter("PIV_param.dat",PivResource.getS("Fichier calcul (PIV_param.dat)"));
    FILE_FLT_COMPUTE_PARAMS.setExtensionListInDescription(false);
    FILE_FLT_XLSX=new BuFileFilter("xlsx",PivResource.getS("Fichier Excel"));
    FILE_FLT_SERA=new BuFileFilter(new String[]{"slf","resu","sel","res3d","res","res2d","ser","geo"},PivResource.getS("Serafin"));
  }

  /**
   * Formatte un num�ro sur 4 caract�res pour un index donn�. Les premiers
   * caract�res sont combl�s par des 0. Exemple : 0013
   * @param _i L'index
   * @return Le num�ro sur 4 caract�res.
   */
  public static String formatOn4Chars(int _i) {
    int pow=3;
    StringBuffer sb=new StringBuffer();
    while (_i<Math.pow(10,pow) && pow>0) { pow--; sb.append("0"); }
    sb.append(_i);
    return sb.toString();
  }

  /**
   * Controle que la valeur du champ de nom donn� est un entier positif.
   * @param _sval La valeur du champ.
   * @param _param Le nom du champ.
   * @return true si la valeur est correcte.
   */
  public static boolean isPositiveInteger(CtuluDialogPanel _pn, String _sval, String _param) {
    boolean bok=false;
    try {
      bok=false;
      int val=Integer.parseInt(_sval.trim());
      bok=val>=0;
    }
    catch (NumberFormatException _exc) {}
    if (!bok) {
      if (_pn != null)
        _pn.setErrorText(PivResource.getS("{0}: Doit �tre un entier >= 0", _param));
      return false;
    }
    return true;
  }

  /**
   * Controle que la valeur du champ de nom donn� est un entier positif.
   * @param _sval La valeur du champ.
   * @param _param Le nom du champ.
   * @return true si la valeur est correcte.
   */
  public static boolean isStrictPositiveInteger(CtuluDialogPanel _pn, String _sval, String _param) {
    boolean bok=false;
    try {
      bok=false;
      int val=Integer.parseInt(_sval.trim());
      bok=val>0;
    }
    catch (NumberFormatException _exc) {}
    if (!bok) {
      if (_pn != null)
        _pn.setErrorText(PivResource.getS("{0}: Doit �tre un entier > 0", _param));
      return false;
    }
    return true;
  }

  /**
   * Controle que la valeur du champ de nom donn� est un entier .
   * @param _sval La valeur du champ.
   * @param _param Le nom du champ.
   * @return true si la valeur est correcte.
   */
  public static boolean isInteger(CtuluDialogPanel _pn, String _sval, String _param) {
    boolean bok=false;
    try {
      bok=false;
      Integer.parseInt(_sval.trim());
      bok=true;
    }
    catch (NumberFormatException _exc) {}
    if (!bok) {
      if (_pn != null)
        _pn.setErrorText(PivResource.getS("{0}: Doit �tre un entier", _param));
      return false;
    }
    return true;
  }

  /**
   * Controle que la valeur du champ de nom donn� est un r�el positif.
   * @param _sval La valeur du champ.
   * @param _param Le nom du champ.
   * @return true si la valeur est correcte.
   */
  public static boolean isPositiveReal(CtuluDialogPanel _pn, String _sval, String _param) {
    boolean bok=false;
    try {
      bok=false;
      double val=Double.parseDouble(_sval.trim());
      bok=val>=0;
    }
    catch (NumberFormatException _exc) {}
    if (!bok) {
      if (_pn != null)
        _pn.setErrorText(PivResource.getS("{0}: Doit �tre un r�el >= 0", _param));
      return false;
    }
    return true;
  }

  /**
   * Controle que la valeur du champ de nom donn� est un r�el.
   * @param _sval La valeur du champ.
   * @param _param Le nom du champ.
   * @return true si la valeur est correcte.
   */
  public static boolean isReal(CtuluDialogPanel _pn, String _sval, String _param) {
    boolean bok=false;
    try {
      bok=false;
      Double.parseDouble(_sval.trim());
      bok=true;
    }
    catch (NumberFormatException _exc) {}
    if (!bok) {
      if (_pn != null)
        _pn.setErrorText(PivResource.getS("{0}: Doit �tre un r�el", _param));
      return false;
    }
    return true;
  }

  /**
   * Controle que la valeur du champ de nom donn� est un r�el positif.
   * @param _sval La valeur du champ.
   * @param _param Le nom du champ.
   * @return true si la valeur est correcte.
   */
  public static boolean isStrictPositiveReal(CtuluDialogPanel _pn, String _sval, String _param) {
    boolean bok=false;
    try {
      bok=false;
      double val=Double.parseDouble(_sval.trim());
      bok=val>0;
    }
    catch (NumberFormatException _exc) {}
    if (!bok) {
      if (_pn != null)
        _pn.setErrorText(PivResource.getS("{0}: Doit �tre un r�el > 0", _param));
      return false;
    }
    return true;
  }
  
  public static Integer tryParseInt(String _sval) {
    Integer ret = null;
    try {
      ret=Integer.parseInt(_sval.trim());
    }
    catch (NumberFormatException _exc) {}
  
    return ret;
  }
  
  public static Double tryParseDouble(String _sval) {
    Double ret = null;
    try {
      ret=Double.parseDouble(_sval.trim());
    }
    catch (NumberFormatException _exc) {}
  
    return ret;
  }
  
  /**
   * Limite un texte avec retours charriots � une largeur maxi. Les caract�res au del� sont remplac�s par ...
   * @param _mess Le message
   * @param _maxWidth La largeur maxi (en nombre de caract�res)
   * @return
   */
  public static String trunkErrorMessage(String _mess, int _maxWidth) {
    StringBuffer ret=new StringBuffer();
    
    String[] lines=_mess.split("\r\n|\r|\n");
    for (String line : lines) {
      if (line.length()>_maxWidth) {
        ret.append(line.substring(0, _maxWidth)).append("...");
      }
      else {
        ret.append(line);
      }
      
      ret.append("\n");
    }
    
    return ret.toString();
  }

  /**
   * Ajoute des retours chariots � un texte suivant la largeur maxi. Les retours chariots sont soit &lt;br&gt; soit \n suivant que le texte commence par &lt;html&gt;.
   * @param _mess Le message
   * @param _maxWidth La largeur maxi (en nombre de caract�res)
   * @return
   */
  public static String wrapMessage(String _mess, int _maxWidth) {
    StringBuffer fullText = new StringBuffer(_mess);
    
    boolean html =  (fullText.toString().startsWith("<html>"));
    int lastStart = html ? 6:0;
    int lastWhite = -1;
    
    int idx = lastStart;
    while (idx<fullText.length()) {
//      System.out.println("'"+fullText.substring(idx, Math.min(fullText.length()-1, idx+3))+"'");
      if (fullText.substring(idx, idx+1).equals(" "))
          lastWhite = idx;
      
      if (html) {
        if (fullText.substring(idx, Math.min(fullText.length()-1, idx+4)).equals("<br>")) {
          lastWhite = -1;
          lastStart = idx+3;
          idx=lastStart;
        }
        else if (fullText.substring(idx, Math.min(fullText.length()-1, idx+5)).equals("<br/>")) {
          lastWhite = -1;
          lastStart = idx+4;
          idx=lastStart;
        }
        else if (fullText.substring(idx, Math.min(fullText.length()-1, idx+5)).equals("</li>")) {
          lastWhite = -1;
          lastStart = idx+4;
          idx=lastStart;
        }
        else if (fullText.substring(idx, Math.min(fullText.length()-1, idx+5)).equals("</ul>")) {
          lastWhite = -1;
          lastStart = idx+4;
          idx=lastStart;
        }
        else if (fullText.substring(idx, Math.min(fullText.length()-1, idx+3)).equals("<p>")) {
          lastWhite = -1;
          lastStart = idx+2;
          idx=lastStart;
        }
      }
      else {
        if (fullText.substring(idx, Math.min(fullText.length()-1, idx+1)).equals("\n")) {
          lastWhite = -1;
          lastStart = idx+1;
          idx=lastStart;
        }
      }
      
      if (idx - lastStart > _maxWidth) {
        if (html) {
          if (lastWhite == -1) {
            fullText.insert(idx, "<br>");
            lastStart = idx+3;
          }
          else {
            fullText.replace(lastWhite, lastWhite+1, "<br>");
            lastStart = lastWhite+3;
          }
        }
        else {
          if (lastWhite == -1) {
            fullText.insert(idx, "\n");
            lastStart = idx+1;
          }
          else {
            fullText.replace(lastWhite, lastWhite+1, "\n");
            lastStart = lastWhite+1;
          }
        }
        
        lastWhite = -1;
        idx = lastStart;
      }
      
      idx++;
    }
    
    return fullText.toString();
  }
  
  /**
   * Supprime l'extension d'un fichier, si l'extension est parmis les extensions donn�es
   * @param _file Le fichier
   * @param _exts Les extensions, sans "."
   * @return Le nouveau nom, sans l'extension ni le "."
   */
  public static String getFilenameWithoutExtension(String _filename, String[] _exts) {
    String filenane = _filename;
    for (String ext : _exts) {
      if (filenane.toLowerCase().endsWith(ext)) {
        filenane = filenane.substring(0, filenane.length() - ext.length() -1);
        break;
      }
    }
    
    return filenane;
  }
  
  /**
   * @return Une representation d'une cellule de tableau contenant un double.
   */
  public static TableCellRenderer getStandardDoubleTableCellRenderer() {

    /**
     * Une classe pour une repr�sentation des doubles.
     */
    class DoubleCellRenderer extends DefaultTableCellRenderer {
      NumberFormat fmt = CtuluLib.getDecimalFormat(3);

      public DoubleCellRenderer() {
        this.setHorizontalAlignment(SwingConstants.RIGHT);
        fmt.setMaximumFractionDigits(3);
        fmt.setMinimumFractionDigits(1);
      }

      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
        
        if (value instanceof Double) {
          this.setText(fmt.format((Double)value));
        }
        return this;
      }
    }
    
    
    return new DoubleCellRenderer();
  }
}

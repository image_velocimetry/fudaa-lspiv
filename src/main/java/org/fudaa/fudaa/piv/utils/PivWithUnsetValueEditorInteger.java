package org.fudaa.fudaa.piv.utils;

import java.awt.Component;

import javax.swing.JComponent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.fudaa.ctulu.editor.CtuluValueEditorInteger;

/**
 * Un editeur de double, avec la possibilit� de ne pas donner de valeur si la checkbox n'est pas activ�e.
 * 
 * @author Bertrand Marchand
 *
 */
public class PivWithUnsetValueEditorInteger extends CtuluValueEditorInteger {
  
  Integer defVal_;
  Integer unsetVal_ = Integer.MAX_VALUE;
  String checkedTooltip;
  String uncheckedTooltip;
  boolean isUnset;
  
  
  public PivWithUnsetValueEditorInteger() {
//    comp_.setUnsetValue(Integer.MAX_VALUE);
  }
  
  public PivWithUnsetValueEditorInteger(Integer _def) {
    this();
    setDefaultValue(_def);
  }

  /**
   * Defiinit les tooltips sur le chackbox.
   * @param _checkedTooltip Tooltip affich� quand le checkbox est s�lectionn�.
   * @param _uncheckedTooltip  Tooltip affich� quand le checkbox est d�s�lectionn�.
   */
  public void setCheckboxTooltips(String _checkedTooltip, String _uncheckedTooltip) {
    checkedTooltip = _checkedTooltip;
    uncheckedTooltip = _uncheckedTooltip;
  }

  private PivCheckboxEditorComponent<Integer> buildComponent() {
    PivCheckboxEditorComponent<Integer> comp = new PivCheckboxEditorComponent<>();
    comp.setUnsetValue(unsetVal_);
    comp.setDefaultValue(defVal_);
    comp.setCheckboxTooltips(checkedTooltip, uncheckedTooltip);
    if (isUnset)
      comp.setValue(unsetVal_);
    
    return comp;
  }
  
  /**
   * @return Le composant pour la saisie de la valeur.
   */
//  public PivCheckboxEditorComponent<Integer> getComponent() {
//    return comp_;
//  }

  @Override
  public TableCellRenderer createTableRenderer() {
    return buildComponent().createTableCellRenderer();
  }

  /**
   * La valeur affich�e dans le textfield, lorsque le chackbox est activ�.
   * @param _val
   */
  public void setDefaultValue(Integer _val) {
    defVal_ = _val;
  }
  
  /**
   * Definit que par defaut, le checkbox du composant ne sera pas activ�.
   * @param _b True : Checkbox non activ�, le composant est avec la valeur par defaut en gris�e.
   */
  public void setUnset(boolean _b) {
    isUnset = _b;
  }
  
  /**
   * La valeur correspondant � pas de valeur donn�e.
   * @param _val La valeur
   */
  public void setUnsetValue(Integer _val) {
    unsetVal_ = _val;
  }

  @Override
  public void setValue(final Object _s, final Component _comp) {
    PivCheckboxEditorComponent<Integer> comp = (PivCheckboxEditorComponent<Integer>) _comp;
    comp.setValue((Integer)_s);
  }

  @Override
  public Object getValue(Component _comp) {
    PivCheckboxEditorComponent<Integer> comp = (PivCheckboxEditorComponent<Integer>) _comp;
    return comp.getValue();
  }

  @Override
  public boolean isValueValidFromComponent(Component _comp) {
    return getValue(_comp) != null;
  }

  @Override
  public boolean isEmpty(Component _c) {
    return ((PivCheckboxEditorComponent<Integer>) _c).isValueSet();
  }

  @Override
  public String getStringValue(final Component _comp) {
    PivCheckboxEditorComponent<Integer> c = (PivCheckboxEditorComponent<Integer>)_comp;
    if (c.getValue() == null) {
      return "";
    }
    else {
      return c.getValue().toString();
    }
  }
  
  @Override
  public boolean isValid(final Object _o) {
    return buildComponent().isValueSet();
  }

  @Override
  public TableCellEditor createTableEditorComponent() {
    return buildComponent().createTableCellEditor();
  }

  @Override
  public PivCheckboxEditorComponent<Integer> createEditorComponent() {
    return buildComponent();
  }

  @Override
  public TableCellEditor createCommonTableEditorComponent() {
    return buildComponent().createTableCellEditor();
  }

  @Override
  public PivCheckboxEditorComponent<Integer> createCommonEditorComponent() {
    return buildComponent();
  }
}

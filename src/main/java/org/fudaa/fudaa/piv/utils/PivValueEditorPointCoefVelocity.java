package org.fudaa.fudaa.piv.utils;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.fudaa.piv.PivResource;

import com.memoire.bu.BuTableCellRenderer;
import com.memoire.bu.BuToolButton;

/**
 * Un editeur de valeur double, avec bouton pour redefinir la valeur par defaut.
 * 
 * @author Bertrand Marchand
 */
public class PivValueEditorPointCoefVelocity extends CtuluValueEditorDouble {

  /**
   * Un composant editeur pour le coefficient ponctuel de vitesses. Il autorise de
   * ne pas mettre de valeur, et de revenir a la valeur par defaut.
   * 
   * @author Bertrand Marchand
   */
  public class PointCoefEditorComponent extends JTextField {

    public class PointCoefTableCellEditor extends AbstractCellEditor implements TableCellEditor {

      @Override
      public Object getCellEditorValue() {
        return PointCoefEditorComponent.this.getValue();
      }

      @Override
      public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row,
          int column) {
        PointCoefEditorComponent.this.setValue((Double) value);
        return PointCoefEditorComponent.this;
      }

      // Editeur activ� uniquement suite a un double click, et si enabled.
      @Override
      public boolean isCellEditable(EventObject e) {
        if (e instanceof MouseEvent) {
          boolean b = PointCoefEditorComponent.this.isEnabled();
          return ((MouseEvent) e).getClickCount() == 2 && b;
        }
        else {
          return true;
        }
      }
    }

    public class PointCoefTableCellRenderer extends BuTableCellRenderer {

      @Override
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
          int row, int column) {

        Object val = (Double) value == PointCoefEditorComponent.UNSET_VALUE ? "" : value;
        return super.getTableCellRendererComponent(table, val, isSelected, hasFocus, row, column);
      }
    }

    public final static double UNSET_VALUE = -1;

    BuToolButton btErase;
    /** Le composant encapsul�, pour un tableau. */
    PointCoefTableCellEditor tableCellEditor = new PointCoefTableCellEditor();
    /** Le renderer du composant, pour un tableau. */
    PointCoefTableCellRenderer tableCellRenderer = new PointCoefTableCellRenderer();

    public PointCoefEditorComponent() {
      setLayout(new BorderLayout(0, 0));

      btErase = new BuToolButton(PivResource.PIV.getToolIcon("effacer"));
      btErase.setToolTipText(CtuluLib.getS("Valeur par defaut"));
      btErase.setOpaque(false);
      btErase.addActionListener((e) -> {
        setText("");
      });

      add(btErase, BorderLayout.EAST);
    }

    @Override
    public void setEnabled(boolean enabled) {
      super.setEnabled(enabled);

      btErase.setEnabled(enabled);
    }

    public boolean isValueSet() {
      return getValue() != null;
    }

    /**
     * Definit la valeur pour le composant. 
     * @param _d La valeur. Si null, indique une valeur multiple.
     */
    public void setValue(Object _d) {
      if (_d == null || (Double)_d == UNSET_VALUE) {
        setText("");
      }
      else {
        setText("" + _d);
      }
    }

    /**
     * @return La valeur, ou null, si la valeur n'est pas correcte.
     */
    public Object getValue() {
      String s = getText().trim();
      if (s.isEmpty())
        return UNSET_VALUE;
      
      try {
        return Double.parseDouble(s);
      }
      catch (Exception _exc) {
        return null;
      }
    }

    public TableCellEditor createTableCellEditor() {
      return tableCellEditor;
    }

    public TableCellRenderer createTableCellRenderer() {
      return tableCellRenderer;
    }
  }

  PointCoefEditorComponent comp_ = new PointCoefEditorComponent();

  @Override
  public void setEditable(boolean _isEditable) {
    super.setEditable(_isEditable);
    comp_.setEnabled(_isEditable);
  }

  @Override
  public TableCellRenderer createTableRenderer() {
    return comp_.createTableCellRenderer();
  }

  public void setValue(final double _v, final Component _comp) {
    setValue((Double)_v, _comp);
  }

  @Override
  public void setValue(final Object _s, final Component _comp) {
    PointCoefEditorComponent comp = (PointCoefEditorComponent) _comp;
    comp.setValue(_s);
  }

  @Override
  public Object getValue(Component _comp) {
    PointCoefEditorComponent comp = (PointCoefEditorComponent) _comp;
    return comp.getValue();
  }

  @Override
  public boolean isValueValidFromComponent(Component _comp) {
    return getValue(_comp) != null;
  }

  @Override
  public boolean isEmpty(Component _c) {
    return ((PointCoefEditorComponent) _c).isValueSet();
  }

  @Override
  public String getStringValue(final Component _comp) {
    PointCoefEditorComponent c = (PointCoefEditorComponent)_comp;
    if (c.getValue() == null) {
      return "";
    }
    else {
      return c.getValue().toString();
    }
  }
  
  @Override
  public boolean isValid(final Object _o) {
    try {
      Double.parseDouble(_o.toString());
      return true;
    }
    catch (Exception _exc) {
      return false;
    }
  }

  @Override
  public TableCellEditor createTableEditorComponent() {
    return comp_.createTableCellEditor();
  }

  @Override
  public JComponent createEditorComponent() {
    return comp_;
  }

  @Override
  public TableCellEditor createCommonTableEditorComponent() {
    return comp_.createTableCellEditor();
  }

  @Override
  public JComponent createCommonEditorComponent() {
    return comp_;
  }
}

/*
 * @creation     18 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.metier.PivComputeParameters;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.utils.PivUtils;

import com.memoire.bu.BuLib;
import com.memoire.bu.BuVerticalLayout;

/**
 * Un panneau pour filtrer les r�sultats instantan�s utilis�s pour faire le calcul de moyenne.
 * Les resultats filtr�s et selectionn�s sont sauv�s dans le projet.
 *  
 * @author marchand@deltacad.fr
 * @version $Id$
 */
public class PivFilterInstantResultsPanel extends CtuluDialogPanel {

  PivImplementation impl_;
  CtuluDialog diProgress_;
  private JTextField tfNormalMinLimit_;
  private JTextField tfNormalMaxLimit_;
  private JCheckBox cbVelFlt_;
  private JTextField tfVxMinLimit_;
  private JTextField tfVxMaxLimit_;
  private JTextField tfVyMinLimit_;
  private JTextField tfVyMaxLimit_;
  private JCheckBox cbCorrelFlt_;
  private JTextField tfCorrelMinLimit_;
  private JTextField tfCorrelMaxLimit_;
  
  private double resolution_;

  /**
   * Constructeur.
   * @param _ui Le parent pour la boite de dialogue.
   */
  public PivFilterInstantResultsPanel(PivImplementation _impl) {
    impl_ = _impl;
    
    // Wrap, sinon, la fenetre est trop large.
    String text = PivUtils.wrapMessage("<html>"+
        PivResource.getS("<u>Vitesses</u><br>Les seuils de vitesse minimale et maximale, en amplitude et en composantes Vx et Vy, sont � manier avec pr�caution de fa�on � ne filtrer que les vitesses aberrantes. L'effet de ce filtre en post-traitement est le m�me que le dimensionnement de l'aire de recherche (SA), qui a l'avantage suppl�mentaire de limiter la dur�e des calculs.<p><p>")+
        PivResource.getS("<u>Corr�lation</u><br>La corr�lation est un important indicateur de la qualit� des r�sultats de vitesse. Le seuil de corr�lation minimale pourra �tre de l'ordre de 0.7 pour des traceurs ind�formables, et de l'ordre de 0.4 pour des traceurs se d�formant (figures de turbulence, �cume). Le seuil de corr�lation maximale (0.98, typiquement) permet d'�carter les vitesses correspondant � des motifs statiques (berge, bord de l'image) ou presque (reflets/ombres, vagues stationnaires).")+
        "</html>", 132);
    setHelpText(text);
    
    JTabbedPane tp=new JTabbedPane();
    
    // Filtre pour la vitesse
    
    JPanel pnVelocity=new JPanel();
    pnVelocity.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
    pnVelocity.setLayout(new BuVerticalLayout(3, true, false));

    cbVelFlt_ = new JCheckBox(PivResource.getS("Filtrer les vitesses"));
    cbVelFlt_.addItemListener(new ItemListener() {
      
      @Override
      public void itemStateChanged(ItemEvent e) {
        boolean b=cbVelFlt_.isSelected();
        tfNormalMinLimit_.setEnabled(b);
        tfNormalMaxLimit_.setEnabled(b);
        tfVxMinLimit_.setEnabled(b);
        tfVxMaxLimit_.setEnabled(b);
        tfVyMinLimit_.setEnabled(b);
        tfVyMaxLimit_.setEnabled(b);
      }
    });
    pnVelocity.add(cbVelFlt_);
    
    JPanel pnNormalLimits=new JPanel();
    pnNormalLimits.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Limites de la norme de vitesse (m/s)")));
    pnNormalLimits.setLayout(new BuVerticalLayout(3,true,false));

    JPanel pn=new JPanel();
    pn.setLayout(new BorderLayout(3,3));
    JLabel lbNormalMinLimit=new JLabel(PivResource.getS("Min:"));
    pn.add(lbNormalMinLimit, BorderLayout.WEST);
    tfNormalMinLimit_ = new JTextField();
    tfNormalMinLimit_.setEnabled(false);
    tfNormalMinLimit_.setPreferredSize(new Dimension(250,tfNormalMinLimit_.getPreferredSize().height));
    pn.add(tfNormalMinLimit_, BorderLayout.CENTER);
    pnNormalLimits.add(pn);
    
    pn=new JPanel();
    pn.setLayout(new BorderLayout(3,3));
    JLabel lbNormalMaxLimit=new JLabel(PivResource.getS("Max:"));
    pn.add(lbNormalMaxLimit, BorderLayout.WEST);
    tfNormalMaxLimit_ = new JTextField();
    tfNormalMaxLimit_.setEnabled(false);
    pn.add(tfNormalMaxLimit_, BorderLayout.CENTER);
    pnNormalLimits.add(pn);
    pnVelocity.add(pnNormalLimits);
    
    JPanel pnVxLimits=new JPanel();
    pnVxLimits.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Limites de la composante Vx (m/s)")));
    pnVxLimits.setLayout(new BuVerticalLayout(3,true,false));

    pn=new JPanel();
    pn.setLayout(new BorderLayout(3,3));
    JLabel lbVxMinLimit=new JLabel(PivResource.getS("Min:"));
    pn.add(lbVxMinLimit, BorderLayout.WEST);
    tfVxMinLimit_ = new JTextField();
    tfVxMinLimit_.setEnabled(false);
    pn.add(tfVxMinLimit_, BorderLayout.CENTER);
    pnVxLimits.add(pn);
    
    pn=new JPanel();
    pn.setLayout(new BorderLayout(3,3));
    JLabel lbVxMaxLimit=new JLabel(PivResource.getS("Max:"));
    pn.add(lbVxMaxLimit, BorderLayout.WEST);
    tfVxMaxLimit_ = new JTextField();
    tfVxMaxLimit_.setEnabled(false);
    pn.add(tfVxMaxLimit_, BorderLayout.CENTER);
    pnVxLimits.add(pn);
    pnVelocity.add(pnVxLimits);
    
    JPanel pnVyLimits=new JPanel();
    pnVyLimits.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Limites de la composante Vy (m/s)")));
    pnVyLimits.setLayout(new BuVerticalLayout(3,true,false));

    pn=new JPanel();
    pn.setLayout(new BorderLayout(3,3));
    JLabel lbVyMinLimit=new JLabel(PivResource.getS("Min:"));
    pn.add(lbVyMinLimit, BorderLayout.WEST);
    tfVyMinLimit_ = new JTextField();
    tfVyMinLimit_.setEnabled(false);
    pn.add(tfVyMinLimit_, BorderLayout.CENTER);
    pnVyLimits.add(pn);
    
    pn=new JPanel();
    pn.setLayout(new BorderLayout(3,3));
    JLabel lbVyMaxLimit=new JLabel(PivResource.getS("Max:"));
    pn.add(lbVyMaxLimit, BorderLayout.WEST);
    tfVyMaxLimit_ = new JTextField();
    tfVyMaxLimit_.setEnabled(false);
    pn.add(tfVyMaxLimit_, BorderLayout.CENTER);
    pnVyLimits.add(pn);
    pnVelocity.add(pnVyLimits);
    
    tp.addTab(PivResource.getS("Vitesse"), pnVelocity);
    
    BuLib.giveSameWidth(lbNormalMinLimit, lbNormalMaxLimit, lbVxMinLimit, lbVxMaxLimit, lbVyMinLimit, lbVyMaxLimit);
    
    // Filtre pour la correlation
    
    JPanel pnCorrel=new JPanel();
    pnCorrel.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
    pnCorrel.setLayout(new BuVerticalLayout(3, true, false));

    cbCorrelFlt_ = new JCheckBox(PivResource.getS("Filtrer les corr�lations"));
    cbCorrelFlt_.addItemListener(new ItemListener() {
      
      @Override
      public void itemStateChanged(ItemEvent e) {
        boolean b=cbCorrelFlt_.isSelected();
        tfCorrelMinLimit_.setEnabled(b);
        tfCorrelMaxLimit_.setEnabled(b);
      }
    });
    pnCorrel.add(cbCorrelFlt_);
    
    JPanel pnCorrelLimits=new JPanel();
    pnCorrelLimits.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Corr�lation")));
    pnCorrelLimits.setLayout(new BuVerticalLayout(3,true,false));

    pn=new JPanel();
    pn.setLayout(new BorderLayout(3,3));
    JLabel lbCorrelMinLimit=new JLabel(PivResource.getS("Min:"));
    pn.add(lbCorrelMinLimit, BorderLayout.WEST);
    tfCorrelMinLimit_ = new JTextField();
    tfCorrelMinLimit_.setEnabled(false);
    pn.add(tfCorrelMinLimit_, BorderLayout.CENTER);
    pnCorrelLimits.add(pn);
    
    pn=new JPanel();
    pn.setLayout(new BorderLayout(3,3));
    JLabel lbCorrelMaxLimit=new JLabel(PivResource.getS("Max:"));
    pn.add(lbCorrelMaxLimit, BorderLayout.WEST);
    tfCorrelMaxLimit_ = new JTextField();
    tfCorrelMaxLimit_.setEnabled(false);
    pn.add(tfCorrelMaxLimit_, BorderLayout.CENTER);
    pnCorrelLimits.add(pn);
    pnCorrel.add(pnCorrelLimits);
    tp.addTab(PivResource.getS("Corr�lation"), pnCorrel);
    
    BuLib.giveSameWidth(lbCorrelMinLimit, lbCorrelMaxLimit);

    setLayout(new BorderLayout());
    add(tp,BorderLayout.CENTER);
  }
  
  /**
   * @param _resol La resolution, pour le calcul des seuils de vitesse par defaut 
   */
  public void setResolution(double _resol) {
    resolution_ = _resol;
  }
  
  /**
   * Definit les valeurs pour les filtres
   * @param _params Les param�tres du calcul, contenant les valeurs de filtres.
   */
  public void setFilterValues(PivComputeParameters _params) {
    if (_params==null) return;
    
    if (!_params.isCorrelationFiltered()) {
      cbCorrelFlt_.setSelected(false);
      tfCorrelMinLimit_.setText("0.4");
      tfCorrelMaxLimit_.setText("0.98");
    }
    else {
      cbCorrelFlt_.setSelected(true);
      tfCorrelMinLimit_.setText(""+_params.getMinCorrelation());
      tfCorrelMaxLimit_.setText(""+_params.getMaxCorrelation());
    }
    
    // #5711 : Vitesse min plus calcul�e en fonction de la r�solution.
//    if (_params.getSmin() == PivUtils.FORTRAN_DOUBLE_MIN) {
//      tfNormalMinLimit_.setText(CtuluLib.getDecimalFormat().format(resolution_*2/_params.getTimeInterval()));
//    }
//    else {
//    }
    
    if (!_params.isVelocityFiltered()) {
      cbVelFlt_.setSelected(false);
      tfNormalMinLimit_.setText("0.");
      tfNormalMaxLimit_.setText("10000.");
      tfVxMinLimit_.setText(""+_params.getVxmin());
      tfVxMaxLimit_.setText(""+_params.getVxmax());
      tfVyMinLimit_.setText(""+_params.getVymin());
      tfVyMaxLimit_.setText(""+_params.getVymax());
    }
    else {
      cbVelFlt_.setSelected(true);
      tfNormalMinLimit_.setText(""+_params.getSmin());
      tfNormalMaxLimit_.setText(""+_params.getSmax());
      tfVxMinLimit_.setText(""+_params.getVxmin());
      tfVxMaxLimit_.setText(""+_params.getVxmax());
      tfVyMinLimit_.setText(""+_params.getVymin());
      tfVyMaxLimit_.setText(""+_params.getVymax());
    }
  }
  
  /**
   * Recupere les valeurs pour les filtres. _params contient d�j� des valeurs, seules les valeurs pour
   * les filtres sont initialis�es.
   * @param _params Les param�tres du calcul.
   */
  public void retrieveFilterValues(PivComputeParameters _params) {
    if (_params==null) return;
    
    if (cbCorrelFlt_.isSelected()) {
      _params.setMinCorrelation(Double.parseDouble(tfCorrelMinLimit_.getText()));
      _params.setMaxCorrelation(Double.parseDouble(tfCorrelMaxLimit_.getText()));
    }
    else {
      _params.setMinCorrelation(PivUtils.FORTRAN_DOUBLE_MIN);
      _params.setMaxCorrelation(PivUtils.FORTRAN_DOUBLE_MAX);
    }
    
    if (cbVelFlt_.isSelected()) {
      _params.setSmin(Double.parseDouble(tfNormalMinLimit_.getText()));
      _params.setSmax(Double.parseDouble(tfNormalMaxLimit_.getText()));
      _params.setVxmin(Double.parseDouble(tfVxMinLimit_.getText()));
      _params.setVxmax(Double.parseDouble(tfVxMaxLimit_.getText()));
      _params.setVymin(Double.parseDouble(tfVyMinLimit_.getText()));
      _params.setVymax(Double.parseDouble(tfVyMaxLimit_.getText()));
    }
    else {
      _params.setSmin(PivUtils.FORTRAN_DOUBLE_MIN);
      _params.setSmax(PivUtils.FORTRAN_DOUBLE_MAX);
      _params.setVxmin(PivUtils.FORTRAN_DOUBLE_MIN);
      _params.setVxmax(PivUtils.FORTRAN_DOUBLE_MAX);
      _params.setVymin(PivUtils.FORTRAN_DOUBLE_MIN);
      _params.setVymax(PivUtils.FORTRAN_DOUBLE_MAX);
    }
  }

  /**
   * Controle que la valeur du champ de nom donn� est un r�el.
   * @param _sval La valeur du champ.
   * @param _param Le nom du champ.
   * @return true si la valeur est correcte.
   */
  private boolean isReal(String _sval, String _param) {
    boolean bok=false;
    try {
      bok=false;
      Double.parseDouble(_sval.trim());
      bok=true;
    }
    catch (NumberFormatException _exc) {}
    if (!bok) {
      setErrorText(PivResource.getS("{0}: Doit �tre un r�el",_param));
      return false;
    }
    return true;
  }

  @Override
  public boolean apply() {
    boolean compute = false;
    
    PivProject prj = impl_.getCurrentProject();
    PivComputeParameters params=new PivComputeParameters(prj.getComputeParameters());
    retrieveFilterValues(params);
    
    if (!params.equals(prj.getComputeParameters())) {
      if (prj.hasAverageResults() && !impl_.question(PivResource.getS("Suppression des r�sultats"), 
          PivResource.getS("Attention : des r�sultats existent et seront supprim�s si vous modifiez les param�tres.\nVoulez-vous continuer ?"))) {
        return false;
      }
      
      prj.setComputeParameters(params);
      compute = true;
    }
    
    else if (!prj.hasInstantFilteredResults()) {
      compute = true;
    }
    
    if (compute) {

      // La tache a ex�cuter.
      PivTaskAbstract r = new PivTaskAbstract(PivResource.getS("Filtrage des r�sultats instantan�s")) {

        public boolean act(PivTaskObserverI _observer) {
          CtuluLog ana = new CtuluLog();

          // Filtrage
          PivExeLauncher.instance().computeFilteredInstantResultats(ana, impl_.getCurrentProject(), _observer);
          if (ana.containsErrorOrSevereError()) {
            impl_.error(ana.getResume());
            return false;
          }

          // Lanc� � la fin, car l'interface se bloque si on ne le fait pas.
          // Probl�me de thread swing probablement...
          SwingUtilities.invokeLater(new Runnable() {
            public void run() {
              impl_.message(PivResource.getS("Calcul termin�"), PivResource.getS("Le calcul s'est termin� avec succ�s"),
                  false);
              impl_.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_REAL_VIEW);
              impl_.get2dFrame().getVisuPanel().setFilteredVelocitiesLayerVisible(true);
            }
          });
          
          return true;
        }
      };

      PivProgressionPanel pnProgress_ = new PivProgressionPanel(r);
      diProgress_ = pnProgress_.createDialog(impl_.getParentComponent());
      diProgress_.setOption(CtuluDialog.ZERO_OPTION);
      diProgress_.setDefaultCloseOperation(CtuluDialog.DO_NOTHING_ON_CLOSE);
      diProgress_.setTitle(r.getName());

      r.start();
      diProgress_.afficheDialogModal();
    }

    return true;
  }
  
  @Override
  public boolean isDataValid() {
    boolean bok=
      !cbVelFlt_.isSelected() || (
      isReal(tfNormalMinLimit_.getText(),PivResource.getS("Min norme de vitesse")) &&
      isReal(tfNormalMaxLimit_.getText(),PivResource.getS("Max norme de vitesse")) &&
      isReal(tfVyMinLimit_.getText(),PivResource.getS("Min Vx")) &&
      isReal(tfVyMaxLimit_.getText(),PivResource.getS("Max Vx")) &&
      isReal(tfVyMinLimit_.getText(),PivResource.getS("Min Vy")) &&
      isReal(tfVyMaxLimit_.getText(),PivResource.getS("Max Vy"))
      );
    if (!bok) return false;
    
    double min;
    double max;
    
    // Les vitesses
    if (cbVelFlt_.isSelected()) {
      min=Double.parseDouble(tfNormalMinLimit_.getText());
      if (min<0) {
        setErrorText(PivResource.getS("{0}: Doit �tre sup�rieur � {1}",
            PivResource.getS("Min norme de vitesse"), 0));
        return false;
      }
      max=Double.parseDouble(tfNormalMaxLimit_.getText());
      if (max>PivUtils.FORTRAN_DOUBLE_MAX) {
        setErrorText(PivResource.getS("{0}: Doit �tre inf�rieur � {1}",
            PivResource.getS("Max norme de vitesse"), PivUtils.FORTRAN_DOUBLE_MAX));
        return false;
      }
      if (min>=max) {
        setErrorText(PivResource.getS("{0}: Min doit �tre inf�rieur � Max",
            PivResource.getS("Norme de vitesse")));
        return false;
      }
      
      min=Double.parseDouble(tfVxMinLimit_.getText());
      if (min<PivUtils.FORTRAN_DOUBLE_MIN) {
        setErrorText(PivResource.getS("{0}: Doit �tre sup�rieur � {1}",
            PivResource.getS("Min Vx"), PivUtils.FORTRAN_DOUBLE_MIN));
        return false;
      }
      max=Double.parseDouble(tfVxMaxLimit_.getText());
      if (max>PivUtils.FORTRAN_DOUBLE_MAX) {
        setErrorText(PivResource.getS("{0}: Doit �tre inf�rieur � {1}",
            PivResource.getS("Max Vx"), PivUtils.FORTRAN_DOUBLE_MAX));
        return false;
      }
      if (min>=max) {
        setErrorText(PivResource.getS("{0}: Min doit �tre inf�rieur � Max",
            PivResource.getS("Vx")));
        return false;
      }
      
      min=Double.parseDouble(tfVyMinLimit_.getText());
      if (min<PivUtils.FORTRAN_DOUBLE_MIN) {
        setErrorText(PivResource.getS("{0}: Doit �tre sup�rieur � {1}",
            PivResource.getS("Min Vy"), PivUtils.FORTRAN_DOUBLE_MIN));
        return false;
      }
      max=Double.parseDouble(tfVyMaxLimit_.getText());
      if (max>PivUtils.FORTRAN_DOUBLE_MAX) {
        setErrorText(PivResource.getS("{0}: Doit �tre inf�rieur � {1}",
            PivResource.getS("Max Vy"), PivUtils.FORTRAN_DOUBLE_MAX));
        return false;
      }
      
      if (min>=max) {
        setErrorText(PivResource.getS("{0}: Min doit �tre inf�rieur � Max",
            PivResource.getS("Vy")));
        return false;
      }
    }
    
    // Les correlations
    bok=
        !cbCorrelFlt_.isSelected() || (
        isReal(tfCorrelMinLimit_.getText(),PivResource.getS("Min corr�lation")) &&
        isReal(tfCorrelMaxLimit_.getText(),PivResource.getS("Max corr�lation")));
    if (!bok) return false;

    if (cbCorrelFlt_.isSelected()) {
      min=Double.parseDouble(tfCorrelMinLimit_.getText());
      if (min < PivUtils.FORTRAN_DOUBLE_MIN) {
        setErrorText(PivResource.getS("{0}: Doit �tre sup�rieur � {1}", 
            PivResource.getS("Min corr�lation"), PivUtils.FORTRAN_DOUBLE_MIN));
        return false;
      }
      max=Double.parseDouble(tfCorrelMaxLimit_.getText());
      if (max > PivUtils.FORTRAN_DOUBLE_MAX) {
        setErrorText(PivResource.getS("{0}: Doit �tre inf�rieur � {1}", 
            PivResource.getS("Max corr�lation"), PivUtils.FORTRAN_DOUBLE_MAX));
        return false;
      }
      
      if (min>=max) {
        setErrorText(PivResource.getS("{0}: Min doit �tre inf�rieur � Max",
            PivResource.getS("Corr�lation")));
        return false;
      }
    }
    return true;
  }
}

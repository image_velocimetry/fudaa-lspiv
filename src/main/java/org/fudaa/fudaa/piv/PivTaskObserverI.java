package org.fudaa.fudaa.piv;

import org.fudaa.ctulu.ProgressionDetailedInterface;
import org.fudaa.fudaa.piv.PivTaskAbstract.TaskStatus;

/**
 * Une interface d'observation d'une tache.
 * La tache utilise cet observer pour informer reguli�rement de son avanc�e, ou pour
 * tester qu'une interruption a �t� demand�e.
 * 
 * Cette interface peut �tre implement�e par une barre de progression par exemple.
 * 
 * @author marchand
 *
 */
public interface PivTaskObserverI extends ProgressionDetailedInterface {

  /**
   * Notifie que le statut la tache change.
   * @param _status Le statut de la tache.
   */
  public void setStatus(TaskStatus _status);
  
  /**
   * Doit etre test� r�guli�rement par une tache.
   * @return True : Si un arret de la tache a ete demand�.
   */
  public boolean isStopRequested();
  
  /**
   * Notifie que la tache est termin�e.
   * Appel� apr�s que la tache soit termin�e.
   */
  public void completed();
}

package org.fudaa.fudaa.piv.metier;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.fudaa.piv.PivPreferences;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivTaskObserverI;
import org.fudaa.fudaa.piv.imageio.PivPGMReader;
import org.fudaa.fudaa.piv.imageio.PivPGMWriter;
import org.fudaa.fudaa.piv.utils.PivUtils;

import com.memoire.fu.FuLog;

/**
 * Le projet contenant toutes les donn�es. Toutes les donn�es sont stock�es en espace r�el de calcul. Chaque projet peut �tre sauv�/relu grace aux
 * fichiers projets.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivProject implements PivProjectStateI {
  /** Le repertoire projet des fichiers de donn�es */
  public static final String OUTPUT_DIR = "outputs.dir";
  /** Le r�pertoire projet des images sources */
  public static final String IMG_PGM_DIR = "img_pgm";
  /** Le r�pertoire projet des images sources stabilis�es */
  public static final String IMG_STAB_DIR = "img_stab";
  /** Le r�pertoire projet des images transform�es */
  public static final String IMG_TRANSF_DIR = "img_transf";
  /** Le r�pertoire des vitesses instantan�es */
  public static final String VEL_RAW_DIR = "vel_raw";
  /** Le r�pertoire des vitesses instantan�es en espace r�el. */
  public static final String VEL_REAL_DIR = "vel_real";
  /** Le r�pertoire des scalaires instantan�s */
  public static final String VEL_SCAL_DIR = "vel_scal";
  /** Le r�pertoire des vitesses instantan�es filtr�es. */
  public static final String VEL_FILTER_DIR = "vel_filter";
  /** Le r�pertoire contenant les transects */
  public static final String TRANSECTS_DIR = "transects";
  /** Le repertoire des donn�es temporaires. Supprim� avant sauvegarde */
  public static final String TMP_DIR = "__tmp";

  /**
   * Le type des images sources. Les images background ne participent pas
   * 
   * @author Bertrand Marchand (marchand@detacad.fr)
   *
   */
  public enum SRC_IMAGE_TYPE {
    CALCULATION, BACKGROUND, ALL
  }

  /**
   * Le type d'othorectification pour le projet.
   * 
   * @author Bertrand Marchand (marchand@detacad.fr)
   */
  public enum OrthoMode {
    /** Orthorectification de type chgt d'�chelle. */
    SCALING,
    /** Orthorectification de type full ortho. Les GRPs ont tous le m�me Z. */
    ORTHO_2D,
    /** Orthorectification de type full ortho. Les GRPs ont des Z. diff�rents */
    ORTHO_3D
  }

  public static final String BACKGROUND_IMAGE_NAME_PREFIX = "[F]";

  public static final FileFilter FLT_FILES = new FileFilter() {
    @Override
    public boolean accept(File _f) {
      return _f.isFile();
    }
  };
  public static final FileFilter FLT_PGM_FILES = new FileFilter() {
    @Override
    public boolean accept(File _f) {
      return _f.isFile() && (_f.getPath().endsWith(".pgm"));
    }
  };

  /** Le r�pertoire temporaire du projet. */
  protected File rootPath;
  /** Le manager de persistence du projet. */
  protected PivProjectPersistence persMng;
  /** Les points GRP */
  protected PivOrthoPoint[] orthoPoints;
  /** Les param�tres d'orthorectification */
  protected PivOrthoParameters orthoParams;
  /** Les param�tres de calcul */
  protected PivComputeParameters computeParams = new PivComputeParameters();
//  /** Les param�tres de calcul pour les d�bits */
//  protected PivFlowParameters flowParams;
  /** Le contour de la grille (null si pas de contour) */
  protected PivCntGrid cntGrid;
  /** La grille de calcul */
  protected PivGrid computeGrid;
  /** Un transect pour le calcul de d�bits */
  protected PivTransect[] transects;
  /** Les r�sultats de vitesses moyennes */
  protected PivResultsI averageResults;
  /** Les r�sultats instantan�s */
  protected PivResultsI[] instantResults;
  /** Les r�sultats instantan�s filtr�s */
  protected PivResultsI[] instantFilteredResults;
  /** Les r�sultats instantan�s utilis�s pour le calcul des vitesses moyennes. */
  protected boolean[] usedInstantResults;
  /** Les r�sultats de d�bit calcul� */
  protected PivFlowResults[] flowResults;
  /** Les r�sultats manuels de vitesses */
  protected PivResultsI manualResults;
  /** La taille des images transform�es */
  protected Dimension imgTransfSize = null;
  /** True : Les images transform�es ont �t� modifi�es */
  protected boolean areTransfImagesChanged = true;
  /** La taille des images sources */
  protected Dimension imgPgmSize = null;
  /** Les param�tres d'�chantillonnage de la vid�o */
  protected PivSamplingVideoParameters samplingParams;
  /** True : Les images d'origine ont �t� modifi�es */
  protected boolean areSrcImagesChanged = true;
  /** True : Les images stabilis�es ont �t� transform�es */
  protected boolean areStabilizedImagesChanged = true;
  /** Les param�tres de transformation du syteme initial vers le systeme de calcul. */
  protected PivTransformationParameters transfParams = new PivTransformationParameters();
  /** Les coefficients de transformation pour l'orthorectification. Length=0 : On est en mise � l'echelle. */
  protected double[] coeffs = null;
  /** Les param�tres pour la stabilisation des images. */
  protected PivStabilizationParameters stabParameters;
  /** Les parametres pour des calculs manuels de vitesses */
  protected PivManualVelocitiesParameters manualVelParameters;

  /** Les listeners notifi�s lors d'une modification du projet. */
  protected HashSet<PivProjectStateListener> listeners = new HashSet<PivProjectStateListener>();
  /** L'etat modifi� ou non du projet */
  protected boolean isModified = false;
  /** L'input stream pour l'image cache source ou stabilis�e */
  ImageInputStream cacheSrcOrStabInputStream_;
  /** L'input stream pour l'image cache transform�e */
  ImageInputStream cacheTransfInputStream_;
  /** L'input stream pour l'image cache r�elle */
  ImageInputStream cacheRealInputStream_;
  /** Les images d'origine dans l'ordre, dans */
  List<File> srcFiles_ = Collections.synchronizedList(new ArrayList<File>());
//  /** Les images de fond dans l'ordre  */
//  List<File> backgroundFiles_=Collections.synchronizedList(new ArrayList<File>());
  /** Les images d'origine cach�es, dans l'ordre */
  List<File> readySrcFiles_ = new ArrayList<File>();
  /** La liste des images transform�es */
  List<File> transfFiles_ = Collections.synchronizedList(new ArrayList<File>());
  /** Les images stabilis�es cach�es, dans l'ordre */
  List<File> readyStabilizedFiles_ = new ArrayList<>();

  /**
   * Constructeur
   */
  public PivProject() {
    persMng = new PivProjectPersistence(this);
  }

  /**
   * Cree l'espace de travail correspondant aux fichiers du fichiers projet.
   * 
   * @return True si l'espace a pu �tre cr��. False sinon.
   */
  public boolean init() {
    boolean bcreate = false;
    try {
      rootPath = CtuluLibFile.createTempDir();
      rootPath.deleteOnExit();

      File outDir = getOutputDir();
      outDir.deleteOnExit();
      bcreate = outDir.mkdirs();

      File tmpDir = getTempDirectory();
      tmpDir.deleteOnExit();
      bcreate &= tmpDir.mkdirs();

    }
    catch (IOException ex) {
    }

    return bcreate;
  }

  /**
   * Retourne la racine de l'espace temporaire du projet.
   * 
   * @return La racine du projet.
   */
  public File getRoot() {
    return rootPath;
  }

  /**
   * @return Le repertoire des donn�es temporaires.
   */
  public File getTempDirectory() {
    return new File(rootPath, TMP_DIR);
  }

  /**
   * @return Le manager de persistence du projet.
   */
  public PivProjectPersistence getPersistenceManager() {
    return persMng;
  }

  /**
   * @return Le repertoire des outputs.
   */
  File getOutputDir() {
    return new File(rootPath, OUTPUT_DIR);
  }

  /**
   * Le projet contient-il au moins une image source ?
   * 
   * @param _imagesType Le type des images a tester.
   * @return true si le projet contient au moins 1 image source.
   */
  public boolean hasSrcImages(SRC_IMAGE_TYPE _imagesType) {
    return getSrcImageFiles(_imagesType).length > 0;
  }

  /**
   * Le projet contient-il au moins 2 images sources (n�cessaire pour aller au bout des calculs) ?
   */
  public boolean hasAtLeast2CalculableSrcImages() {
    return getSrcImageFiles(SRC_IMAGE_TYPE.CALCULATION).length > 1;
  }

  /**
   * Le projet contient-il des r�sultats de vitesse brutes ?
   * 
   * @return true si le projet contient au moins 1 fichier de vitesses brutes.
   */
  public boolean hasRawVelocities() {
    return getRawVelocityFiles().length > 0;
  }

  /**
   * @return True si le projet contient des resultats bruts instantan�s.
   */
  public boolean hasInstantRawResults() {
    return instantResults != null && instantResults.length > 0;
  }

  /**
   * @return True si le projet contient des resultats manuels.
   */
  public boolean hasManualResults() {
    return manualResults != null;
  }

  /**
   * @return True si le projet contient des resultats filtr�s instantan�s.
   */
  public boolean hasInstantFilteredResults() {
    return instantFilteredResults != null && instantFilteredResults.length > 0;
  }

  /**
   * @return True si le projet contient des resultats moyenn�s.
   */
  public boolean hasAverageResults() {
    return averageResults != null;
  }

  /**
   * @return True si le projet contient des resultats de debit.
   */
  public boolean hasFlowResults() {
    return flowResults != null && flowResults.length > 0;
  }

  /**
   * @return True : Si le projet contient des fichiers scalaires resultats instantan�s. En principe, dans la nouvelle version, ces fichiers existent
   *         forcement si les resultats de vitesses sont existants.
   */
  public boolean hasScalInstantResultsFiles() {
    File[] instantScalFiles = new File(rootPath, VEL_SCAL_DIR).listFiles();
    return instantScalFiles != null && instantScalFiles.length > 0;
  }

  /**
   * Retourne le chemin des fichiers vitesses brutes, ordonn�es par ordre alphab�tique.
   * 
   * @return Les chemins des fichiers
   */
  public File[] getRawVelocityFiles() {
    File velRawDir = new File(rootPath, VEL_RAW_DIR);
    if (!velRawDir.isDirectory())
      return new File[0];

    File[] files = velRawDir.listFiles(new FileFilter() {
      public boolean accept(File _f) {
        return _f.isFile() && (_f.getPath().endsWith(".dat"));
      }
    });
    // files n'est jamais null.
    Arrays.sort(files);

    return files;
  }

  /**
   * Retourne le chemin des images sources, dans l'ordre utilisateur.
   * 
   * @param _imagesType Le type des images a retourner.
   * @return Le chemin des images.
   */
  public File[] getSrcImageFiles(SRC_IMAGE_TYPE _imagesType) {
    List<File> ret = new ArrayList<>();

    for (File f : srcFiles_) {
      if ((_imagesType == SRC_IMAGE_TYPE.BACKGROUND && f.getName().startsWith(BACKGROUND_IMAGE_NAME_PREFIX))
          || (_imagesType == SRC_IMAGE_TYPE.CALCULATION && !f.getName().startsWith(BACKGROUND_IMAGE_NAME_PREFIX))
          || (_imagesType == SRC_IMAGE_TYPE.ALL)) {
        ret.add(f);
      }
    }

    return ret.toArray(new File[0]);
  }

  /**
   * @return Le chemin des images pretes a etre exploit�es (celle en cache en particulier).
   */
  public File[] getReadySrcImageFiles() {
    return readySrcFiles_.toArray(new File[0]);
  }

  /**
   * Reconstruit les caches si necessaire.
   * 
   * @param _task La progression.
   * @return false : Si un probl�me s'est produit lors de la production.
   */
  public boolean rebuiltAllCacheImagesIfNeeded(PivTaskObserverI _task) {
    File imgSrcDir = new File(rootPath, IMG_PGM_DIR);
    File imgTransfDir = new File(rootPath, IMG_TRANSF_DIR);
    File imgStabDir = new File(rootPath, IMG_STAB_DIR);

    boolean b;
    b = rebuiltCacheImagesIfNeeded(srcFiles_, readySrcFiles_, imgSrcDir, _task);
    fireProjectStateChanged("pgmImages");
    b &= rebuiltCacheImagesIfNeeded(Arrays.asList(getStabilizedImageFiles()), readyStabilizedFiles_, imgStabDir, _task);
    fireProjectStateChanged("stabImages");

    return b;
  }

  /**
   * Controle que les images cache existent pour le r�pertoire des images et les cr�e en asynchrone si ce n'est pas le cas.
   * 
   * @param _files      Les fichiers � mettre en cache.
   * @param _readyFiles LA liste des fichiers prets a etre exploit�s
   * @param _dir        Le r�pertoire contenant les fichiers � mettre en cache.
   * @return false : Si un probl�me s'est produit lors de la production.
   */
  private boolean rebuiltCacheImagesIfNeeded(List<File> _files, final List<File> _readyFiles, final File _dir,
      final PivTaskObserverI _prog) {
    boolean b = true;

    File imgCacheDir = new File(_dir, "cache");
    imgCacheDir.mkdirs();

    _readyFiles.clear();

//    final boolean bsrc=_dir.equals(new File(rootPath,IMG_PGM_DIR));
    final List<File> file2Cache = new ArrayList<File>();
    final List<Integer> posFile2Cache = new ArrayList<Integer>();

    final List<File> cacheFiles = new ArrayList<File>();

    // Cr�ation des images cache manquantes
    if (_prog != null) {
      _prog.reset();
      _prog.setDesc(PivResource.getS("Cr�ation des caches images"));
    }

    // Une seule image cache doit �tre regener�e. S'il en existe deja au moins 1, pas de r�g�n�ration.
    boolean bonlyOne = !PivPreferences.PIV.getBooleanProperty(PivPreferences.PIV_AUTO_CACHE, true);
    int nb2Cache = 0;

    for (int i = 0; i < _files.size(); i++) {
      File cache = getCacheImageFile(_files.get(i));
      cacheFiles.add(cache);

      if (cache.exists()) {
        _readyFiles.add(_files.get(i));
        // Le cache est diff�rent de l'image : Fichier pgm, donc nb2Cache++
        if (!cache.getPath().equals(_files.get(i).getPath()))
          nb2Cache++;
      }
      else if (!bonlyOne || nb2Cache < 1) {
        file2Cache.add(_files.get(i));
        posFile2Cache.add(i);
        nb2Cache++;
      }
    }

    Runnable r = new Runnable() {

      @Override
      public void run() {
        for (int i = 0; i < file2Cache.size(); i++) {
          if (_prog != null)
            _prog.setProgression(i * 100 / file2Cache.size());

          getCacheImageFile(file2Cache.get(i));
          _readyFiles.add(posFile2Cache.get(i), file2Cache.get(i));
//          fireProjectStateChanged(bsrc ? "pgmImages":"transfImages");

          if (_prog != null && _prog.isStopRequested()) {
            break;
          }
        }

        // Suppression des images cache inutiles
        for (File f : new File(_dir, "cache").listFiles()) {
          if (!cacheFiles.contains(f))
            f.delete();
        }

        if (_prog != null)
          _prog.setProgression(100);
      }
    };
//    new Thread(r).start();
    r.run();

    return b;
  }

  /**
   * Verifie si une image PGM est en cache ou non
   * 
   * @param _file Le chemin du fichier PGM
   * @return True si le fichier est d�j� en cache.
   */
  public boolean cacheExists(File _file) {
    File fcache = getCacheImageFile(_file);
    return (fcache.exists());
  }

  /**
   * Retourne le chemin du fichier cache pour un fichier donn�. Les fichiers cache sont utilis�s pour l'affichage. Si le type de l'image n'est pas pgm,
   * le fichier lui m�me est retourn�.
   * 
   * @param _file Le fichier pgm donn�.
   * @return Le chemin du fichier cache sous la forme "__&lt;file&gt;.gif"
   */
  public File getCacheImageFile(File _file) {
    if (!_file.getPath().endsWith(".pgm"))
      return _file;

    String name = "__" + CtuluLibFile.getSansExtension(_file.getName()) + ".gif";
    
    File cacheFile = new File(_file.getParentFile(), "cache" + File.separator + name);
    if (!cacheFile.exists()) {
      // On cr�e le cache.
      createCacheFromImage(_file, cacheFile);
    }
    
    return cacheFile;
  }

  /**
   * Retourne le flux d'entr�e de l'image cache indiqu�e. L'ancien flux est alors ferm�.
   * 
   * @param _f L'image cache
   * @return Le flux.
   */
  public ImageInputStream getSrcOrStabCacheImageInputStream(File _f) {
    try {
      closeSrcOrCacheImageInputStream();
      cacheSrcOrStabInputStream_ = ImageIO.createImageInputStream(_f);
    }
    catch (IOException _exc) {
    }
    return cacheSrcOrStabInputStream_;
  }

  /**
   * Fermeture de l'input stream cache
   */
  private void closeSrcOrCacheImageInputStream() {
    try {
      if (cacheSrcOrStabInputStream_ != null)
        cacheSrcOrStabInputStream_.close();
      cacheSrcOrStabInputStream_ = null;
    }
    catch (IOException _exc) {
    }
  }

  /**
   * Retourne le flux d'entr�e de l'image cache transform�e en espace transform�. L'ancien flux est alors ferm�.
   * 
   * @param _f L'image cache
   * @return Le flux.
   */
  public ImageInputStream getTransfCacheImageInputStream(File _f) {
    try {
      closeTransfCacheImageInputStream();
      cacheTransfInputStream_ = ImageIO.createImageInputStream(_f);
    }
    catch (IOException _exc) {
    }
    return cacheTransfInputStream_;
  }

  /**
   * Fermeture de l'input stream cache image transform�e
   */
  private void closeTransfCacheImageInputStream() {
    try {
      if (cacheTransfInputStream_ != null)
        cacheTransfInputStream_.close();
      cacheTransfInputStream_ = null;
    }
    catch (IOException _exc) {
    }
  }

  /**
   * Retourne le flux d'entr�e de l'image cache transform�e en espace r�el indiqu�e. L'ancien flux est alors ferm�.
   * 
   * @param _f L'image cache
   * @return Le flux.
   */
  public ImageInputStream getRealCacheImageInputStream(File _f) {
    try {
      closeRealCacheImageInputStream();
      cacheRealInputStream_ = ImageIO.createImageInputStream(_f);
    }
    catch (IOException _exc) {
    }
    return cacheRealInputStream_;
  }

  /**
   * Fermeture de l'input stream cache image reel
   */
  private void closeRealCacheImageInputStream() {
    try {
      if (cacheRealInputStream_ != null)
        cacheRealInputStream_.close();
      cacheRealInputStream_ = null;
    }
    catch (IOException _exc) {
    }
  }

  /**
   * Retourne la taille des images source.
   * 
   * @return La taille, ou [0,0] si aucune image source.
   */
  public Dimension getSrcImageSize() {
    if (areSrcImagesChanged || imgPgmSize == null) {
      areSrcImagesChanged = false;
      imgPgmSize = new Dimension(0, 0); // Valeur retourn�e si une erreur

      File[] imgs = getSrcImageFiles(PivProject.SRC_IMAGE_TYPE.ALL);
      if (imgs.length == 0)
        return imgPgmSize;

      Dimension size = getImageSize(getCacheImageFile(imgs[0]));
      if (size != null)
        imgPgmSize = size;
    }
    return imgPgmSize;
  }

  /**
   * @param _params Les parametres d'�chantillonnage. Ils sont sauvegard�s pour le rapport de jaugeage.
   */
  public void setSamplingParameters(PivSamplingVideoParameters _params) {
    samplingParams = _params;
  }

  public PivSamplingVideoParameters getSamplingParameters() {
    return samplingParams;
  }

  /**
   * Retourne la taille des images transform�es.
   * 
   * @return La taille, ou [0,0] si aucune image transform�e.
   */
  public Dimension getTransfImageSize() {
    if (areTransfImagesChanged || imgTransfSize == null) {
      areTransfImagesChanged = false;
      imgTransfSize = new Dimension(0, 0); // Valeur retourn�e si une erreur

      File[] imgs = getTransfImageFiles();
      if (imgs.length == 0)
        return imgTransfSize;

      Dimension size = getImageSize(getCacheImageFile(imgs[0]));
      if (size != null)
        imgTransfSize = size;
    }
    return imgTransfSize;
  }

  /**
   * Retourne la dimension d'une image.
   * 
   * @param _imgFile Le fichier image.
   * @return La taille ou <tt>null</tt> si le fichier n'est pas un fichier image.
   */
  private Dimension getImageSize(File _imgFile) {
    // Calcul de la taille par lecture de l'image.
    String extension = CtuluLibFile.getExtension(_imgFile.getName());
    final Iterator<ImageReader> it = ImageIO.getImageReadersBySuffix(extension);
    ImageReader imgReader = null;
    if (it != null && it.hasNext()) {
      imgReader = it.next();
    }
    if (imgReader == null) {
      return null;
    }

    try {
      imgReader.setInput(ImageIO.createImageInputStream(_imgFile));
      return new Dimension(imgReader.getWidth(0), imgReader.getHeight(0));
    }
    catch (final IOException _evt) {
      FuLog.error(_evt);
    }
    finally {
      imgReader.dispose();
    }

    return null;
  }

  /**
   * Cree l'image cache pour une image donn�e. Le cache est cr�� dans le projet a l'endroit de l'image donn�e.
   * 
   * @param _srcFile Le fichier image source
   * @return true si l'op�ration a �t� r�alis�e.
   */
  private boolean createCacheFromImage(File _srcFile, File _destFile) {
    try {
      // Lecture du fichier PGM forcement ascii
      BufferedImage buffer = new PivPGMReader().read(_srcFile);
      if (buffer == null)
        return false;

      // Si le repertoire cache n'existe pas, on le cr�e.
      _destFile.getParentFile().mkdirs();
      
      boolean b = ImageIO.write(buffer, "gif", _destFile);
      // On vide l'image pgm de d�part, pour gain de place.
      _srcFile.delete();
      _srcFile.createNewFile();

      return b;
    }
    catch (IOException _exc) {
      return false;
    }
  }

  /**
   * D�finit que les images transform�es ont chang� suite au calcul.
   */
  public void setTransfImagesChanged(PivTaskObserverI _prog) {
    areTransfImagesChanged = true;
    isModified = true;

    // Lib�re les stream cache.
    closeRealCacheImageInputStream();
    closeTransfCacheImageInputStream();

    File imgTransfDir = new File(rootPath, IMG_TRANSF_DIR);

    // Destruction de toutes les images caches.
    File imgCacheDir = new File(imgTransfDir, "cache");
    File[] oldCacheFiles = imgCacheDir.listFiles();
    if (oldCacheFiles != null) {
      for (File f : oldCacheFiles) {
        f.delete();
      }
    }

    fireProjectStateChanged("transfImages");

    setInstantRawResults(null);
  }

  /**
   * Recharge les images en m�moire, apr�s modification sur le FS.
   * En fait, remet a jour le cache.
   */
  public void reloadStabilizedImages() {
    setStabilizedImagesChanged(null);
  }

  /**
   * Recharge les images en m�moire, apr�s modification sur le FS.
   * En fait, remet a jour le cache.
   */
  public void reloadTransfImages() {
    setTransfImagesChanged(null);
  }
  
  /**
   * Recharge les resultats instantan�s en memoire, apres modifications sur le FS.
   */
  public void reloadInstantRawResults() {
    setInstantRawResults(persMng.loadInstantRawResults(null));
  }
  
  /**
   * Recharge les resultats manuels en memoire, apres modifications sur le FS.
   */
  public void reloadManualResults() {
    setManualResults(persMng.loadManualResults(null));
  }
  
  /**
   * Recharge les resultats filtr�s en memoire, apres modifications sur le FS.
   */
  public void reloadInstantFilteredResults() {
    setInstantFilteredResults(persMng.loadInstantFilteredResults(null));
  }
  
  /**
   * Recharge les resultats moyenn�s en memoire, apres modifications sur le FS.
   */
  public void reloadAverageResults() {
    setAverageResults(persMng.loadAverageResults(null));
  }
  
  /**
   * Recharge les resultats de d�bit en memoire, apres modifications sur le FS.
   */
  public void reloadFlowResults() {
    setFlowResults(persMng.loadFlowResults(null));
  }
  
  /**
   * D�finit que les images stabilis�es ont chang� sur le FS.
   */
  public void setStabilizedImagesChanged(PivTaskObserverI _prog) {
    areStabilizedImagesChanged = true;
    isModified = true;

    // Lib�re les stream cache.
//    closeRealCacheImageInputStream();
//    closeTransfCacheImageInputStream();

    File imgStabDir = new File(rootPath, IMG_STAB_DIR);

    // Destruction de toutes les images caches.
    File imgCacheDir = new File(imgStabDir, "cache");
    File[] oldCacheFiles = imgCacheDir.listFiles();
    if (oldCacheFiles != null) {
      for (File f : oldCacheFiles) {
        f.delete();
      }
    }

    rebuiltCacheImagesIfNeeded(Arrays.asList(getStabilizedImageFiles()), readyStabilizedFiles_, imgStabDir, _prog);
    fireProjectStateChanged("stabImages");
  }

  /**
   * Supprime les images transform�es, et tous les r�sultats qui s'en suivent.
   */
  public void removeTransformedImages() {
    removeTransformedImagesOnFS();

    setTransfImagesChanged(null);
  }

  /**
   * Supprime les images transform�es sur le FS
   */
  public void removeTransformedImagesOnFS() {
    File prjTransfImgs = new File(rootPath, IMG_TRANSF_DIR);

    // Nettoyage du repertoire des images
    prjTransfImgs.mkdirs();
    for (File img : prjTransfImgs.listFiles())
      img.delete();
    
    // Et celui du cache
    closeTransfCacheImageInputStream();
    File prjTransFImgsCache = new File(prjTransfImgs, "cache");
    // Nettoyage du repertoire des images
    prjTransFImgsCache.mkdirs();
    for (File img : prjTransFImgsCache.listFiles())
      img.delete();
  }

  /**
   * Supprime les resultats brut sur le FS.
   */
  public void removeInstantRawResultsOnFS() {
    File prjRoot = getRoot();
    File prjOutputs = new File(prjRoot, OUTPUT_DIR);
    File prjVelRaw = new File(prjRoot, VEL_RAW_DIR);
    File prjVelFlt = new File(prjRoot, VEL_FILTER_DIR);
    File prjScalFlt = new File(prjRoot, VEL_SCAL_DIR);
    File prjVelReal = new File(prjRoot, VEL_REAL_DIR);

    // Nettoyage eventuel du repertoire des r�sultats.
    CtuluLibFile.deleteDir(prjVelRaw);
    prjVelRaw.mkdirs();
    // Nettoyage eventuel du repertoire des r�sultats vitesse
    CtuluLibFile.deleteDir(prjVelReal);
    prjVelReal.mkdirs();
  }

  /**
   * Supprime les resultats moyenn�s sur le FS.
   */
  public void removeAverageResultsOnFS() {
    File prjRoot = getRoot();
    File prjOutputs = new File(prjRoot, OUTPUT_DIR);

    prjOutputs.mkdirs();
    // Fichier des vitesses du repertoire projet
    String faverage = "average_vel.out";
    File resVelFile = new File(prjOutputs, faverage);
    resVelFile.delete();

    // Fichier des resultats du repertoire projet
    faverage = "average_scal.out";
    File resScalFile = new File(prjOutputs, faverage);
    resScalFile.delete();
  }
  
  /**
   * Supprime les r�sultats de vitesses manuelles
   */
  public void removeManualResultsOnFS() {
    File prjRoot = getRoot();
    File prjOutputs = new File(prjRoot, OUTPUT_DIR);

    prjOutputs.mkdirs();
    // Fichier des points de d�placements
    String filename = "manual_tracking.dat";
    File fpoints = new File(prjOutputs, filename);
    fpoints.delete();

    // Fichier des resultats du repertoire projet
    filename = "manual_vel.out";
    File fres = new File(prjOutputs, filename);
    fres.delete();
  }

  /**
   * Supprime les resultats filtr�s sur le FS.
   */
  public void removeInstantFilteredResultsOnFS() {
    File prjRoot = getRoot();
    File prjVelFlt = new File(prjRoot, VEL_FILTER_DIR);
    File prjScalFlt = new File(prjRoot, VEL_SCAL_DIR);

    // Nettoyage eventuel du repertoire des r�sultats.
    CtuluLibFile.deleteDir(prjVelFlt);
    prjVelFlt.mkdirs();
    // Nettoyage des scalaires filtr�s
    CtuluLibFile.deleteDir(prjScalFlt);
    prjScalFlt.mkdirs();
  }

  /**
   * Supprime les resultats de d�bit sur le FS.
   */
  public void removeFlowResultsOnFS() {
    File prjRoot = getRoot();
    File prjTransects = new File(prjRoot, TRANSECTS_DIR);

    // Suppression des r�sultats de d�bit pr�c�dents.
    prjTransects.mkdirs();
    for (File resFile : prjTransects.listFiles(PivUtils.FILE_FLT_TRANS_RES))
      resFile.delete();
  }

  /**
   * Supprime les images stabilis�es du projet.
   */
  public void removeStabilizedImages() {
    removeStabilizedImagesOnFS();
    setStabilizedImagesChanged(null);
    removeTransformedImages();
  }
  
  /**
   * Supprime les images stabilis�es sur le FS.
   */
  public void removeStabilizedImagesOnFS() {
    File prjStabImgs = new File(rootPath, IMG_STAB_DIR);

    // Nettoyage du repertoire des images stabilis�es
    prjStabImgs.mkdirs();
    for (File stabImg : prjStabImgs.listFiles())
      stabImg.delete();
    
    // Et celui du cache
    closeSrcOrCacheImageInputStream();
    File prjStabImgsCache = new File(prjStabImgs, "cache");
    // Nettoyage du repertoire des images
    prjStabImgsCache.mkdirs();
    for (File img : prjStabImgsCache.listFiles())
      img.delete();
  }

  /**
   * Definit et traite si necessaire les images d'origine de tous types. Toutes les images doivent avoir la m�me taille. L'ordre des images est utilis�
   * pour les calculs. Les images d�j� existantes ne subissent aucun traitement.
   * 
   * @param _files     Les images sources.
   * @param _fileTypes Le type de chaque image.
   */
  public void setSrcImagesFiles(File[] _files, SRC_IMAGE_TYPE[] _fileTypes, PivTaskObserverI _task, CtuluLog _ana) {

    // Les images de calcul

    File imgInpDir = new File(rootPath, IMG_PGM_DIR);
    imgInpDir.mkdirs();

    areSrcImagesChanged = !CtuluLibArray.isEquals(_files, srcFiles_.toArray(new File[0]));

    // Lib�re le stream.
    closeSrcOrCacheImageInputStream();

    List<File> newFiles = new ArrayList<File>();
    List<File> newpgm = new ArrayList<File>();

    // Traitement des fichiers .pgm, pour mise en cache.
    for (int i = 0; i < _files.length; i++) {
      File f = _files[i];

      // Le nom des images de fond commence par "[F]"
      String filename = f.getName();
      if (filename.startsWith(BACKGROUND_IMAGE_NAME_PREFIX)) {
        filename = filename.substring(BACKGROUND_IMAGE_NAME_PREFIX.length());
      }
      if (_fileTypes[i] == SRC_IMAGE_TYPE.BACKGROUND)
        filename = BACKGROUND_IMAGE_NAME_PREFIX + filename;

      File fimg = new File(imgInpDir, filename);

      if (!srcFiles_.contains(fimg)) {
        // Reconditionnement de l'image, uniquement si le cache n'est pas automatique.
        if (fimg.getName().endsWith(".pgm")
            && !PivPreferences.PIV.getBooleanProperty(PivPreferences.PIV_AUTO_CACHE, true)) {
          newpgm.add(fimg);
        }
        else {
          // Copie du fichier localement au projet.
          CtuluLibFile.copyFile(f, fimg);
        }
      }
      newFiles.add(fimg);
    }

    // Reconditionnement des pgm. Necessaire si une seule image est mise en cache.
    repack(_ana, _task, newpgm.toArray(new File[0]));

    // Suppression des anciennes images sources
    for (File f : imgInpDir.listFiles(FLT_FILES)) {
      if (!newFiles.contains(f))
        f.delete();
    }

    // Si les images ont chang�, le projet est modifi�.
    if (!CtuluLibArray.isEquals(newFiles.toArray(new File[0]), srcFiles_.toArray(new File[0])))
      isModified = true;

    srcFiles_ = newFiles;

    // Reconstruction des images caches
    rebuiltCacheImagesIfNeeded(srcFiles_, readySrcFiles_, imgInpDir, _task);
    fireProjectStateChanged("pgmImages");

    if (areSrcImagesChanged)
      removeStabilizedImages();
  }

  /**
   * Reconditionne les images d'origine de pgm ligne a pgm matrice, et les place dans le r�pertoire des images reconditionn�es.
   * 
   * @param _ana     L'analyse pour la tache ex�cut�e.
   * @param _task    La tache en cours d'execution.
   * @param _imgOrig Les fichiers d'origine � reconditionner.
   * @return true : Tout s'est bien d�roul�.
   */
  public boolean repack(CtuluLog _ana, PivTaskObserverI _task, File[] _imgOrig) {
    File imgInpDir = new File(rootPath, IMG_PGM_DIR);

    if (_task != null)
      _task.setProgression(10);

    int prog = 0;
    for (int i = 0; i < _imgOrig.length; i++) {
      File dstImgInp = new File(imgInpDir, _imgOrig[i].getName());

      String desc = PivResource.getS("Reconditionnement de l'image {0} sur {1}", (i + 1), _imgOrig.length);
      FuLog.trace(desc);
      if (_task != null) {
        _task.setDesc(desc);
        _task.setProgression(prog);
      }

      try {
        BufferedImage buf = new PivPGMReader().read(_imgOrig[i]);
        new PivPGMWriter().write(dstImgInp, buf);

        if (_task != null && _task.isStopRequested()) {
          _ana.addError(PivResource.getS("Reconditionnement interrompu"));
          return false;
        }
      }
      catch (IOException ex) {
        _ana.addError(ex.getMessage());
        return false;
      }
      prog = (i + 1) * 90 / _imgOrig.length;
      if (_task != null)
        _task.setProgression(prog);
    }

    FuLog.trace(PivResource.getS("Reconditionnement ok."));

    if (_task != null)
      _task.setProgression(100);

    return true;
  }

  /**
   * Les images transform�es ont-elles �t� modifi�es par le calcul ?
   * 
   * @return true : elles ont �t� modifi�es.
   */
  public boolean areTransfImagesChanged() {
    return areTransfImagesChanged;
  }

  /**
   * Le projet contient-il au moins une image transfom�e ?
   * 
   * @return true si le projet contient au moins 1 images transform�e.
   */
  public boolean hasTransfImages() {
    return getTransfImageFiles().length > 0;
  }

  /**
   * Retourne le chemin des fichiers images transform�es, ordonn�es par ordre alphab�tique, et limit�s aux seules images transform�es sous
   * echantillonn�es
   * 
   * @return Les chemins des fichiers.
   */
  public File[] getTransfImageFiles() {
    File imgOutDir = new File(rootPath, IMG_TRANSF_DIR);
    if (!imgOutDir.isDirectory())
      return new File[0];

    File[] files = imgOutDir.listFiles(new FileFilter() {
      public boolean accept(File _f) {
        return _f.isFile() && (_f.getPath().endsWith(".pgm"));
      }
    });
    // files n'est jamais null.
    Arrays.sort(files);

    // On limite le nombre d'images en fonction du sous echantillonnage.
    int samplingPeriod = getComputeParameters().getUnderSamplingPeriod();
    if (samplingPeriod != 1) {
      ArrayList<File> filteredFiles = new ArrayList<>();
      int icpt = 0;
      while (icpt < files.length) {
        filteredFiles.add(files[icpt]);
        icpt += samplingPeriod;
      }

      files = filteredFiles.toArray(new File[0]);
    }

    return files;
  }

  /**
   * @return Les chemins des fichiers images stabilis�s
   */
  public File[] getStabilizedImageFiles() {
    File imgOutDir = new File(rootPath, IMG_STAB_DIR);
    if (!imgOutDir.isDirectory())
      return new File[0];

    File[] files = imgOutDir.listFiles(new FileFilter() {
      public boolean accept(File _f) {
        return _f.isFile() && (_f.getPath().endsWith(".pgm"));
      }
    });
    // files n'est jamais null.
    Arrays.sort(files);

    return files;

  }

  /**
   * Relache les ressources li�es au projet. Doit �tre appel� � la fermeture du projet.
   */
  public void dispose() {
    closeSrcOrCacheImageInputStream();
    closeRealCacheImageInputStream();
    closeTransfCacheImageInputStream();

    // Supprime aussi le repertoire temporaire
    CtuluLibFile.deleteDir(rootPath);
  }

  /**
   * Charge le projet depuis le fichier projet donn�. Le fichier projet est d�zipp�, puis il est charg� s'il est conforme.
   * 
   * @param _prjFile Le fichier contenant le projet.
   * @param _prog    L'interface de progression. Peut etre <tt>null</tt>
   * @return True Si le fichier donn� est bien un fichier projet.
   */
  public boolean load(PivTaskObserverI _task) {
    return persMng.load(_task);
  }

  /**
   * Sauve le projet sur le fichier projet. Les infos sont sauv�es sur le r�pertoire temporaire, puis zip�es.
   * 
   * @param _prog L'interface de progression. Peut etre <tt>null</tt>
   * @return True si le projet a bien �t� sauv�.
   */
  public boolean save(ProgressionInterface _prog) {
    return persMng.save(this, _prog);
  }

  /**
   * Test si le projet a �t� modifi�.
   * 
   * @return true si le projet a �t� modifi�.
   */
  public boolean isParamsModified() {
    return isModified;
  }

  /**
   * @return Le mode d'orthorectification. La distinction entre 2D et 3D se fait grace aux GRP. Si les GRP ne sont pas d�finis, l'ortho est 2D (sauf si
   *         definit en SCALING).
   */
  public OrthoMode getOrthoMode() {

    // Full ortho
    if (coeffs == null || coeffs.length > 0) {
      if (orthoPoints != null && orthoPoints.length > 1) {
        Double z = orthoPoints[0].realPoint.z_;
        for (int i = 1; i < orthoPoints.length; i++) {
          if (orthoPoints[i].realPoint.z_ != z) {
            return OrthoMode.ORTHO_3D;
          }
        }
      }
      return OrthoMode.ORTHO_2D;
    }

    // Scaling.
    else {
      return OrthoMode.SCALING;
    }
  }

  /**
   * Definit le mode d'orthorectification.
   * 
   * @param _mode Le mode. Le mode ORTHO_3D ou ORTHO_2D agit de la m�me mani�re.
   */
  public void setOrthoMode(OrthoMode _mode) {
    if (_mode == getOrthoMode())
      return;

    // On supprime les parametres de l'ortho, qui peuvent contenir des couples de points.
    setOrthoParameters(null);

    if (_mode == OrthoMode.ORTHO_2D || _mode == OrthoMode.ORTHO_3D) {
      coeffs = null;
    }
    else {
      coeffs = new double[0];
      // Suppression des points d'orthorectification
      setOrthoPoints(null);
    }

    isModified = true;
    fireProjectStateChanged("orthoMode");

    // Le mode ortho est modifi�, les images transform�es sont supprim�es, les r�sultats aussi.
    removeTransformedImages();
  }

  /**
   * @return True si la stabilisation est active sur le projet.
   */
  public boolean isStabilizationActive() {
    return stabParameters != null && stabParameters.isActive();
  }

  /**
   * Retourne les points d'orthorectification.
   * 
   * @return Les points, ou <tt>null</tt> s'ils n'ont pas �t� d�finis.
   */
  public PivOrthoPoint[] getOrthoPoints() {
    return orthoPoints;
  }

  /**
   * Definit les points d'orthorectification, et notifie que le projet a �t� modifi�.
   * 
   * @param _pts Les points. <tt>null</tt> est autoris�.
   */
  public void setOrthoPoints(PivOrthoPoint[] _pts) {
    orthoPoints = _pts;
    isModified = true;
    fireProjectStateChanged("orthoPoints");
  }

  /**
   * Retourne les param�tres d'orthorectification.
   * 
   * @return Les param�tres, ou <tt>null</tt> s'ils n'ont pas �t� d�finis.
   */
  public PivOrthoParameters getOrthoParameters() {
    return orthoParams;
  }

  /**
   * Definit les param�tres d'orthorectification, et notifie que le projet a �t� modifi�.
   * 
   * @param _params Les param�tres. <tt>null</tt> est autoris�.
   */
  public void setOrthoParameters(PivOrthoParameters _params) {
    double newResol = _params != null ? _params.getResolution() != null ? _params.getResolution() : 0 : 0;
    double oldResol = orthoParams != null ? orthoParams.getResolution() != null ? orthoParams.getResolution() : 0 : 0;

    // Si changement de resolution, on modifie la grille, IA/SA pour que les coordonn�es m�triques restent constantes.
    // Si la nouvelle resolution ou l'ancienne sont nulles ou egale � 0, pas de recalcul.
    if (newResol != 0 && oldResol != 0 && newResol != oldResol) {
      if (getComputeGrid() != null) {
        PivGrid grid = getComputeGrid();
        grid.changeScale(oldResol / newResol);
        setComputeGrid(grid);
      }

      if (getComputeParameters() != null) {
        PivComputeParameters params = getComputeParameters();
        params.changeScale(oldResol / newResol);
        setComputeParameters(params);
      }
    }

    // Il se peut que la resolution soit nulle, si elle n'a pas pu �tre calcul�e. Dans ce cas, on garde la pr�c�dente, pour ne pas
    // risquer da division par 0 lors du changement d'echelle de la grille.
    if (oldResol != 0 && newResol == 0 && _params != null) {
      _params.resolution = oldResol;
    }

    orthoParams = _params;

    isModified = true;
    fireProjectStateChanged("orthoParameters");
  }

  /**
   * Retourne les param�tres de calcul.
   * 
   * @return Les param�tres, ou <tt>null</tt> s'ils n'ont pas �t� d�finis.
   */
  public PivComputeParameters getComputeParameters() {
    return computeParams;
  }

  /**
   * Definit les param�tres de calcul, et notifie que le projet a �t� modifi�.
   * 
   * @param _params Les param�tres. <tt>null</tt> est autoris�.
   */
  public void setComputeParameters(PivComputeParameters _params) {
    if ((_params == null ^ computeParams == null) || (_params == null || !_params.equals(computeParams))) {

      computeParams = _params;
      isModified = false;
      fireProjectStateChanged("computeParameters");
    }
  }

  /**
   * Modifie le pas de temps.
   * 
   * @param _timeStep Le pas de temps.
   */
  public void setTimeStep(Double _timeStep) {
    if (computeParams == null) {
      computeParams = new PivComputeParameters();
      isModified = true;
    }
    // Si changement d'intervalle de temps, on modifie SA pour que les coordonn�es m�triques restent constantes.
    if ((computeParams.getTimeInterval() == null ^ _timeStep == null)
        || !computeParams.getTimeInterval().equals(_timeStep)) {
      computeParams.changeTimeInterval(_timeStep);
      isModified = true;
    }

    if (isModified)
      fireProjectStateChanged("computeParameters");
  }

  /**
   * @return Le pas de temps.
   */
  public Double getTimeStep() {
    if (computeParams == null)
      return null;

    return computeParams.getTimeInterval();
  }
  
  public void setComputeSamplingPeriod(Integer _period) {
    if (computeParams == null) {
      computeParams = new PivComputeParameters();
    }
    
    // Si changement de periode...
    if (computeParams.getUnderSamplingPeriod() != _period) {
      Integer oldPeriod = computeParams.getUnderSamplingPeriod();
      // on modifie SA pour que les coordonn�es m�triques restent constantes.      
      computeParams.changeUnderSamplingPeriod(_period);
      // On modifie les indices d'images pour le calcul des vitesses manuelles.
      manualVelParameters.changeUnderSamplingPeriod(oldPeriod, _period);
      
      isModified = true;
    }
        
    if (isModified)
      fireProjectStateChanged("transfImages");
  }
  
  public Integer getComputeSamplingPeriod() {
    if (computeParams == null)
      return null;

    return computeParams.getUnderSamplingPeriod();
  }

  /**
   * Retourne les points de la grille de calcul.
   * 
   * @return Les points, ou <tt>null</tt> s'ils n'ont pas �t� d�finis.
   */
  public PivGrid getComputeGrid() {
    return computeGrid;
  }

  /**
   * Definit les points de la grille de calcul, et notifie que le projet a �t� modifi�.
   * 
   * @param _params Les points. <tt>null</tt> est autoris�.
   */
  public void setComputeGrid(PivGrid _params) {
    computeGrid = _params;
    isModified = true;
    fireProjectStateChanged("computeGrid");
  }

  /**
   * Retourne le contour de grille.
   * 
   * @return Le contour, ou <tt>null</tt> s'il n'a pas �t� d�fini.
   */
  public PivCntGrid getComputeCntGrid() {
    return cntGrid;
  }

  /**
   * Definit le contour de grille, et notifie que le projet a �t� modifi�.
   * 
   * @param _pg Le contour. <tt>null</tt> est autoris�.
   */
  public void setComputeCntGrid(PivCntGrid _pg) {
    cntGrid = _pg;
    isModified = true;
    fireProjectStateChanged("cntGrid");
  }

  /**
   * Ajoute un transect aux transects existants.
   * 
   * @param _trans Le transect � ajouter.
   */
  public void addTransect(PivTransect _trans) {
    PivTransect[] trans;

    if (transects == null) {
      trans = new PivTransect[] { _trans };
    }
    else {
      trans = Arrays.copyOf(transects, transects.length + 1);
      trans[trans.length - 1] = _trans;
    }

    setTransects(trans);
  }

  /**
   * Definit les transects pour le calcul de d�bit.
   * 
   * @param _trans Les transects. <tt>null</tt> est autoris�.
   */
  public void setTransects(PivTransect[] _trans) {
    // On met a null les r�sultats de d�bit en cas de tableaux diff�rents,
    // car le nombre de r�sultats doit �tre identique.
    if ((transects == null ^ _trans == null) || transects != null && transects.length != _trans.length) {
      setFlowResults(null);
    }
    transects = _trans;
    isModified = true;
    fireProjectStateChanged("transect");
  }

  /**
   * Retourne les transects pour le calcul de d�bit.
   * 
   * @return Les transects ou <tt>null</tt> si aucun n'a �t� d�fini.
   */
  public PivTransect[] getTransects() {
    return transects;
  }

  /**
   * Retourne les r�sultats.
   * 
   * @return Les r�sultats, ou <tt>null</tt> s'il n'a pas �t� d�fini.
   */
  public PivResultsI getAverageResults() {
    return averageResults;
  }

  /**
   * Definit les r�sultats de calcul, et notifie que le projet a �t� modifi�.
   * 
   * @param _res Les r�sultats. <tt>null</tt> est autoris�.
   */
  public void setAverageResults(PivResultsI _res) {
    averageResults = _res;

    setAverageResultsChanged();
  }

  public void setAverageResultsChanged() {
    isModified = true;
    fireProjectStateChanged("averageResults");

    setFlowResults(null);
  }

  /**
   * Retourne les r�sultats instantan�s filtr�s.
   * 
   * @return Les r�sultats, ou <tt>null</tt> s'ils n'ont pas �t� d�finis.
   */
  public PivResultsI[] getInstantFilteredResults() {
    return instantFilteredResults;
  }

  /**
   * Definit les r�sultats instantan�s filtr�s de calcul, et notifie que le projet a �t� modifi�.
   * 
   * @param _res Les r�sultats. <tt>null</tt> est autoris�.
   */
  public void setInstantFilteredResults(PivResultsI[] _res) {
    if (CtuluLibArray.isEquals(instantFilteredResults, _res))
      return;

    instantFilteredResults = _res;

    setInstantFilteredResultsChanged();
  }

  public void setInstantFilteredResultsChanged() {
    isModified = true;
    fireProjectStateChanged("instantFilteredResults");

    setAverageResults(null);
  }

  /**
   * Retourne les r�sultats manuels.
   * 
   * @return Les r�sultats, ou <tt>null</tt> s'ils n'ont pas �t� d�finis.
   */
  public PivResultsI getManualResults() {
    return manualResults;
  }

  /**
   * Definit les r�sultats instantan�s bruts de calcul, et notifie que le projet a �t� modifi�.
   * 
   * @param _res Les r�sultats. <tt>null</tt> est autoris�.
   */
  public void setManualResults(PivResultsI _res) {
    if (CtuluLib.isEquals(manualResults, _res))
      return;

    manualResults = _res;

    setManualResultsChanged();
  }

  public void setManualResultsChanged() {
    isModified = true;
    fireProjectStateChanged("manualResults");

    // Les resultats manuels ont chang�, les r�sultats moyenn�s sont supprim�s si le mode combinatoire est activ�.
    if (manualVelParameters.areCombined())
      setAverageResults(null);
  }

  /**
   * Retourne les r�sultats instantan�s bruts.
   * 
   * @return Les r�sultats, ou <tt>null</tt> s'ils n'ont pas �t� d�finis.
   */
  public PivResultsI[] getInstantRawResults() {
    return instantResults;
  }

  /**
   * Definit les r�sultats instantan�s bruts de calcul, et notifie que le projet a �t� modifi�.
   * 
   * @param _res Les r�sultats. <tt>null</tt> est autoris�.
   */
  public void setInstantRawResults(PivResultsI[] _res) {
    if (CtuluLibArray.isEquals(instantResults, _res))
      return;

    instantResults = _res;

    usedInstantResults = new boolean[instantResults == null ? 0 : instantResults.length];
    Arrays.fill(usedInstantResults, true);

    setInstantRawResultsChanged();
  }

  public void setInstantRawResultsChanged() {
    isModified = true;
    fireProjectStateChanged("instantResults");

    // Les r�sultats instantan�s ont chang�, les r�sultats suivants sont r�initialis�s
    setInstantFilteredResults(null);
  }

  /**
   * Retourne les r�sultats de d�bit.
   * 
   * @return Les r�sultats, ou <tt>null</tt> s'ils n'ont pas �t� d�finis.
   */
  public PivFlowResults[] getFlowResults() {
    return flowResults;
  }

  /**
   * Definit les r�sultats de d�bit, et notifie que le projet a �t� modifi�.
   * 
   * @param _res Les r�sultats. <tt>null</tt> est autoris�.
   */
  public void setFlowResults(PivFlowResults[] _res) {
    flowResults = _res;
    isModified = true;
    fireProjectStateChanged("flowResults");
  }

  public void setFlowResultsChanged() {
    isModified = true;
    fireProjectStateChanged("flowResults");
  }

  public PivStabilizationParameters getStabilizationParameters() {
    return stabParameters;
  }

  public void setStabilizationParameters(PivStabilizationParameters _params) {
    File imgStabDir = new File(rootPath, IMG_STAB_DIR);

    // Si les param�tres n'ont pas chang�, on sort.
    if ((stabParameters == null && _params == null) || (stabParameters != null && stabParameters.equals(_params))) {
      return;
    }

    stabParameters = _params;
    isModified = true;
    fireProjectStateChanged("stabilizationParams");

    if (stabParameters == null || !stabParameters.isActive()) {
      // Si la stabilisation est d�sactiv�e, on supprime le repertoire de stabilisation. Les images n'existeront plus.
      CtuluLibFile.deleteDir(imgStabDir);
      setStabilizedImagesChanged(null);
    }
  }

  public void setManualVelocitiesParameters(PivManualVelocitiesParameters _params) {
    // Si les param�tres n'ont pas chang�, on sort.
    if ((manualVelParameters == null && _params == null)
        || (manualVelParameters != null && manualVelParameters.equals(_params))) {
      return;
    }

    manualVelParameters = _params;
    isModified = true;
    fireProjectStateChanged("manualVelocitiesParams");
  }

  public PivManualVelocitiesParameters getManualVelocitiesParameters() {
    return manualVelParameters;
  }

  /**
   * Definit les resultats instantan�s � utiliser pour le calcul de moyenne.
   * 
   * @param _usedResults Les r�sultats utilis�s
   */
  public void setUsedInstantResults(boolean[] _usedResults) {
    usedInstantResults = _usedResults;
  }

  /**
   * @return La liste des resultats instantan�s utilis�s pour le calcul de moyenne . La valeur a l'index du resultat est True si utilis�.
   */
  public boolean[] getUsedInstantResults() {
    return usedInstantResults;
  }

  /**
   * @param _matrix La matrice de transformation pour passer du repere initial au repere calcul
   */
  public void setTransformationParameters(PivTransformationParameters _params) {
    transfParams = _params;
    isModified = true;
    fireProjectStateChanged("transfParams");
  }

  public PivTransformationParameters getTransformationParameters() {
    return transfParams;
  }

  void fireProjectStateChanged(String _prop) {
    for (PivProjectStateListener listener : listeners) {
      listener.projectStateChanged(this, _prop);
    }
  }

  /**
   * Ajoute un auditeur notifi� quand le projet est modifi�.
   * 
   * @param _l L'auditeur.
   */
  public void addListener(PivProjectStateListener _l) {
    listeners.add(_l);
  }

  /**
   * Supprime un auditeur notifi� quand le projet est modifi�.
   * 
   * @param _l L'auditeur.
   */
  public void removeListener(PivProjectStateListener _l) {
    listeners.remove(_l);
  }
}

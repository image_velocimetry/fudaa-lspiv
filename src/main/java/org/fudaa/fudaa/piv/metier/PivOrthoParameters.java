package org.fudaa.fudaa.piv.metier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.fudaa.ebli.geometrie.GrPoint;

/**
 * Les parametres pour l'orthorectification
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivOrthoParameters {
  /** Coordonn�es r�elle xmin de l'image */
  protected double xmin;
  /** Coordonn�es r�elle ymin de l'image */
  protected double ymin;
  /** Coordonn�es r�elle xmax de l'image */
  protected double xmax;
  /** Coordonn�es r�elle ymax de l'image */
  protected double ymax;
  /** La resolution pour la transformation */
  protected Double resolution = 0.;
  /** Cote du plan d'eau (est null au depart, indiquant qu'aucune valeur n'a �t� donn�e) */
  protected Double waterElevation = 0.;
  /** Mise a l'echelle : Les 2 points pour la transformation ([0] si aucune transformation de repere) */
  protected GrPoint[] scalingImgPoints=new GrPoint[0];
  /** Mise a l'echelle : Les 2 points pour la transformation ([0] si aucune transformation de repere) */
  protected GrPoint[] scalingRealPoints=new GrPoint[0];
  /** Mise a l'echelle : La liste de couples de points pour la resolution. Si vide, la resolution est donn�e directement. */
  protected List<GrPoint[]> scalingResolutionImgPoints = new ArrayList<>();
  /** Mise a l'echelle : La liste de couples de points reels pour la resolution. Si vide, les distances sont donn�es. */
  protected List<GrPoint[]> scalingResolutionRealPoints = new ArrayList<>();
  /** Mise a l'echelle : L'altitude du drone. null : Le mode drone n'est pas activ�. */
  private Double scalingZDrone;
  /** Mise a l'�chelle : El�vation des lignes de points pour les images par drone */
  private List<Double> scalingZSegments  = new ArrayList<>();
  /** Mise a l'echelle : La liste de distances pour la resolution. Si vide, les points reels sont donn�s. */
  protected List<Double> scalingResolutionDistances = new ArrayList<>();
  

  public PivOrthoParameters() {}
  
  public PivOrthoParameters(PivOrthoParameters _params) {
    if (_params == null) {
      return;
    }
    
    xmin = _params.xmin;
    xmax = _params.xmax;
    ymin = _params.ymin;
    ymax = _params.ymax;
    resolution = _params.resolution;
    waterElevation = _params.waterElevation;
    
    scalingZDrone = _params.scalingZDrone;
    
    scalingImgPoints = new GrPoint[_params.scalingImgPoints.length];
    for (int i=0; i<scalingImgPoints.length; i++) {
      scalingImgPoints[i] = new GrPoint(_params.scalingImgPoints[i]);
    }
    
    scalingRealPoints = new GrPoint[_params.scalingRealPoints.length];
    for (int i=0; i<scalingRealPoints.length; i++) {
      scalingRealPoints[i] = new GrPoint(_params.scalingRealPoints[i]);
    }
    
    for (GrPoint[] pts : _params.scalingResolutionImgPoints) {
      GrPoint[] ptsCopy = new GrPoint[pts.length];
      for (int i = 0; i<pts.length; i++) {
        ptsCopy[i] = new GrPoint(pts[i]);
      }
      scalingResolutionImgPoints.add(ptsCopy);
    }
    
    for (GrPoint[] pts : _params.scalingResolutionRealPoints) {
      GrPoint[] ptsCopy = new GrPoint[pts.length];
      for (int i = 0; i<pts.length; i++) {
        ptsCopy[i] = new GrPoint(pts[i]);
      }
      scalingResolutionRealPoints.add(ptsCopy);
    }
    
    for (Double dist : _params.scalingResolutionDistances) {
      scalingResolutionDistances.add(dist);
    }
    
    for (Double dist : _params.scalingResolutionDistances) {
      scalingResolutionDistances.add(dist);
    }
    
    for (Double z : _params.scalingZSegments) {
      scalingZSegments.add(z);
    }
  }

  /**
   * @return the xmin
   */
  public double getXmin() {
    return xmin;
  }

  /**
   * @param xmin the xmin to set
   */
  public void setXmin(double xmin) {
    this.xmin = xmin;
  }

  /**
   * @return the ymin
   */
  public double getYmin() {
    return ymin;
  }

  /**
   * @param ymin the ymin to set
   */
  public void setYmin(double ymin) {
    this.ymin = ymin;
  }

  /**
   * @return the xmax
   */
  public double getXmax() {
    return xmax;
  }

  /**
   * @param xmax the xmax to set
   */
  public void setXmax(double xmax) {
    this.xmax = xmax;
  }

  /**
   * @return the ymax
   */
  public double getYmax() {
    return ymax;
  }

  /**
   * @param ymax the ymax to set
   */
  public void setYmax(double ymax) {
    this.ymax = ymax;
  }

  /**
   * @return the resolution
   */
  public Double getResolution() {
    return resolution;
  }

  /**
   * @param resolution the resolution to set
   */
  public void setResolution(Double resolution) {
    this.resolution = resolution;
  }

  /**
   * @return the waterElevation
   */
  public Double getWaterElevation() {
    return waterElevation;
  }

  /**
   * @param waterElevation the waterElevation to set
   */
  public void setWaterElevation(Double waterElevation) {
    this.waterElevation = waterElevation;
  }
  
  public void setScalingTranformationImgPoints(GrPoint[] _pts) {
    this.scalingImgPoints = _pts;
  }
  
  public GrPoint[] getScalingTranformationImgPoints() {
    return scalingImgPoints;
  }
  
  public void setScalingTranformationRealPoints(GrPoint[] _pts) {
    this.scalingRealPoints = _pts;
  }
  
  public GrPoint[] getScalingTranformationRealPoints() {
    return scalingRealPoints;
  }
  
  public void setScalingResolutionImgPoints(List<GrPoint[]> _couples) {
    this.scalingResolutionImgPoints = _couples;
  }
  
  public List<GrPoint[]> getScalingResolutionImgPoints() {
    return scalingResolutionImgPoints;
  }
  
  public void setScalingResolutionRealPoints(List<GrPoint[]> _couples) {
    this.scalingResolutionRealPoints = _couples;
  }
  
  public List<GrPoint[]> getScalingResolutionRealPoints() {
    return scalingResolutionRealPoints;
  }

  public void setScalingResolutionDistances(List<Double> _distances) {
    this.scalingResolutionDistances = _distances;
  }
  
  public List<Double> getScalingResolutionDistances() {
    return scalingResolutionDistances;
  }

  public Double getScalingZDrone() {
    return scalingZDrone;
  }

  public void setScalingZDrone(Double scalingZDrone) {
    this.scalingZDrone = scalingZDrone;
  }

  public List<Double> getScalingZSegments() {
    return scalingZSegments;
  }

  public void setScalingZSegments(List<Double> scalingZSegments) {
    this.scalingZSegments = scalingZSegments;
  }
  
  /**
   * @return True : Le param�trage scaling est pour des images prises par drone.
   */
  public boolean isScalingDroneMode() {
    return this.scalingZDrone != null;
  }

  public boolean equals(Object _o) {
    if (!(_o instanceof PivOrthoParameters)) return false;
    PivOrthoParameters o=(PivOrthoParameters)_o;
    
    return xmin==o.xmin && xmax==o.xmax && ymin==o.ymin && ymax==o.ymax &&
           resolution==o.resolution && 
           ((waterElevation==null && o.waterElevation == null) || (waterElevation!=null && waterElevation.equals(o.waterElevation)) || (o.waterElevation!=null && o.waterElevation.equals(waterElevation))) &&
           ((scalingZDrone==null && o.scalingZDrone == null) || (scalingZDrone!=null && scalingZDrone.equals(o.scalingZDrone)) || (o.scalingZDrone!=null && o.scalingZDrone.equals(scalingZDrone))) &&
           Arrays.equals(scalingImgPoints, o.scalingImgPoints) &&
           Arrays.equals(scalingRealPoints, o.scalingRealPoints) &&
           scalingResolutionImgPoints.equals(o.scalingResolutionImgPoints) &&
           scalingResolutionRealPoints.equals(o.scalingResolutionRealPoints) &&
           scalingZSegments.equals(o.scalingZSegments) &&
           scalingResolutionDistances.equals(o.scalingResolutionDistances);
  }
}

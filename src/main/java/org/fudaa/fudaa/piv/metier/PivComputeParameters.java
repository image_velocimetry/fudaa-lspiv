package org.fudaa.fudaa.piv.metier;

import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.piv.utils.PivUtils;

/**
 * Les parametres pour le calcul des vitesses par PIV.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivComputeParameters {
  
  // Si une valeur n'a pas encore �t� renseign�e, est elle null.
  
  /** La taille en pixels de l'aire d'interrogation IA */
  protected Integer iaSize;
  /** Le min en pixels suivant i de l'aire de recherche */
  protected Integer sim;
  /** Le max en pixels suivant i de l'aire de recherche */
  protected Integer sip;
  /** Le min en pixels suivant j de l'aire de recherche */
  protected Integer sjm;
  /** Le max en pixels suivant j de l'aire de recherche */
  protected Integer sjp;
  /** L'intervale de temps entre 2 images en secondes */
  protected Double timeInterval;
  /** La frequence de sous echantillonnage des images transform�es */
  protected Integer underSamplingPeriod = 1;
  /** La position suivant j et i du centre de l'aire. */
  protected GrPoint ptCenter;
  /** La correlation minimale */
  protected double minCorrelation=0.4;
  /** La correlation maximale */
  protected double maxCorrelation=0.98;
  /** Valeur min des normes de vitesse */
  protected double smin=0.; // #5711 : Le filtre par defaut est fix� � 0 pour les vitesses min. 
  /** Valeur max des normes de vitesse */
  protected double smax=10000.; // #5583 : On force a 10000 pour avoir un filtre par defaut.
  /** Valeur min composante Vx */
  protected double vxmin=PivUtils.FORTRAN_DOUBLE_MIN;
  /** Valeur max composante Vx */
  protected double vxmax=PivUtils.FORTRAN_DOUBLE_MAX;
  /** Valeur min composante Vy */
  protected double vymin=PivUtils.FORTRAN_DOUBLE_MIN;
  /** Valeur max composante Vy */
  protected double vymax=PivUtils.FORTRAN_DOUBLE_MAX;

  public PivComputeParameters() {
  }
  
  /**
   * Constructeur copie.
   * @param _o
   */
  public PivComputeParameters(PivComputeParameters _o) {
    if (_o == null) {
      return;
    }
    
    iaSize = _o.iaSize;
    sim = _o.sim;
    sip = _o.sip;
    sjm = _o.sjm;
    sjp = _o.sjp;
    timeInterval = _o.timeInterval;
    underSamplingPeriod = _o.underSamplingPeriod;
    ptCenter = _o.ptCenter;
    minCorrelation = _o.minCorrelation;
    maxCorrelation = _o.maxCorrelation;
    smin = _o.smin;
    smax = _o.smax;
    vxmin = _o.vxmin;
    vxmax = _o.vxmax;
    vymin = _o.vymin;
    vymax = _o.vymax;
  }

  /**
   * @return the iaSize
   */
  public Integer getIASize() {
    return iaSize;
  }

  /**
   * @param iaSize the iaSize to set
   */
  public void setIASize(Integer iaSize) {
    this.iaSize = iaSize;
  }

  /**
   * @return the sim
   */
  public Integer getSim() {
    return sim;
  }

  /**
   * @param sim the sim to set
   */
  public void setSim(Integer sim) {
    this.sim = sim;
  }

  /**
   * @return the sip
   */
  public Integer getSip() {
    return sip;
  }

  /**
   * @param sip the sip to set
   */
  public void setSip(Integer sip) {
    this.sip = sip;
  }

  /**
   * @return the sjm
   */
  public Integer getSjm() {
    return sjm;
  }

  /**
   * @param sjm the sjm to set
   */
  public void setSjm(Integer sjm) {
    this.sjm = sjm;
  }

  /**
   * @return the sjp
   */
  public Integer getSjp() {
    return sjp;
  }

  /**
   * @param sjp the sjp to set
   */
  public void setSjp(Integer sjp) {
    this.sjp = sjp;
  }

  /**
   * @return the timeInterval
   */
  public Double getTimeInterval() {
    return timeInterval;
  }

  /**
   * @param timeInterval the timeInterval to set
   */
  public void setTimeInterval(Double timeInterval) {
    this.timeInterval = timeInterval;
  }

  /**
   * @return the minCorrelation
   */
  public double getMinCorrelation() {
    return minCorrelation;
  }

  /**
   * @param minCorrelation the minCorrelation to set
   */
  public void setMinCorrelation(double minCorrelation) {
    this.minCorrelation = minCorrelation;
  }

  /**
   * @return the maxCorrelation
   */
  public double getMaxCorrelation() {
    return maxCorrelation;
  }

  /**
   * @param maxCorrelation the maxCorrelation to set
   */
  public void setMaxCorrelation(double maxCorrelation) {
    this.maxCorrelation = maxCorrelation;
  }

  /**
   * @return the smin
   */
  public double getSmin() {
    return smin;
  }

  /**
   * @param smin the smin to set
   */
  public void setSmin(double smin) {
    this.smin = smin;
  }

  /**
   * @return the smax
   */
  public double getSmax() {
    return smax;
  }

  /**
   * @param smax the smax to set
   */
  public void setSmax(double smax) {
    this.smax = smax;
  }

  /**
   * @return the vmin
   */
  public double getVxmin() {
    return vxmin;
  }

  /**
   * @param vmin the vmin to set
   */
  public void setVxmin(double vmin) {
    this.vxmin = vmin;
  }

  /**
   * @return the vmax
   */
  public double getVxmax() {
    return vxmax;
  }

  /**
   * @param vmax the vmax to set
   */
  public void setVxmax(double vmax) {
    this.vxmax = vmax;
  }

  /**
   * @return the vmin
   */
  public double getVymin() {
    return vymin;
  }

  /**
   * @param vmin the vmin to set
   */
  public void setVymin(double vmin) {
    this.vymin = vmin;
  }

  /**
   * @return the vmax
   */
  public double getVymax() {
    return vymax;
  }

  /**
   * @param vmax the vmax to set
   */
  public void setVymax(double vmax) {
    this.vymax = vmax;
  }
  
  /**
   * Definit la position de IA suivant J et I
   */
  public void setIACenterPosition(GrPoint _pt) {
    ptCenter=_pt;
  }
  
  /**
   * @return La position du centre de IA/SA. Peut �tre nulle, dans ce cas,
   * l'IA est mise au centre de l'image.
   */
  public GrPoint getIACenterPosition() {
    return ptCenter;
  }
  
  public Integer getUnderSamplingPeriod() {
    return underSamplingPeriod;
  }

  /**
   * D�finit la p�riode de sous echantillonnage.
   * @param _samplingPeriod La p�riode d'�chantillonnage
   */
  public void setUnderSamplingPeriod(Integer _samplingPeriod) {
    this.underSamplingPeriod = _samplingPeriod;
  }

  /**
   * Modifie la p�riode de sous echantillonnage. On conserve les coordonn�es m�triques.
   * @param _samplingPeriod La p�riode d'�chantillonnage
   */
  public void changeUnderSamplingPeriod(Integer _samplingPeriod) {
    if (underSamplingPeriod != _samplingPeriod && underSamplingPeriod != null && _samplingPeriod != null) {
      double ratio=(float)_samplingPeriod/(float)underSamplingPeriod;

      if (sim != null) {
        sim=(int) (sim * ratio + 0.5);
      }
      if (sip != null) {
        sip=(int) (sip * ratio + 0.5);
      }
      if (sjm != null) {
        sjm=(int) (sjm * ratio + 0.5);
      }
      if (sjp != null) {
        sjp=(int) (sjp * ratio + 0.5);
      }
    }
    
    this.underSamplingPeriod = _samplingPeriod;
  }
  
  /**
   * @return True si les param�tres sont tous remplis pour le calcul par PIV.
   */
  public boolean isFilledForComputing() {
    return iaSize != null && sim != null && sip != null && sjm != null && sjp != null;
  }
  
  /**
   * Les parametres pour le calcul sont tous mis � null.
   */
  public void clearComputingParameters() {
    iaSize = null;
    sim = null;
    sip = null;
    sjm = null;
    sjp = null;
  }
  
  /**
   * Les param�tres de filtre sont tous reinitialis�s.
   */
  public void clearFilters() {
    smin = PivUtils.FORTRAN_DOUBLE_MIN;
    smax = PivUtils.FORTRAN_DOUBLE_MAX;
    vxmin = PivUtils.FORTRAN_DOUBLE_MIN;
    vxmax = PivUtils.FORTRAN_DOUBLE_MAX;
    vymin = PivUtils.FORTRAN_DOUBLE_MIN; 
    vymax = PivUtils.FORTRAN_DOUBLE_MAX;
    minCorrelation = PivUtils.FORTRAN_DOUBLE_MIN;
    maxCorrelation = PivUtils.FORTRAN_DOUBLE_MAX;
  }
  
  /**
   * @return True si les vitesses sont filtr�es
   */
  public boolean isVelocityFiltered() {
    return smin  != PivUtils.FORTRAN_DOUBLE_MIN || 
           smax  != PivUtils.FORTRAN_DOUBLE_MAX || 
           vxmin != PivUtils.FORTRAN_DOUBLE_MIN || 
           vxmax != PivUtils.FORTRAN_DOUBLE_MAX ||
           vymin != PivUtils.FORTRAN_DOUBLE_MIN || 
           vymax != PivUtils.FORTRAN_DOUBLE_MAX;
  }
  
  /**
   * @return True si la correlation est filtr�e
   */
  public boolean isCorrelationFiltered() { 
    return minCorrelation  != PivUtils.FORTRAN_DOUBLE_MIN || 
           maxCorrelation  != PivUtils.FORTRAN_DOUBLE_MAX;
  }
  
  /**
   * Modifie l'echelle des parametres. 
   * Sert en particulier en cas de changement de resolution, pour conserver les coordonn�es m�triques.
   * @param _scale
   */
  public void changeScale(double _scale) {
    // #5443 : La taille doit rester une valeur paire.
    if (iaSize != null) {
      iaSize = (int) (iaSize * _scale);
      if (iaSize % 2 != 0)
        iaSize++;
    }
    if (ptCenter != null) {
      ptCenter.x_*=_scale;
      ptCenter.y_*=_scale;
    }
    if (sim != null) {
      sim = (int)(sim*_scale+0.5);
    }
    if (sip != null) {
      sip = (int)(sip*_scale+0.5);
    }
    if (sjm != null) {
      sjm = (int)(sjm*_scale+0.5);
    }
    if (sjp != null) {
      sjp = (int)(sjp*_scale+0.5);
    }
  }
  
  /**
   * Modifie l'intervalle de temps. Sert a conserver les coordonn�es m�triques.
   * @param _newTimeInterval Le nouvel intervalle de temps.
   */
  public void changeTimeInterval(Double _newTimeInterval) {
    if (timeInterval != null && _newTimeInterval != null) {
      double ratio=_newTimeInterval/timeInterval;

      if (sim != null) {
        sim=(int) (sim * ratio + 0.5);
      }
      if (sip != null) {
        sip=(int) (sip * ratio + 0.5);
      }
      if (sjm != null) {
        sjm=(int) (sjm * ratio + 0.5);
      }
      if (sjp != null) {
        sjp=(int) (sjp * ratio + 0.5);
      }
    }
    
    timeInterval = _newTimeInterval;
  }

  @Override
  public boolean equals(Object _o) {
    if (!(_o instanceof PivComputeParameters)) return false;
    PivComputeParameters o=(PivComputeParameters)_o;
    
    return solverParametersEquals(o) && 
        !(ptCenter == null ^ o.ptCenter == null) && (ptCenter == null || ptCenter.equals(o.ptCenter)) &&
        !(underSamplingPeriod == null ^ o.underSamplingPeriod == null) && (underSamplingPeriod == null || underSamplingPeriod == o.underSamplingPeriod);
  }
  
  /**
   * Test que les parametres pour le solveur uniquement sont egaux.
   * @param o Les parametres a comparer
   * @return True : Si les parametres sont identiques
   */
  public boolean solverParametersEquals(PivComputeParameters o) {
    
    return !(iaSize == null ^ o.iaSize == null) && (iaSize == null || iaSize.equals(o.iaSize)) &&
           maxCorrelation == o.maxCorrelation && minCorrelation == o.minCorrelation &&
           !(sim == null ^ o.sim == null) && (sim == null || sim.equals(o.sim)) &&
           !(sip == null ^ o.sip == null) && (sip == null || sip.equals(o.sip)) &&
           !(sjm == null ^ o.sjm == null) && (sjm == null || sjm.equals(o.sjm)) &&
           !(sjp == null ^ o.sjp == null) && (sjp == null || sjp.equals(o.sjp)) &&
           smax == o.smax && smin == o.smin &&
           !(timeInterval == null ^ o.timeInterval == null) && (timeInterval == null || timeInterval.equals(o.timeInterval)) &&
           vxmax == o.vxmax && vxmin == o.vxmin &&
           vymax == o.vymax && vymin == o.vymin;
  }
}

package org.fudaa.fudaa.piv.metier;

import org.fudaa.ebli.geometrie.GrMorphisme;

/**
 * Les param�tres de transformation du rep�re intial vers le rep�re de calcul. On stocke ces param�tres,
 * car ils sont difficiles a retrouver depuis les matrices de transformation.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class PivTransformationParameters {
  /** La translation suivant x */
  private double translationX;
  /** La translation suivant y */
  private double translationY;
  /** La translation suivant z */
  private double translationZ;
  /** La rotation suivant z, en degr�s */
  private double rotationZ;
  /** La coordonn�e X du point de rotation Z */
  private double xcenter;
  /** La coordonn�e Y du point de rotation Z */
  private double ycenter;
  /** Morphisme de initial -> calcul */
  private GrMorphisme toComputing;
  /** Morphisme de calcul -> initial */
  private GrMorphisme toOriginal;
  /** Definit si on est dans le system de coordonn�es initial ou de calcul. Par defaut : calcul */
  private boolean isInInitialSystemCoords;
  /** Morphisme identit� */
  private GrMorphisme identite=GrMorphisme.identite();
  
  public PivTransformationParameters() {
    this(0, 0, 0, 0, 0, 0);
  }
  
  public PivTransformationParameters(double _tx, double _ty, double _tz, double _rz, double _xpt, double _ypt) {
    translationX=_tx;
    translationY=_ty;
    translationZ=_tz;
    rotationZ=_rz;
    xcenter = _xpt;
    ycenter = _ypt;
  }
  
  /**
   * @return the translationX
   */
  public double getTranslationX() {
    return translationX;
  }

  /**
   * @param _translationX the translationX to set
   */
  public void setTranslationX(double _translationX) {
    if (translationX != _translationX) {
      this.translationX=_translationX;
      toComputing=null;
      toOriginal=null;
    }
  }

  /**
   * @return the translationY
   */
  public double getTranslationY() {
    return translationY;
  }

  /**
   * @param _translationY the translationY to set
   */
  public void setTranslationY(double _translationY) {
    if (translationY != _translationY) {
      this.translationY=_translationY;
      toComputing=null;
      toOriginal=null;
    }
  }

  /**
   * @return the translationZ
   */
  public double getTranslationZ() {
    return translationZ;
  }

  /**
   * @param _translationZ the translationZ to set
   */
  public void setTranslationZ(double _translationZ) {
    if (translationZ != _translationZ) {
      this.translationZ=_translationZ;
      toComputing=null;
      toOriginal=null;
    }
  }

  /**
   * @return the rotationZ
   */
  public double getRotationZ() {
    return rotationZ;
  }

  /**
   * @param _rotationZ the rotationZ to set
   */
  public void setRotationZ(double _rotationZ) {
    if (rotationZ != _rotationZ) {
      this.rotationZ=_rotationZ;
      toComputing=null;
      toOriginal=null;
    }
  }

  /**
   * @return the X rotation
   */
  public double getXCenter() {
    return xcenter;
  }

  /**
   * @param _xRotation the X rotation to set
   */
  public void setXCenter(double _xRotation) {
    if (xcenter != _xRotation) {
      this.xcenter=_xRotation;
      toComputing=null;
      toOriginal=null;
    }
  }

  /**
   * @return the Y rotation
   */
  public double getYCenter() {
    return ycenter;
  }

  /**
   * @param _yRotation the Y rotation to set
   */
  public void setYCenter(double _yRotation) {
    if (ycenter != _yRotation) {
      this.ycenter=_yRotation;
      toComputing=null;
      toOriginal=null;
    }
  }

  public GrMorphisme getToComputing() {
    if (toComputing==null)
      toComputing=GrMorphisme.translation(translationX-xcenter,translationY-ycenter,translationZ).composition(GrMorphisme.rotationZ(Math.toRadians(rotationZ))).composition(GrMorphisme.translation(xcenter,ycenter,0));
    return toComputing;
  }
  
  public GrMorphisme getToInitial() {
    if (toOriginal==null)
      toOriginal=GrMorphisme.translation(-xcenter,-ycenter,0).composition(GrMorphisme.rotationZ(-Math.toRadians(rotationZ))).composition(GrMorphisme.translation(xcenter-translationX,ycenter-translationY,-translationZ));
    return toOriginal;
  }
  
  /**
   * @return Morphisme pour passer des coordonn�es data aux coordonn�es r�elles courantes.
   */
  public GrMorphisme getToReal() {
    if (isCurrentSystemInitial())
      return getToInitial();
    else
      return identite;
  }
  
  /**
   * @return Morphisme pour passer des coordonn�es r�elles courantes aux coordonn�es data.
   */
  public GrMorphisme getToData() {
    if (isCurrentSystemInitial())
      return getToComputing();
    else
      return identite;
  }
  
  public void setCurrentSystemInitial(boolean _b) {
    isInInitialSystemCoords=_b;
  }
  
  public boolean isCurrentSystemInitial() {
    return isInInitialSystemCoords;
  }
  
}

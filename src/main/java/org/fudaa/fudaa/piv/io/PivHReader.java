/**
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.io.EOFException;
import java.io.IOException;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.dodico.fortran.FileCharSimpleReaderAbstract;
import org.fudaa.fudaa.piv.PivResource;

/**
 * Un lecteur pour le fichier de la cote d'eau.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivHReader extends FileCharSimpleReaderAbstract<Double> implements CtuluActivity {

  /** Drapeau d'interruption */
  boolean bstop_;

  /**
   * Le constructeur.
   */
  public PivHReader() {
  }

  /**
   * Lit la cote d'eau et la retourne.
   * @return La cote d'eau.
   */
  protected Double internalRead() {
    return readCote();
  }

  /**
   * Utilise FortranReader ( donc un buffer).
   * 
   * @return les infos non bloquantes.
   */
  private synchronized Double readCote() {
    Double cote=new Double(0);

    if (super.in_ == null) {
      analyze_.addError(PivResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    try {
      if (progress_ != null) {
        progress_.setProgression(0);
      }

      in_.setJumpBlankLine(true);

      // Cote d'eau
      in_.readFields();
      cote=in_.doubleField(0);

      if (progress_ != null) {
        progress_.setProgression(100);
      }
    }
    catch (final EOFException e) {
      analyze_.manageException(e);
    }
    catch (final IOException e) {
      analyze_.manageException(e);
    }
    catch (final NumberFormatException e) {
      analyze_.manageException(e);
    }
    return cote;
  }

  /**
   * Interruption asynchrone de l'activit� de lecture.
   */
  public void stop() {
    bstop_ = true;
  }
}

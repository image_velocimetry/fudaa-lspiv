/*
 * @creation 15 f�vr. 07
 * @modification $Date: 2007-05-04 13:47:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.awt.Dimension;
import java.io.PrintWriter;

import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.VecteurGrPoint;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;
import org.fudaa.fudaa.piv.metier.PivStabilizationParameters;

/**
 * La classe pour ecrire les zones de stabilisation sur fichier. Cette
 * classe ne g�re pas le format �tendu.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivStabAreasWriter extends FileCharSimpleWriterAbstract<PivStabilizationParameters> {

  /**
   * Ecrit les zones.
   * @param _stabParams Les parametres de stabilisation.
   */
  protected void internalWrite(final PivStabilizationParameters _stabParams) {

    final PrintWriter writer = new PrintWriter(out_);

    try {
      // Entete
//      writer.println("GRP");
      // Nombre de points
      writer.println("# V1.0 Stabilization areas");

      // Le type de zones
      writer.println("Nature des zones (1: ecoulement, 2: fixes)");
      writer.println(_stabParams.isFlowAreas() ? 1 : 2);
      // Le nombre de zones
      writer.println("Nombre de zones");
      writer.println(_stabParams.getOutlines().size());

      // Boucle sur les polygones
      writer.println("Zones");
      for (GrPolygone pg : _stabParams.getOutlines()) {
        VecteurGrPoint pts = pg.sommets_;

        writer.println(pts.nombre());

        for (int i = 0; i < pts.nombre(); i++) {
          int x = (int) pts.renvoieX(i);
          int y = (int) pts.renvoieY(i);

          writer.println(x + " " + y);
        }
      }

    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
  }
}

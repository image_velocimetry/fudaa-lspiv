/**
 * @creation 2002-11-21
 * @modification $Date: 2007/05/04 13:47:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.dodico.fortran.FileCharSimpleReaderAbstract;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.metier.PivTransect;

import gnu.trove.TDoubleArrayList;

/**
 * Un lecteur pour les fichiers de bathy.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivBathyReader extends FileCharSimpleReaderAbstract<PivTransect> implements CtuluActivity {

  /** Drapeau d'interruption */
  boolean bstop_;
  /** Nb d'octets du fichier a lire pour stat sur pourcentage effectu� */
  int nbOctets;

  /**
   * Constructeur.
   */
  public PivBathyReader() {
  }

  /**
   * Lit les points et retourne le transect.
   * @return Le transect
   */
  @Override
  protected PivTransect internalRead() {
    return readParams();
  }

  @Override
  protected void processFile(final File _f) {
    nbOctets = (int) _f.length();
  }

  /**
   * Utilise FortranReader ( donc un buffer).
   * 
   * @return les infos non bloquantes.
   */
  private synchronized PivTransect readParams() {
    PivTransect ret=null;
    GrPolyligne pl=new GrPolyligne();
    TDoubleArrayList velCoefs = new TDoubleArrayList();

    if (super.in_ == null) {
      analyze_.addErrorFromFile(PivResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    try {
      int lu=0;
      boolean afficheAvance = false;
      if ((progress_ != null) && (nbOctets > 0)) {
        afficheAvance = true;
        progress_.setProgression(0);
      }
      int pourcentageEnCours = 0;

      in_.setJumpBlankLine(true);
      
      // Le nombre de colonnes, qui peut etre 3 ou 4 (si les coef vitesse sont donn�s).
      int nbCol = -1;

      // Boucle jusque fin de fichier. Exception EOF si fin.
      while (true) {
        in_.readFields();
        if (nbCol == -1) {
          nbCol = in_.getNumberOfFields();
        }
        
        pl.sommets_.ajoute(in_.doubleField(0),in_.doubleField(1),in_.doubleField(2));
        lu+=26; // Si le formattage en fortran

        // Les coefficients de vitesse
        if (nbCol >= 4) {
          velCoefs.add(in_.doubleField(3));
        }
        else {
          velCoefs.add(-1);
        }
        
        if ((afficheAvance) && ((lu * 100 / nbOctets) >= (pourcentageEnCours + 20))) {
          pourcentageEnCours += 20;
          progress_.setProgression(pourcentageEnCours);
        }
      }
    }
    // Sortie normale
    catch (final EOFException e) {
      ret=new PivTransect();
      ret.setStraight(pl);
      ret.setCoefs(velCoefs.toNativeArray());
      return ret;
    }
    catch (final IOException | NumberFormatException e) {
      analyze_.addErrorFromFile(PivResource.getS("Une erreur de lecture s'est produite"), in_.getLineNumber());
      return ret;
    }
    finally {
      if (progress_ != null) {
        progress_.setProgression(100);
      }
    }
  }

  /**
   * Interruption asynchrone de l'activit� de lecture.
   */
  @Override
  public void stop() {
    bstop_ = true;
  }
}

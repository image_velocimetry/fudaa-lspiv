package org.fudaa.fudaa.piv.io;

import java.awt.Dimension;
import java.io.PrintWriter;

import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;
import org.fudaa.fudaa.piv.metier.PivComputeParameters;
import org.fudaa.fudaa.piv.metier.PivTransectParams;


/**
 * Une classe pour ecrire sur fichier les parametres pour le calcul des vitesses.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivParamWriter extends FileCharSimpleWriterAbstract<Object[]> {
  /** Version du fichier */
  public static final String VERSION="3.0";

  /**
   * Ecrit les param�tres d'orthorectification.
   * 
   * param _o Un tableau contenant 3 objets PivComputeParameters, PivFlowParameters et Dimension.
   * 
   * Les objets peuvent �tre null (s'ils n'ont pas �t� d�finis). Dans ce cas, -1 sera
   * inscrit dans les champs correspondant pour indiquer que cette partie des infos
   * n'a pas �t� renseign�e.
   */
  protected void internalWrite(final Object[] _o) {
    if (_o.length!=3 ||
        (_o[0]!=null && !(_o[0] instanceof PivComputeParameters)) ||
        (_o[1]!=null && !(_o[1] instanceof PivTransectParams)) ||
        !(_o[2] instanceof Dimension)) {
      donneesInvalides(_o);
      return;
    }

    PivComputeParameters params = (PivComputeParameters)_o[0];
    PivTransectParams paramsFlow = (PivTransectParams)_o[1];
    Dimension imgSize = (Dimension)_o[2];
    
    boolean pivDefined=params!=null;
    boolean flowDefined=paramsFlow!=null;
    boolean centerDefined=pivDefined && params.getIACenterPosition()!=null;

    final PrintWriter writer = new PrintWriter(out_);

    try {
      // Entete
      writer.println("# FileVersion "+VERSION+ " / IA Size");
      
      // IA size
      writer.println(pivDefined && params.getIASize() != null ? params.getIASize():-1);

      // SA size
      writer.println("SA size: Sim - Sip - Sjm - Sjp");
      writer.println(pivDefined && params.getSim() != null ? params.getSim():-1);
      writer.println(pivDefined && params.getSip() != null ? params.getSip():-1);
      writer.println(pivDefined && params.getSjm() != null ? params.getSjm():-1);
      writer.println(pivDefined && params.getSjp() != null ? params.getSjp():-1);

      // Time interval & sampling period.
      writer.println("Time interval / Under sampling period");
      writer.print(pivDefined && params.getTimeInterval() != null ? params.getTimeInterval():-1);
      writer.print(" ");
      writer.println(pivDefined && params.getUnderSamplingPeriod() != null ? params.getUnderSamplingPeriod():-1);

      // Minimum correlation
      writer.println("Minimum correlation");
      writer.println(pivDefined ? params.getMinCorrelation():-1);

      // Maximum correlation
      writer.println("Maximum correlation");
      writer.println(pivDefined ? params.getMaxCorrelation():-1);

      // Taille des images transform�es
      writer.println("Taille des images transformees : nj - ni");
      writer.println(imgSize.width);
      writer.println(imgSize.height);

      // Seuils limites de norme de vitesse : smin - smax
      writer.println("Seuils limites de norme de vitesse : smin - smax");
      writer.println(pivDefined ? params.getSmin():-1);
      writer.println(pivDefined ? params.getSmax():-1);

      writer.println("Seuils limites de la composante Vx : min - max");
      writer.println(pivDefined ? params.getVxmin():-1);
      writer.println(pivDefined ? params.getVxmax():-1);

      writer.println("Seuils limites de la composante Vy : min - max");
      writer.println(pivDefined ? params.getVymin():-1);
      writer.println(pivDefined ? params.getVymax():-1);

      // Coefficient de surface
      writer.println("Coefficient de surface");
      writer.println(flowDefined ? paramsFlow.getSurfaceCoef():-1);

      // Rayon de recherche des vitesses autour du point bathy : dmax
      writer.println("Rayon de recherche des vitesses autour du point bathy : rxmax - rymax");
      writer.println((flowDefined ? (paramsFlow.getRadiusX() + " " + paramsFlow.getRadiusY()) : "-1 -1"));

      // Distance au-dela de laquelle on extrapole les vitesses : trunc
      writer.println("Pas d espace d interpolation de la bathymetrie : Dxp / Nombre max de points");
      writer.println(flowDefined ? (paramsFlow.getInterpolationStep() + " " + paramsFlow.getMaxPoints()) : "-1 -1");
      
      // Coordonn�es du centre
      // BM 26/08/2020 - Ces coordonn�es ne sont pas lues par les programmes exe, elles pourraient �tre mise
      // en dehors des fichiers de transfert vers les exe.
      writer.println("IA j position");
      writer.println(centerDefined ? (int)params.getIACenterPosition().x_:-1);
      writer.println("IA i position");
      writer.println(centerDefined ? (int)params.getIACenterPosition().y_:-1);
    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
  }
}

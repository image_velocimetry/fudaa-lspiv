package org.fudaa.fudaa.piv.io;

import java.io.PrintWriter;

import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;


/**
 * Une classe pour ecrire les coefficients d'orthorectification sur fichier.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivHWriter.java 9015 2015-02-12 16:45:55Z bmarchan $
 */
public class PivCoeffWriter extends FileCharSimpleWriterAbstract<double[]> {

  /**
   * Ecrit les paramètres d'orthorectification.
   * param _o Les coefs.
   */
  protected void internalWrite(final double[] _coefs) {

    final PrintWriter writer = new PrintWriter(out_);

    try {
      // Nombre de coeffs
      writer.println(_coefs.length);
      
      // Les coefs
      for (int i=0; i<_coefs.length; i++) {
        writer.println(_coefs[i]);
      }
    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }

    writer.close();
  }
}

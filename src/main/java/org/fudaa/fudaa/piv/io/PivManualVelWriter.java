/*
 * @creation 15 f�vr. 07
 * @modification $Date: 2007-05-04 13:47:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv.io;

import java.awt.Dimension;
import java.io.PrintWriter;

import org.fudaa.dodico.fortran.FileCharSimpleWriterAbstract;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.piv.metier.PivComputeParameters;
import org.fudaa.fudaa.piv.metier.PivManualVelocitiesParameters;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;

/**
 * La classe pour ecrire les couples de velocit�s manuelles.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivManualVelWriter extends FileCharSimpleWriterAbstract<Object[]> {

  /**
   * Ecrit les couples.
   * Ecrit les points d'orthorectification.
   * param _o[0] PivManualVelocitiesParameters, _o[1] Le temps entre 2 images
   */
  protected void internalWrite(final Object[] _o) {
    
    if (_o.length != 4 ||
        !(_o[0] instanceof PivManualVelocitiesParameters) ||
        !(_o[1] instanceof PivOrthoParameters) ||
        !(_o[2] instanceof PivComputeParameters) ||
        !(_o[3] instanceof Dimension)) {
      donneesInvalides(_o);
      return;
    }
    
    PivManualVelocitiesParameters velParams = (PivManualVelocitiesParameters)_o[0];
    PivOrthoParameters orthoParams = (PivOrthoParameters)_o[1];
    PivComputeParameters pivParams = (PivComputeParameters)_o[2];
    Dimension imgSize = (Dimension)_o[3];

    Double dt = pivParams.getTimeInterval()*pivParams.getUnderSamplingPeriod();
    
    final PrintWriter writer = new PrintWriter(out_);

    try {
      // Entete
      writer.println("# V1.0 Manual velocities parameters");
      writer.println(velParams.getDisplacementImgPoints().size());
      writer.println("X1 Y1 X2 Y2 Dt");

      double xmin = orthoParams.getXmin();
      double xmax = orthoParams.getXmax();
      double ymin = orthoParams.getYmin();
      double ymax = orthoParams.getYmax();
      for (int i = 0; i < velParams.getDisplacementImgPoints().size(); i++) {
        GrPoint[] pts = velParams.getDisplacementImgPoints().get(i);
        Integer[] imgInds = velParams.getImagesIndexes().get(i);
        
        double x1 = (double) xmin + (xmax-xmin) * pts[0].x_ / imgSize.width;
        double y1 = (double) ymin + (ymax-ymin) * pts[0].y_ / imgSize.height;
        double x2 = (double) xmin + (xmax-xmin) * pts[1].x_ / imgSize.width;
        double y2 = (double) ymin + (ymax-ymin) * pts[1].y_ / imgSize.height;
        double dt2 = dt * (imgInds[1] - imgInds[0]);

        writer.println(x1 + " " + y1 + " " + x2 + " " + y2 + " " + dt2);
      }

    }
    catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
  }
}

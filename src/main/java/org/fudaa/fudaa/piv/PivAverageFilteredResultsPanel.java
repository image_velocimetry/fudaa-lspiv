/*
 * @creation     18 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;
import org.fudaa.fudaa.piv.io.PivExeLauncher;

import com.memoire.bu.BuVerticalLayout;

/**
 * Un panneau pour moyenner les r�sultats filtr�s instantan�s.
 * Les resultats selectionn�s sont sauv�s dans le projet.
 *  
 * @author marchand@deltacad.fr
 * @version $Id: PivFilterInstantResultsPanel.java 9455 2016-11-18 11:05:02Z bmarchan $
 */
public class PivAverageFilteredResultsPanel extends CtuluDialogPanel {

  PivImplementation impl;
  /** Le mod�le pour la liste des resultats */
  private CtuluListEditorModel mdResults_;
  /** La liste des r�sultats dans l'ordre */
  private CtuluListEditorPanel pnListResults_;
  CtuluDialog diProgress_;

  /**
   * Constructeur.
   * @param _ui Le parent pour la boite de dialogue.
   */
  public PivAverageFilteredResultsPanel(PivImplementation _impl) {
    impl = _impl;
    
    // Moyenne
    
    JPanel pnAverage=new JPanel();
    pnAverage.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
    pnAverage.setLayout(new BuVerticalLayout(3, true, true));
    
    // Label r�sultats
    pnAverage.add(new JLabel(PivResource.getS("Liste des r�sultats instantan�s"),JLabel.LEFT));
    
    // Liste des r�sultats
    mdResults_=new CtuluListEditorModel(false) {
      @Override
      public boolean isCellEditable(int _rowIndex, int _columnIndex) {
        return false;
      }
      @Override
      public Object createNewObject() {
        return null;
      }
    };
    
    pnListResults_ = new CtuluListEditorPanel(mdResults_, false, false, false, false, false);
    pnAverage.setPreferredSize(new Dimension(300,200));
    pnAverage.add(pnListResults_);

    setLayout(new BorderLayout());
    add(pnAverage,BorderLayout.CENTER);
  }
  
  @Override
  public void setValue(Object _usedResults) {
    if (!(_usedResults instanceof boolean[]))
      throw new IllegalArgumentException("bad type parameter");
    setSelectedResults((boolean[])_usedResults);
  }
  
  @Override
  public boolean[] getValue() {
    return getSelectedResults();
  }
  
  /**
   * D�finit les resultats s�lectionn�s pour la moyenne.
   * @param _tool L'utilitaire
   */
  public void setSelectedResults(boolean[] _usedResults) {
    if (_usedResults==null) return;
    
    String[] values=new String[_usedResults.length];
    for (int i=0; i<_usedResults.length; i++) {
      values[i]=PivResource.getS("R�sultat : {0}", (i+1));
    }
    
    mdResults_.setData(values);
    
    for (int i=0; i<_usedResults.length; i++) {
      if (_usedResults[i])
        pnListResults_.getTable().getSelectionModel().addSelectionInterval(i, i);
    }
    
  }
  
  /**
   * @return Les r�sultats selectionn�s.
   */
  public boolean[] getSelectedResults() {
    boolean[] idSels=new boolean[mdResults_.getRowCount()];
    int[] sel=pnListResults_.getTable().getSelectedRows();
    for (int i : sel) {
      idSels[i]=true;
    }
    
    return idSels;
  }
  
  @Override
  public boolean isDataValid() {
    if (pnListResults_.getTable().getSelectedRowCount()==0) {
      setErrorText(PivResource.getS("Vous devez s�lectionner 1 r�sultat au moins pour la moyenne"));
      return false;
    }
    return true;
  }

  @Override
  public boolean apply() {
    boolean compute = false;
    
    if (!CtuluLibArray.isEquals(impl.getCurrentProject().getUsedInstantResults(), getSelectedResults())) {
      
      if (impl.getCurrentProject().hasFlowResults() && !impl.question(PivResource.getS("Suppression des r�sultats"), 
          PivResource.getS("Attention : des r�sultats existent et seront supprim�s si vous modifiez les param�tres.\nVoulez-vous continuer ?"))) {
        return false;
      }
      
      impl.getCurrentProject().setUsedInstantResults(getSelectedResults());
      compute = true;
    }
    
    else if (!impl.getCurrentProject().hasAverageResults()) {
      compute = true;
    }
    
    if (compute) {
      // La tache a ex�cuter.
      PivTaskAbstract r=new PivTaskAbstract(PivResource.getS("Moyenne des r�sultats instantan�s filtr�s")) {

        public boolean act(PivTaskObserverI _observer) {
          CtuluLog ana = new CtuluLog();

          // Moyenne
          PivExeLauncher.instance().computeAverageResults(ana, impl.getCurrentProject(), _observer);
          if (ana.containsErrorOrSevereError()) {
            impl.error(ana.getResume());
            return false;
          }

          // Lanc� � la fin, car l'interface se bloque si on ne le fait pas.
          // Probl�me de thread swing probablement...
          SwingUtilities.invokeLater(new Runnable() {
            public void run() {
              impl.message(PivResource.getS("Calcul termin�"), PivResource.getS("Le calcul s'est termin� avec succ�s"), false);
              impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_REAL_VIEW);
              impl.get2dFrame().getVisuPanel().setAveVelocitiesLayerVisible(true);
            }
          });
          
          return true;
        }
      };

      PivProgressionPanel pnProgress_=new PivProgressionPanel(r);
      diProgress_ = pnProgress_.createDialog(impl.getParentComponent());
      diProgress_.setOption(CtuluDialog.ZERO_OPTION);
      diProgress_.setDefaultCloseOperation(CtuluDialog.DO_NOTHING_ON_CLOSE);
      diProgress_.setTitle(r.getName());

      r.start();
      diProgress_.afficheDialogModal();
    }
    
    return true;
  }
}

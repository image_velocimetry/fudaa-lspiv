package org.fudaa.fudaa.piv;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.table.AbstractTableModel;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.courbe.EGAxeHorizontal;
import org.fudaa.ebli.courbe.EGAxeVertical;
import org.fudaa.ebli.courbe.EGCourbe;
import org.fudaa.ebli.courbe.EGCourbeSimple;
import org.fudaa.ebli.courbe.EGFillePanel;
import org.fudaa.ebli.courbe.EGGraphe;
import org.fudaa.ebli.courbe.EGGrapheModel;
import org.fudaa.ebli.courbe.EGGrapheSimpleModel;
import org.fudaa.ebli.courbe.EGModel;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.fudaa.piv.action.PivImportTransectAbsZAction;
import org.fudaa.fudaa.piv.metier.PivTransect;
import org.fudaa.fudaa.piv.metier.PivTransectParams;
import org.fudaa.fudaa.piv.utils.PivUtils;
import org.fudaa.fudaa.sig.FSigLib;

import gnu.trove.TIntArrayList;

/**
 * Un panneau pour cr�er un transect en mode Abscisse/Cote.
 * 
 * @author Bertrand Marchand (marchand@detacad.fr)
 */
public class PivCreateTransectAbsZPanel extends CtuluDialogPanel {

  /**
   * Un modele pour le tableau et la courbe, bas� sur un transect.
   * 
   * @author marchand
   */
  class AbsZCoordsModel extends AbstractTableModel implements EGModel {
    PivTransect transect_;
    EGCourbe crb_;
    /** Les indices selectionn�s */
    TIntArrayList selectedIdx_ = new TIntArrayList();
    /** Les abscisses */
    List<Double> abscisses_ = new ArrayList<>();
    /** Les 2 points de la trace du transect */
    GrPoint[] ptTrace_;

    public AbsZCoordsModel(PivTransect _transect) {
      transect_ = _transect;
      ptTrace_ = new GrPoint[] { transect_.getStraight().sommet(0),
          transect_.getStraight().sommet(transect_.getStraight().nombre() - 1) };

      abscisses_.add(0.);
      abscisses_.add(ptTrace_[0].distanceXY(ptTrace_[1]));
    }

    public void setCourbeListener(EGCourbe _crb) {
      crb_ = _crb;
    }

    public void setSelectedIdx(int[] _idx) {
      selectedIdx_.clear();
      selectedIdx_.add(_idx);

      if (crb_ != null) {
        crb_.fireCourbeAspectChanged(true);
      }
    }

    @Override
    public int getRowCount() {
      return transect_.getStraight().nombre();
    }

    @Override
    public int getColumnCount() {
      return 4;
    }

    @Override
    public String getColumnName(int columnIndex) {
      if (columnIndex == 0) {
        return PivResource.getS("N�");
      }
      else if (columnIndex == 1) {
        return PivResource.getS("Abscisse");
      }
      else if (columnIndex == 2) {
        return PivVisuPanel.ATT_IND_ZR.getName();
      }
      else {
        return PivVisuPanel.ATT_P_COEF_VIT.getName();
      }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
      return columnIndex == 0 ? Integer.class : Double.class;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
      GrPolyligne pl = transect_.getStraight();

      // Abscisse
      if (columnIndex == 1) {
        Double abs = parseVal(aValue);
        if (abs == null)
          return;

        // Controle que les abscisses des points interm�diaires restent dans
        // l'intervalle [absPt-1, absPt+1]
        if (rowIndex > 0) {
          if (abscisses_.get(rowIndex - 1) >= abs) {
            return;
          }
        }
        if (rowIndex < abscisses_.size() - 1) {
          if (abscisses_.get(rowIndex + 1) <= abs) {
            return;
          }
        }
        abscisses_.set(rowIndex, abs);

        GrPoint ptCompute = abs2XY(abs, pl.sommet(rowIndex).z_);
        pl.sommets_.remplace(ptCompute, rowIndex);
      }

      // Z
      else if (columnIndex == 2) {
        Double abs = parseVal(aValue);
        if (abs == null)
          return;
        pl.sommet(rowIndex).z_ = abs;
      }

      // Coefficient
      else if (columnIndex == 3) {
        Double abs = parseVal(aValue);
        if (!transect_.getParams().isSetSurfaceCoef()) {
          transect_.getCoefs()[rowIndex] = abs;
        }
      }

      fireTableDataChanged();
      if (crb_ != null) {
        crb_.fireCourbeContentChanged();
      }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      if (columnIndex == 0 || (columnIndex == 1 && rowIndex == 0)) {
        return false;
      }
      if (columnIndex == 3 && transect_.getParams().isSetSurfaceCoef()) {
        return false;
      }

      return true;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      GrPolyligne pl = transect_.getStraight();
      if (columnIndex == 0) {
        return rowIndex + 1;
      }
      else if (columnIndex == 1) {
        return abscisses_.get(rowIndex);
      }
      else if (columnIndex == 2) {
        return pl.sommet(rowIndex).z_;
      }
      else {
        if (transect_.getParams().isSetSurfaceCoef()) {
          return "";
        }
        else {
          return transect_.getCoefs()[rowIndex];
        }
      }
    }

    public void clearPoints() {
      abscisses_.clear();
      transect_.clearPoints();

      fireTableDataChanged();
      if (crb_ != null) {
        crb_.fireCourbeContentChanged();
      }
    }

    public void addPoint(double _abs, double _z) {
      abscisses_.add(_abs);
      transect_.addPoint(-1, abs2XY(_abs, _z), null);

      fireTableDataChanged();
      if (crb_ != null) {
        crb_.fireCourbeContentChanged();
      }
    }

    /**
     * Ajout d'un point apr�s l'index. Si _ind est -1, ajout a la fin.
     * 
     * @param _ind L'index apr�s lequel ins�rer le point.
     */
    public void addPoint(int _ind) {

      // Plusieurs points
      GrPolyligne pl = transect_.getStraight();
      double absDeb = 0;
      double absFin = 0;
      double z = 0;

      // Ajout en fin de transect.
      double abs;
      if (_ind == -1) {
        if (abscisses_.size() == 0) {
          z = 0;
          abs = 0;
        }
        else {
          z = pl.sommet(pl.nombre()-1).z_;
          abs = abscisses_.get(abscisses_.size() - 1) + 10.;
        }
        abscisses_.add(abs);
        transect_.addPoint(_ind, abs2XY(abs, z), null);
      }

      // Ajout en d�but
      else if (_ind == 0) {
        if (abscisses_.size() == 0) {
          z = 0;
        }
        else {
          z = pl.sommet(0).z_;
        }

        abscisses_.add(0, -10.);
        transect_.addPoint(_ind, abs2XY(-10, z), null);
      }

      // Ajout entre 2 points
      else {
        absDeb = abscisses_.get(_ind - 1);
        absFin = abscisses_.get(_ind);
        abs = (absFin + absDeb) / 2;
        z = (pl.sommet(_ind).z_ + pl.sommet(_ind - 1).z_) / 2.;
        abscisses_.add(_ind, abs);
        transect_.addPoint(_ind, abs2XY(abs, z), null);
      }

      realignOnOrigin();

      fireTableDataChanged();
      if (crb_ != null) {
        crb_.fireCourbeContentChanged();
      }
    }

    /**
     * @param _inds Les indices des points a supprimer. Les indices ne sont pas
     *              necessairement tri�s
     */
    public void removePoints(int[] _inds) {
      int[] inds = Arrays.copyOf(_inds, _inds.length);
      Arrays.sort(inds);

      for (int i = _inds.length - 1; i >= 0; i--) {
        transect_.removePoint(_inds[i]);
        abscisses_.remove(_inds[i]);
      }

      realignOnOrigin();

      fireTableDataChanged();
      if (crb_ != null) {
        crb_.fireCourbeContentChanged();
      }
    }

    /**
     * Realigne le transect sur le 1er point de trace (si un point a �t� ajout� ou supprim� en d�but)
     */
    private void realignOnOrigin() {
      if (abscisses_.size() > 0 && abscisses_.get(0) != 0) {
        double decal = abscisses_.get(0);
        for (int i = 0; i < abscisses_.size(); i++) {
          double z = transect_.getStraight().sommet(i).z_;
          double abs = abscisses_.get(i) - decal;

          abscisses_.set(i, abs);
          transect_.replacePoint(i, abs2XY(abs, z), null);
        }
      }
    }

    /**
     * Cr�e un point a partir d'un abscisse sur la droite du transect.
     */
    private GrPoint abs2XY(double _abs, double _z) {
      GrPoint ptdeb = ptTrace_[0];
      GrPoint ptfin = ptTrace_[1];
      double absfin = ptdeb.distanceXY(ptfin);
      double x = ptdeb.x_ + (ptfin.x_ - ptdeb.x_) / absfin * _abs;
      double y = ptdeb.y_ + (ptfin.y_ - ptdeb.y_) / absfin * _abs;

      return new GrPoint(x, y, _z);
    }

    /**
     * @param aValue La valeur a parser
     * @return Le double si le parse est ok, ou null sinon.
     */
    private Double parseVal(Object aValue) {
      try {
        return Double.parseDouble(aValue.toString());
      }
      catch (NumberFormatException _exc) {
        return null;
      }
    }

    @Override
    public boolean isRemovable() {
      return false;
    }

    @Override
    public boolean isDuplicatable() {
      return false;
    }

    @Override
    public int getNbValues() {
      return transect_.getStraight().nombre();
    }

    @Override
    public boolean isSegmentDrawn(int _i) {
      return true;
    }

    @Override
    public boolean isPointDrawn(int _i) {
      return true;
    }

    @Override
    public String getPointLabel(int _i) {
      return null;
    }

    @Override
    public double getX(int _idx) {
      return abscisses_.get(_idx);
    }

    @Override
    public double getY(int _idx) {
      return transect_.getStraight().sommet(_idx).z_;
    }

    @Override
    public double getXMin() {
      return 0;
    }

    @Override
    public double getXMax() {
      return transect_.getStraight().longueurXY();
    }

    @Override
    public double getYMin() {
      GrPolyligne pl = transect_.getStraight();
      double ymin = Double.POSITIVE_INFINITY;
      for (int i = 0; i < pl.nombre(); i++) {
        ymin = Math.min(ymin, pl.sommet(i).z_);
      }
      return ymin;
    }

    @Override
    public double getYMax() {
      GrPolyligne pl = transect_.getStraight();
      double ymax = Double.NEGATIVE_INFINITY;
      for (int i = 0; i < pl.nombre(); i++) {
        ymax = Math.max(ymax, pl.sommet(i).z_);
      }
      return ymax;
    }

    @Override
    public boolean isModifiable() {
      return true;
    }

    @Override
    public boolean isXModifiable() {
      return true;
    }

    @Override
    public boolean setValue(int _i, double _x, double _y, CtuluCommandContainer _cmd) {
      return false;
    }

    @Override
    public boolean addValue(double _x, double _y, CtuluCommandContainer _cmd) {
      return false;
    }

    @Override
    public boolean addValue(double[] _x, double[] _y, CtuluCommandContainer _cmd) {
      return false;
    }

    @Override
    public boolean removeValue(int _i, CtuluCommandContainer _cmd) {
      return false;
    }

    @Override
    public boolean removeValue(int[] _i, CtuluCommandContainer _cmd) {
      return false;
    }

    @Override
    public boolean deplace(int[] _selectIdx, double _deltaX, double _deltaY, CtuluCommandContainer _cmd) {
      return false;
    }

    @Override
    public boolean setValues(int[] _idx, double[] _x, double[] _y, CtuluCommandContainer _cmd) {
      return false;
    }

    @Override
    public String getTitle() {
      return null;
    }

    @Override
    public boolean isTitleModifiable() {
      return false;
    }

    @Override
    public boolean setTitle(String _newName) {
      return false;
    }

    @Override
    public void fillWithInfo(InfoData _table, CtuluListSelectionInterface _selectedPt) {
    }

    @Override
    public EGModel duplicate() {
      return null;
    }

    @Override
    public void viewGenerationSource(Map infos, CtuluUI impl) {
    }

    @Override
    public boolean isGenerationSourceVisible() {
      return false;
    }

    @Override
    public void replayData(EGGrapheModel model, Map infos, CtuluUI impl) {
    }

    @Override
    public boolean isReplayable() {
      return false;
    }

    @Override
    public Object savePersistSpecificDatas() {
      return null;
    }

    @Override
    public void restoreFromSpecificDatas(Object data, Map infos) {
    }

    @Override
    public boolean useSpecificIcon(int idx) {
      return selectedIdx_.contains(idx);
    }

    @Override
    public Color getSpecificColor(int idx) {
      return null;
    }

    @Override
    public int[] getInitRows() {
      return null;
    }
  }

  /**
   * Un panneau pour l'affichage des coordonn�es des points du transect sous forme
   * de tableau.
   * 
   * @author marchand
   *
   */
  class CoordsTablePanel extends JPanel {

    PivImplementation impl_;
    AbsZCoordsModel mdlCoords_;
    CtuluTable tbCoords_;

    public CoordsTablePanel(PivImplementation _impl, AbsZCoordsModel _mdlCoords) {
      mdlCoords_ = _mdlCoords;
      impl_ = _impl;
      customize();
    }

    protected void customize() {

      tbCoords_ = new CtuluTable(mdlCoords_);
      tbCoords_.setDefaultRenderer(Double.class, PivUtils.getStandardDoubleTableCellRenderer());
      JScrollPane spCoords = new JScrollPane();
      spCoords.setPreferredSize(new Dimension(200, getPreferredSize().height));
      spCoords.setViewportView(tbCoords_);

      JToolBar toolbar = new JToolBar();
      List<EbliActionInterface> actions = buildTableActions();
      for (EbliActionInterface act : actions) {
        toolbar.add(act.buildToolButton(EbliComponentFactory.INSTANCE));
      }

      setLayout(new BorderLayout());
      add(toolbar, BorderLayout.NORTH);
      add(spCoords, BorderLayout.CENTER);
    }

    public List<EbliActionInterface> buildTableActions() {
      ArrayList<EbliActionInterface> actions = new ArrayList<>();

      EbliActionInterface actAdd = new EbliActionSimple(PivResource.getS("Ajouter un point"),
          PivResource.PIV.getToolIcon("node-add"), PivResource.getS("Ajouter un point")) {

        @Override
        public void actionPerformed(ActionEvent _e) {
          // Pour inserer apr�s la s�lection
          int indsel = tbCoords_.getSelectedRow();
          mdlCoords_.addPoint(indsel);
          setSelection(new int[] { indsel == -1 ? tbCoords_.getRowCount() - 1 : indsel });
        }
      };
      actions.add(actAdd);

      EbliActionInterface actRemove = new EbliActionSimple(PivResource.getS("Supprimer un point"),
          PivResource.PIV.getToolIcon("node-delete"), PivResource.getS("Supprimer les points s�lectionn�s")) {

        @Override
        public void actionPerformed(ActionEvent _e) {
          mdlCoords_.removePoints(tbCoords_.getSelectedRows());
        }
      };
      actRemove.setEnabled(false);
      actions.add(actRemove);

      tbCoords_.getSelectionModel().addListSelectionListener((evt) -> {
        actRemove.setEnabled(
            tbCoords_.getSelectedRowCount() > 0 && tbCoords_.getSelectedRowCount() < tbCoords_.getRowCount() - 1);
        mdlCoords_.setSelectedIdx(tbCoords_.getSelectedRows());
      });

      PivImportTransectAbsZAction actImport = new PivImportTransectAbsZAction(impl_);
      actImport.setImportListener((points) -> {
        if (points.size() < 2) {
          impl_.error(PivResource.getS("Le transect doit comporter au moins 2 points"));
          return;
        }

        if (!impl_.question(PivResource.getS("Import des points"),
            PivResource.getS("L'import va supprimer les points existants.\nVoulez-vous continuer ?")))
          return;

        mdlCoords_.clearPoints();
        for (Double[] point : points) {
          mdlCoords_.addPoint(point[0], point[1]);
        }
      });
      actions.add(actImport);

      return actions;
    }

    /**
     * @return Un tableau contenant les index selectionn�s.
     */
    public int[] getSelection() {
      return tbCoords_.getSelectedRows();
    }

    /**
     * Selectionne les points dont les indices sont pass�s en param�tre
     * 
     * @param _idxSelection Les indices s�lectionn�s
     */
    public void setSelection(int[] _idxSelection) {
      tbCoords_.getSelectionModel().clearSelection();
      if (_idxSelection != null) {
        for (int i = 0; i < _idxSelection.length; i++)
          tbCoords_.getSelectionModel().addSelectionInterval(_idxSelection[i], _idxSelection[i]);
      }
    }

  }

  /**
   * Un panneau d'affichage des coordonn�es du transect sous forme de graphe (le transect est
   * repr�sent� vertical)
   * @author marchand
   *
   */
  class CourbeGraphePanel extends JPanel {
    AbsZCoordsModel mdlCoords_;

    public CourbeGraphePanel(AbsZCoordsModel _mdlCoords) {
      mdlCoords_ = _mdlCoords;
      customize();
    }

    protected void customize() {

      EGGrapheSimpleModel grapheModel = new EGGrapheSimpleModel();
      EGGraphe grapheVue = new EGGraphe(grapheModel);
      grapheVue.setAutoRestore(true);
      grapheVue.addKeyListener(grapheVue.getRepereController());
      grapheVue.addMouseListener(grapheVue.getRepereController());
      grapheVue.addMouseWheelListener(grapheVue.getRepereController());

      // Axe des X
      EGAxeHorizontal axeX_ = new EGAxeHorizontal(false);
      axeX_.setTitre(FSigLib.getS("Abscisse curviligne"));
      axeX_.setUnite("m");
      axeX_.setGraduations(true);
      grapheVue.setXAxe(axeX_);
      // Axe des Y
      EGAxeVertical axeY_ = new EGAxeVertical();
      axeY_.setGraduations(true);
      axeY_.setTitre(FSigLib.getS("Ordonn�e") + " : " + "Z");
      axeY_.setUnite("m");
      DecimalFormat df = CtuluLib.getDecimalFormat();
      df.setMaximumFractionDigits(2);
      axeY_.setSpecificFormat(new CtuluNumberFormatDefault(df));
      // Le modele
      CtuluCommandManager cmd = new CtuluCommandManager();
      // La courbe
      EGCourbeSimple courbeVue_ = new EGCourbeSimple(axeY_, mdlCoords_);
      courbeVue_.setAspectContour(Color.BLUE);
      courbeVue_.setUseSpecificIcon(true);
      courbeVue_.setIconeModel(new TraceIconModel(TraceIcon.CARRE_PLEIN, 2, Color.BLUE));
      courbeVue_.setIconeModelSpecific(new TraceIconModel(TraceIcon.CARRE_SELECTION, 5, Color.BLUE));
      grapheModel.addCourbe(courbeVue_, cmd);

      mdlCoords_.setCourbeListener(courbeVue_);

      JToolBar tbRightButtons = new JToolBar();
      // Conteneur de graphe
      EGFillePanel grapheContainer = new EGFillePanel(grapheVue);
      EbliActionInterface[] grapheActions = grapheContainer.getSpecificActions();
      // On ne garde que les actions interessantes.
      HashSet<String> actionsKept = new HashSet<String>(Arrays.asList("RESTORE", "AUTO_REST", "ZOOM"));
      for (EbliActionInterface act : grapheActions) {
        if (act != null && actionsKept.contains(act.getValue(Action.ACTION_COMMAND_KEY))) {
          tbRightButtons.add(act.buildToolButton(EbliComponentFactory.INSTANCE));
        }
      }

      this.setLayout(new BorderLayout());
      this.add(tbRightButtons, BorderLayout.NORTH);
      this.add(grapheVue, BorderLayout.CENTER);
    }
  }

  protected PivImplementation impl_;
  /** Les transects avant ajout de celui cr�� */
  PivTransect[] oldtransects_;
  /** Le panneau des param�tres du transect */
  PivTransectParamSubPanel pnParams_;
  // Le transect en cours de cr�ation */
  PivTransect currentTransect_ = new PivTransect();
  /** Le modele pour le talbeau et la courbe bas� sur un transect */
  AbsZCoordsModel mdlCoords_;

  public PivCreateTransectAbsZPanel(PivImplementation _impl, PivTransect _transect) {
    impl_ = _impl;
    oldtransects_ = impl_.getCurrentProject().getTransects();
    currentTransect_ = _transect;

    customize();
  }

  protected void customize() {
    setLayout(new BorderLayout());
    setPreferredSize(new Dimension(650, 450));

    mdlCoords_ = new AbsZCoordsModel(currentTransect_);

    JPanel pnRight = new CourbeGraphePanel(mdlCoords_);
    JPanel pnLeft = new CoordsTablePanel(impl_, mdlCoords_);

    JSplitPane spMain = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
    spMain.add(pnLeft, JSplitPane.LEFT);
    spMain.add(pnRight, JSplitPane.RIGHT);

    this.add(spMain, BorderLayout.CENTER);

    // La panneau des param�tres
    pnParams_ = new PivTransectParamSubPanel(this, impl_);
    pnParams_.setFlowParams(currentTransect_.getParams());
    pnParams_.addPropertyChangeListener("params", (evt) -> {
      currentTransect_.setParams((PivTransectParams) evt.getNewValue());
      // Pour que le tableau reaffiche correctmeent les coerficients ponctuels lors du changement d'�tat.
      pnLeft.repaint();
    });

    this.add(pnParams_, BorderLayout.NORTH);

  }

  @Override
  public boolean isDataValid() {
    if (!pnParams_.isDataValid())
      return false;

    if (currentTransect_.getStraight().nombre() < 2) {
      setErrorText(PivResource.getS("Le transect doit comporter au moins 2 points"));
      return false;
    }

    return true;
  }

  @Override
  public boolean apply() {
    if (!super.apply())
      return false;

    // On ajoute le nouveau transect.
    currentTransect_.setParams(pnParams_.getFlowParams());

    // Fix #5737 : La liste des transects peut �tre null => On la cr�e
    PivTransect[] transects;
    if (oldtransects_ == null) {
      transects = new PivTransect[] { currentTransect_ };
    }
    else {
      transects = Arrays.copyOf(oldtransects_, oldtransects_.length + 1);
      transects[transects.length - 1] = currentTransect_;
    }

    impl_.getCurrentProject().setTransects(transects);

    return true;
  }

  @Override
  public boolean cancel() {
    if (!super.cancel())
      return false;

    impl_.getCurrentProject().setTransects(oldtransects_);
    return true;
  }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.piv;

import java.awt.BorderLayout;
import java.awt.Cursor;

import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.piv.metier.PivTransectParams;
import org.fudaa.fudaa.piv.utils.PivCheckboxEditorComponent;
import org.fudaa.fudaa.piv.utils.PivUtils;
import org.fudaa.fudaa.piv.utils.PivWithUnsetValueEditorInteger;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuInsets;
import com.memoire.bu.BuLib;

/**
 * Un panneau de saisie des param�tres des transects s�lectionn�s pour le calcul des d�bits.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivTransectParamSubPanel extends JPanel {

  PivImplementation impl_;
  PivCheckboxEditorComponent<Double> tfCoef;
  PivCheckboxEditorComponent<Integer> tfMaxPoints;
  CtuluDialogPanel pnMain_;
  PivTransectParams[] transParams_;

  /**
   * Constructeur.
   */
  public PivTransectParamSubPanel(CtuluDialogPanel _pnMain, PivImplementation _impl) {
    pnMain_ = _pnMain;
    impl_=_impl;
    initComponents();
    customize();
  }

  private void customize() {
    // BM : Repris de BuTransparentToggleButton, on ne peut pas creer un BuTransparentToggleButton dans le designer de NetBeans.
    btLock.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    btLock.setIcon(CtuluResource.CTULU.getIcon("non-lie.png"));
    btLock.setMargin(BuInsets.INSETS0000);
    btLock.setBorder(BuBorders.EMPTY0000);
    btLock.setRequestFocusEnabled(false);
    btLock.setOpaque(false);
    btLock.setContentAreaFilled(false);
    btLock.setHorizontalAlignment(SwingConstants.LEFT);
    btLock.setRolloverEnabled(true);
    btLock.setToolTipText(PivResource.getS("Lie/d�lie la valeur des rayons Rx et Ry"));
    btLock.addActionListener(_evt -> {
      rxValueChanged();
    });
    BuLib.setSelectedIcons(btLock,CtuluResource.CTULU.getIcon("lie.png"),true);
    
    lbCoef.setText(PivResource.getS("Coefficient de vitesse"));
    lbRx.setText(PivResource.getS("Rayon de recherche Rx")+" (m)");
    lbRy.setText(PivResource.getS("Rayon de recherche Ry")+" (m)");
    lbInterpolationStep.setText(PivResource.getS("Pas d'espace d'interpolation de la bathym�trie (m)"));
    
    tfCoef = new PivCheckboxEditorComponent<>();
    tfCoef.setCheckboxTooltips(PivResource.getS("D�sactivez pour d�finir des coefficients ponctuels de vitesse"), PivResource.getS("Activez pour d�finir un coefficient unique de vitesse"));
    tfCoef.addPropertyChangeListener("value", (evt) -> {
      firePropertyChange("params", null, getFlowParams());
    });
    
    pnCoef.setLayout(new BorderLayout(0,0));
    pnCoef.add(tfCoef);
    
    tfMaxPoints = ((PivWithUnsetValueEditorInteger)PivVisuPanel.ATT_MAX_POINT_NUMBER.getEditor()).createEditorComponent();
    tfMaxPoints.addPropertyChangeListener("value", (evt) -> {
      firePropertyChange("params", null, getFlowParams());
    });
    
    pnMaxPoints.setLayout(new BorderLayout(0,0));
    pnMaxPoints.add(tfMaxPoints);
    
    tfRx.getDocument().addDocumentListener(new DocumentListener() {
      
      @Override
      public void removeUpdate(DocumentEvent e) {
        update(e.getDocument());
      }
      
      @Override
      public void insertUpdate(DocumentEvent e) {
        update(e.getDocument());
      }
      
      @Override
      public void changedUpdate(DocumentEvent e) {
        update(e.getDocument());
      }
    });
    
    tfRy.getDocument().addDocumentListener(new DocumentListener() {
      
      @Override
      public void removeUpdate(DocumentEvent e) {
        update(e.getDocument());
      }
      
      @Override
      public void insertUpdate(DocumentEvent e) {
        update(e.getDocument());
      }
      
      @Override
      public void changedUpdate(DocumentEvent e) {
        update(e.getDocument());
      }
    });
  }
  

  private void update(final Document _src) {
    if (_src == tfRx.getDocument()) {
      rxValueChanged();
    } else {
      ryValueChanged();
    }

  }

  boolean update_ = false;
  
  private void ryValueChanged() {
    if (!update_ && btLock.isSelected()) {
      update_ = true;
      tfRx.setText(tfRy.getText());
      update_ = false;

    }
  }

  private void rxValueChanged() {
    if (!update_ && btLock.isSelected()) {
      update_ = true;
      tfRy.setText(tfRx.getText());
      update_ = false;
    }
  }
  
  
  
  /**
   * Remplit le panneau depuis les parametres des transect.
   */
  public void setFlowParams(PivTransectParams... _params) {
    transParams_ = _params;
    
    boolean setMaxPoints = false;
    Double scoef = null;
    Integer maxPoints = null;
    String radiusX=null;
    String radiusY=null;
    String sstep=null;
    
    for (PivTransectParams param : _params) {
      if (scoef==null)
        scoef=param.getSurfaceCoef();
      else if (!scoef.equals(param.getSurfaceCoef())) {
        scoef=-1.;
      }
      if (!setMaxPoints) {
        maxPoints=param.getMaxPoints();
        setMaxPoints = true;
      }
      else if (!maxPoints.equals(param.getMaxPoints())) {
        maxPoints=null;
      }
      if (radiusX==null)
        radiusX=""+param.getRadiusX();
      else if (!radiusX.equals(""+param.getRadiusX())) {
        radiusX="";
      }
      if (radiusY==null)
        radiusY=""+param.getRadiusY();
      else if (!radiusY.equals(""+param.getRadiusY())) {
        radiusY="";
      }
      if (sstep==null)
        sstep=""+param.getInterpolationStep();
      else if (!sstep.equals(""+param.getInterpolationStep())) {
        sstep="";
      }
    }
    
//    if (scoef==null)
//      scoef="";
    if (sstep==null)
      sstep="";
    if (radiusX==null)
      radiusX="";
    if (radiusY==null)
      radiusY="";
    
    tfCoef.setValue(scoef);
    tfMaxPoints.setValue(maxPoints);
    tfRx.setText(radiusX);
    tfRy.setText(radiusY);
    tfInterpolationStep.setText(sstep);
    
    if (radiusX.equals(radiusY)) {
      btLock.setSelected(true);
    }
  }
  
  /**
   * Recup�re les param�tres saisis par l'utilisateur. Si l'utilisateur n'a pas renseign� une valeur, la valeur retourn�e est null.
   */
  public PivTransectParams getFlowParams() {
    String scoef=tfCoef.getValue() == null ? "" : tfCoef.getValue().toString().trim();
    String smaxPoints=tfMaxPoints.getValue() == null ? "" : tfMaxPoints.getValue().toString().trim();
    String radiusX=tfRx.getText().trim();
    String radiusY=tfRy.getText().trim();
    String sstep=tfInterpolationStep.getText().trim();
    
    PivTransectParams params = new PivTransectParams();
    params.setSurfaceCoef(scoef.isEmpty() ? null : Double.parseDouble(scoef));
    params.setMaxPoints(smaxPoints.isEmpty() ? null : Integer.parseInt(smaxPoints));
    params.setRadiusX(radiusX.isEmpty() ? null : Double.parseDouble(radiusX));
    params.setRadiusY(radiusY.isEmpty() ? null : Double.parseDouble(radiusY));
    params.setInterpolationStep(sstep.isEmpty() ? null : Double.parseDouble(sstep));
    
    return params;
  }

  public boolean isDataValid() {
    boolean bOne = transParams_ != null && transParams_.length == 1;
    
    pnMain_.setErrorText("");
    boolean bok=
        ((tfCoef.getValue() != null && !bOne) || PivUtils.isStrictPositiveReal(pnMain_,tfCoef.getValue() == null ? "":tfCoef.getValue().toString(),PivResource.getS("Coefficient de vitesse"))) &&
        ((tfMaxPoints.getValue() != null && !bOne) || PivUtils.isPositiveInteger(pnMain_,tfMaxPoints.getValue() == null ? "":tfMaxPoints.getValue().toString(),PivResource.getS("Nombre de points max"))) &&
        ((tfRx.getText().trim().isEmpty() && !bOne) || PivUtils.isStrictPositiveReal(pnMain_,tfRx.getText(),PivResource.getS("Rayon de recherche Rx"))) &&
        ((tfRy.getText().trim().isEmpty() && !bOne) || PivUtils.isStrictPositiveReal(pnMain_,tfRy.getText(),PivResource.getS("Rayon de recherche Ry"))) &&
        ((tfInterpolationStep.getText().trim().isEmpty() && !bOne) || PivUtils.isStrictPositiveReal(pnMain_,tfInterpolationStep.getText(),PivResource.getS("Distance d'extrapolation")));

    return bok;
  }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbInterpolationStep = new javax.swing.JLabel();
        lbRx = new javax.swing.JLabel();
        lbRy = new javax.swing.JLabel();
        lbCoef = new javax.swing.JLabel();
        tfInterpolationStep = new javax.swing.JTextField();
        tfRx = new javax.swing.JTextField();
        tfRy = new javax.swing.JTextField();
        btLock = new javax.swing.JToggleButton();
        pnCoef = new javax.swing.JPanel();
        pnMaxPoints = new javax.swing.JPanel();
        lbMaxPoints = new javax.swing.JLabel();

        lbInterpolationStep.setText("Pas d'espace d'interolation de bathym�trie");

        lbRx.setText("Rayon de recherche Rx");

        lbRy.setText("Rayon de recherche Ry");

        lbCoef.setText("Coefficient de vitesse");

        tfInterpolationStep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfInterpolationStepActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnCoefLayout = new javax.swing.GroupLayout(pnCoef);
        pnCoef.setLayout(pnCoefLayout);
        pnCoefLayout.setHorizontalGroup(
            pnCoefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        pnCoefLayout.setVerticalGroup(
            pnCoefLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout pnMaxPointsLayout = new javax.swing.GroupLayout(pnMaxPoints);
        pnMaxPoints.setLayout(pnMaxPointsLayout);
        pnMaxPointsLayout.setHorizontalGroup(
            pnMaxPointsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        pnMaxPointsLayout.setVerticalGroup(
            pnMaxPointsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 21, Short.MAX_VALUE)
        );

        lbMaxPoints.setText("Nombre de points max");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbInterpolationStep)
                    .addComponent(lbRx)
                    .addComponent(lbRy)
                    .addComponent(lbCoef)
                    .addComponent(lbMaxPoints))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnCoef, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tfRy)
                    .addComponent(tfInterpolationStep)
                    .addComponent(tfRx, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
                    .addComponent(pnMaxPoints, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btLock, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfInterpolationStep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbInterpolationStep))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(btLock, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tfRx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbRx))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tfRy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbRy))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbCoef, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pnCoef, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pnMaxPoints, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbMaxPoints)))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tfInterpolationStepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfInterpolationStepActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfInterpolationStepActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton btLock;
    private javax.swing.JLabel lbCoef;
    private javax.swing.JLabel lbInterpolationStep;
    private javax.swing.JLabel lbMaxPoints;
    private javax.swing.JLabel lbRx;
    private javax.swing.JLabel lbRy;
    private javax.swing.JPanel pnCoef;
    private javax.swing.JPanel pnMaxPoints;
    private javax.swing.JTextField tfInterpolationStep;
    private javax.swing.JTextField tfRx;
    private javax.swing.JTextField tfRy;
    // End of variables declaration//GEN-END:variables
}

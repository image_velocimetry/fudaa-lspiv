/*
 * @creation 7 juin 07
 * @modification $Date: 2009-06-03 16:56:42 +0200 (mer., 03 juin 2009) $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISAttributeInteger;
import org.fudaa.ctulu.gis.GISAttributeString;
import org.fudaa.ebli.animation.EbliAnimationSourceInterface;
import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueAffichage;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.ZEbliCalquesPanel;
import org.fudaa.ebli.calque.action.SceneDeplacementAction;
import org.fudaa.ebli.calque.action.SceneRotationAction;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteraction;
import org.fudaa.ebli.calque.edition.ZEditorDefault;
import org.fudaa.ebli.calque.edition.ZEditorValidatorI;
import org.fudaa.ebli.calque.edition.ZModeleEditable;
import org.fudaa.ebli.commun.EbliActionAbstract;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.piv.action.PivChangeCoordinatesSystemAction;
import org.fudaa.fudaa.piv.action.PivCreateTransectAbsZAction;
import org.fudaa.fudaa.piv.action.PivDefineGridAction;
import org.fudaa.fudaa.piv.action.PivEditAction;
import org.fudaa.fudaa.piv.action.PivNewTransectAction;
import org.fudaa.fudaa.piv.action.PivOrthoGRPAction;
import org.fudaa.fudaa.piv.action.PivShowVelocityAction;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.layer.PivEditableModel;
import org.fudaa.fudaa.piv.layer.PivTransectLayer;
import org.fudaa.fudaa.piv.layer.PivTransectModel;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivTransect;
import org.fudaa.fudaa.piv.metier.PivTransformationParameters;
import org.fudaa.fudaa.piv.utils.PivValueEditorGlobalCoefVelocity;
import org.fudaa.fudaa.piv.utils.PivValueEditorPointCoefVelocity;
import org.fudaa.fudaa.piv.utils.PivValueEditorPointCoefVelocity.PointCoefEditorComponent;
import org.fudaa.fudaa.piv.utils.PivWithUnsetValueEditorInteger;

import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuSpecificBar;

/**
 * Le panneau de visu 2D des donn�es de l'application au travers des calques.
 * Contient le composant vue des calques + le composant d'affichage des
 * coordonn�es + le composant d'affichage du mode de selection
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivVisuPanel extends ZEbliCalquesPanel {
  /** La vue en espace image d'origine */
  public static final int MODE_ORIGINAL_VIEW=0;
  /** La vue en espace image transform�e */
  public static final int MODE_TRANSF_VIEW=1;
  /** La vue en espace r�el */
  public static final int MODE_REAL_VIEW=2;
  

  /** L'attribut utilis� pour la visu des labels. */
  public static final GISAttributeString ATT_LABEL=new GISAttributeString(PivResource.getS("Label"),true) {
    @Override
    public String getID() {
      return "ATT_LABEL";
    }
    @Override
    public boolean isEditable() {
      return false;
    }
  };
  
  /** L'attribut utilis� pour la saisie de Z r�el. L'unit� devrait �tre dans CommonUnit, mais l'affichage ne se fait pas quand c'est le cas. */
  public static final GISAttributeDouble ATT_IND_ZR=new GISAttributeDouble(PivResource.getS("Z")+" (m)",true) {
    @Override
    public String getID() {
      return "ATT_REAL_Z";
    }
  };
  
  /** L'attribut utilis� pour le pas d'espace d'interpolation. */
  public static final GISAttributeDouble ATT_INTER=new GISAttributeDouble(PivResource.getS("Pas d'interpolation"),false,1000000.) {
    @Override
    public String getID() {
      return "ATT_INTER";
    }
  };
  
  /** L'attribut utilis� pour le rayon rx de recherche des vitesses. */
  public static final GISAttributeDouble ATT_RX=new GISAttributeDouble(PivResource.getS("Rayon de recherche Rx"),false,10.) {
    @Override
    public String getID() {
      return "ATT_RX";
    }
  };
  
  /** L'attribut utilis� pour le rayon rx de recherche des vitesses. */
  public static final GISAttributeDouble ATT_RY=new GISAttributeDouble(PivResource.getS("Rayon de recherche Ry"),false,10.) {
    @Override
    public String getID() {
      return "ATT_RY";
    }
  };
    
  /** L'attribut utilis� pour le coefficient de vitesses unique. */
  public static final GISAttributeDouble ATT_COEF_VIT=new GISAttributeDouble(PivResource.getS("Coef. unique de vitesse"),false,0.85) {
    PivValueEditorGlobalCoefVelocity editor;
    
    @Override
    public String getID() {
      return "ATT_COEF_VIT";
    }

    @Override
    public CtuluValueEditorI getEditor() {
      if (editor == null) {
        editor = new PivValueEditorGlobalCoefVelocity();
        editor.setDefaultValue((Double)getDefaultValue());
        editor.setLinkedEditor(ATT_P_COEF_VIT.getEditor());
      }

      return editor;
    }
  };
  
  /** L'attribut utilis� pour le nombre de points max. */
  public static final GISAttributeInteger ATT_MAX_POINT_NUMBER = new GISAttributeInteger(PivResource.getS("Nombre de points max"), false, 24) {
    PivWithUnsetValueEditorInteger editor;

    @Override
    public String getID() {
      return "ATT_MAX_POINT_NUMBER";
    }

    @Override
    public CtuluValueEditorI getEditor() {
      if (editor == null) {
        editor = new PivWithUnsetValueEditorInteger();
        editor.setDefaultValue((Integer)getDefaultValue());
        editor.setUnsetValue(0);
        editor.setUnset(true);
        editor.setCheckboxTooltips(PivResource.getS("D�sactivez pour ne pas purger le transect"), PivResource.getS("Activez pour purger le transect"));
      }

      return editor;
    }
  };

  /** L'attribut utilis� pour le coefficient de vitesses ponctuel. */
  public static final GISAttributeDouble ATT_P_COEF_VIT=new GISAttributeDouble(PivResource.getS("Coef."),true,PointCoefEditorComponent.UNSET_VALUE) {
    PivValueEditorPointCoefVelocity editor;
    
    @Override
    public String getID() {
      return "ATT_P_COEF_VIT";
    }

    @Override
    public CtuluValueEditorI getEditor() {
      if (editor == null) {
        editor = new PivValueEditorPointCoefVelocity();
      }

      return editor;
    }
  };
  

  PivProject projet;

  /** Le calque d'�dition. */
  ZCalqueEditionInteraction cqEdition_;

  /** La vue courante s�lectionn�e */
  int viewMode_=0;
  /** Les vues */
  private PivOriginalView origView_;
  private PivTransfView transfView_;
  private PivRealView realView_;

  private PivDefineGridAction actComputeGrid_;
  private PivShowVelocityAction actShowVelocities_;
  private PivEditAction actEdit_;
  private SceneRotationAction actRotation_;
  private SceneDeplacementAction actDeplacement_;
  private PivChangeCoordinatesSystemAction actTransfMatrix_;
  private JComboBox<String> cbRep_;

  AbstractButton btRotation_;
  AbstractButton btDeplacement_;
  AbstractButton btTransfRepere_;
  
  PivImplementation impl_;

  /**
   * Construction des calques. Une partie est d�j� construite par d�faut dans 
   * les classes m�res.
   * @param _impl L'implementation de l'application.
   */
  public PivVisuPanel(PivImplementation _impl) {
    super(null, new PivVisuPanelController(_impl));
    impl_ = _impl;

//    getCqSelectionI().setEditSelectionWhenDoubleClick(false);
    getScene().setRestrictedToCalqueActif(true);

    initCommonLayers();
    // Construction des vues.
    getViews();
    buildActions();
    buildSpecificTools();
  }
  
  @Override
  protected ZEditorDefault createGisEditor() {
    ZEditorDefault editor=new ZEditorDefault(this) {
      @Override
      public String edit() {
        String ret = "";
        ret=super.edit();
        
        // Necessaire pour que le modele soit notifi� que la g�om�trie a �t� �dit�e.
        if (getScene().getCalqueActif() instanceof ZCalqueEditable) {
          ZCalqueEditable cq=(ZCalqueEditable) getScene().getCalqueActif();
          if (cq.getLayerSelection()!=null && !cq.getLayerSelection().isEmpty()) {
            if (cq.modeleDonnees() instanceof PivEditableModel) {
              ((PivEditableModel)cq.modeleDonnees()).geometryChanged(cq.getLayerSelection().getSelectedIndex());
            }
          }
        }
        
        // Si nous sommes sur le calque transect, reconditionnement de la g�om�trie.
        // Attention : Ceci peut provoquer des effets de bord inattendus. Par exemple, si on supprime des points et que la distance entre point est modifi�e,
        // le reconditionnement va reconstruire des points interm�diaire, et donc annuler la suppression de points...
        if (getScene().getCalqueActif() instanceof PivTransectLayer) {
          PivTransectLayer cq = (PivTransectLayer) getScene().getCalqueActif();

          int[] selIds = cq.getSelectedIndex();

          if (selIds != null) {
            PivTransect[] trans = projet.getTransects();
            for (int id : selIds) {
              CtuluLog log = new CtuluLog();
              log.setDesc(PivResource.getS("Reconditionnement du transect"));
              PivExeLauncher.instance().computeTransectRecond(log, impl_.getCurrentProject(), trans[id], null);
              if (log.containsErrorOrSevereError()) {
                impl_.error(log.getResume());
                return "Bad reshape of transect";
              }
            }

            // Pour forcer le rafraichissement d'�cran
            impl_.getCurrentProject().setTransects(trans);
            // Conserve la selection pour l'affichage des cercles.
            impl_.get2dFrame().getVisuPanel().getRealView().getTransectLayer().setSelection(selIds);
          }
        }

        return ret;
      }
    };
    
    editor.setEditionValidator(new ZEditorValidatorI() {
      
      @Override
      public boolean isSingleEditionValid(ZModeleEditable _mdl) {
        return isValid(_mdl);
      }
      
      @Override
      public boolean isSegmentEditionValid(ZModeleEditable _mdl) {
        return isValid(_mdl);
      }
      
      @Override
      public boolean isMultiEditionValid(ZModeleEditable _mdl) {
        return isValid(_mdl);
      }
      
      @Override
      public boolean isAtomicEditionValid(ZModeleEditable _mdl) {
        return isValid(_mdl);
      }
      
      public boolean isValid(ZModeleEditable _mdl) {
        // En cas de modification des propri�t�s transect, suppression des resultats.
        if (_mdl instanceof PivTransectModel) {
          if (impl_.getCurrentProject().hasFlowResults() && !impl_.question(PivResource.getS("Suppression des r�sultats"), PivResource.getS(
              "Attention : des r�sultats existent et seront supprim�s si vous modifiez les param�tres.\nVoulez-vous continuer ?"))) {
            return false;
          }
          
          impl_.getCurrentProject().setFlowResults(null);
        }
        
        return true;
      }
    });
    
    return editor;
  }



  public PivOriginalView getOriginalView() {
    if (origView_==null) {
      origView_=new PivOriginalView(this);
    }
    return origView_;
  }
  
  public PivTransfView getTransfView() {
    if (transfView_==null) {
      transfView_=new PivTransfView(this);
    }
    return transfView_;
  }
  
  public PivRealView getRealView() {
    if (realView_==null) {
      realView_=new PivRealView(this);
    }
    return realView_;
  }
  
  private PivViewI[] getViews() {
    return new PivViewI[]{getOriginalView(),getTransfView(),getRealView()};
  }

  /**
   * Definit le projet courant.
   * @param _prj Le projet.
   */
  public void setProjet(PivProject _prj) {
    projet=_prj;
    for (PivViewI view : getViews()) {
      view.setProject(_prj);
    }
    
    // -1 : pour forcer la mise a jour de la vue.
    viewMode_=-1;
    
    initCommonLayers();
    
    if (projet.hasRawVelocities())
      setViewMode(MODE_REAL_VIEW);
    else if (projet.hasTransfImages())
      setViewMode(MODE_TRANSF_VIEW);
    else
      setViewMode(MODE_ORIGINAL_VIEW);
    
    cbRep_.setSelectedIndex(projet.getTransformationParameters().isCurrentSystemInitial()?1:0);
  }

  /**
   * Retourne le projet courant, dont les donn�es dsont affich�es dans le panneau.
   * @return Le projet.
   */
  public PivProject getProject() {
    return projet;
  }

  /**
   * Met a jour le panneau en fonction du projet.
   * @param _prop La propri�t� modifi�e du projet.
   */
//  public void refresh(String _prop) {
//
//    getVueCalque().repaint();
//  }

  /**
   * Initialise les calques contenus dans le panneau de visualisation.
   */
  public void initCommonLayers() {
    removeAllCalqueDonnees();
    
    addCqInfos();
    buildEditionLayer();
  }
  
  /**
   * Retourne les propri�tes graphiques des calques.
   * @return Les propri�tes pour tous les calques
   */
   public Map<String,EbliUIProperties> getLayerProperties() {
     Map<String,EbliUIProperties> props=new HashMap<String,EbliUIProperties>();
     for (PivViewI view : getViews()) {
       props.putAll(view.saveLayerProperties());
     }
     return props;
   }
   
   /**
   * Restaure les propri�tes graphiques des calques.
   * @param _props Les propri�tes pour tous les calques
    */
   public void setLayerProperties(Map<String,EbliUIProperties> _props) {
     if (_props==null) return;
     
     for (PivViewI view : getViews()) {
       view.restoreLayerProperties(_props);
     }
   }

  /**
   * Construit les actions du panneau. Ces actions peuvent intervenir sur
   * le rendu du panneau (choix de la vue, etc.).
   */
  protected void buildActions() {
    actComputeGrid_=new PivDefineGridAction(this);
    actShowVelocities_=new PivShowVelocityAction((PivImplementation)getCtuluUI());
    actEdit_=new PivEditAction(this);
    actTransfMatrix_=new PivChangeCoordinatesSystemAction((PivImplementation)getCtuluUI());
    getScene().addSelectionListener(actEdit_);
    // Les actions affich�es dans le menu contextuel, pour toutes les vues.
    getEditor().getSceneEditor().setActions(new EbliActionInterface[]{actEdit_, realView_.getInvertTransectAction(), null});
  }

  protected void buildSpecificTools() {
    // La liste des rep�res
    cbRep_=new JComboBox<>();
    cbRep_.setToolTipText(PivResource.getS("Changement de rep�re"));
    cbRep_.addItem(PivResource.getS("Rep�re de calcul"));
    cbRep_.addItem(PivResource.getS("Rep�re initial"));
    cbRep_.setPreferredSize(new Dimension(120, cbRep_.getPreferredSize().height));
    cbRep_.setMaximumSize(cbRep_.getPreferredSize());
    cbRep_.addItemListener(new ItemListener() {
      
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (projet != null) {
          PivTransformationParameters params=projet.getTransformationParameters();
          params.setCurrentSystemInitial(cbRep_.getSelectedIndex() != 0);
          projet.setTransformationParameters(params);
          restaurer();
        }
      }
    });
  }
  
  /**
   * Retourne l'action pour mettre la vue en espace d'origine.
   * @return L'action.
   */
  public EbliActionAbstract getOriginalViewAction() {
    return getOriginalView().getActivationAction();
  }

  /**
   * Retourne l'action pour mettre la vue en espace r�el.
   * @return L'action.
   */
  public EbliActionAbstract getRealViewAction() {
    return getRealView().getActivationAction();
  }

  /**
   * Retourne l'action pour mettre la vue en espace image transform�e.
   * @return L'action.
   */
  public EbliActionAbstract getTransfViewAction() {
    return getTransfView().getActivationAction();
  }

  /**
   * Retourne l'action pour la saisie des points de r�f�rence.
   * @return L'action.
   */
  public PivOrthoGRPAction getOrthoGRPAction() {
    return getOriginalView().getOrthoGRPAction();
  }

  /**
   * Retourne l'action pour la saisie du contour de grille.
   * @return L'action.
   */
  public PivDefineGridAction getGridDefinitionAction() {
    return actComputeGrid_;
  }

  /**
   * Retourne l'action pour la saisie d'un transect.
   * @return L'action.
   */
  public PivNewTransectAction getCreateTransectXYZAction() {
    return getRealView().getCreateTransectXYZAction();
  }

  /**
   * Retourne l'action pour la saisie d'un transect.
   * @return L'action.
   */
  public PivCreateTransectAbsZAction getCreateTransectAbsZAction() {
    return getRealView().getCreateTransectAbsZAction();
  }

  /**
   * Retourne l'action pour voir les vitesses.
   * @return L'action.
   */
  public PivShowVelocityAction getShowVelocitiesAction() {
    return actShowVelocities_;
  }

  /**
   * Retourne l'action pour �diter une g�om�trie.
   * @return L'action.
   */
  public PivEditAction getEditAction() {
    return actEdit_;
  }
  
  public SceneRotationAction getRotationAction() {
    if (actRotation_==null) {
      actRotation_=new SceneRotationAction(getArbreCalqueModel().getTreeSelectionModel(), gisEditor_, gisEditor_.getSceneEditor(),getEbliFormatter()) {

        /**
         * Surchag� pour d�finir le desktop au moment de l'affichage.
         */
        @Override
        public void showWindow() {
          // Necessaire, sinon la palette est visualis�e en externe. Ne peut �tre fait avant, car le desktop n'est pas
          // encore existant.
          BuDesktop desk=(BuDesktop)SwingUtilities.getAncestorOfClass(BuDesktop.class,PivVisuPanel.this);
          if (desk!=null)
            setDesktop(desk);
          super.showWindow();
        }
      };
    }

    return actRotation_;
  }
  
  public SceneDeplacementAction getDeplacementAction() {
    if (actDeplacement_==null) {
      actDeplacement_=new SceneDeplacementAction(getArbreCalqueModel().getTreeSelectionModel(), gisEditor_, getEbliFormatter(), null) {

        /**
         * Surchag� pour d�finir le desktop au moment de l'affichage.
         */
        @Override
        public void showWindow() {
          // Necessaire, sinon la palette est visualis�e en externe. Ne peut �tre fait avant, car le desktop n'est pas
          // encore existant.
          BuDesktop desk=(BuDesktop)SwingUtilities.getAncestorOfClass(BuDesktop.class,PivVisuPanel.this);
          if (desk!=null)
            setDesktop(desk);
          super.showWindow();
        }
      };
    }

    return actDeplacement_;
  }
  
  /**
   * @return Le calque d'edition de formes.
   */
  public ZCalqueEditionInteraction getEditionLayer() {
    return cqEdition_;
  }
  
  /**
   * Cr�e un source pour l'animation.
   * @return La source
   */
  public EbliAnimationSourceInterface createAnimSource() {
    return new AnimAdapter();
  }
  
  public PivViewI getCurrentView() {
    return getViews()[viewMode_];
  }

  /**
   * Switch les calques de la vue suivant le type de vue attendu.
   * @param _mode Le mode pour la vue parmi {@link #MODE_ORIGINAL_VIEW},
   * {@link #MODE_REAL_VIEW}, {@link #MODE_TRANSF_VIEW}
   */
  public void setViewMode(int _mode) {
    if (_mode==viewMode_) return;
    
    BuSpecificBar sb=((PivImplementation)getCtuluUI()).getSpecificBar();
    if (viewMode_!=-1) {
      JComponent[] oldCmps=getSpecificTools();
      sb.removeTools(oldCmps);
    }
    
    viewMode_=_mode;
    
    removeAllCalqueDonnees();
    getCqLegend().enleveTousCalques();
    
    for (BCalque cq : getCurrentView().getLayers()) {
      addCalque(cq);
      addToLegend(cq);
    }
    
    // Ouvre l'arbre
    ((PivImplementation)getCtuluUI()).get2dFrame().getArbreCalque().expandAllTree(true);
    
    getCurrentView().getActivationAction().putValue(Action.SELECTED_KEY,true);
    
    setCoordinateDefinitions(getCurrentView().getCoordinateDefinitions());
    ((PivImplementation)getCtuluUI()).get2dFrame().changeTitle(getCurrentView().getTitle());
    sb.addTools(getSpecificTools());
    sb.repaint();
    
    restaurer();
    getVueCalque().revalidate();
    getVueCalque().repaint();
  }

  /**
   * Ajoute un calque au calque des legendes
   * @param _cq Le calque. Seul les calques affichage sont ajout�s.
   */
  protected void addToLegend(BCalque cq) {
    // Pas terrible, mais ca marche. Il serait mieux de pouvoir ajouter le
    // calque au calque de l�gende par getCqLegend().ajoute(calque);
    
    if (cq instanceof BGroupeCalque) {
      BGroupeCalque gc = (BGroupeCalque)cq;
      for (BCalque cqChild : gc.getCalques()) {
        addToLegend(cqChild);
      }
    }
    else if (cq instanceof BCalqueAffichage) {
      ((BCalqueAffichage) cq).setLegende(null);
      ((BCalqueAffichage) cq).setLegende(getCqLegend());
    }
  }
  
  /**
   * Rend visible le calque de controle des points d'orthorectification.
   * @param _b True : Le calque est visible.
   */
  public void setControlPointsLayerVisible(boolean _b) {
    getRealView().setControlPointsLayerVisible(_b);
  }
  
  public void setScalingPointsLayerVisible(boolean _b) {
    getOriginalView().setScalingPointsLayerVisible(_b);
  }
  
  public void setOrthoPointsLayerVisible(boolean _b) {
    getOriginalView().setOrthoPointsLayerVisible(_b);
  }

  /**
   * Rend visible le calque des vitesses.
   * @param _b True : Le calque est visible.
   */
  public void setAveVelocitiesLayerVisible(boolean _b) {
    getRealView().setAveVelocitiesLayerVisible(_b);
  }

  /**
   * Rend visible le calque des vitesses.
   * @param _b True : Le calque est visible.
   */
  public void setFilteredVelocitiesLayerVisible(boolean _b) {
    getRealView().setFilteredVelocitiesLayerVisible(_b);
  }

  /**
   * Rend visible le calque des vitesses.
   * @param _b True : Le calque est visible.
   */
  public void setRawVelocitiesLayerVisible(boolean _b) {
    getRealView().setRawVelocitiesLayerVisible(_b);
  }

  /**
   * Rend visible le calque des vitesses.
   * @param _b True : Le calque est visible.
   */
  public void setManualVelocitiesLayerVisible(boolean _b) {
    getRealView().setManualVelocitiesLayerVisible(_b);
  }
  
  /**
   * Rend visible le calque des d�bits.
   * @param _b True : Le calque est visible.
   */
  public void setFlowLayerVisible(boolean _b) {
    getRealView().setFlowLayerVisible(_b);
  }

  /**
   * Rend visible le calque image.
   * @param _b True : Le calque est visible.
   */
  public void setImageLayerVisible(boolean _b) {
    getRealView().setImageLayerVisible(_b);
  }

  /**
   * Retourne la visibilit� du calque image.
   * @return True : Le calque est visible.
   */
  public boolean isImageLayerVisible() {
    return getRealView().isImageLayerVisible();
  }

  /**
   * Retourne le mode de vue.
   * @return Le mode de vue parmi {@link #MODE_ORIGINAL_VIEW},
   * {@link #MODE_REAL_VIEW}, {@link #MODE_TRANSF_VIEW}
   */
  public int getViewMode() {
    return viewMode_;
  }

  /**
   * Construction du calque de saisie de formes.
   */
  protected void buildEditionLayer() {
    if (cqEdition_ == null) {
      cqEdition_=new ZCalqueEditionInteraction();
      cqEdition_.setName("cqEdition");
      cqEdition_.setGele(true);
      cqEdition_.setLineModel(new TraceLigneModel(TraceLigne.LISSE, 1, Color.RED));
      cqEdition_.setIconModel(new TraceIconModel(TraceIcon.PLUS, 5, Color.RED));

      // Ce calque peut �tre ajout� d�s le d�but, il n'apparait pas dans l'arbre de calques
      getController().addCalqueInteraction(cqEdition_);
    }
  }

  /**
   * Necessaire m�me si aucune action sinon plantage lors de l'ouverture
   * de l'internal Frame.
   *
   * @return Les actions.
   */
  @Override
  public EbliActionInterface[] getApplicationActions() {
    return new EbliActionInterface[0];
  }
  
  /**
   * @return Les actions sp�cifiques de la vue affich�es dans la barre d'actions.
   */
  public JComponent[] getSpecificTools() {
    BuDesktop desk=((PivImplementation)getCtuluUI()).getMainPanel().getDesktop();
    
    JComponent[] cpsView=getCurrentView().getSpecificTools();
    JComponent[] cps=new JComponent[cpsView.length+4];
    System.arraycopy(cpsView, 0, cps, 4, cpsView.length);
    
    if (btRotation_==null)
      btRotation_=getRotationAction().buildToolButton(EbliComponentFactory.INSTANCE);
    if (btDeplacement_==null)
      btDeplacement_=getDeplacementAction().buildToolButton(EbliComponentFactory.INSTANCE);
    if (btTransfRepere_==null)
      btTransfRepere_=actTransfMatrix_.buildToolButton(EbliComponentFactory.INSTANCE);
    
    cps[0]=btRotation_;
    cps[1]=btDeplacement_;
    cps[2]=btTransfRepere_;
    cps[3]=cbRep_;
        
    for (JComponent cp : cps) {
      if (cp instanceof AbstractButton) {
        AbstractButton bt=(AbstractButton)cp;
        if (bt.getAction() instanceof EbliActionPaletteAbstract) {
          ((EbliActionPaletteAbstract)bt.getAction()).setDesktop(desk);
        }
      }
    }
    return cps;
  }
  
  /**
   * Cr�e le menu specifique avec les actions de vues.
   */
  @Override
  protected BuMenu[] createSpecificMenus(final String _title) {
    final BuMenu[] res = new BuMenu[1];
    res[0] = new BuMenu(PivResource.getS("Vue 2D"), "VIEW");
    res[0].setIcon(null);
    
    ButtonGroup bg=new ButtonGroup();
    AbstractButton itOriginalView=getOriginalViewAction().buildMenuItem(EbliComponentFactory.INSTANCE);
    AbstractButton itRealView=getRealViewAction().buildMenuItem(EbliComponentFactory.INSTANCE);
    AbstractButton itTransfView=getTransfViewAction().buildMenuItem(EbliComponentFactory.INSTANCE);
    bg.add(itTransfView);
    bg.add(itRealView);
    bg.add(itOriginalView);
    res[0].add(itOriginalView);
    res[0].add(itRealView);
    res[0].add(itTransfView);
    res[0].addSeparator();
    
    
    res[0].add(BArbreCalque.buildZNormalMenu(modelArbre_));
    fillMenuWithToolsActions(res[0]);
    return res;
  }
}
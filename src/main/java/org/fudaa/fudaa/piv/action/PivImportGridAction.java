/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;
import java.io.File;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.io.PivGridReader;
import org.fudaa.fudaa.piv.metier.PivGrid;
import org.fudaa.fudaa.piv.utils.PivUtils;

/**
 * Une action pour importer les points d'une grille, ou pour les combiner � ceux
 * existants.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivImportGridAction extends EbliActionSimple {
  PivImplementation impl;
  CtuluFileChooser fcGrid;

  /**
   * Constructeur
   * @param _impl L'implementation.
   */
  public PivImportGridAction(PivImplementation _impl) {
    super(PivResource.getS("Import des points de grille..."), null, "IMPORT_GRID");

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Selectionne le fichier d'import, et propose � l'utilisateur de remplacer ou
   * combiner les nouveaux points.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }

    boolean keepPreviousGrid=false;
    if (impl.getCurrentProject().getComputeGrid()!=null) {
      keepPreviousGrid=!impl.question(
              PivResource.getS("Import des points de grille"),
              PivResource.getS("Des points de grille existent d�j�.\nVoulez-vous les supprimer avant import des nouveaux ?"));
    }

    // Selection du fichier
    if (fcGrid == null) {
      fcGrid=new CtuluFileChooser(true);
      fcGrid.setAcceptAllFileFilterUsed(true);
      fcGrid.setFileFilter(PivUtils.FILE_FLT_GRID);
      // Pas terrible, mais evite de redonner le nom manuellement.
      fcGrid.setSelectedFile(new File(PivUtils.FILE_FLT_GRID.getFirstExt()));
      fcGrid.setMultiSelectionEnabled(false);
      fcGrid.setDialogTitle(PivResource.getS("S�lection d'un fichier de grille"));
    }
    
    if (fcGrid.showOpenDialog(impl.getFrame()) == CtuluFileChooser.CANCEL_OPTION) {
      return;
    }
    File gridFile=fcGrid.getSelectedFile();

    CtuluIOResult<PivGrid> ret = new PivGridReader().read(gridFile, null);
    if (ret.getAnalyze().containsErrorOrSevereError()) {
      impl.error(ret.getAnalyze().getResume());
      return;
    }

    PivGrid newGrid=(PivGrid)ret.getSource();
    if (keepPreviousGrid) {
      newGrid.merge(impl.getCurrentProject().getComputeGrid());
    }
    impl.getCurrentProject().setComputeGrid(newGrid);

    impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_TRANSF_VIEW);
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Il doit exister au moins une image transform�e");
  }
}

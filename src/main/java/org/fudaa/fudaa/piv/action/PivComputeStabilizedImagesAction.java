/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivExePanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivProgressionPanel;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivTaskAbstract;
import org.fudaa.fudaa.piv.PivTaskObserverI;
import org.fudaa.fudaa.piv.io.PivExeLauncher;

/**
 * Une action pour lancer le calcul de stabilisation d'images.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivComputeStabilizedImagesAction extends EbliActionSimple {
  PivImplementation impl;
  CtuluDialog diProgress_;

  /**
   * Constructeur.
   * @param _impl L'implementation.
   */
  public PivComputeStabilizedImagesAction(PivImplementation _impl) {
    super(PivResource.getS("Stabiliser les images"), null, "COMPUTE_STAB_IMAGES");
//    setKey(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_MASK | KeyEvent.SHIFT_MASK));

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Lance l'analyse par PIV, dans un thread s�par�.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }
    
    if (!PivExeLauncher.instance().areExeOK()) {
      PivExePanel pnExe = new PivExePanel();

      if (!pnExe.afficheModaleOk(impl.getFrame(), PivResource.getS("R�pertoire contenant les executables"))) {
        return;
      }
    }

    // La tache a ex�cuter.
    PivTaskAbstract r=new PivTaskAbstract(PivResource.getS("Calcul des images")) {

      @Override
      public boolean act(PivTaskObserverI _observer) {
        CtuluLog ana = new CtuluLog();
        ana.setDesc(this.getName());
        PivExeLauncher.instance().computeStabilizedImages(ana, impl.getCurrentProject(), _observer);
        if (ana.containsErrorOrSevereError()) {
          impl.error(ana.getResume());
          return false;
        }

        return true;

        // Lanc� � la fin, car l'interface se bloque si on ne le fait pas.
        // Probl�me de thread swing probablement...
//        SwingUtilities.invokeLater(new Runnable() {
//          public void run() {
////            impl.message(PivResource.getS("Calcul termin�"), PivResource.getS("Le calcul s'est termin� avec succ�s"), false);
////            impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_REAL_VIEW);
////            PivFlowInfoPanel pnInfo=new PivFlowInfoPanel(impl.getCurrentProject().getFlowResults(), impl.getCurrentProject().getOrthoParameters());
////            pnInfo.afficheModale(impl.getFrame(), PivResource.getS("Calcul termin� avec succ�s"), CtuluDialog.OK_OPTION);
//            impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_ORIGINAL_VIEW);
//            impl.get2dFrame().getVisuPanel().setFlowLayerVisible(true);              
//          }
//        });
      }
    };

    PivProgressionPanel pnProgress_=new PivProgressionPanel(r);
    diProgress_ = pnProgress_.createDialog(impl.getParentComponent());
    diProgress_.setOption(CtuluDialog.ZERO_OPTION);
    diProgress_.setDefaultCloseOperation(CtuluDialog.DO_NOTHING_ON_CLOSE);
    diProgress_.setTitle(r.getName());

    r.start();
    diProgress_.afficheDialogModal();
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    if (!impl.getCurrentProject().isStabilizationActive()) {
      impl.error(PivResource.getS("Erreur"), PivResource.getS("Les param�tres de stabilisation ne sont pas d�finis"));
      return false;
    }
    
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Les parametres de stabilisation doivent �tre renseign�s");
  }
}

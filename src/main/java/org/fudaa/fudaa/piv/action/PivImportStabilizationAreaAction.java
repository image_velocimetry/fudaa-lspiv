/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Arrays;

import javax.swing.JFileChooser;

import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivStabilizationParamPanel;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.io.PivFlowAreaReader;
import org.fudaa.fudaa.piv.io.PivGRPReader;
import org.fudaa.fudaa.piv.io.PivStabAreasReader;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;
import org.fudaa.fudaa.piv.metier.PivStabilizationParameters;
import org.fudaa.fudaa.piv.utils.PivUtils;

/**
 * Une action pour importer les zones pour la stabilisation.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivImportStabilizationAreaAction extends EbliActionSimple {
  PivImplementation impl;
  PivStabilizationParamPanel pn_;
  CtuluFileChooser fcStabAreas;

  /**
   * Constructeur
   * @param _impl L'implementation.
   */
  public PivImportStabilizationAreaAction(PivImplementation _impl, PivStabilizationParamPanel _pn) {
    super(PivResource.getS("Importer..."), null, "IMPORT_AREA");
    setDefaultToolTip(PivResource.getS("Importe les zones et remplace celles existantes"));

    impl=_impl;
    pn_ = _pn;
    setEnabled(false);
  }

  /**
   * Affiche une boite de dialogue pour permettre de selectionner le fichier
   * d'import.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }

    // Selection du fichier
    if (fcStabAreas == null) {
      fcStabAreas=new CtuluFileChooser(true);
      fcStabAreas.setAcceptAllFileFilterUsed(true);
      fcStabAreas.setFileFilter(PivUtils.FILE_FLT_STAB_AREAS);
      // Pas terrible, mais evite de redonner le nom manuellement.
      fcStabAreas.setSelectedFile(new File(PivUtils.FILE_FLT_STAB_AREAS.getFirstExt()));
      fcStabAreas.setMultiSelectionEnabled(false);
      fcStabAreas.setDialogTitle(PivResource.getS("S�lection d'un fichier zones de stabilisation"));
    }
    
    if (fcStabAreas.showOpenDialog(impl.getFrame()) == JFileChooser.CANCEL_OPTION) {
      return;
    }
    File stabPolyFile=fcStabAreas.getSelectedFile();

    CtuluIOResult<PivStabilizationParameters> ret = new PivStabAreasReader().read(stabPolyFile, null);
    if (ret.getAnalyze().containsErrorOrSevereError()) {
      impl.error(ret.getAnalyze().getResume());
      return;
    }
    
    pn_.setAreas(ret.getSource().getOutlines());
    pn_.setFlowAreas(ret.getSource().isFlowAreas());
    
    pn_.fireParametersChanged();
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Il doit exister au moins une image source");
  }
}

/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivOrthoParamPanel;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;
import org.fudaa.fudaa.piv.metier.PivProject.OrthoMode;

/**
 * Une action pour saisir les parametres de l'orthorectification.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivDefineOrthoParamAction extends EbliActionSimple {
  PivImplementation impl;

  /**
   * Constructeur.
   * @param _impl L'implementation.
   */
  public PivDefineOrthoParamAction(PivImplementation _impl) {
    super(PivResource.getS("Param�tres de transformation..."), null, "DEFINE_ORTHO_PARAM");
    impl=_impl;
    setEnabled(false);
  }

  public String getEnableCondition() {
    return PivResource.getS("Il doit exister au moins une image source");
  }

  /**
   * Affichage du panneau des param�tres d'orthorectification.
   * @param _e L'evenement pour l'action.
   */
  public void actionPerformed(final ActionEvent _e) {
    // On ne saisit les donn�es que dans le rep�re de calcul
    if (impl.getCurrentProject().getTransformationParameters().isCurrentSystemInitial()) {
      impl.error(PivResource.getS("Erreur"), PivResource.getS("Les param�tres ne peuvent �tre saisis\nque dans le rep�re de calcul."));
      return;
    }
    
    if (!isValide()) return;
    
    impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_REAL_VIEW);

    PivOrthoParamPanel pnParams=new PivOrthoParamPanel(impl);

    // La fenetre est affich�e en mode non modal, le traitement est fait dans la
    // fenetre elle meme.
    String mode = impl.getCurrentProject().getOrthoMode() == OrthoMode.ORTHO_2D ? PivResource.getS("Ortho 2D"):PivResource.getS("Ortho 3D");
    pnParams.affiche(impl.getFrame(), PivResource.getS("Param�tres de transformation") + " (" + mode + ")",CtuluDialog.OK_CANCEL_APPLY_OPTION);
  }
  
  public boolean isValide() {
    String mes=PivOrthoPoint.areOrthoPointsOk(impl.getCurrentProject().getOrthoPoints());
    if (mes!=null) {
      impl.error(PivResource.getS("Erreur"), mes);
      return false;
    }
    return true;
  }
}

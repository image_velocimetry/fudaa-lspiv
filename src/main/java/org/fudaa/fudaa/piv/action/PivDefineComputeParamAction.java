/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivComputeParamPanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.metier.PivComputeParameters;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;

/**
 * Une action pour saisir les parametres de calcul.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivDefineComputeParamAction extends EbliActionSimple {
  PivImplementation impl;
  
  public PivDefineComputeParamAction(PivImplementation _impl) {
    super(PivResource.getS("Paramètres de calcul..."), null, "DEFINE_PIV_PARAM");
    impl=_impl;
    setEnabled(false);
  }

  /**
   * Affiche le panneau des paramètres de calcul.
   * @param _e L'evenement pour l'action.
   */
  public void actionPerformed(final ActionEvent _e) {
    impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_TRANSF_VIEW);
    
    PivComputeParamPanel pn=new PivComputeParamPanel(impl);
    
    PivOrthoParameters orthoParams = impl.getCurrentProject().getOrthoParameters();
    if (orthoParams!=null)
      pn.setResolution(orthoParams.getResolution());
    
    PivComputeParameters params=new PivComputeParameters(impl.getCurrentProject().getComputeParameters());
    pn.setComputeParams(params);

    pn.affiche(impl.getFrame(), PivResource.getS("Paramètres de calcul"), CtuluDialog.OK_CANCEL_APPLY_OPTION);
  }

  public String getEnableCondition() {
    return PivResource.getS("Il doit exister au moins une image transformée");
  }
}

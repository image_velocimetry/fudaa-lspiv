/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivScalingParamPanel;
import org.fudaa.fudaa.piv.PivVisuPanel;

/**
 * Une action pour selectionner et reconditionner les images sources.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivSelectImagesAction.java 9500 2017-01-09 17:22:31Z bmarchan $
 */
public class PivDefineParamScalingAction extends EbliActionSimple {
  PivImplementation impl;
  PivScalingParamPanel pnParams;
  CtuluDialog diProgress_;

  /**
   * Constructeur.
   * @param _impl L'implementation.
   */
  public PivDefineParamScalingAction(PivImplementation _impl) {
    super(PivResource.getS("Param�tres..."), null, "SCALING_PARAMS");

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Selectionne les images, pour les reconditionne dans un thread s�par�.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }
    
    impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_ORIGINAL_VIEW);

    // Selection des images
    pnParams = new PivScalingParamPanel(impl);
    pnParams.affiche(impl.getFrame(), PivResource.getS("Param�tres") + " (" + PivResource.getS("Mise � l'�chelle") + ")", CtuluDialog.OK_CANCEL_APPLY_OPTION);
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Il doit exister au moins une image source");
  }
}

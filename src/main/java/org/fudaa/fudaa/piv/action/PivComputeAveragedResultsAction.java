/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivAverageFilteredResultsPanel;
import org.fudaa.fudaa.piv.PivExePanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.io.PivExeLauncher;

/**
 * Une action pour moyenner les r�sultats filtr�s
 * instantan�s.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivComputeAveragedResultsAction extends EbliActionSimple {
  PivImplementation impl;

  /**
   * Constructeur.
   * @param _impl L'implementation.
   */
  public PivComputeAveragedResultsAction(PivImplementation _impl) {
    super(PivResource.getS("Moyenne des r�sultats instantan�s filtr�s..."), null, "COMPUTE_AVERAGE");

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Lance l'analyse par PIV, dans un thread s�par�.
   * @param _e L'evenement pour l'action.
   */
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }
    
    if (!PivExeLauncher.instance().areExeOK()) {
      PivExePanel pnExe = new PivExePanel();

      if (!pnExe.afficheModaleOk(impl.getFrame(), PivResource.getS("R�pertoire contenant les executables"))) {
        return;
      }
    }
    
    PivAverageFilteredResultsPanel pn=new PivAverageFilteredResultsPanel(impl);
    pn.setSelectedResults(impl.getCurrentProject().getUsedInstantResults());
    if (!pn.afficheModaleOk(impl.getFrame(), PivResource.getS("Moyenne des r�sultats instantan�s filtr�s"), CtuluDialog.OK_CANCEL_OPTION)) {
      return;
    }
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    if (impl.getCurrentProject().getInstantFilteredResults()==null) {
      impl.error(PivResource.getS("Erreur"), PivResource.getS("Le projet ne contient aucune valeur de r�sultats instantan�s filtr�s"));
      return false;
    }

    return true;
  }

  public String getEnableCondition() {
    return PivResource.getS("Des r�sultats instantan�s filtr�s doivent exister");
  }
}

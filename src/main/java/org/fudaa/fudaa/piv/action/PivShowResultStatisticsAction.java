/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivFlowInfoPanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivStatisticsInfoPanel;

/**
 * Une action pour afficher les statistiques sur les r�sultats de calcul.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivComputeFlowAction.java 9133 2015-06-16 16:36:57Z bmarchan $
 */
public class PivShowResultStatisticsAction extends EbliActionSimple {
  PivImplementation impl;
  CtuluDialog diProgress_;

  /**
   * Constructeur.
   * @param _impl L'implementation.
   */
  public PivShowResultStatisticsAction(PivImplementation _impl) {
    super(PivResource.getS("Afficher les statistiques r�sultats..."), null, "SHOW_RESULTS_STATS");

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Affiche la fenetre resultats.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }
    
    PivStatisticsInfoPanel pnInfo=new PivStatisticsInfoPanel(impl);
    pnInfo.afficheModale(impl.getFrame(), PivResource.getS("Statistiques r�sultats"), CtuluDialog.OK_OPTION);
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    if (impl.getCurrentProject().getInstantRawResults()==null) {
      impl.error(PivResource.getS("Erreur"), PivResource.getS("Il n'existe pas de r�sultats bruts de calcul"));
      return false;
    }
    
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Les r�sultats bruts de calcul doivent exister");
  }
}

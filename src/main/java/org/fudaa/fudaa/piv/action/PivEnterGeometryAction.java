/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.Container;

import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.SwingUtilities;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteraction;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteractionTargetI;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivSimplifiedEditionPalette;
import org.fudaa.fudaa.piv.PivVisuPanel;

import com.memoire.bu.BuDesktop;

/**
 * Une action pour saisir une g�om�trie (ligne, polyligne, ...).
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id: PivDefineAreaCenterAction.java 6709 2011-11-24 16:17:00Z bmarchan $
 */
public class PivEnterGeometryAction extends EbliActionPaletteAbstract {

  private PivVisuPanel pnCalques_;
  /** Le calque d'�dition. */
  private ZCalqueEditionInteraction cqEdition_;
  /** Le controller d'edition */
  private ZCalqueEditionInteractionTargetI controller_;
  /** La palette pour la reprise. */
  private PivSimplifiedEditionPalette palette_;
  /** La forme pour la saisie */
  private int form_ = DeForme.LIGNE_BRISEE;
  /** Le bouton de fin est-il visible */
  private boolean isEndButtonVisible_ = true;
  

  /**
   * Constructeur.
   * @param _pn Le panneau des calques.
   * @param _form La geometrie a saisir.
   */
  public PivEnterGeometryAction(PivVisuPanel _pn, String _text, String _tooltip, boolean _isEndButtonVisible) {
    super(_text, null, "DEFINE_GEOMETRY");
    setDefaultToolTip(_tooltip);
    pnCalques_=_pn;
    isEndButtonVisible_ = _isEndButtonVisible;
    
    // Pour ecouter le statut du calque d'edition
//    listener_=new PropertyChangeListener() {
//      @Override
//      public void propertyChange(PropertyChangeEvent evt) {
//        setSelected(!(Boolean)evt.getNewValue());
//      }
//    };
    
    setCalqueInteraction(_pn.getEditionLayer());
    setPaletteResizable(true);
  }
  
  /**
   * Deifnit la forme a saisir.
   * @param _form
   */
  public void setGeometryForm(int _form) {
    form_ = _form;
    if (cqEdition_ != null) {
      cqEdition_.setTypeForme(form_);
    }
  }

  /**
   * Definit le calque d'interaction associ� � l'action.
   * @param _cq Le calque. Peut �tre <tt>null</tt>.
   */
  private void setCalqueInteraction(ZCalqueEditionInteraction _cq) {
//    if (cqEdition_==_cq) return;
//    
//    if (cqEdition_!=null)
//      cqEdition_.removePropertyChangeListener("gele", listener_);
    
    cqEdition_=_cq;
    
//    if (cqEdition_!=null)
//      cqEdition_.addPropertyChangeListener("gele", listener_);
  }
  
  /**
   * @return Le calque d'interaction li� � cette action.
   */
  public ZCalqueEditionInteraction getCalqueInteraction() {
    return cqEdition_;
  }
  
  /**
   * Definit le controlleur d'edition.
   * @param _ctrl Le controlleur.
   */
  public void setEditionController(ZCalqueEditionInteractionTargetI _ctrl) {
    controller_=_ctrl;
  }

  /**
   * Surcharge pour activer/desactiver le calque d'edition.
   */
  @Override
  public void changeAction() {
//    if (isSelected())
//      pnCalques_.setViewMode(PivVisuPanel.MODE_ORIGINAL_VIEW);
    
//      cqEdition_.setListener(controller_);
//      cqEdition_.setTypeForme(DeForme.LIGNE_BRISEE);
//      cqEdition_.setFormEndedByDoubleClic(true);
//      pnCalques_.setCalqueInteractionActif(cqEdition_);
//    }
//    else {
//      pnCalques_.unsetCalqueInteractionActif(cqEdition_);
//      cqEdition_.cancelEdition();
//    }
    super.changeAction();
  }

  /**
   * Surchag� pour d�finir le desktop au moment de l'affichage.
   */
  @Override
  public void showWindow() {
    // Necessaire, sinon la palette est visualis�e en externe. Ne peut �tre fait avant, car le desktop n'est pas
    // encore existant.
    setDesktop((BuDesktop)SwingUtilities.getAncestorOfClass(BuDesktop.class,pnCalques_));
    super.showWindow();
  }
  
  /**
   * Surcharg� pour rendre inactif le calque d'�dition apr�s que la palette a
   * �t� ferm�e.
   */
  @Override
  public void hideWindow() {
    pnCalques_.unsetCalqueInteractionActif(cqEdition_);
    cqEdition_.cancelEdition();
    setSelected(false);

    super.hideWindow();
  }

  /**
   * Surcharg� pour activer le calque d'�dition apr�s affichage.
   */
  @Override
  public void doAfterDisplay() {
    cqEdition_.setListener(controller_);
    cqEdition_.setTypeForme(form_);
    cqEdition_.setFormEndedByDoubleClic(true);
    pnCalques_.setCalqueInteractionActif(cqEdition_);
    palette_.getButton(PivSimplifiedEditionPalette.BUTTON_END).setEnabled(false);
  }

  @Override
  protected JComponent buildContentPane() {
    if (palette_==null){
      // Cr�ation de l'internal frame de reprise
      palette_=new PivSimplifiedEditionPalette(pnCalques_, PivResource.getS("Points"));
      palette_.setCalqueInteraction(cqEdition_);
      palette_.setButtonVisible(PivSimplifiedEditionPalette.BUTTON_END, isEndButtonVisible_);
      // Ecoute de la fen�tre principale pour pouvoir se r�duire avec elle
      final Container c = SwingUtilities.getAncestorOfClass(JInternalFrame.class, pnCalques_);
      
      if (c instanceof JInternalFrame) {
        ((JInternalFrame) c).addInternalFrameListener(new InternalFrameAdapter() {
          @Override
          public void internalFrameActivated(final InternalFrameEvent _e) {
            if(window_!=null)
              window_.setVisible(true);
          }
          @Override
          public void internalFrameDeactivated(final InternalFrameEvent _e) {
            if(window_!=null)
              window_.setVisible(false);
          }
        });
      }
    }
    return palette_.getComponent();
  }
}

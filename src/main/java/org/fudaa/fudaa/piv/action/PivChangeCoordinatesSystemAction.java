package org.fudaa.fudaa.piv.action;

import javax.swing.JComponent;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.commun.EbliFormatter;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.piv.PivCoordinatesSystemPanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;

public class PivChangeCoordinatesSystemAction extends EbliActionPaletteAbstract {
  PivCoordinatesSystemPanel pn_;
  PivImplementation impl_;
  
  public PivChangeCoordinatesSystemAction(PivImplementation _impl) {
    super(PivResource.getS("Rep�re"), EbliResource.EBLI.getIcon("repere"), "CHANGE_TRANSF");
    setDefaultToolTip(PivResource.getS("Transformations du rep�re initial vers le rep�re de calcul"));
    impl_=_impl;
    
    setPaletteResizable(true);
  }

  @Override
  protected JComponent buildContentPane() {
    return getPanel();
  }
  
  public PivCoordinatesSystemPanel getPanel() {
    if (pn_==null) {
      EbliFormatter fmt=new EbliFormatter();
      fmt.setFmt(CtuluLib.getDecimalFormat(2));
      pn_=new PivCoordinatesSystemPanel(fmt);
    }
    return pn_;
  }

  @Override
  public void updateBeforeShow() {
    super.updateBeforeShow();
    pn_.setProject(impl_.getCurrentProject());
  }
}

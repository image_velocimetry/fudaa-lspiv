/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.ZSelectionEvent;
import org.fudaa.ebli.calque.ZSelectionListener;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivTransectParamPanel;

/**
 * Une action pour saisir les parametres de calcul de débit des transects selectectionnés.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivTransectParamAction extends EbliActionSimple implements ZSelectionListener {
  PivImplementation impl;
  
  public PivTransectParamAction(PivImplementation _impl) {
    super(PivResource.getS("Paramètres du transect..."), null, "DEFINE_FLOW_PARAM");
    impl=_impl;
    setEnabled(false);
  }

  /**
   * Affiche le panneau des paramètres de calcul.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    int[] selIds=impl.get2dFrame().getVisuPanel().getScene().getLayerSelection().getSelectedIndex();
    
    PivTransectParamPanel pn=new PivTransectParamPanel(impl);
    pn.setSelected(selIds);

    pn.afficheModale(impl.getFrame(), PivResource.getS("Paramètres du transect"),CtuluDialog.OK_CANCEL_APPLY_OPTION);
  }
  
  @Override
  public String getEnableCondition() {
    return PivResource.getS("Sélectionner un ou plusieurs transects");
  }

  @Override
  public void selectionChanged(ZSelectionEvent _evt) {
    boolean b=false;
    BCalque cq=impl.get2dFrame().getVisuPanel().getCalqueActif();

    if (cq==impl.get2dFrame().getVisuPanel().getRealView().getTransectLayer() && 
        ((ZCalqueLigneBriseeEditable)cq).getLayerSelection() !=null &&
        !((ZCalqueLigneBriseeEditable)cq).getLayerSelection().isEmpty()) {
      b=true;
    }
    super.setEnabled(b);
  }
}

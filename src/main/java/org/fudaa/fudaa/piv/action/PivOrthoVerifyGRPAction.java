/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivExePanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivOrthoVerifyGRPPanel;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;

/**
 * Une action pour verifier les points de r�f�rence au sol.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivOrthoVerifyGRPAction extends EbliActionSimple {
  PivImplementation impl;

  /**
   * Constrcuteur.
   * @param _impl L'implementation.
   */
  public PivOrthoVerifyGRPAction(PivImplementation _impl) {
    super(PivResource.getS("Verification des points de r�f�rence..."), null, "VERIFY_GRP");
    impl=_impl;
    setEnabled(false);
  }

  /**
   * Lancement de la verification des points de r�f�rence sous forme de thread
   * s�par�, puis affichage du calque correspondant.
   * @param _e L'evenement pour l'action.
   */
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }

    impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_REAL_VIEW);

    if (!PivExeLauncher.instance().areExeOK()) {
      PivExePanel pnExe = new PivExePanel();

      if (!pnExe.afficheModaleOk(impl.getFrame(), PivResource.getS("R�pertoire contenant les executables"))) {
        return;
      }
    }

    CtuluLog ana=new CtuluLog();
    ana.setDesc(PivResource.getS("V�rification des points d'orthorectification"));
//    PivExeLauncher.instance().computeOrthoCoefs(ana, impl.getCurrentProject(),null);
//    if (ana.containsErrorOrSevereError()) {
//      impl.error(ana.getResume());
//      return;
//    }
    PivExeLauncher.instance().computeVerifOrtho(ana, impl.getCurrentProject(),null);
    if (ana.containsErrorOrSevereError()) {
      impl.error(ana.getResume());
      return;
    }

    PivOrthoVerifyGRPPanel pn=new PivOrthoVerifyGRPPanel(impl);
    impl.get2dFrame().getVisuPanel().setControlPointsLayerVisible(true);
    pn.affiche(impl.getFrame(),PivResource.getS("V�rification des points de r�f�rence"), CtuluDialog.OK_OPTION);
  }

  public boolean isValide() {
    if (impl.getCurrentProject().getOrthoPoints()==null) {
      impl.error(PivResource.getS("Erreur"), PivResource.getS("Aucun point de r�f�rence au sol n'a �t� d�fini"));
      return false;
    }
    String mes=PivOrthoPoint.areOrthoPointsOk(impl.getCurrentProject().getOrthoPoints());
    if (mes!=null) {
      impl.error(PivResource.getS("Erreur"), mes);
      return false;
    }

    return true;
  }

  public String getEnableCondition() {
    return PivResource.getS("Les points de r�f�rence doivent avoir �t� saisis");
  }
}

/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.piv.PivExePanel;
import org.fudaa.fudaa.piv.PivFlowInfoPanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivProgressionPanel;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivTaskAbstract;
import org.fudaa.fudaa.piv.PivTaskObserverI;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;
import org.fudaa.fudaa.piv.metier.PivTransect;

/**
 * Une action pour lancer le calcul de d�bit.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivComputeFlowAction extends EbliActionSimple {
  PivImplementation impl;
  CtuluDialog diProgress_;

  /**
   * Constructeur.
   * @param _impl L'implementation.
   */
  public PivComputeFlowAction(PivImplementation _impl) {
    super(PivResource.getS("Calcul du d�bit"), null, "COMPUTE_FLOW");
    setKey(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_MASK | KeyEvent.SHIFT_MASK));

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Lance l'analyse par PIV, dans un thread s�par�.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }
    
    if (!PivExeLauncher.instance().areExeOK()) {
      PivExePanel pnExe = new PivExePanel();

      if (!pnExe.afficheModaleOk(impl.getFrame(), PivResource.getS("R�pertoire contenant les executables"))) {
        return;
      }
    }

    final boolean recond;
    if (impl.question(
        PivResource.getS("Modification des transects initiaux"),
        PivResource.getS("Souhaitez vous remplacer les transects initiaux par ceux calcul�s ?"))) {
      recond=true;
    }
    else {
      recond=false;
    }

    // La tache a ex�cuter.
    PivTaskAbstract r=new PivTaskAbstract(PivResource.getS("Calcul des d�bits")) {

      @Override
      public boolean act(PivTaskObserverI _observer) {
        CtuluLog ana = new CtuluLog();
        ana.setDesc(this.getName());
        PivExeLauncher.instance().computeFlowResults(ana, impl.getCurrentProject(), recond, _observer);
        if (ana.containsErrorOrSevereError()) {
          impl.error(ana.getResume());
          return false;
        }

        // Lanc� � la fin, car l'interface se bloque si on ne le fait pas.
        // Probl�me de thread swing probablement...
        SwingUtilities.invokeLater(new Runnable() {
          public void run() {
//            impl.message(PivResource.getS("Calcul termin�"), PivResource.getS("Le calcul s'est termin� avec succ�s"), false);
//            impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_REAL_VIEW);
            PivFlowInfoPanel pnInfo=new PivFlowInfoPanel(impl.getCurrentProject().getFlowResults(), impl.getCurrentProject().getTransects(), impl.getCurrentProject().getOrthoParameters());
            pnInfo.afficheModale(impl.getFrame(), PivResource.getS("Calcul termin� avec succ�s"), CtuluDialog.OK_OPTION);
            impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_REAL_VIEW);
            impl.get2dFrame().getVisuPanel().setFlowLayerVisible(true);              
          }
        });
        
        return true;
      }
    };

    PivProgressionPanel pnProgress_=new PivProgressionPanel(r);
    diProgress_ = pnProgress_.createDialog(impl.getParentComponent());
    diProgress_.setOption(CtuluDialog.ZERO_OPTION);
    diProgress_.setDefaultCloseOperation(CtuluDialog.DO_NOTHING_ON_CLOSE);
    diProgress_.setTitle(r.getName());

    r.start();
    diProgress_.afficheDialogModal();
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    if (impl.getCurrentProject().getTransects()==null || impl.getCurrentProject().getTransects().length==0) {
      impl.error(PivResource.getS("Erreur"), PivResource.getS("Aucun transect n'a �t� d�fini"));
      return false;
    }
    if (impl.getCurrentProject().getAverageResults()==null) {
      impl.error(PivResource.getS("Erreur"), PivResource.getS("Le projet ne contient pas de r�sultats moyenn�s"));
      return false;
    }
    if (impl.getCurrentProject().getOrthoParameters()==null) {
      impl.error(PivResource.getS("Erreur"), PivResource.getS("Les param�tres d'orthorectification n'ont pas �t� donn�s"));
      return false;
    }
    
    PivTransect[] trans=impl.getCurrentProject().getTransects();
    PivOrthoParameters param=impl.getCurrentProject().getOrthoParameters();
    for (int i=0; i<trans.length; i++) {
      GrPolyligne pl = trans[i].getStraight();
      if (pl.sommet(0).z_ < param.getWaterElevation() || pl.sommet(pl.nombre() - 1).z_ < param.getWaterElevation()) {
        impl.error(PivResource.getS("Erreur"), PivResource.getS("Au moins un point extremit� du transect n�{0} a un Z inf�rieur au niveau d'eau.",(i+1)));
        return false;
      }
    }
    
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Un transect doit �tre d�fini et des r�sultats moyenn�s doivent exister");
  }
}

/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import java.awt.event.ActionEvent;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.piv.PivExePanel;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivManageOriginalFilesPanel;
import org.fudaa.fudaa.piv.PivProgressionPanel;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivTaskAbstract;
import org.fudaa.fudaa.piv.PivTaskObserverI;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.metier.PivProject;

/**
 * Une action pour selectionner et reconditionner les images sources.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivSelectImagesAction extends EbliActionSimple {
  PivImplementation impl;
  PivManageOriginalFilesPanel pnMngImages;
  CtuluDialog diProgress_;

  /**
   * Constructeur.
   * @param _impl L'implementation.
   */
  public PivSelectImagesAction(PivImplementation _impl) {
    super(PivResource.getS("Gestion des images sources..."), null, "SELECT_IMAGES");

    impl=_impl;
    setEnabled(false);
  }

  /**
   * Selectionne les images, pour les reconditionne dans un thread s�par�.
   * @param _e L'evenement pour l'action.
   */
  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (!isValide()) {
      return;
    }

    if (!PivExeLauncher.instance().areExeOK()) {
      PivExePanel pnExe = new PivExePanel();

      // TODO BM : Verifier que les nouvelles images sont compatibles avec les donn�es d'ortho ou message.

      if (!pnExe.afficheModaleOk(impl.getFrame(), PivResource.getS("R�pertoire contenant les executables"))) {
        return;
      }
    }

    // Selection des images
    if (pnMngImages==null)
      pnMngImages = new PivManageOriginalFilesPanel(impl);
    pnMngImages.setRootProject(impl.getCurrentProject().getRoot());
    pnMngImages.setFiles(impl.getCurrentProject().getSrcImageFiles(PivProject.SRC_IMAGE_TYPE.ALL));
    
    Double timeStep=impl.getCurrentProject().getTimeStep();
    pnMngImages.setTimeStep(timeStep);
    
    if (!pnMngImages.afficheModaleOk(impl.getFrame(), PivResource.getS("Gestion des images"), CtuluDialog.OK_CANCEL_OPTION)) {
      return;
    }

    // Le reconditionnement des images
    PivTaskAbstract r=new PivTaskAbstract(PivResource.getS("Traitement des images sources")) {

      @Override
      public boolean act(PivTaskObserverI _observer) {
        CtuluLog ana = new CtuluLog();
        ana.setDesc(getName());
        impl.getCurrentProject().setSrcImagesFiles(pnMngImages.getFiles(), pnMngImages.getFileTypes(), _observer, ana);

        double timeStep = pnMngImages.getTimeStep();
        impl.getCurrentProject().setTimeStep(timeStep);

        if (ana.containsErrorOrSevereError()) {
          impl.error(ana.getResume());
          return false;
        }

        return true;
      }
    };

    PivProgressionPanel pnProgress_=new PivProgressionPanel(r);
    diProgress_ = pnProgress_.createDialog(impl.getParentComponent());
    diProgress_.setOption(CtuluDialog.ZERO_OPTION);
    diProgress_.setDefaultCloseOperation(CtuluDialog.DO_NOTHING_ON_CLOSE);
    diProgress_.setTitle(r.getName());

    r.start();
    diProgress_.afficheDialogModal();
    
    impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_ORIGINAL_VIEW);
    impl.get2dFrame().restaurer();
  }

  /**
   * @return true Si toutes les donn�es sont pr�sentes pour un lancement.
   */
  public boolean isValide() {
    return true;
  }

  @Override
  public String getEnableCondition() {
    return PivResource.getS("Le projet doit �tre cr��");
  }
}

/*
 * @creation     3 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.piv.action;

import org.fudaa.ebli.commun.EbliActionChangeState;
import org.fudaa.fudaa.piv.PivImplementation;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;

/**
 * Une action pour voir les vitesses apr�s calcul en espace r�el.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivShowVelocityAction extends EbliActionChangeState {
  PivImplementation impl;

  /**
   * Constructeur.
   * @param _impl L'implementation.
   */
  public PivShowVelocityAction(PivImplementation _impl) {
    super(PivResource.getS("Voir les vitesses moyennes de surface"), null, "SHOW_VELOCITIES_RESULTS");

    impl=_impl;
    setEnabled(false);
  }


  public String getEnableCondition() {
    return PivResource.getS("Il doit exister des r�sultats de calcul");
  }


  @Override
  public void changeAction() {
    // On bascule dans la bonne vue.
    if (isSelected()) {
      impl.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_REAL_VIEW);
    }
    impl.get2dFrame().getVisuPanel().setAveVelocitiesLayerVisible(isSelected());
  }
}

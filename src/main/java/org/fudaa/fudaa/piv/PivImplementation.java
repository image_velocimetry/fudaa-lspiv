/*
 * @creation 7 juin 07
 * @modification $Date: 2008/05/13 12:10:12 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.swing.AbstractButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluFileChooserTestWritable;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.impression.EbliMiseEnPagePreferencesPanel;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.impl.FudaaLookPreferencesPanel;
import org.fudaa.fudaa.commun.impl.FudaaStartupExitPreferencesPanel;
import org.fudaa.fudaa.piv.action.PivChooseOrthoModeAction;
import org.fudaa.fudaa.piv.action.PivComputeAllAction;
import org.fudaa.fudaa.piv.action.PivComputeAveragedResultsAction;
import org.fudaa.fudaa.piv.action.PivComputeCleaningAction;
import org.fudaa.fudaa.piv.action.PivComputeFilteredResultsAction;
import org.fudaa.fudaa.piv.action.PivComputeFlowAction;
import org.fudaa.fudaa.piv.action.PivComputeRawResultsAction;
import org.fudaa.fudaa.piv.action.PivComputeStabilizedImagesAction;
import org.fudaa.fudaa.piv.action.PivComputeTransfImagesAction;
import org.fudaa.fudaa.piv.action.PivCreateGRPInDistanceModeAction;
import org.fudaa.fudaa.piv.action.PivDefineComputeParamAction;
import org.fudaa.fudaa.piv.action.PivDefineOrthoParamAction;
import org.fudaa.fudaa.piv.action.PivDefineParamScalingAction;
import org.fudaa.fudaa.piv.action.PivDefineParamStabilizationAction;
import org.fudaa.fudaa.piv.action.PivExportExcelResultsAction;
import org.fudaa.fudaa.piv.action.PivExportGaugingReportAction;
import org.fudaa.fudaa.piv.action.PivExportImagesAction;
import org.fudaa.fudaa.piv.action.PivExportSerafinResultsAction;
import org.fudaa.fudaa.piv.action.PivImportComputeParamAction;
import org.fudaa.fudaa.piv.action.PivImportGRPAction;
import org.fudaa.fudaa.piv.action.PivImportGridAction;
import org.fudaa.fudaa.piv.action.PivImportOrthoParamAction;
import org.fudaa.fudaa.piv.action.PivImportTransectAction;
import org.fudaa.fudaa.piv.action.PivImportVideoAction;
import org.fudaa.fudaa.piv.action.PivManageTransformedImagesAction;
import org.fudaa.fudaa.piv.action.PivManualVelocitiesComputeAction;
import org.fudaa.fudaa.piv.action.PivNextImageAction;
import org.fudaa.fudaa.piv.action.PivOrthoVerifyGRPAction;
import org.fudaa.fudaa.piv.action.PivPreviousImageAction;
import org.fudaa.fudaa.piv.action.PivSelectImagesAction;
import org.fudaa.fudaa.piv.action.PivShow3DTransectAction;
import org.fudaa.fudaa.piv.action.PivShowFlowResultsAction;
import org.fudaa.fudaa.piv.action.PivShowResultStatisticsAction;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivProject.OrthoMode;
import org.fudaa.fudaa.piv.metier.PivProjectStateListener;
import org.fudaa.fudaa.piv.utils.PivUtils;

import com.memoire.bu.BuAbstractPreferencesComponent;
import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuBrowserPreferencesPanel;
import com.memoire.bu.BuContainerPreferencesPanel;
import com.memoire.bu.BuDesktopPreferencesPanel;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuLanguagePreferencesPanel;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuMenuBar;
import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuMenuRecentFiles;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuRegistry;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuUserPreferencesPanel;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;

/**
 * La classe principale de mise en place de l'application, de gestion des actions, des �tats de l'interface, etc.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivImplementation extends FudaaCommonImplementation implements PivProjectStateListener {

  protected static BuInformationsSoftware isPIV_ = new BuInformationsSoftware();
  static {
    isPIV_.name = "LSPIV";
    isPIV_.version = "1.10.0";
    isPIV_.date = "2023-08-29";
    isPIV_.rights = PivResource.getS("Tous droits r�serv�s") + ". CEREMA (c)1999-2023";
    isPIV_.license = "GPL2";
    isPIV_.languages = "en,fr";
    isPIV_.authors = new String[] { "Alexandre Hauet, Magali Jodeau, J�r�me Le Coz, Guillaume Bodart",
        "Jean-Baptiste Faure, Bertrand Marchand" };
    isPIV_.contact = "alexandre.hauet@edf.fr, magali.jodeau@edf.fr, jerome.lecoz@inrae.fr";
    isPIV_.http = "http://forge.irstea.fr/projects/fudaa-lspiv";
    isPIV_.man = "index.html";
    isPIV_.citation = PivResource
        .getS("Ichiro Fujita (Prof.), Universit� de Kob�\nMarian Muste, IIHR - Universit� de Iowa");
    isPIV_.update = System.getProperty("piv.update", "https://forge.irstea.fr/projects/fudaa-lspiv/files");

    isPIV_.logo = PivResource.PIV.getIcon("icone-lspiv.png");
    isPIV_.banner = PivResource.PIV.getIcon("banniere.jpg");
  }

  /** Le dernier projet ouvert, null si nouveau */
  private File prjFile;
  /** Le projet en cours. */
  private PivProject project;
  /** La fenetre 2D */
  private PivFille2d piv2dFrame;
  /** Une fenetre de progression */
  private CtuluDialog diProgress;
  /** L'UI est-il modifi� ? */
  private boolean isUIModified_ = false;
  /** Action pour l'export du rapport de jaugeage */
  private PivExportGaugingReportAction actExportGaugingReport_;

  /**
   * Retourne les informations de l'application pour affichage dans "a propos".
   * 
   * @return Les infos.
   */
  public static BuInformationsSoftware informationsSoftware() {
    return isPIV_;
  }

  /**
   * Constructeur.
   */
  public PivImplementation() {
    super();
    useNewHelp_ = false;
    create2dFrame();
  }

  /**
   * Retourne le projet en cours.
   * 
   * @return Le projet.
   */
  public PivProject getCurrentProject() {
    return project;
  }

  /**
   * @return Retourne le nom courant du projet, ou null si le projet n'a pas �t� sauvegard�.
   */
  public File getCurrentProjectFile() {
    return prjFile;
  }

  /**
   * @return Le nom par default pour la sauvegarde. Si le nom courant est null, renvoie un nom par defaut.
   */
  public File getSavedProjectFile() {
    if (getCurrentProjectFile() == null)
      return new File("fudaa-lspiv.lspiv.zip");
    else {
      return getCurrentProjectFile();
    }
  }

  /**
   * Appel�e quand le projet courant est modifi�.
   * 
   * @param _prop La propri�t� modifi�e du projet.
   */
  public void projectStateChanged(PivProject _prj, String _prop) {
    updateActionsState();
//    update2dFrame(_prop);
  }

  /**
   * Mise � jour de l'�tat des menus.
   */
  public void updateActionsState() {
    FuLog.debug("Project state change");

    boolean bprjOpen = project != null;
    boolean bprjMod = bprjOpen && (project.isParamsModified() || isUIModified_);
    boolean bprjHasSrcImg = bprjOpen && project.hasSrcImages(PivProject.SRC_IMAGE_TYPE.ALL);
    boolean bprjHasAtLeast2CalculableSrcImg = bprjOpen && project.hasAtLeast2CalculableSrcImages();
    boolean bprjHasTransfImg = bprjOpen && project.hasTransfImages();
    boolean bprjHasStabParams = bprjOpen && project.isStabilizationActive();

    setEnabledForAction("ENREGISTRER", bprjMod);
    setEnabledForAction("ENREGISTRERSOUS", bprjOpen);
    setEnabledForAction("FERMER", bprjOpen);
    setEnabledForAction("IMPORT_VIDEO", bprjOpen);
    setEnabledForAction("SELECT_IMAGES", bprjOpen);
    setEnabledForAction("CREATE_GRP", bprjOpen && bprjHasSrcImg);
    setEnabledForAction("IMPORT_GRP", bprjOpen && bprjHasSrcImg);
    setEnabledForAction("DISTANCE_MODE_GRP", bprjOpen && bprjHasSrcImg);
    setEnabledForAction("VERIFY_GRP", bprjOpen && project.getOrthoPoints() != null);
    setEnabledForAction("DEFINE_ORTHO_PARAM", bprjOpen && project.getOrthoPoints() != null && bprjHasSrcImg);
    setEnabledForAction("IMPORT_ORTHO_PARAM", bprjOpen && project.getOrthoPoints() != null && bprjHasSrcImg);
    setEnabledForAction("COMPUTE_ORTHO", bprjOpen);
    setEnabledForAction("DEFINE_PIV_PARAM", bprjOpen && bprjHasTransfImg);
    setEnabledForAction("IMPORT_PIV_PARAM", bprjOpen && bprjHasTransfImg);
    setEnabledForAction("DEFINE_GRID", bprjOpen && bprjHasTransfImg);
    setEnabledForAction("IMPORT_GRID", bprjOpen && bprjHasTransfImg);
    setEnabledForAction("IMPORT_TRANSECT", bprjOpen && bprjHasTransfImg);
    setEnabledForAction("CREATE_TRANSECT", bprjOpen && bprjHasTransfImg);
    setEnabledForAction("CREATE_TRANS_ABSZ", bprjOpen && bprjHasTransfImg);
    setEnabledForAction("COMPUTE_FLOW", bprjOpen && project.getAverageResults() != null);
    setEnabledForAction("COMPUTE_PIV", bprjOpen);
    setEnabledForAction("MANAGE_TRANSFORMED_IMAGES", bprjOpen);
    setEnabledForAction("COMPUTE_DIRECT_VELOCITIES", bprjOpen && bprjHasTransfImg);
    setEnabledForAction("FILTER_RAW_RESULTS", bprjOpen && project.getInstantRawResults() != null);
    setEnabledForAction("COMPUTE_AVERAGE", bprjOpen && project.getInstantFilteredResults() != null);
    setEnabledForAction("SHOW_VELOCITIES_RESULTS", bprjOpen && project.getAverageResults() != null);
    setEnabledForAction("SHOW_FLOW_RESULTS", bprjOpen && project.getFlowResults() != null);
    setEnabledForAction("SHOW_RESULTS_STATS", bprjOpen && project.getInstantRawResults() != null);
    setEnabledForAction("EXPORT_TRANSF_IMAGES", bprjOpen && bprjHasTransfImg);
    setEnabledForAction("EXPORT_STAB_IMAGES", bprjOpen && project.getStabilizedImageFiles().length > 0);
    setEnabledForAction("EXPORT_SRC_IMAGES", bprjOpen && bprjHasSrcImg);
    setEnabledForAction("EXPORT_GAUGING_REPORT", bprjOpen);
    setEnabledForAction("SCALING_PARAMS", bprjHasSrcImg);
    setEnabledForAction("DEFINE_ORTHO_MODE", bprjHasSrcImg);
    setEnabledForAction("MN_SCALING", bprjHasSrcImg && project.getOrthoMode() == OrthoMode.SCALING);
    setEnabledForAction("MN_ORTHO", bprjHasSrcImg && project.getOrthoMode() != OrthoMode.SCALING);
    setEnabledForAction("EXPORT_RAW_RESULTS", bprjOpen && project.getInstantRawResults() != null);
    setEnabledForAction("EXPORT_FILTERED_RESULTS", bprjOpen && project.getInstantFilteredResults() != null);
    setEnabledForAction("EXPORT_AVERAGE_RESULTS", bprjOpen && project.getAverageResults() != null);
    setEnabledForAction("STABILIZATION_PARAMS", bprjHasSrcImg);
    setEnabledForAction("COMPUTE_STAB_IMAGES", bprjHasStabParams);
    setEnabledForAction("COMPUTE_ALL", bprjHasAtLeast2CalculableSrcImg);
    setEnabledForAction("CLEAN_COMPUTATIONS", bprjHasAtLeast2CalculableSrcImg);
    setEnabledForAction("NEXT_IMAGE", bprjHasSrcImg);
    setEnabledForAction("PREVIOUS_IMAGE", bprjHasSrcImg);

    refreshTitle();
  }

  /**
   * Met a jour les calques de la fenetre 2D suivant les modifications sur le projet.
   *
   * @param _prop La propri�t� du projet modifi�e.
   */
//  void update2dFrame(String _prop) {
//    if (piv2dFrame==null) return;
//    piv2dFrame.getVisuPanel().refresh(_prop);
//  }

  /**
   * Met a jour le titre de l'application en fonction de l'�tat du projet.
   */
  public void refreshTitle() {
    String title = isPIV_.name + " " + isPIV_.version;
    if (project != null) {
      title += " - ";
      if (prjFile == null)
        title += PivResource.getS("Nouveau projet");
      else
        title += prjFile;
      if (project.isParamsModified() || isUIModified_)
        title += "*";
    }
    setTitle(title);
  }

//  protected boolean buildExportDataToolIcon() {
//    return true;
//  }
//
//  protected boolean buildFudaaReportTool() {
//    return true;
//  }
//
//  protected boolean buildImageToolIcon() {
//    return true;
//  }

//  protected boolean useScrollInBuDesktop() {
//    return true;
//  }

  /**
   * Interne : Appel� quand une action est d�clench�e.
   * 
   * @param _evt L'evenement d�clench�.
   */
  @Override
  public void actionPerformed(ActionEvent _evt) {
    final String action = _evt.getActionCommand();
    if (action == null) {
      return;
    }

    if ("OUVRIR".equals(action)) {
      ouvrir(null);
    }
    else if ("ENREGISTRER".equals(action)) {
      save(prjFile);
    }
    else if ("ENREGISTRERSOUS".equals(action)) {
      save(null);
    }
    else if ("FERMER".equals(action)) {
      saveAndCloseProjet();
    }
    else if ("CREER".equals(action)) {
      create();
    }
    else if ("AIDE_INDEX".equals(action)) {
      showHelp();
    }
    else if ("MAJ".equals(action)) {
      if (!checkForUpdate(false)) {
        message(PivResource.getS("Information"),
            PivResource.getS("Site de mises � jour : {0}\nLa version de Fudaa-LSPIV est � jour.", isPIV_.update),
            false);
      }
    }
    else if (action.startsWith("REOUVRIR")) {
      FuLog.trace(action.substring(9, action.length() - 1));
      ouvrir(new File(action.substring(9, action.length() - 1)));
    }
    else
      super.actionPerformed(_evt);
  }

  /**
   * Met a jour les fichiers recents avec le nouveau fichier chaque fois que necessaire.
   * 
   * @param _fichier Le fichier � ajouter.
   */
  private void updateRecentFiles(File _fichier) {
    getMainMenuBar().addRecentFile(_fichier.getPath(), null);
    PivPreferences.PIV.writeIniFile();
  }

  /**
   * Affiche la documentation depuis un fichier externe (pdf, html, ...)
   */
  private void showHelp() {
    String docPath = System.getProperty(PivPreferences.PIV_DOC_PATH, System.getProperty("user.dir") + "/doc") + "/"
        + BuPreferences.BU.getStringProperty("locale.language", System.getProperty("piv.lang", "fr"));
    File manFile = new File(docPath, getInformationsSoftware().man);
    FuLog.trace("Affichage de la doc depuis " + manFile.getAbsolutePath());

    try {
      Desktop.getDesktop().open(manFile);
    }
    catch (Exception e) {
      error(PivResource.getS("Impossible d'ouvrir le fichier {0}.\nPas d'application ou fichier introuvable.",
          manFile.getAbsolutePath()));
    }
  }

  /**
   * Surcharge de la m�thode pour pouvoir sauvegarder les pr�f�rences.
   */
//  public void exit() {
//    confirmExit();
//  }

  /**
   * Confirmation de la sortie avec ou sans sauvegarde.
   * 
   * @return True s'il y a eu confirmation.
   */
  @Override
  public boolean confirmExit() {
    if (!FudaaStartupExitPreferencesPanel.isExitConfirmed() || question(BuResource.BU.getString("Quitter"),
        BuResource.BU.getString("Voulez-vous vraiment quitter ce logiciel ?"))) {
      return saveAndCloseProjet();
    }
    return false;
//      return saveAndCloseProjet(new Runnable() {
//        // Lanc� apr�s la sauvegarde, si op�ration de sauvegarde concluante ou si pas de sauvegarde demand�e.
//        public void run() {
//          savePreferencesAndTerminate();
//        }
//      });
//    }
//    return true;
  }

  /**
   * Cr�e un nouveau projet apr�s avoir ferm� celui en cours.
   */
  public void create() {
    if (!saveAndCloseProjet()) {
      return;
    }

    prjFile = null;
    project = new PivProject();
    if (!project.init()) {
      error(PivResource.getS("Probl�me de cr�ation du projet."));
      project = null;
      return;
    }
    project.addListener(this);
    open2dFrame();

    setUIIsNotModified();
  }

  /**
   * Pr�cise que l'action "Fermer" concerne la fermeture d'un projet et non une fenetre interne s�lectionn�e.
   * 
   * @return Fix� � false : L'action est g�r�e au niveau applicatif.
   */
  @Override
  public boolean isCloseFrameMode() {
    return false;
  }

  /**
   * Ouvre la fenetre 2D lorsque le projet est charg�.
   */
  public void open2dFrame() {
    if (project == null)
      return;
    piv2dFrame.getVisuPanel().setProjet(project);
    addInternalFrame(piv2dFrame);

    // Pas indispensable, mais permet � la colonne de droite
    // de se mettre en place avant de restaurer la vue.
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        try {
          piv2dFrame.setMaximum(true);
          piv2dFrame.getVisuPanel().restaurer();
          setUIIsNotModified();
        }
        catch (PropertyVetoException ex) {
        }
      }
    });
  }

  /**
   * Affecte un statut "non modifi�" � l'UI.
   */
  public void setUIIsNotModified() {
    SwingUtilities.invokeLater(new Runnable() {

      public void run() {

        // Important : isUIModified doit �tre initialis� tout a la fin du thread swing.
        // Pour cela, on le place dans un invokeLater d'un code d�j� lanc� � la fin.
        // Par ce biais, on est "quasi" s�r que ce sera bien en fin de file.
        SwingUtilities.invokeLater(new Runnable() {

          public void run() {
            isUIModified_ = false;
            updateActionsState();
          }
        });
      }
    });
  }

  /**
   * Cr�e la fenetre 2D.
   */
  public void create2dFrame() {
    piv2dFrame = new PivFille2d(this);

    // On capte toutes les modifications UI.
    piv2dFrame.getVisuPanel().getArbreCalqueModel().getObservable().addObserver(new Observer() {
      public void update(Observable o, Object arg) {
        isUIModified_ = true;
        updateActionsState();
      }
    });
  }

  /**
   * Retourne la fenetre 2D.
   * 
   * @return La fenetre 2D.
   */
  public PivFille2d get2dFrame() {
    return piv2dFrame;
  }

  /**
   * Ferme la fenetre 2D.
   */
  public void close2dFrame() {
    if (piv2dFrame == null)
      return;

    try {
      piv2dFrame.setClosed(true);
    }
    catch (PropertyVetoException _evt) {
      FuLog.error(_evt);
    }
    removeInternalFrame(piv2dFrame);
  }

  /**
   * Ferme le projet, puis la fenetre 2D associ�e.
   */
  @Override
  public void close() {
    if (project != null)
      project.dispose();
    project = null;

    close2dFrame();
    updateActionsState();
  }

  /**
   * Retourne l'URL racine de l'aide.
   * 
   * @return L'URL.
   */
  protected String getAideIndexUrl() {
    return getHelpDir() + "lspiv/index.html";
  }

  /**
   * Retourne les pr�f�rences applicatives.
   * 
   * @return Les pr�f�rences.
   */
  public BuPreferences getApplicationPreferences() {
    return null;
  }

  /**
   * Retourne les informations de l'application pour affichage dans "a propos".
   * 
   * @return Les infos.
   */
  public BuInformationsSoftware getInformationsSoftware() {
    return informationsSoftware();
  }

  /**
   * Ouvre un projet existant. Peut demander la sauvegarde du projet pr�c�demment ouvert si existant.
   * 
   * @param _f Le fichier projet � ouvrir. null, si le fichier doit etre choisi par l'utilisateur.
   */
  public void ouvrir(final File _f) {
    if (!saveAndCloseProjet())
      return;

    // Choix du r�pertoire
    if (_f == null) {
      CtuluFileChooser fc = new CtuluFileChooser(true);
      fc.setFileFilter(PivUtils.FILE_FLT_PROJ);
      fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
      fc.setAcceptAllFileFilterUsed(false);
      fc.setDialogTitle(PivResource.getS("Ouverture d'un projet"));
      if (fc.showOpenDialog(this.getFrame()) == CtuluFileChooser.CANCEL_OPTION) {
        return;
      }

      prjFile = fc.getSelectedFile();
    }
    else {
      prjFile = _f;
    }

    project = null;

    // L'ouverture est lanc�e en tache de fond.
    PivTaskAbstract r = new PivTaskAbstract(PivResource.getS("Chargement du projet")) {
      @Override
      public boolean act(PivTaskObserverI _observer) {
        boolean b = true;
        final PivPersistence persist = new PivPersistence(PivImplementation.this);
        try {
          b = persist.load(prjFile, _observer);
          return b;
        }
        finally {
          final boolean b2 = b;
          SwingUtilities.invokeLater(new Runnable() {
            public void run() {
              diProgress.dispose();
              if (!b2) {
                error(PivResource.getS(
                    "Le fichier ne semble pas �tre un projet Fudaa-LSPIV.\nCauses possibles:\n- Le fichier n'est pas dans un format g�r� par Fudaa-LSPIV\n- Il ne contient pas le r�pertoire '{0}'",
                    PivProject.OUTPUT_DIR));
                project = null;
              }
              else {
                project = persist.getProject();
                project.addListener(PivImplementation.this);
                open2dFrame();
                piv2dFrame.getVisuPanel().setLayerProperties(persist.getLayerProperties());

                updateRecentFiles(prjFile);
                setUIIsNotModified();
              }
            }
          });
        }
      }
    };

    PivProgressionPanel pnProgress_ = new PivProgressionPanel(r, false);
    diProgress = pnProgress_.createDialog(this.getParentComponent());
    diProgress.setOption(CtuluDialog.ZERO_OPTION);
    diProgress.setDefaultCloseOperation(CtuluDialog.DO_NOTHING_ON_CLOSE);
    diProgress.setTitle(r.getName());

    r.start();
    diProgress.afficheDialogModal();
  }

  /**
   * Sauve le projet courant sur l'emplacement choisi � sa cr�ation.
   * 
   * @param _f Le nom du fichier sur lequel sauver le projet. null, si le fichier doit etre choisi par l'utilisateur.
   */
  public void save(File _f) {
    // Pas de sauvegarde si aucune modification
    if (_f != null && !isProjectModified())
      return;

    if (_f == null) {
      CtuluFileChooser fc = new CtuluFileChooser(true);
      CtuluFileChooserTestWritable tester = new CtuluFileChooserTestWritable(this) {
        @Override
        public File getDestFile(File _initFile, CtuluFileChooser _chooser) {
          return _initFile;
        }
      };
      tester.setAppendStrictExtension(true);
      fc.setTester(tester);
      fc.setFileFilter(PivUtils.FILE_FLT_PROJ);
      fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
      fc.setAcceptAllFileFilterUsed(false);
      fc.setDialogTitle(PivResource.getS("Sauvegarde du projet"));
      if (prjFile != null) {
        fc.setSelectedFile(prjFile);
      }

      if (fc.showSaveDialog(this.getFrame()) == JFileChooser.CANCEL_OPTION) {
        return;
      }
      _f = CtuluLibFile.appendStrictExtensionIfNeeded(fc.getSelectedFile(), PivUtils.FILE_FLT_PROJ.getExtensions());
    }
    final File f = _f;

    // La sauvegarde est lanc�e en tache de fond.
    PivTaskAbstract r = new PivTaskAbstract(PivResource.getS("Sauvegarde du projet")) {
      @Override
      public boolean act(PivTaskObserverI _observer) {
        boolean b = true;
        try {
          b = new PivPersistence(PivImplementation.this).save(f, _observer);

          prjFile = f;
          return b;
        }
        finally {
          final boolean b2 = b;
          SwingUtilities.invokeLater(new Runnable() {
            public void run() {
              diProgress.dispose();
              if (!b2) {
                error(PivResource.getS("Erreur lors de la sauvegarde du fichier projet.\nIl ne sera pas sauvegard�"));
              }
              else {
                updateRecentFiles(prjFile);
                setUIIsNotModified();
              }
            }
          });
        }
      }
    };

    PivProgressionPanel pnProgress_ = new PivProgressionPanel(r, false);
    diProgress = pnProgress_.createDialog(this.getParentComponent());
    diProgress.setOption(CtuluDialog.ZERO_OPTION);
    diProgress.setDefaultCloseOperation(CtuluDialog.DO_NOTHING_ON_CLOSE);
    diProgress.setTitle(r.getName());

    r.start();
    diProgress.afficheDialogModal();
  }

  /**
   * Teste si le projet courant a �t� modifi� depius la derni�re sauvegarde.
   * 
   * @return True : Le projet courant a �t� modifi�.
   */
  protected boolean isProjectModified() {
    return project != null && (project.isParamsModified() || isUIModified_);
  }

  /**
   * Sauve le projet si necessaire et le ferme.
   * 
   * @return true : L'utilisateur a autoris� la fermeture du projet.
   */
  public boolean saveAndCloseProjet() {
    if (isProjectModified()) {
      final int ret = CtuluLibDialog.confirmExitIfProjectisModified(getFrame());
      // L'utilisateur annule l'op�ration
      if (ret == JOptionPane.CANCEL_OPTION) {
        FuLog.debug("FSI: close operation cancelled by user");
        return false;
      }
      else if (ret == JOptionPane.OK_OPTION) {
        save(prjFile);
      }
    }
    close();
    return true;
  }

  /**
   * Methode surcharg�e pour les panneau de pr�f�rence.
   * 
   * @param _frAddTab La liste contenant tous les panneaux de pr�f�rences.
   */
  @Override
  protected void buildPreferences(final List<BuAbstractPreferencesPanel> _frAddTab) {
    _frAddTab.add(new BuUserPreferencesPanel(this));
    _frAddTab.add(new BuLanguagePreferencesPanel(this));
    _frAddTab.add(new BuDesktopPreferencesPanel(this));
    _frAddTab.add(new FudaaStartupExitPreferencesPanel(true));
    _frAddTab.add(new FudaaLookPreferencesPanel(this));
    _frAddTab.add(new BuBrowserPreferencesPanel(this));
    _frAddTab.add(new EbliMiseEnPagePreferencesPanel());
    _frAddTab.add(new BuContainerPreferencesPanel(PivResource.getS("Calcul"),
        new BuAbstractPreferencesComponent[] { new PivImagesPreferencesComponent() }));
  }

  /**
   * Sauvegarde des pr�f�rences de l'appli � la sortie.
   * 
   * Remarque importante : Certaines infos sauv�es par cette m�thode sont relues par d'autres applications Fudaa, qui risquent alors de s'afficher de
   * facon inattendue.
   */
  protected void savePreferencesAndTerminate() {
    final Point p = getFrame().getLocation();
    final Dimension d = getFrame().getSize();
    BuPreferences.BU.putIntegerProperty("window.x", p.x);
    BuPreferences.BU.putIntegerProperty("window.y", p.y);
    BuPreferences.BU.putIntegerProperty("window.w", d.width);
    BuPreferences.BU.putIntegerProperty("window.h", d.height);
    BuPreferences.BU.writeIniFile();

    BuRegistry.unregister(this.getFrame());
  }

  /**
   * Mise en place de l'application avant affichage.
   */
  @Override
  public void init() {

    CtuluImageExport.removeWritableFormats(new String[] { "ps" });

    // DragAndDrop
    installDragAndDrop();

    super.init();
    // Pour forcer l'activation du command listener.
    getUndoCmdListener();

    removeUnusedActions();
    getSpecificBar().setToolsFocusable(true);
    final BuMenuBar mb = getMainMenuBar();

    setEnabledForAction("QUITTER", true);
    setEnabledForAction("PREFERENCE", true);
    setEnabledForAction("CREER", true);
    setEnabledForAction("OUVRIR", true);
    setEnabledForAction("IMPORTER", true);
    setEnabledForAction("EXPORTER", true);

    actExportGaugingReport_ = new PivExportGaugingReportAction(this);

    setEnabledForAction("MAJ", true);

    BuMenuRecentFiles mr = (BuMenuRecentFiles) mb.getMenu("REOUVRIR");
    if (mr != null) {
      mr.setPreferences(PivPreferences.PIV);
      mr.setResource(PivResource.PIV);
    }

    // Le menu file
    BuMenu mn = (BuMenu) mb.getMenu("MENU_FICHIER");
    int idx = mn.indexOf("EXPORTER") + 1;
    if (idx == 0)
      idx = mn.getComponentCount();
    mn.insertSeparator(idx++);
    mn.add(new PivComputeAllAction(this).buildMenuItem(EbliComponentFactory.INSTANCE), idx++);
    mn.add(new PivComputeCleaningAction(this).buildMenuItem(EbliComponentFactory.INSTANCE), idx++);

    // Le menu Editer
    mn = (BuMenu) mb.getMenu("MENU_EDITION");
    idx = mn.indexOf("COPIER");
    // Au cas ou...
    if (idx == -1)
      idx = mn.getComponentCount();
    mn.add(get2dFrame().getVisuPanel().getEditAction().buildMenuItem(EbliComponentFactory.INSTANCE), idx++);
    mn.add(get2dFrame().getVisuPanel().getDeplacementAction().buildMenuItem(EbliComponentFactory.INSTANCE), idx++);
    mn.add(get2dFrame().getVisuPanel().getRotationAction().buildMenuItem(EbliComponentFactory.INSTANCE), idx++);
    mn.add(get2dFrame().getVisuPanel().getRealView().getInvertTransectAction()
        .buildMenuItem(EbliComponentFactory.INSTANCE), idx++);
    mn.add(get2dFrame().getVisuPanel().getEditor().getActionDelete().buildMenuItem(EbliComponentFactory.INSTANCE),
        idx++);
    mn.insertSeparator(idx++);
    mn.add(new PivPreviousImageAction(this).buildMenuItem(EbliComponentFactory.INSTANCE), idx++);
    mn.add(new PivNextImageAction(this).buildMenuItem(EbliComponentFactory.INSTANCE), idx++);

    setEnabledForAction("PREVIOUS_IMAGE", false);
    setEnabledForAction("NEXT_IMAGE", false);

    
    // les menus exporter et importer sont construit dynamiquement
//    buildImportMenu();
    setEnabledForAction("IMPORTER", false);
    buildExportMenu();

    mb.addMenu(buildImagesMenu());
    mb.addMenu(buildOrthorectificationMenu());
    mb.addMenu(buildPivMenu());
    mb.addMenu(buildPostMenu());
    mb.addMenu(buildDischargeMenu());
  }

  /**
   * Met en place le drag&drop pour les fichiers projet
   */
  protected void installDragAndDrop() {
    ((JFrame) getFrame()).setTransferHandler(new TransferHandler() {

      @Override
      public boolean canImport(TransferSupport support) {
        for (DataFlavor df : support.getDataFlavors()) {
          if (df.equals(DataFlavor.javaFileListFlavor)) {
            return true;
          }
        }
        return false;
      }

      /**
       * L'importation de donn�es.
       */
      @Override
      public boolean importData(TransferSupport support) {
        if (canImport(support)) {
          try {
            @SuppressWarnings("unchecked")
            final List<File> files = (List<File>) support.getTransferable()
                .getTransferData(DataFlavor.javaFileListFlavor);
            if (files.size() > 1) {
              error(PivResource.getS("Un seul fichier projet est autoris� pour le transfert"));
              return false;
            }
            // Un thread est lanc�, sinon l'explorer de fichier d'ou provient le fichier est bloqu�
            // durant le processus d'ouverture
            new Thread(new Runnable() {
              @Override
              public void run() {
                ouvrir(files.get(0));
              }
            }).start();

            return true;
          }
          catch (UnsupportedFlavorException ufe) {
            ufe.printStackTrace();
          }
          catch (IOException ioe) {
            ioe.printStackTrace();
          }
        }
        return false;
      }
    });
  }

  /**
   * @return Le menu de gestion des images
   */
  protected BuMenu buildImagesMenu() {
    BuMenu mn = new BuMenu(PivResource.getS("Images"), "mnImages");
    mn.add(new PivImportVideoAction(this));
    mn.add(new PivSelectImagesAction(this));
    mn.addSeparator();
    mn.add(new PivDefineParamStabilizationAction(this));
    mn.add(new PivComputeStabilizedImagesAction(this));

    return mn;
  }

  /**
   * @return Le menu de l'othorectification
   */
  protected BuMenu buildOrthorectificationMenu() {
    AbstractButton bt;
    BuMenu sm;

    BuMenu mn = new BuMenu(PivResource.getS("Orthorectification"), "mnOrtho");

    mn.add(new PivChooseOrthoModeAction(this).buildMenuItem(EbliComponentFactory.INSTANCE));

    BuMenu mnScaling = new BuMenu(PivResource.getS("Mise � l'�chelle"), "MN_SCALING");
    mnScaling.add(new PivDefineParamScalingAction(this).buildMenuItem(EbliComponentFactory.INSTANCE));
    mnScaling.setEnabled(false);

    mn.add(mnScaling);

    BuMenu mnOrtho = new BuMenu(PivResource.getS("Orthorectification"), "MN_ORTHO");
    sm = new BuMenu(PivResource.getS("Points de r�f�rence"), "mnGRP");
    bt = get2dFrame().getVisuPanel().getOrthoGRPAction().buildMenuItem(EbliComponentFactory.INSTANCE);
    bt.setText(PivResource.getS("Nouveaux..."));
    sm.add(bt);
    bt = new PivImportGRPAction((this)).buildMenuItem(EbliComponentFactory.INSTANCE);
    bt.setText(PivResource.getS("Importer..."));
    sm.add(bt);
    sm.addSeparator();
    bt = new PivCreateGRPInDistanceModeAction((this)).buildMenuItem(EbliComponentFactory.INSTANCE);
    bt.setText(PivResource.getS("Mode 4 points..."));
    sm.add(bt);
    mnOrtho.add(sm);
    mnOrtho.add(new PivOrthoVerifyGRPAction(this));
//    mn.add(sm);
//    mn.add(new PivOrthoVerifyGRPAction(this));

    sm = new BuMenu(PivResource.getS("Param�tres de transformation"), "mnTRF", true);
    bt = new PivDefineOrthoParamAction(this).buildMenuItem(EbliComponentFactory.INSTANCE);
    bt.setText(PivResource.getS("D�finir..."));
    sm.add(bt);
    bt = new PivImportOrthoParamAction(this).buildMenuItem(EbliComponentFactory.INSTANCE);
    bt.setText(PivResource.getS("Importer..."));
    sm.add(bt);
    mnOrtho.add(sm);
    mnOrtho.setEnabled(false);
    mn.add(mnOrtho);
//    mn.add(sm);

    mn.addSeparator();
    mn.add(new PivComputeTransfImagesAction(this));
    mn.add(new PivManageTransformedImagesAction(this));

    return mn;
  }

  /**
   * @return Le menu pour le calcul par PIV.
   */
  protected BuMenu buildPivMenu() {
    AbstractButton bt;
    BuMenu sm;

    BuMenu mn = new BuMenu(PivResource.getS("Analyse LSPIV"), "mnLSPIV");

    mn.add(new PivManualVelocitiesComputeAction(this));
    mn.addSeparator();

    sm = new BuMenu(PivResource.getS("Points de grille"), "mnGRP");
    bt = get2dFrame().getVisuPanel().getGridDefinitionAction().buildMenuItem(EbliComponentFactory.INSTANCE);
    bt.setText(PivResource.getS("D�finir..."));
    sm.add(bt);
    bt = new PivImportGridAction(this).buildMenuItem(EbliComponentFactory.INSTANCE);
    bt.setText(PivResource.getS("Importer..."));
    sm.add(bt);
    mn.add(sm);

    sm = new BuMenu(PivResource.getS("Param�tres de calcul"), "mnCAL", true);
    bt = new PivDefineComputeParamAction(this).buildMenuItem(EbliComponentFactory.INSTANCE);
    bt.setText(PivResource.getS("D�finir..."));
    sm.add(bt);
    bt = new PivImportComputeParamAction(this).buildMenuItem(EbliComponentFactory.INSTANCE);
    bt.setText(PivResource.getS("Importer..."));
    sm.add(bt);
    mn.add(sm);

    mn.addSeparator();
    mn.add(new PivComputeRawResultsAction(this));

    return mn;
  }

  /**
   * @return Le menu pour le post traitement.
   */
  protected BuMenu buildPostMenu() {
    AbstractButton bt;
    BuMenu sm;

    BuMenu mn = new BuMenu(PivResource.getS("Post-traitement"), "mnPost");
    mn.add(new PivComputeFilteredResultsAction(this));
    mn.add(new PivComputeAveragedResultsAction(this));
    mn.add(get2dFrame().getVisuPanel().getShowVelocitiesAction().buildMenuItem(EbliComponentFactory.INSTANCE));
    mn.addSeparator();
    mn.add(new PivShowResultStatisticsAction(this));
    mn.addSeparator();
    mn.add(get2dFrame().getVisuPanel().getRealView().getTraceParticlesAction()
        .buildMenuItem(EbliComponentFactory.INSTANCE));

    return mn;
  }

  /**
   * @return Le menu pour le calcul de d�bit.
   */
  protected BuMenu buildDischargeMenu() {
    AbstractButton bt;
    BuMenu sm;

    BuMenu mn = new BuMenu(PivResource.getS("D�bit"), "mnDischarge");

    sm = new BuMenu(PivResource.getS("Transect"), "mnGRP");
    bt = get2dFrame().getVisuPanel().getCreateTransectXYZAction().buildMenuItem(EbliComponentFactory.INSTANCE);
    bt.setText(PivResource.getS("Nouveau..."));
    sm.add(bt);
    bt = new PivImportTransectAction(this).buildMenuItem(EbliComponentFactory.INSTANCE);
    bt.setText(PivResource.getS("Importer..."));
    sm.add(bt);
    sm.addSeparator();
    bt = get2dFrame().getVisuPanel().getCreateTransectAbsZAction().buildMenuItem(EbliComponentFactory.INSTANCE);
    sm.add(bt);
//    bt=new PivCreateTransectAbsZAction2(this).buildMenuItem(EbliComponentFactory.INSTANCE);
//    sm.add(bt);
    mn.add(sm);

    mn.add(get2dFrame().getVisuPanel().getRealView().getParamsTransectAction()
        .buildMenuItem(EbliComponentFactory.INSTANCE));
    mn.add(get2dFrame().getVisuPanel().getRealView().getInvertTransectAction()
        .buildMenuItem(EbliComponentFactory.INSTANCE));
    mn.addSeparator();
    mn.add(new PivComputeFlowAction(this));
    mn.addSeparator();
    mn.add(new PivShowFlowResultsAction(this));
    mn.add(new PivShow3DTransectAction(this));
    mn.addSeparator();
    mn.add(actExportGaugingReport_);

    return mn;
  }

  /**
   * Construit le menu import.
   */
  protected void buildImportMenu() {
    BuMenu mnImport = (BuMenu) getMainMenuBar().getMenu("IMPORTER");
    mnImport.add(new PivImportGRPAction((this)));
    mnImport.add(new PivImportGridAction((this)));
    mnImport.add(new PivImportTransectAction((this)));
  }

  /**
   * Construit le menu export
   */
  protected void buildExportMenu() {
    BuMenu mnExport = (BuMenu) getMainMenuBar().getMenu("EXPORTER");

    BuMenuItem itExportImage = new BuMenuItem();
    super.initExportImageButton(itExportImage);
    mnExport.add(itExportImage);

    BuMenuItem itExportImageToClip = new BuMenuItem();
    super.initExportImageToClipborad(itExportImageToClip);
    mnExport.add(itExportImageToClip);
    mnExport.addSeparator();
    mnExport.add(new PivExportImagesAction(this, PivExportImagesAction.EXPORT_SRC));
    mnExport.add(new PivExportImagesAction(this, PivExportImagesAction.EXPORT_TRANSF));
    mnExport.add(new PivExportImagesAction(this, PivExportImagesAction.EXPORT_STAB));
    mnExport.addSeparator();

    BuMenu mnExportSerafinResults = new BuMenu(PivResource.getS("Exporter les r�sultats vers Serafin"),
        "EXPORT_RESULTS_AS_SERAFIN");
    mnExportSerafinResults.add(new PivExportSerafinResultsAction(this, PivExportSerafinResultsAction.ResultsType.RAW));
    mnExportSerafinResults
        .add(new PivExportSerafinResultsAction(this, PivExportSerafinResultsAction.ResultsType.FILTERED));
    mnExportSerafinResults
        .add(new PivExportSerafinResultsAction(this, PivExportSerafinResultsAction.ResultsType.AVERAGE));
    mnExport.add(mnExportSerafinResults);

    BuMenu mnExportExcelResults = new BuMenu(PivResource.getS("Exporter les r�sultats vers Excel"),
        "EXPORT_RESULTS_AS_EXCEL");
    mnExportExcelResults.add(new PivExportExcelResultsAction(this, PivExportExcelResultsAction.ResultsType.RAW));
    mnExportExcelResults.add(new PivExportExcelResultsAction(this, PivExportExcelResultsAction.ResultsType.FILTERED));
    mnExportExcelResults.add(new PivExportExcelResultsAction(this, PivExportExcelResultsAction.ResultsType.AVERAGE));
    mnExport.add(mnExportExcelResults);

    mnExport.addSeparator();
    mnExport.add(actExportGaugingReport_);
  }

  /**
   * @return True pour export de la fenetre active sous forme d'image.
   */
  @Override
  protected boolean buildImageToolIcon() {
    return true;
  }

  /**
   * Suppression des commandes par d�faut dans Fudaa.
   */
  protected void removeUnusedActions() {
    removeAction("ASSISTANT");
    removeAction("ASTUCE");
    removeAction("POINTEURAIDE");
    removeAction("INDEX_THEMA");
    removeAction("INDEX_ALPHA");
    removeAction("PROPRIETE");
    removeAction("PLEINECRAN");
    removeAction("VISIBLE_LEFTCOLUMN");
  }

  /**
   * Controle que la librairie JAI est bien install�e. Si ce n'est pas le cas un message est d�livr� � l'utilisateur.
   */
  public void controlJAILibrary() {
    ImageIO.scanForPlugins();
    Iterator<ImageReader> it = ImageIO.getImageReadersBySuffix("pgm");
    if (!it.hasNext()) {
      error(PivResource.getS("Probl�me d'installation"), PivResource.getS(
          "La biblioth�que JAI Image I/O n'est probablement pas install�e.\nElle doit l'�tre dans le r�pertoire des extensions Java.\n\nL'application ne fonctionnera pas correctement."));
    }
  }

  /**
   * Controle l'existence d'une mise a jour sur l'URL de mise a jour et affiche un dialogue pour t�l�charger si c'est le cas.
   * 
   * @param _showHideButton True : Le bouton "Ne plus afficher" est affich� dans le dialogue.
   * @return True : Une mise a jour existe.
   */
  public boolean checkForUpdate(boolean _showHideButton) {
    try {
//      for (int i=2000; i<10000; i++) {
//        URL urlFlag=new URL("https://forge.irstea.fr/attachments/download/"+i+"/exe_fudaa_linux.zip");
//        FuLog.trace("test="+i);
//        if (FuLib.existsURL(urlFlag)) {
//          String lastversion = "1.7.2";
//          URL urlJar = new URL(isPIV_.update+"/fudaa-lspiv-"+lastversion+"-setup.jar");
//          new PivUpdatePanel(lastversion, urlJar, _showHideButton).afficheModale(this.getFrame(),PivResource.getS("Mise � jour disponible"));
//          return true;
//        }
//      }

      URL urlFlag = new URL(isPIV_.update + "/fudaa-lspiv-lastversion.txt");
      if (FuLib.existsURL(urlFlag)) {
        LineNumberReader is = null;
        try {
          is = new LineNumberReader(new InputStreamReader(urlFlag.openStream()));
          String lastversion;
          while ((lastversion = is.readLine()) != null && lastversion.trim().startsWith("#")) {
          }

          if (lastversion.compareTo(isPIV_.version) > 0) {
            URL urlJar = new URL(isPIV_.update + "/fudaa-lspiv-" + lastversion + "-setup.jar");
            new PivUpdatePanel(lastversion, urlJar, _showHideButton).afficheModale(this.getFrame(),
                PivResource.getS("Mise � jour disponible"));
            return true;
          }
        }
        finally {
          if (is != null) {
            is.close();
          }
        }
      }
    }
    catch (IOException e) {
      e.printStackTrace();
    }

    return false;
  }

  /**
   * D�marre l'application.
   */
  @Override
  public void start() {
    super.start();
    super.addFrameListLeft();

    controlJAILibrary();

    if (PivPreferences.PIV.getBooleanProperty(FudaaStartupExitPreferencesPanel.PREF_CHECK_NEW_VERSION, true)) {
      checkForUpdate(true);
    }
  }

}

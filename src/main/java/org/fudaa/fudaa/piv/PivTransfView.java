/**
 * 
 */
package org.fudaa.fudaa.piv;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.measure.Unit;
import javax.swing.JComponent;

import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.BCalqueAffichage;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.dessin.DeForme;
import org.fudaa.ebli.calque.edition.ZCalqueLigneBriseeEditable;
import org.fudaa.ebli.calque.edition.ZCalquePointEditable;
import org.fudaa.ebli.commun.EbliCoordinateDefinition;
import org.fudaa.ebli.commun.EbliFormatter;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.piv.action.PivDefineAreaCenterAction;
import org.fudaa.fudaa.piv.action.PivTransfViewAction;
import org.fudaa.fudaa.piv.layer.PivCntGridModel;
import org.fudaa.fudaa.piv.layer.PivPointsDisplacementLayer;
import org.fudaa.fudaa.piv.layer.PivPointsDisplacementModel;
import org.fudaa.fudaa.piv.layer.PivGridModel;
import org.fudaa.fudaa.piv.layer.PivIASALayer;
import org.fudaa.fudaa.piv.layer.PivIASAModel;
import org.fudaa.fudaa.piv.layer.PivImageRasterLayer;
import org.fudaa.fudaa.piv.layer.PivTransfImageModel;
import org.fudaa.fudaa.piv.metier.PivGrid;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivProjectStateListener;
import org.fudaa.fudaa.piv.utils.PivUtils;

import com.memoire.bu.BuComboBox;

import tec.uom.se.unit.BaseUnit;

/**
 * Une vue en espace d'images transformées.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class PivTransfView implements PivViewI, PivProjectStateListener {
  /** Le nom de la vue en espace image transformée */
  public static final String TITLE=PivResource.getS("Espace image transformée");
  
  public  static final Unit PIXEL= new BaseUnit<>(PivResource.getS("Pixel"));
  
  /** La définition de coordonnée pour X */
  private static final EbliCoordinateDefinition DEF_COOR_I=new EbliCoordinateDefinition("I", new EbliFormatter(PIXEL));
  /** La définition de coordonnée pour Y */
  private static final EbliCoordinateDefinition DEF_COOR_J=new EbliCoordinateDefinition("J", new EbliFormatter(PIXEL));
  
  private PivTransfViewAction actTransfView_;
  private PivVisuPanel pnLayers_;
  public BuComboBox cbImg_;
  private PivProject prj_;
  private List<BCalqueAffichage> layers_=new ArrayList<BCalqueAffichage>();
  
  PivIASALayer cqIASA_;
  ZCalquePointEditable cqGrille_;
  ZCalqueLigneBriseeEditable cqCntGrille_;
  PivImageRasterLayer cqTransfImage_;
  PivTransfImageModel mdlTransfImage;
  PivIASAModel mdlIASA;
  PivCntGridModel mdlCntGrid;
  PivGridModel mdlGrid;
  PivPointsDisplacementLayer cqDisplacement_;
  PivPointsDisplacementModel mdlDisplacement_;
  private PivDefineAreaCenterAction actDefineAreaCenter_;

  boolean enableEvents_=true;
  
  /**
   * Constructeur.
   * @param _pn Le panneau de calques.
   */
  public PivTransfView(PivVisuPanel _pn) {
    pnLayers_=_pn;
    buildLayers();
    buildTools();
  }
  
  /**
   * Définit le projet.
   * @param _prj Le projet.
   */
  public void setProject(PivProject _prj) {
    if (prj_==_prj) return;
    
    if (prj_!=null)
      prj_.removeListener(this);
    prj_=_prj;
    if (prj_!=null)
      prj_.addListener(this);
    
    buildLayers();
    majLayers();
    majTools();
  }
  
  /**
   * Construction des calques pour l'espace transformée.
   */
  private void buildLayers() {
    layers_.clear();
    
    // Layer des IA/SA
    cqIASA_=new PivIASALayer();
    cqIASA_.setTitle(PivResource.getS("IA/SA"));
    cqIASA_.setLineModel(0, new TraceLigneModel(TraceLigne.LISSE, 1, Color.BLACK));
    cqIASA_.setLineModel(1, new TraceLigneModel(TraceLigne.LISSE, 1, Color.BLACK));
    cqIASA_.setIconModel(0, new TraceIconModel(TraceIcon.RIEN, 1, Color.BLACK));
    cqIASA_.setIconModel(1, new TraceIconModel(TraceIcon.RIEN, 1, Color.BLACK));
    cqIASA_.setName("cqIASA");
    cqIASA_.setDestructible(false);
    
    // Layer des bords de grille
    cqCntGrille_=new ZCalqueLigneBriseeEditable();
    cqCntGrille_.setFormeEnable(new int[]{DeForme.POLYGONE});
    cqCntGrille_.setTitle(PivResource.getS("Contour grille"));
    cqCntGrille_.setLineModel(0, new TraceLigneModel(TraceLigne.POINTILLE, 1, Color.RED));
    cqCntGrille_.setIconModel(0, new TraceIconModel(TraceIcon.PLUS, 1, Color.RED));
    cqCntGrille_.setName("cqCntGrille");
    cqCntGrille_.setDestructible(false);
    cqCntGrille_.setAttributForLabels(PivVisuPanel.ATT_LABEL);
    // Legerement transparent.
    cqCntGrille_.setLabelsBackgroundColor(PivUtils.TRANPARENT_WHITE_COLOR);
    cqCntGrille_.setEditor(pnLayers_.getEditor());

    // Layer des points de grille
    cqGrille_=new ZCalquePointEditable();
    cqGrille_.setTitle(PivResource.getS("Grille"));
    cqGrille_.setIconModel(0, new TraceIconModel(TraceIcon.PLUS_DOUBLE, 3, Color.RED));
    cqGrille_.setName("cqGrille");
    cqGrille_.setDestructible(false);
    cqGrille_.setEditor(pnLayers_.getEditor());

    // Layer des couples de deplacement
    cqDisplacement_=new PivPointsDisplacementLayer();
    cqDisplacement_.setTitle(PivResource.getS("Déplacements"));
    cqDisplacement_.setIconModel(0, new TraceIconModel(TraceIcon.PLUS_DOUBLE, 3, Color.RED));
    cqDisplacement_.setLineModel(0, new TraceLigneModel(TraceLigne.TIRETE, 2.5f, Color.ORANGE));
    cqDisplacement_.setName("cqDisplacment");
    cqDisplacement_.setDestructible(false);
    

    // Layer de l'image
    cqTransfImage_ = new PivImageRasterLayer();
    cqTransfImage_.setTitle(PivResource.getS("Image ortho"));
    cqTransfImage_.setName("cqTransfImg");
    
    layers_.add(cqIASA_);
    layers_.add(cqDisplacement_);
    layers_.add(cqCntGrille_);
    layers_.add(cqGrille_);
    layers_.add(cqTransfImage_);
  }
  
  /**
   * Mise a jour des calques depuis le projet.
   */
  private void majLayers() {
    mdlTransfImage=new PivTransfImageModel();
    mdlTransfImage.setProjet(prj_);
    cqTransfImage_.setModele(mdlTransfImage);

    PivGrid grid=prj_.getComputeGrid();
    if (grid==null)
      mdlGrid=new PivGridModel();
    else
      mdlGrid=new PivGridModel(grid);
    cqGrille_.setModele(mdlGrid);

    mdlCntGrid=new PivCntGridModel();
    mdlCntGrid.setProjet(prj_);
    cqCntGrille_.modele(mdlCntGrid);

    mdlIASA=new PivIASAModel();
    mdlIASA.setProjet(prj_);
    cqIASA_.modele(mdlIASA);
    
    mdlDisplacement_ = new PivPointsDisplacementModel();
    mdlDisplacement_.setProjet(prj_);
    cqDisplacement_.modele(mdlDisplacement_);
  }
  
  @Override
  public BCalqueAffichage[] getLayers() {
    return layers_.toArray(new BCalqueAffichage[0]);
  }
  
  /**
   * @return Le calque comportant le contour de grille
   */
  public ZCalqueLigneBriseeEditable getCntGridLayer() {
    return cqCntGrille_;
  }
  
  public int getSelectedImageIndex() {
    return cbImg_.getSelectedIndex();
  }
  
  /**
   * Construction des outils spécifiques à cette vue.
   */
  private void buildTools() {
    // La liste déroulante des images
    cbImg_ = new BuComboBox();
    cbImg_.addItemListener(new ItemListener() {
      
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange()==ItemEvent.SELECTED) {
          if (!enableEvents_) return;
          
          if (cqTransfImage_!=null && cqTransfImage_.getModelImage()!=null) {
            cqTransfImage_.getModelImage().setSelectedImage(cbImg_.getSelectedIndex());
            // Pour que le calque soit réaffiché.
            cqTransfImage_.repaint();
          }
        }
      }
    });
    cbImg_.setPreferredSize(new Dimension(200, cbImg_.getPreferredSize().height));
    cbImg_.setMaximumSize(cbImg_.getPreferredSize());
    cbImg_.setToolTipText(PivResource.getS("Affiche l'image sélectionnée"));
    cbImg_.setEnabled(false);
  }

  /**
   * Mise a jour les outils.
   */
  private void majTools() {
    enableEvents_=false;
    
    // La liste déroulante des images, avec sélection d'une image différente si inexistante.
    Object o=cbImg_.getSelectedItem();
    cbImg_.removeAllItems();
    cbImg_.setEnabled(false);

    if (prj_ != null) {
      for (File f : prj_.getTransfImageFiles()) {
        cbImg_.addItem(f.getName());
      }
      cbImg_.setEnabled(prj_.getTransfImageFiles().length > 0);
    }
    
    boolean found=false;
    if (o != null) {
      for (int i=0; i<cbImg_.getItemCount(); i++) {
        if (cbImg_.getItemAt(i).equals(o)) {
          cbImg_.setSelectedItem(o);
          found=true;
          break;
        }
      }
    }
    if (!found) {
      cbImg_.setSelectedItem(cbImg_.getItemCount()==0?-1:0);
    }
    
    enableEvents_=true;
  }

  @Override
  public void projectStateChanged(PivProject _prj, String _prop) {
    if ("transfImages".equals(_prop)) {
      majTools();
      mdlTransfImage.update();
      mdlIASA.update();
    }
    else if("computeParameters".equals(_prop)) {
      mdlIASA.update();
    }
    else if("cntGrid".equals(_prop)) {
      mdlCntGrid.update();
    }
    else if("computeGrid".equals(_prop)) {
      if (prj_.getComputeGrid()==null)
        cqGrille_.setModele(new PivGridModel());
      else
        cqGrille_.setModele(new PivGridModel(prj_.getComputeGrid()));
    }
    else if ("manualVelocitiesParams".equals(_prop)) {
      mdlDisplacement_.update();
    }
    
    pnLayers_.getVueCalque().repaint();
  }

  @Override
  public String getTitle() {
    return TITLE;
  }

  @Override
  public JComponent[] getSpecificTools() {
    return new JComponent[]{cbImg_};
  }

  @Override
  public EbliCoordinateDefinition[] getCoordinateDefinitions() {
    return new EbliCoordinateDefinition[]{DEF_COOR_J,DEF_COOR_I};
  }

  @Override
  public void restoreLayerProperties(Map<String, EbliUIProperties> _props) {
    for (BCalqueAffichage cq : layers_) {
      restoreLayerProperties(_props, cq);
    }
  }

  @Override
  public Map<String, EbliUIProperties> saveLayerProperties() {
    HashMap<String, EbliUIProperties> props=new HashMap<String, EbliUIProperties>();
    for (BCalqueAffichage cq : layers_) {
      saveLayerProperties(props, cq);
    }
    return props;
  }
  
  /**
   * Restore les propriétés graphiques recursivement.
   * @param _props Les propriétés
   * @param _cq Le calque a restaurer
   */
  private void restoreLayerProperties(Map<String, EbliUIProperties> _props, BCalque _cq) {
    if (_cq instanceof BGroupeCalque) {
      for (BCalque cq : ((BGroupeCalque)_cq).getCalques()) {
        restoreLayerProperties(_props, cq);
      }
    }
    else {
      _cq.initFrom(_props.get(_cq.getName()));
    }
  }
  
  /**
   * Sauve les propriétés graphiques recursivement.
   * @param _props Les propriétés
   * @param _cq Le calque a sauver
   */
  private void saveLayerProperties(Map<String, EbliUIProperties> _props, BCalque _cq) {
    if (_cq instanceof BGroupeCalque) {
      for (BCalque cq : ((BGroupeCalque)_cq).getCalques()) {
        saveLayerProperties(_props, cq);
      }
    }
    else {
      _props.put(_cq.getName(),_cq.saveUIProperties());
    }
  }

  @Override
  public PivTransfViewAction getActivationAction() {
    if (actTransfView_==null) {
      actTransfView_=new PivTransfViewAction((PivImplementation)pnLayers_.getCtuluUI());
    }
    return actTransfView_;
  }
  
  /**
   * @return L'action pour definir le centre de l'IA/SA.
   */
  public PivDefineAreaCenterAction getDefineAreaCenterAction() {
    if (actDefineAreaCenter_==null) {
      actDefineAreaCenter_=new PivDefineAreaCenterAction(pnLayers_);
    }
    return actDefineAreaCenter_;
  }

  @Override
  public void nextImage() {
    int nextInd = cbImg_.getSelectedIndex() + 1;
    if (nextInd < cbImg_.getItemCount()) {
      cbImg_.setSelectedIndex(nextInd);
    }
  }

  @Override
  public void previousImage() {
    int previousInd = cbImg_.getSelectedIndex() -1;
    if (previousInd >= 0) {
      cbImg_.setSelectedIndex(previousInd);
    }
  }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.piv;

import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.piv.PivTaskAbstract.TaskStatus;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.metier.PivOrthoParameters;
import org.fudaa.fudaa.piv.metier.PivOrthoPoint;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivProject.OrthoMode;

import com.memoire.bu.BuResource;
import com.memoire.fu.FuLog;

/**
 *
 * @author marchand
 */
public class PivComputeTrackingPanel extends CtuluDialogPanel implements PivTaskObserverI {
  private PivImplementation impl_;
  private GlobalTask stepsTask_;
  private StepsTableModel mdSteps_;
  private int progression_;
  private String desc_;
  private boolean isStopRequested_;
  private boolean isRunning_ = false;
  private boolean isCompleted_ = false;
  
  public enum TaskType {
    COMPUTE,
    CLEAN
  }
  
  public abstract class StepTask extends PivTaskAbstract {
    TaskType type_;
    
    public StepTask(TaskType _type, String _name) {
      super(_name);
      type_= _type;
    }
  }
  
  /**
   * La tache de stabilisation
   * @author marchand
   */
  public class StabilizationTask extends StepTask {
    
    public StabilizationTask(TaskType _type) {
      super(_type, PivResource.getS("Stabilisation"));
    }

    @Override
    public boolean act(PivTaskObserverI _observer) {
      if (type_ == TaskType.COMPUTE) {
        CtuluLog ana = new CtuluLog();
        ana.setDesc(this.getName());

        PivExeLauncher.instance().computeStabilizedImages(ana, impl_.getCurrentProject(), _observer);
        if (ana.containsErrorOrSevereError()) {
          impl_.error(ana.getResume());
          return false;
        }
      }
      
      //  Nettoyage de l'�tape stabilisation.
      else {
        impl_.getCurrentProject().removeStabilizedImagesOnFS();
        impl_.getCurrentProject().reloadStabilizedImages();
      }
      
      return true;
    }
  }
  
  /**
   * La tache d'orthorectification.
   * @author marchand
   *
   */
  public class OrthoTask extends StepTask {
    
    public OrthoTask(TaskType _type) {
      super(_type, PivResource.getS("Orthorectification"));
    }

    @Override
    public boolean act(PivTaskObserverI _observer) {
      if (type_ == TaskType.COMPUTE) {
        if (impl_.getCurrentProject().getOrthoMode() != OrthoMode.SCALING) {
          CtuluLog ana = new CtuluLog();
          ana.setDesc(getName());

          // Sp�cifique orthorectification, quand tous les points ont la m�me cote.
          Double z = PivOrthoPoint.getIdenticalZ(impl_.getCurrentProject().getOrthoPoints());
          PivOrthoParameters params = impl_.getCurrentProject().getOrthoParameters();
          if (z != null && !z.equals(params.getWaterElevation())) {
//          impl_.message(PivResource.getS("La cote du plan d'eau va �tre modifi�e pour correspondre aux cotes Z des GRP"));
            params.setWaterElevation(z);
            impl_.getCurrentProject().setOrthoParameters(params);
          }

          PivExeLauncher.instance().computeTransfImg(ana, impl_.getCurrentProject(), false, _observer);
          if (ana.containsErrorOrSevereError()) {
            impl_.error(ana.getResume());
            return false;
          }
        }
        else {
          CtuluLog ana = new CtuluLog();
          ana.setDesc(getName());
          PivExeLauncher.instance().computeScalingImages(ana, impl_.getCurrentProject(), false, _observer);
          if (ana.containsErrorOrSevereError()) {
            impl_.error(ana.getResume());
            return false;
          }
        }
      }
      
      // Nettoyage de l'�tape de calcul des images orthorectifi�es.
      else {
        impl_.getCurrentProject().removeTransformedImagesOnFS();
        impl_.getCurrentProject().reloadTransfImages();
      }

      return true;
    }
  }
  
  /**
   * La tache de calcul des vitesses manuelles.
   * @author marchand
   *
   */
  public class ComputeManualVelocitiesTask extends StepTask {
    
    public ComputeManualVelocitiesTask(TaskType _type) {
      super(_type, PivResource.getS("Calcul des vitesses manuelles"));
    }

    @Override
    public boolean act(PivTaskObserverI _observer) {
      if (type_ == TaskType.COMPUTE) {
        CtuluLog ana = new CtuluLog();
        ana.setDesc(PivResource.getS("Calcul manuel des vitesses"));

        PivExeLauncher.instance().computeManualVelocities(ana, impl_.getCurrentProject(), null);
        if (ana.containsErrorOrSevereError()) {
          impl_.error(ana.getResume());
          return false;
        }
      }

      // Nettoyage de l'�tape de calcul des vitesses manuelles.
      else {
        impl_.getCurrentProject().removeManualResultsOnFS();
        impl_.getCurrentProject().reloadManualResults();
      }

      return true;

    }
  }
  
  /**
   * La tache de calcul des vitesses brutes.
   * @author marchand
   *
   */
  public class ComputeRawVelocitiesTask extends StepTask {
    
    public ComputeRawVelocitiesTask(TaskType _type) {
      super(_type, PivResource.getS("Calcul des vitesses brutes"));
    }

    @Override
    public boolean act(PivTaskObserverI _observer) {
      if (type_ == TaskType.COMPUTE) {
        CtuluLog ana = new CtuluLog();
        ana.setDesc(this.getName());
        PivExeLauncher.instance().computeRawInstantResults(ana, impl_.getCurrentProject(), false, _observer);
        if (ana.containsErrorOrSevereError()) {
          impl_.error(ana.getResume());
          return false;
        }
      }

      // Nettoyage de l'�tape de calcul des vitesses instantan�es.
      else {
        impl_.getCurrentProject().removeInstantRawResultsOnFS();
        impl_.getCurrentProject().reloadInstantRawResults();
      }

      return true;
    }
    
  }
  
  /**
   * La tache de calcul des vitesses filtr�es.
   * @author marchand
   *
   */
  public class ComputeFilteredVelocitiesTask extends StepTask {
    
    public ComputeFilteredVelocitiesTask(TaskType _type) {
      super(_type, PivResource.getS("Calcul des vitesses filtr�es"));
    }

    @Override
    public boolean act(PivTaskObserverI _observer) {
      if (type_ == TaskType.COMPUTE) {
        CtuluLog ana = new CtuluLog();
        ana.setDesc(getName());

        // Filtrage
        PivExeLauncher.instance().computeFilteredInstantResultats(ana, impl_.getCurrentProject(), _observer);
        if (ana.containsErrorOrSevereError()) {
          impl_.error(ana.getResume());
          return false;
        }
      }

      // Nettoyage de l'�tape de calcul des vitesses filtr�es.
      else {
        impl_.getCurrentProject().removeInstantFilteredResultsOnFS();
        impl_.getCurrentProject().reloadInstantFilteredResults();
      }
      
      return true;
    }
  }
  
  /**
   * La tache de calcul des vitesses moyennes.
   * @author marchand
   *
   */
  public class ComputeAveragedVelocitiesTask extends StepTask {
    
    public ComputeAveragedVelocitiesTask(TaskType _type) {
      super(_type, PivResource.getS("Calcul des vitesses moyennes"));
    }

    @Override
    public boolean act(PivTaskObserverI _observer) {
      if (type_ == TaskType.COMPUTE) {
        CtuluLog ana = new CtuluLog();
        ana.setDesc(getName());

        // Moyenne
        PivExeLauncher.instance().computeAverageResults(ana, impl_.getCurrentProject(), _observer);
        if (ana.containsErrorOrSevereError()) {
          impl_.error(ana.getResume());
          return false;
        }
      }

      // Nettoyage de l'�tape de calcul des vitesses moyennes.
      else {
        impl_.getCurrentProject().removeAverageResultsOnFS();
        impl_.getCurrentProject().reloadAverageResults();
      }
      
      return true;
    }
  }
  
  /**
   * La tache de calcul des d�bits.
   * @author marchand
   *
   */
  public class ComputeFlowTask extends StepTask {
    
    public ComputeFlowTask(TaskType _type) {
      super(_type, PivResource.getS("Calcul des d�bits"));
    }

    @Override
    public boolean act(PivTaskObserverI _observer) {
      if (type_ == TaskType.COMPUTE) {
        CtuluLog ana = new CtuluLog();
        ana.setDesc(this.getName());

        PivExeLauncher.instance().computeFlowResults(ana, impl_.getCurrentProject(), true, _observer);
        if (ana.containsErrorOrSevereError()) {
          impl_.error(ana.getResume());
          return false;
        }
      }

      // Nettoyage de l'�tape de calcul des d�bits.
      else {
        impl_.getCurrentProject().removeFlowResultsOnFS();
        impl_.getCurrentProject().reloadFlowResults();
      }

      return true;
    }
  }
  
  /**
   * La tache pour toutes les etapes de calcul.
   * @author marchand
   */
  public class GlobalTask extends PivTaskAbstract {
    PivImplementation impl_;
    PivTaskAbstract currentStep_;
    List<PivTaskAbstract> steps_ = new ArrayList<>();
    // La liste des steps susceptibles d'etre ex�cut�s qui sont s�lectionn�s
    List<Boolean> isStepSelected_ = new ArrayList<>();
    
    PivTaskAbstract stabTask_;
    PivTaskAbstract orthoTask_;
    PivTaskAbstract rawVelTask_;
    PivTaskAbstract manualVelTask_;
    PivTaskAbstract fltVelTask_;
    PivTaskAbstract aveVelTask_;
    PivTaskAbstract flowTask_;
    
    public GlobalTask(PivImplementation _impl) {
      super();
      impl_ = _impl;
      TaskType type = TaskType.COMPUTE;
      
      steps_.add(stabTask_ = new StabilizationTask(type));
      steps_.add(orthoTask_ = new OrthoTask(type));
      steps_.add(rawVelTask_ = new ComputeRawVelocitiesTask(type));
      steps_.add(manualVelTask_ = new ComputeManualVelocitiesTask(type));
      steps_.add(fltVelTask_ = new ComputeFilteredVelocitiesTask(type));
      steps_.add(aveVelTask_ = new ComputeAveragedVelocitiesTask(type));
      // On reconditionne obligatoirement les transects.
      steps_.add(flowTask_ = new ComputeFlowTask(type));
    }
    
    @Override
    public void setObserver(PivTaskObserverI _observer) {
      super.setObserver(_observer);
      
      for (PivTaskAbstract task : steps_) {
        task.setObserver(_observer);
      }
    }
    
    @Override
    public boolean act(PivTaskObserverI _observer) {
      try {
        for (PivTaskAbstract task : steps_) {
          if (task.status_ == TaskStatus.WAITING) {

            // Avant de lancer, on met en aborted si demand�.
            if (isStopRequested()) {
              task.setStatus(TaskStatus.ABORTED);
              return false;
            }

            task.setStatus(TaskStatus.RUNNING);
            boolean b = task.act(PivComputeTrackingPanel.this);
            if (b) {
              task.setStatus(TaskStatus.SUCCESS);
            }
            else {
              if (isStopRequested()) {
                task.setStatus(TaskStatus.ABORTED);
              }
              else {
                task.setStatus(TaskStatus.FAILED);
              }
              return false;
            }
          }
        }

        return true;
      }
      
      finally {
        if (isStopRequested()) {
          boolean foundAborted = false;
          for (PivTaskAbstract task : steps_) {
            if (foundAborted) {
              if (task.getStatus() == TaskStatus.WAITING) 
                task.setStatus(TaskStatus.FAILED);
            }
            else if (task.getStatus() == TaskStatus.ABORTED) {
              foundAborted = true;
            }
          }
        }
      }
    }

    public List<PivTaskAbstract> getSteps() {
      return steps_;
    }
    
    
    /**
     * Reinitialise les etapes en fonction des param�tres et resultats pour cette �tape.
     */
    public void reset() {
      PivProject prj = impl_.getCurrentProject();
      TaskStatus state;
      // Indique qu'une etape pr�c�dente est waiting, pour que les suivantes qui seraient � jour soient mises en Waiting.
      boolean previousIsWaiting = false;
      //  Indique que l'etape de  calcul manuel est en attente
      boolean manualIsWaiting = false;
      
      // Stabilisation
      if (prj.getStabilizationParameters() == null || !prj.getStabilizationParameters().isActive()) {
        state = TaskStatus.PASSED;
      }
      else if (prj.getStabilizedImageFiles().length > 0 && !previousIsWaiting) {
        state = TaskStatus.UP_TO_DATE;
      }
      else {
        state = TaskStatus.WAITING;
        previousIsWaiting = true;
      }
      stabTask_.setStatus(state);
      
      // Orthorectification
      if (prj.getOrthoParameters() == null) {
        state = TaskStatus.PASSED;
      }
      // 1 seule image signifie qu'un apply a �t� r�alis�, pas que toutes les images ont �t� orthorectifi�es.
      else if (prj.getTransfImageFiles().length > 1 && !previousIsWaiting) {
        state = TaskStatus.UP_TO_DATE;
      }
      else {
        state = TaskStatus.WAITING;
        previousIsWaiting = true;
      }
      orthoTask_.setStatus(state);
      
      if (orthoTask_.getStatus() == TaskStatus.PASSED || prj.getManualVelocitiesParameters() == null) {
        state = TaskStatus.PASSED;
      }
      else if (prj.hasManualResults() && !previousIsWaiting) {
        state = TaskStatus.UP_TO_DATE;
      }
      else {
        state = TaskStatus.WAITING;
        manualIsWaiting = true;
      }
      manualVelTask_.setStatus(state);
      
      // Calcul des vitesses brutes
      if (orthoTask_.getStatus() == TaskStatus.PASSED || prj.getComputeParameters() == null || prj.getComputeGrid() == null) {
        state = TaskStatus.PASSED;
      }
      else if (prj.hasInstantRawResults() && !previousIsWaiting) {
        state = TaskStatus.UP_TO_DATE;
      }
      else {
        state = TaskStatus.WAITING;
        previousIsWaiting = true;
      }
      rawVelTask_.setStatus(state);
      
      // Calcul des vitesses filtr�es
      if (rawVelTask_.getStatus() == TaskStatus.PASSED || prj.getComputeParameters() == null) {
        state = TaskStatus.PASSED;
      }
      else if (prj.hasInstantFilteredResults() && !previousIsWaiting) {
        state = TaskStatus.UP_TO_DATE;
      }
      else {
        state = TaskStatus.WAITING;
        previousIsWaiting = true;
      }
      fltVelTask_.setStatus(state);
      
      // Calcul des vitesses moyenn�es
      if (fltVelTask_.getStatus() == TaskStatus.PASSED || prj.getUsedInstantResults() == null) {
        state = TaskStatus.PASSED;
      }
      else if (prj.hasAverageResults() && !previousIsWaiting && !manualIsWaiting) {
        state = TaskStatus.UP_TO_DATE;
      }
      else {
        state = TaskStatus.WAITING;
        previousIsWaiting = true;
      }
      aveVelTask_.setStatus(state);
      
      // Calcul des d�bits
      if (aveVelTask_.getStatus() == TaskStatus.PASSED || prj.getTransects() == null || prj.getTransects().length == 0) {
        state = TaskStatus.PASSED;
      }
      else if (prj.hasFlowResults() && !previousIsWaiting) {
        state = TaskStatus.UP_TO_DATE;
      }
      else {
        state = TaskStatus.WAITING;
        previousIsWaiting = true;
      }
      flowTask_.setStatus(state);
    }
  }
  
  /**
   * Le modele pour la table des taches.
   * @author marchand
   */
  public class StepsTableModel extends AbstractTableModel {
    String[] colNames = new String[] { "", PivResource.getS("Etape"), PivResource.getS("Statut") };
    
    @Override
    public int getRowCount() {
      return stepsTask_.getSteps().size();
    }

    @Override
    public int getColumnCount() {
      return 3;
    }
    
    @Override
    public String getColumnName(int _col) {
      return colNames[_col];
    }

    @Override
    public Class<?> getColumnClass(int _col) {
      if (_col == 0) {
        return Boolean.class;
      }
      else if (_col == 1) {
        return String.class;
      }
      else {
        return TaskStatus.class;
      }
     }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      if (columnIndex == 0) {
        TaskStatus ts = stepsTask_.getSteps().get(rowIndex).getStatus();
        if (ts == TaskStatus.WAITING)
          return true;
        else if (ts == TaskStatus.RUNNABLE)
          return false;
        else
          return null;
      }
      else if (columnIndex == 1) {
        return stepsTask_.getSteps().get(rowIndex).getName();
      }
      else {
        return stepsTask_.getSteps().get(rowIndex).getStatus();
      }
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      TaskStatus ts = stepsTask_.getSteps().get(rowIndex).getStatus();
      return columnIndex == 0 && !isRunning_ &&  (ts == TaskStatus.WAITING || ts == TaskStatus.RUNNABLE || ts == TaskStatus.UP_TO_DATE);
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
      if (columnIndex == 0) {
        boolean bsel = (Boolean)aValue;
        if (bsel) {
          for (int i = 0; i < rowIndex + 1; i++) {
            PivTaskAbstract task = stepsTask_.getSteps().get(i);
            if (task.getStatus() == TaskStatus.RUNNABLE) {
              task.setStatus(TaskStatus.WAITING);
            }
          }
        }
        else {
          for (int i = rowIndex; i < stepsTask_.getSteps().size(); i++) {
            PivTaskAbstract task = stepsTask_.getSteps().get(i);
            if (task.getStatus() == TaskStatus.WAITING) {
              task.setStatus(TaskStatus.RUNNABLE);
            }
          }
        }
        
        fireTableDataChanged();
      }
    }
  }
  
  public class StepSelectedRenderer extends JCheckBox implements TableCellRenderer {

    public StepSelectedRenderer() {
      setOpaque(true);
      setHorizontalAlignment(SwingConstants.CENTER);
    }
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
        int row, int column) {
      
      if (value instanceof Boolean) {
        setEnabled(true);
        setSelected((Boolean)value);
      }
      else {
        setSelected(false);
        setEnabled(false);
      }
      
      if (isSelected) {
        setBackground(UIManager.getColor("Table.selectionBackground"));
      }
      else {
        setBackground(UIManager.getColor("Table.background"));
      }
      
      return this;
    }
  }
  
  /**
   * Un renderer pour le statut d'une etape.
   * @author marchand
   */
  public class StepStatusRenderer extends JComponent implements TableCellRenderer {
    private JLabel lb = new JLabel();
    private JProgressBar pb = new JProgressBar();
    
    public StepStatusRenderer() {
      lb.setOpaque(true);
      lb.setFont(UIManager.getFont("Table.font"));
      pb.setMinimum(0);
      pb.setMaximum(100);
      pb.setBackground(UIManager.getColor("Table.background"));
      pb.setFont(UIManager.getFont("Table.font"));
      pb.setBorderPainted(false);
      pb.setStringPainted(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
        int row, int column) {
      
      if (isSelected) {
        lb.setBackground(UIManager.getColor("Table.selectionBackground"));
      }
      else {
        lb.setBackground(UIManager.getColor("Table.background"));
      }
      
      JComponent cp = lb;
      
      if (value instanceof TaskStatus) {
        TaskStatus status = (TaskStatus)value;
        switch (status) {
        case FAILED:
          lb.setIcon(BuResource.BU.getIcon("annuler"));
          lb.setText(PivResource.getS("Erreur"));
          cp = lb;
          break;
        case ABORTED:
          lb.setIcon(BuResource.BU.getIcon("annuler"));
          lb.setText(PivResource.getS("Interrompu"));
          cp = lb;
          break;
        case SUCCESS:
          lb.setIcon(BuResource.BU.getIcon("valider"));
          lb.setText(PivResource.getS("Succ�s"));
          cp = lb;
          break;
        case PASSED:
        case RUNNABLE:
          lb.setIcon(null);
          lb.setText(PivResource.getS("Ignor�"));
          break;
        case RUNNING:
          String s = progression_ + "%";
          if (desc_ != null && desc_ != "") {
            s += " : " + desc_;
          }
          pb.setString(s);
          pb.setValue(progression_);
          cp = pb;
          break;
        case UP_TO_DATE:
          lb.setIcon(null);
          lb.setText(PivResource.getS("A jour"));
          cp = lb;
          break;
        case WAITING:
          lb.setIcon(null);
          lb.setText(PivResource.getS("En attente..."));
          cp = lb;
          break;
        }
      }
      else {
        lb.setText("");
        lb.setIcon(null);
        cp = lb;
      }
      
      return cp;
    }
    
  }

  /**
   * Creates new form PivComputingTrackingPanel
   */
  public PivComputeTrackingPanel(PivImplementation _impl) {
    impl_ = _impl;

    initComponents();
    customize();
    
    stepsTask_ = new GlobalTask(_impl);
    stepsTask_.setObserver(this);
    stepsTask_.reset();
  }
  
  private void customize() {
    setHelpText(PivResource.getS("Les �tapes ignor�es sont celles dont les param�tres ne sont pas renseign�s,\nou dont la d�pendance aux �tapes pr�c�dentes ne permet pas le calcul."));
    lbSteps.setText(PivResource.getS("Etapes du calcul"));
    lbConfirm.setText(PivResource.getS("<html>Les �tapes s�lectionn�es seront lanc�es avec les param�tres actuels. Voulez vous continuer ?</html>"));

    mdSteps_ = new StepsTableModel();
    tbSteps.setModel(mdSteps_);
    tbSteps.getColumnModel().getColumn(0).setPreferredWidth(15);
    tbSteps.getColumnModel().getColumn(0).setCellRenderer(new StepSelectedRenderer());
    tbSteps.getColumnModel().getColumn(1).setPreferredWidth(200);
    tbSteps.getColumnModel().getColumn(2).setCellRenderer(new StepStatusRenderer());
    tbSteps.getColumnModel().getColumn(2).setPreferredWidth(300);
    tbSteps.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    
    setPreferredSize(new Dimension(600,250));
  }
  
  public void setDialog(CtuluDialog _dialog) {
    super.setDialog(_dialog);
    
//    _dialog.setButtonText(CtuluDialog.OK_OPTION, PivResource.getS("Fermer"));
//    _dialog.setButtonEnabled(CtuluDialog.OK_OPTION, false);
//    _dialog.setButtonText(CtuluDialog.CANCEL_OPTION, PivResource.getS("Stop"));
    
    // Pour etre sur que la task n'est lanc�e qu'un fois le dialogue cr�� (sinon blocage).
//    stepsTask_.start();
  }
  
  /**
   * Surcharg� pour ne pas fermer la fen�tre et afficher un message d'attente
   * d'interruption de la tache.
   */
  @Override
  public boolean cancel() {
    if (isRunning_) {
      isStopRequested_ = true;

      this.setErrorText(PivResource.getS("Interruption en cours. Merci de patienter..."));
      return false;
    }
    else {
      return true;
    }
  }
  
  @Override
  public boolean ok() {
    if (!isCompleted_) {

      getDialog().setButtonText(CtuluDialog.OK_OPTION, PivResource.getS("Fermer"));
      getDialog().setButtonEnabled(CtuluDialog.OK_OPTION, false);
      getDialog().setButtonText(CtuluDialog.CANCEL_OPTION, PivResource.getS("Stop"));
      getDialog().setDefaultCloseOperation(CtuluDialog.DO_NOTHING_ON_CLOSE);
      lbConfirm.setText("");
    
      stepsTask_.start();
      isRunning_ = true;
      return false;
    }
    else {
      return true;
    }
  }
  
  @Override
  public boolean isStopRequested() {
    return isStopRequested_;
  }

  @Override
  public void completed() {
    isCompleted_ = true;
    
    getDialog().setButtonEnabled(CtuluDialog.OK_OPTION, true);
    getDialog().setButtonEnabled(CtuluDialog.CANCEL_OPTION, false);
    this.setErrorText(null);
    getDialog().setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
  }

  @Override
  public void appendDetail(String _s) {
  }

  @Override
  public void appendDetailln(String _s) {
  }

  @Override
  public void setProgression(int _v) {
    progression_ = _v;
    FuLog.trace("Prog="+progression_);
    mdSteps_.fireTableDataChanged();
  }

  @Override
  public void setDesc(String _s) {
    desc_ = _s;
    FuLog.trace("Desc="+desc_);
    mdSteps_.fireTableDataChanged();
  }

  @Override
  public void reset() {
    mdSteps_.fireTableDataChanged();
  }

  @Override
  public void setStatus(TaskStatus _state) {
    mdSteps_.fireTableDataChanged();
  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbSteps = new javax.swing.JLabel();
        spSteps = new javax.swing.JScrollPane();
        tbSteps = new javax.swing.JTable();
        lbConfirm = new javax.swing.JLabel();

        lbSteps.setText("Etapes du calcul");

        tbSteps.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"Stabilisation", "Passed (no parameters defined)"},
                {"Orthorectification", "Up to date"},
                {"Calcul des r�sultats bruts", "Ok"},
                {"Calcul des r�sultats filtr�s", "1/10 image..."},
                {"Calcul des r�sultats moyenn�s", "Waiting"},
                {"Calcul des d�bits", "Passed (no parameters defined)"}
            },
            new String [] {
                "Etape", "Progression"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        spSteps.setViewportView(tbSteps);

        lbConfirm.setText("<html>Les �tapes s�lectionn�es seront lanc�es avec les param�tres actuels</html>");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(spSteps, javax.swing.GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbSteps)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(lbConfirm, javax.swing.GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbSteps)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(spSteps, javax.swing.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(lbConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lbConfirm;
    private javax.swing.JLabel lbSteps;
    private javax.swing.JScrollPane spSteps;
    private javax.swing.JTable tbSteps;
    // End of variables declaration//GEN-END:variables
}

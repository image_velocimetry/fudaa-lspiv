package org.fudaa.fudaa.piv.imageio;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

import com.memoire.fu.FuLog;

/**
 * Une classe de lecture d'une image PGM. Cette classe ne respecte pas le standard IIO, mais
 * est beaucoup simple et rapide.
 * Les format P5 ou P2 sont g�r�s automatiquement.
 * 
 * @author Bertrand Marchand
 * @since 1.3
 * @version $Id$
 */
public class PivPGMReader {
  
  /** Les champs d'une ligne, stock�s pour plus d'efficacit�. */
  String[] fields=new String[0];
  /** Le compteur de champs de la ligne, remis � zero � chaque nouvelle lecture de ligne. */
  int icptField=0;
  
  /**
   * Lit le fichier PGM en entr�e, et retourne l'image sous forme de buffer raster.
   * @param _srcFile Le fichier image.
   * @return L'image bufferis�e.
   * @throws IOException Si le format est incorrect.
   */
  public BufferedImage read(File _srcFile) throws IOException {
    PivPGMFormat fmt = PivPGMFormat.P2;
    
    FuLog.trace("Lecture du fichier "+_srcFile.getName());

    BufferedImage buf=null;
    
    LineNumberReader reader=new LineNumberReader(new FileReader(_srcFile));
    
    String s=getNextField(reader);
    if (s.equals("P2"))
      fmt = PivPGMFormat.P2;
    else if (s.equals("P5"))
      fmt = PivPGMFormat.P5;
    else
      throw new IOException("Bad file");
    
    try {
      int width=Integer.parseInt(getNextField(reader));
      int height=Integer.parseInt(getNextField(reader));
      int maxValue=Integer.parseInt(getNextField(reader));
      if (maxValue!=255)
        throw new IOException("Bad file");
      
      buf=new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
      
      if (fmt == PivPGMFormat.P2)
        readP2Format(width, height, reader, buf);
      else
        readP5Format(width, height, reader, buf);
    }
    catch (NumberFormatException _exc) {
      throw new IOException("Bad file");
    }
    reader.close();
    
    
    return buf;
  }
  
  /**
   * Lit la suite du fichier au format P2
   */
  private void readP2Format(int width, int height, LineNumberReader reader, BufferedImage buf) throws IOException {
    for (int i=0; i<height; i++) {
      for (int j=0; j<width; j++) {
        int val=Integer.parseInt(getNextField(reader));
        buf.getRaster().setPixel(j, i, new double[]{val});
      }
    }
  }
  
  /**
   * Lit la suite du fichier au format P5
   */
  private void readP5Format(int width, int height, LineNumberReader reader, BufferedImage buf) throws IOException {
    for (int i=0; i<height; i++) {
      for (int j=0; j<width; j++) {
        int val = reader.read();
        buf.getRaster().setPixel(j, i, new double[]{val});
      }
    }
  }
  
  /**
   * Retourne le champs suivant. Si aucun champs sur la ligne, lecture des
   * lignes suivantes jusqu'� trouver un nouveau champ. Si la fin de fichier
   * est trouv�e, retourne null.
   * 
   * @return Le champs suivant, ou null si plus aucun champs trouv�.
   */
  private String getNextField(LineNumberReader _r) throws IOException {
    if (icptField >= fields.length) {
      boolean bfound = false;
      while (!bfound) {
        final String line=_r.readLine();
        if (line == null)
          throw new IOException();
        if (line.startsWith("#"))
          continue;

        fields = line.trim().split("\\s+");
        bfound = (fields.length != 0);
      }
      icptField = 0;
    }

    return fields[icptField++];
  }

}

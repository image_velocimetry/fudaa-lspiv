package org.fudaa.fudaa.piv.imageio;

import java.io.IOException;
import java.util.Locale;

import javax.imageio.ImageReader;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.ImageInputStream;

/**
 * Cette classe n'est pas assez avanc�e pour etre utilis�e.
 * @author Bertrand Marchand
 * @deprecated Use {@link PivPGMReader} instead
 */
public class PivPGMImageReaderSpi extends ImageReaderSpi {

  static final String vendorName = "DeltaCAD";
  static final String version = "1.0";
  static final String readerClassName = "org.fudaa.fudaa.piv.imageio.PivPGMImageReader";
  static final String[] names = { "pgm" };
  static final String[] suffixes = { "pgm" };
  static final String[] MIMETypes = { "image/x-portable-graymap" };
  static final String[] writerSpiNames = { "org.fudaa.fudaa.piv.imageio.PivPGMImageWriterSpi" };

  // Metadata formats, more information below
  static final boolean supportsStandardStreamMetadataFormat = false;
  static final String nativeStreamMetadataFormatName = null;
  static final String nativeStreamMetadataFormatClassName = null;
  static final String[] extraStreamMetadataFormatNames = null;
  static final String[] extraStreamMetadataFormatClassNames = null;
  static final boolean supportsStandardImageMetadataFormat = false;
  static final String nativeImageMetadataFormatName = null;
  static final String nativeImageMetadataFormatClassName = null;
  static final String[] extraImageMetadataFormatNames = null;
  static final String[] extraImageMetadataFormatClassNames = null;

  public PivPGMImageReaderSpi() {
    super(
        vendorName,
        version,
        names,
        suffixes,
        MIMETypes,
        readerClassName,
        STANDARD_INPUT_TYPE,
        writerSpiNames,
        supportsStandardStreamMetadataFormat,
        nativeStreamMetadataFormatName,
        nativeStreamMetadataFormatClassName,
        extraStreamMetadataFormatNames,
        extraStreamMetadataFormatClassNames,
        supportsStandardImageMetadataFormat,
        nativeImageMetadataFormatName,
        nativeImageMetadataFormatClassName,
        extraImageMetadataFormatNames,
        extraImageMetadataFormatClassNames);
  }

  public String getDescription(Locale locale) {
    // Localize as appropriate
    return "Grey Ascii PGM";
  }

  public boolean canDecodeInput(Object input) throws IOException {
    if (!(input instanceof ImageInputStream)) {
      return false;
    }

    ImageInputStream stream = (ImageInputStream) input;
    byte[] b = new byte[2];
    try {
      stream.mark();
      stream.readFully(b);
      stream.reset();
    }
    catch (IOException e) {
      return false;
    }

    // Cast unsigned character constants prior to comparison
    return (b[0] == (byte) 'P' && b[1] == (byte) '2');
  }

  public ImageReader createReaderInstance(Object extension) {
    return new PivPGMImageReader(this);
  }
}

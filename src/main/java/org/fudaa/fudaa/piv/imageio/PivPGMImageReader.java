package org.fudaa.fudaa.piv.imageio;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.imageio.IIOException;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.spi.ImageReaderSpi;
import javax.imageio.stream.ImageInputStream;

/**
 * Cette classe n'est pas assez avanc�e pour etre utilis�e.
 * @author Bertrand Marchand
 * @deprecated Use {@link PivPGMReader} instead
 */
public class PivPGMImageReader extends ImageReader {

  ImageInputStream stream = null;
  int width, height;
  int colorType;
  int maxValue;

  // Constants enumerating the values of colorType
  static final int COLOR_TYPE_GRAY = 0;
  static final int COLOR_TYPE_RGB = 1;

  boolean gotHeader = false;

  public PivPGMImageReader(ImageReaderSpi originatingProvider) {
    super(originatingProvider);
  }

  public void setInput(Object input, boolean seekForwardOnly, boolean ignoreMetadata) {
    super.setInput(input, seekForwardOnly, ignoreMetadata);
    if (input == null) {
      this.stream = null;
      return;
    }
    if (input instanceof ImageInputStream) {
      this.stream = (ImageInputStream) input;
    }
    else {
      throw new IllegalArgumentException("bad input");
    }
  }

  public int getNumImages(boolean allowSearch) throws IIOException {
    return 1; // format can only encode a single image
  }

  private void checkIndex(int imageIndex) {
    if (imageIndex != 0) {
      throw new IndexOutOfBoundsException("bad index");
    }
  }

  public int getWidth(int imageIndex) throws IIOException {
    checkIndex(imageIndex); // must throw an exception if != 0
    readHeader();
    return width;
  }

  public int getHeight(int imageIndex) throws IIOException {
    checkIndex(imageIndex);
    readHeader();
    return height;
  }
  
  public Iterator<ImageTypeSpecifier> getImageTypes(int imageIndex) throws IIOException {
    checkIndex(imageIndex);
//    readHeader();

    ImageTypeSpecifier imageType = null;
    int datatype = DataBuffer.TYPE_BYTE;
    java.util.List<ImageTypeSpecifier> l = new ArrayList<ImageTypeSpecifier>();
//    switch (colorType) {
    
//    case COLOR_TYPE_GRAY:
    // Seul le type Gray scale est autorise
    imageType = ImageTypeSpecifier.createGrayscale(8, datatype, false);
//      break;
//
//    case COLOR_TYPE_RGB:
//      ColorSpace rgb = ColorSpace.getInstance(ColorSpace.CS_sRGB);
//      int[] bandOffsets = new int[3];
//      bandOffsets[0] = 0;
//      bandOffsets[1] = 1;
//      bandOffsets[2] = 2;
//      imageType = ImageTypeSpecifier.createInterleaved(rgb, bandOffsets, datatype, false, false);
//      break;
//    }
    l.add(imageType);
    return l.iterator();
  }

  public void readHeader() throws IIOException {
    if (gotHeader) {
      return;
    }
    gotHeader = true;

    if (stream == null) {
      throw new IllegalStateException("No input stream");
    }

    // Read `myformat\n' from the stream
    try {
      stream.seek(0);
      
      String s;
      s=getNextField();
      if (!s.equals("P2")) {
        throw new IIOException("Bad file signature!");
      }
      this.width=Integer.parseInt(getNextField());
      this.height=Integer.parseInt(getNextField());
      this.maxValue=Integer.parseInt(getNextField());
    }
    catch (NumberFormatException e) {
      throw new IIOException("Error reading signature", e);
    }
    catch (IOException e) {
      throw new IIOException("Error reading signature", e);
    }
  }
  
  String[] fields=new String[0];
  int icptField=0;
  
  /**
   * @return Le champs suivant, ou null si plus aucun champs trouv�.
   */
  final public String getNextField() throws IOException {
    if (icptField >= fields.length) {
      boolean bfound = false;
      while (!bfound) {
        final String line=getLine();
        if (line == null)
          throw new IOException();
        if (line.startsWith("#"))
          continue;

        fields = line.trim().split("\\s+");
        bfound = (fields.length != 0);
      }
      icptField = 0;
    }

    return fields[icptField++];
  }
  
  final StringBuffer sb=new StringBuffer();
  final byte[] cache=new byte[1024*50];
  int ioffcache=0;
  int lg=0;

  /**
   * Lit une ligne du fichier. Version optimis�e pour acc�l�rer la lecture des pgm. L'optimisation
   * consiste a utiliser un cache de lecture des bytes. La lecture standard des byte 1 � 1 est en 
   * effet tr�s long.
   * @return La ligne, ou null si plus rien a lire.
   */
  private final String getLine() {
    sb.setLength(0);

    try {
      do {
        while (ioffcache < lg) {
          if (cache[ioffcache] == '\r' || cache[ioffcache] == '\n') {
            ioffcache++;
            if (sb.length() == 0) {
              continue;
            }
            else {
              return sb.toString();
            }
          }
          else {
            sb.append((char) cache[ioffcache]);
            ioffcache++;
          }
        }
        
        lg = stream.read(cache);
        ioffcache=0;
      }
      while (lg != -1);

      return null;
    }
    catch (IOException e) {
      return null;
    }
  }
  
  public IIOMetadata getStreamMetadata() throws IIOException {
    return null;
  }

  /**
   * Pas de meta-data a lire.
   * 
   * @throws IIOException
   */
  public IIOMetadata getImageMetadata(int imageIndex) throws IIOException {
    if (imageIndex != 0) {
      throw new IndexOutOfBoundsException("imageIndex != 0!");
    }
    return null;
  }
  
  public BufferedImage read(int imageIndex, ImageReadParam param)
      throws IIOException {
      readHeader(); // Stream is positioned at start of image data
      
      // Compute initial source region, clip against destination later
      Rectangle sourceRegion = getSourceRegion(param, width, height);
      
      // Set everything to default values
      int sourceXSubsampling = 1;
      int sourceYSubsampling = 1;
      int[] sourceBands = null;
      int[] destinationBands = null;
      Point destinationOffset = new Point(0, 0);

      // Get values from the ImageReadParam, if any
      if (param != null) {
              sourceXSubsampling = param.getSourceXSubsampling();
              sourceYSubsampling = param.getSourceYSubsampling();
              sourceBands = param.getSourceBands();
              destinationBands = param.getDestinationBands();
              destinationOffset = param.getDestinationOffset();
      }
      
      // Get the specified detination image or create a new one
      BufferedImage dst = getDestination(param,
                                         getImageTypes(0),
                                         width, height);
      
      // Ensure band settings from param are compatible with images
      int inputBands = 1;
      checkReadParamBandSettings(param, inputBands,
                                 dst.getSampleModel().getNumBands());
      int[] bandOffsets = new int[inputBands];
      for (int i = 0; i < inputBands; i++) {
              bandOffsets[i] = i;
      }
      
      int bytesPerRow = width*inputBands;
      DataBufferByte rowDB = new DataBufferByte(bytesPerRow);
      WritableRaster rowRas =
              Raster.createInterleavedRaster(rowDB,
                                             width, 1, bytesPerRow,
                                             inputBands, bandOffsets,
                                             new Point(0, 0));
      byte[] rowBuf = rowDB.getData();

      // Create an int[] that can a single pixel
      int[] pixel = rowRas.getPixel(0, 0, (int[])null);
      
      WritableRaster imRas = dst.getWritableTile(0, 0);
      int dstMinX = imRas.getMinX();
      int dstMaxX = dstMinX + imRas.getWidth() - 1;
      int dstMinY = imRas.getMinY();
      int dstMaxY = dstMinY + imRas.getHeight() - 1;

      // Create a child raster exposing only the desired source bands
      if (sourceBands != null) {
              rowRas = rowRas.createWritableChild(0, 0,
                                                  width, 1,
                                                  0, 0,
                                                  sourceBands);
      }

      // Create a child raster exposing only the desired dest bands
      if (destinationBands != null) {
              imRas = imRas.createWritableChild(0, 0,
                                                imRas.getWidth(),
                                                imRas.getHeight(),
                                                0, 0,
                                                destinationBands);
      }
      
      for (int srcY = 0; srcY < height; srcY++) {
        // Read the row
        try {
          for (int srcX = 0; srcX < width ; srcX++) {
            String s=getNextField();
            rowBuf[srcX]=(byte)Integer.parseInt(s);
          }
        } catch (NumberFormatException e) {
          throw new IIOException("Error reading line " + srcY, e);
        } 
        catch (IOException e) {
                throw new IIOException("Error reading line " + srcY, e);
        }
                
        // Reject rows that lie outside the source region,
        // or which aren't part of the subsampling
        if ((srcY < sourceRegion.y) ||
            (srcY >= sourceRegion.y + sourceRegion.height) ||
            (((srcY - sourceRegion.y) %
              sourceYSubsampling) != 0)) {
                continue;
        }

        // Determine where the row will go in the destination
        int dstY = destinationOffset.y +
                (srcY - sourceRegion.y)/sourceYSubsampling;
        if (dstY < dstMinY) {
                continue; // The row is above imRas
        }
        if (dstY > dstMaxY) {
                break; // We're done with the image
        }

        // Copy each (subsampled) source pixel into imRas
        for (int srcX = sourceRegion.x;
             srcX < sourceRegion.x + sourceRegion.width;
             srcX++) {
                if (((srcX - sourceRegion.x) % sourceXSubsampling) != 0) {
                        continue;
                }
                int dstX = destinationOffset.x +
                        (srcX - sourceRegion.x)/sourceXSubsampling;
                if (dstX < dstMinX) {
                        continue;  // The pixel is to the left of imRas
                }
                if (dstX > dstMaxX) {
                        break; // We're done with the row
                }

                // Copy the pixel, sub-banding is done automatically
                rowRas.getPixel(srcX, 0, pixel);
                imRas.setPixel(dstX, dstY, pixel);
        }
}
return dst;
  }
}

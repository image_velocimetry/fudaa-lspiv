/**
 * Licence GPL Copyright Genesis
 */
package org.fudaa.fudaa.piv.particles;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluAnalyzeGroup;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISAttributeModel;
import org.fudaa.ctulu.gis.GISAttributeModelBooleanInterface;
import org.fudaa.ctulu.gis.GISAttributeModelDoubleInterface;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gui.CtuluAnalyzeGUI;
import org.fudaa.dodico.ef.operation.EfTrajectoireActivity;
import org.fudaa.dodico.ef.operation.EfTrajectoireParameters;
import org.fudaa.dodico.ef.operation.EfTrajectoireResultBuilder;
import org.fudaa.ebli.calque.ZModeleLigneBrisee;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeDefault;
import org.fudaa.ebli.commun.EbliUIProperties;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.palette.BPalettePlageProperties;
import org.fudaa.ebli.palette.PaletteManager;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.sig.layer.FSigLayerLine;

/**
 * Le layer charge d'afficher les donn�es d'une trajectoire
 * 
 * @author deniger
 */
public final class PivParticlesLayer extends FSigLayerLine /*implements BSelecteurListTarget, ListSelectionListener*/ {

//  final DefaultListModel attListModel_;
//  final ListSelectionModel attSelectionModel_;
  final List<GISAttributeInterface> attList_;
  GISAttributeInterface attSel_;

  /**
   * Only modified form constructor or from initFrom method.
   */
//  GISAttributeInterface attrMark_=PivParticlesGISDataModel.ATT_MARK;
//  GISAttributeInterface attrTime_=new GISAttributeDouble("Time", true);
//  GISAttributeInterface attrVX_=new GISAttributeDouble("VX", true);
//  GISAttributeInterface attrVY_=new GISAttributeDouble("VY", true);

  Map<String, BPalettePlage> namePalette_;

  PivVisuPanel vue2d_;
  PivParticlesSourceI src_;

  /**
   * Structures qui contient toutes les donn�es importantes de construction de la trajectoire. A serialiser pour pouvoir
   * rejouer les donnees.
   */
  EfTrajectoireParameters dataMemory_;

  public PivParticlesLayer(final PivVisuPanel vue2d, final PivParticlesSourceI _src, final ZModeleLigneBrisee _modele) {
    super(_modele==null ? new ZModeleLigneBriseeDefault():_modele);
    
//    if (_modele==null) {
//      modeleDonnees().getGeomData().setAttributes(new GISAttributeInterface[]{attrMark_,attrTime_,attrVX_,attrVY_}, null);
//    }
    
    src_=_src;

    vue2d_ = vue2d;
//    dataMemory_ = _data;
//    if (_markAttr == null) {
//      attrMark_ = findMarkAttribute(modeleDonnees());
//    } else {
//      attrMark_ = _markAttr;
//    }
    final GISZoneCollection geomData = modeleDonnees().getGeomData();
//    attListModel_ = new DefaultListModel();
    final int nbAttributes = geomData.getNbAttributes();
    attList_ = new ArrayList<GISAttributeInterface>(nbAttributes);
    updateVariablesList();
//    attSelectionModel_ = new DefaultListSelectionModel();
//    attSelectionModel_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//    attSelectionModel_.setSelectionInterval(0, 0);
//    attSelectionModel_.addListSelectionListener(this);

    iconModel_.setType(TraceIcon.RIEN);
    // les icones marks sont dessin�s par le modele ouvert
    iconeModelOuvert_.setType(TraceIcon.CARRE_PLEIN);
    iconeModelOuvert_.setTaille(3);
    setDestructible(true);
//    setActions(new EbliActionInterface[] { vue2d.getEditor().getVisuAction(), vue2d.getEditor().getExportAction() });
  }
  
  public void setSource(PivParticlesSourceI _src) {
    src_=_src;
    repaint();
  }
  
  public PivParticlesSourceI getSource() {
    return src_;
  }
  
  public void setTrajectoireParams(final EfTrajectoireParameters _data) {
    dataMemory_=_data;
  }
  
  public EfTrajectoireParameters getTrajectoireParams() {
    return dataMemory_;
  }

  public void updateVariablesList() {
//    attListModel_.clear();
    attList_.clear();
    final GISZoneCollection geomData = modele_.getGeomData();
    final int nbAttributes = geomData.getNbAttributes();
    for (int i = 0; i < nbAttributes; i++) {
      if (geomData.getAttribute(i) != PivParticlesGISDataModel.ATT_MARK) {
        attList_.add(geomData.getAttribute(i));
//        attListModel_.addElement(geomData.getAttribute(i).getLongName());
      }
    }
    firePropertyChange("list", null, attList_);
  }

  protected BPalettePlage createPlageForSelectedVar() {
    final BPalettePlage s = new BPalettePlage();
    s.setTitre(getSelectedAttributeName());
    final CtuluRange r = new CtuluRange();
    getRange(r);
    if (r.max_ - r.min_ < 0.01) {
      s.initPlages(1, r.min_, r.max_);
    } else {
      s.initPlages(10, r.min_, r.max_);
    }
    s.initCouleurs(PaletteManager.INSTANCE);
    updateSavedPalBeforeSet(s);
    return s;
  }

//  /**
//   * @param _modele le modele
//   * @return l'attribut de type boolean
//   */
//  protected GISAttributeInterface findMarkAttribute(final ZModeleLigneBrisee _modele) {
//    final GISZoneCollection geomData = _modele.getGeomData();
//    final int nbAttributes = geomData.getNbAttributes();
//    for (int i = nbAttributes - 1; i >= 0; i--) {
//      if (geomData.getAttribute(i).getDataClass() == Boolean.class) {
//        return geomData.getAttribute(i);
//      }
//
//    }
//    return null;
//
//  }

  public GISAttributeInterface getAttForName(final String _attName) {
    for (int i = 0; i < attList_.size(); i++) {
      if (attList_.get(i).getName().equals(_attName)) {
        return attList_.get(i);
      }
    }
    return null;
  }

  @Override
  public TraceLigneModel getLineModel(final int _idx) {
    if (_idx == 1) {
      return null;
    }
    return ligneModel_;
  }

//  @Override
//  public final ListModel getListModel() {
//    return attListModel_;
//  }

//  @Override
//  public final ListSelectionModel getListSelectionModel() {
//    return attSelectionModel_;
//  }

  @Override
  public int getNbSet() {
    return 2;
  }

//  @Override
//  public BCalquePersistenceInterface getPersistenceMng() {
//    return new FSigTrPostTrajectoireLineLayerPersistence();
//  }

  @Override
  public boolean getRange(final CtuluRange _b) {
//    final int idxAttSelected = attSelectionModel_.getMaxSelectionIndex();
    if (attSel_==null) {
      return false;
    }
    final GISZoneCollection geomData = super.modeleDonnees().getGeomData();
    final GISAttributeModel dataModel = geomData.getDataModel(geomData.getIndiceOf(attSel_));
    for (int i = dataModel.getSize() - 1; i >= 0; i--) {
      final GISAttributeModelDoubleInterface values = (GISAttributeModelDoubleInterface) dataModel.getObjectValueAt(i);
      _b.expandTo(values.getMax());
      _b.expandTo(values.getMin());
    }
    return true;

  }

  protected void setSelectedAttribute(GISAttributeInterface _att) {
    attSel_=_att;
    varUpdated();
  }
  
  protected String getSelectedAttributeLongName() {
    return attSel_==null ? "":attSel_.getLongName();
  }

  protected String getSelectedAttributeName() {
    return attSel_==null ? "":attSel_.getName();
  }

  @Override
  public String getSetTitle(final int _idx) {
    if (_idx == 0) {
      return PivResource.getS("Ligne");
    }
    return PivResource.getS("Marqueur");
  }

  @Override
  public void initFrom(final EbliUIProperties _p) {
    if (_p != null) {

      // attSelectionModel_.clearSelection();
      super.initFrom(_p);
      initPaletteMap();

      // if (idx >= 0) {
      // attSelectionModel_.setSelectionInterval(idx, idx);
      // }
      // final EfTrajectoireParameters initParam=(EfTrajectoireParameters)
      // _p.get(FSigTrPostTrajectoireLineLayerPersistence.INIT_DATA);
      // if (initParam != null) {
      // this.dataMemory_=initParam;
      // }
      // else {
      // final FSigTrPostTrajectoireLineLayerPersistData
      // data=(FSigTrPostTrajectoireLineLayerPersistData)
      // _p.get(FSigTrPostTrajectoireLineLayerPersistence.COMPUTE_DATA);
      // if (data != null) {
      final CtuluAnalyze log=new CtuluAnalyze();
      final PivParticlesLayerPersistData data=(PivParticlesLayerPersistData) _p.get(PivParticlesLayerPersistData.COMPUTE_DATA);
      if (data != null) {
        EfTrajectoireParameters restore=data.restore(src_, log);
        // final EfTrajectoireParameters
        // restore=(EfTrajectoireParameters)_p.get(FSigTrPostTrajectoireLineLayerPersistence.COMPUTE_DATA);
        if (!log.isEmpty()) {
          final CtuluAnalyzeGroup gr=new CtuluAnalyzeGroup(null);
          gr.addAnalyzer(log);
          CtuluAnalyzeGUI.showDialog(gr, vue2d_.getCtuluUI(), PivResource.getS("Rechargement des trac�s de particules"));
        }
        // if (restore != null &&
        // _p.getBoolean(FSigTrPostTrajectoireLineLayerPersistence.RECOMPUTE)) {
        if (restore != null)
          restoreData(log, restore);
        this.dataMemory_=restore;
      }
      else {
        this.dataMemory_=null;
      }
      // }
      // }
      // }
//      final String markId=(String) _p.get("mark.attribute");
//      if (markId != null) {
//        attrMark_=modeleDonnees().getGeomData().getAttributeWithID(markId);
//      }
      attSel_=getAttForName(_p.getString("att.selected"));

      if (restorePalettes(_p, namePalette_)) {
        updateLegende();
      }
    }
  }

  public static boolean restorePalettes(final EbliUIProperties _p, final Map<String, BPalettePlage> _dest) {
    final String[] names = (String[]) _p.get("post.paletteNames");
    if (names != null && names.length > 0) {
      final BPalettePlageProperties[] props = (BPalettePlageProperties[]) _p.get("post.paletteProps");
      if (_dest.size() > 0) {
        _dest.clear();
      }
      for (int i = 0; i < names.length; i++) {
        if (props[i] != null && names[i] != null) {
          BPalettePlage plage = new BPalettePlage(props[i]);
//          initSubTitleLabel(plage);
          _dest.put(names[i], plage);
        }
      }
      return true;
    }
    return false;
  }

  public static EbliUIProperties savePalettes(final Map<String, BPalettePlage> _nameBPalette, final EbliUIProperties _prop) {
    if (_prop == null) {
      return null;
    }
    if (_nameBPalette == null || _nameBPalette.size() == 0) {
      return _prop;
    }
    final List<String> paletteNames = new ArrayList<>(_nameBPalette.size());
    final List<BPalettePlageProperties> palettes = new ArrayList<>(_nameBPalette.size());
    
    for (final Iterator<Entry<String,BPalettePlage>> it = _nameBPalette.entrySet().iterator(); it.hasNext();) {
      Entry<String, BPalettePlage> e=it.next();
      final String key = e.getKey();
      if (!CtuluLibString.isEmpty(key)) {
        paletteNames.add(key);
        palettes.add(((BPalettePlage) e.getValue()).save());
      }
    }
    _prop.put("post.paletteNames", paletteNames.toArray(new String[paletteNames.size()]));
    _prop.put("post.paletteProps", palettes.toArray(new BPalettePlageProperties[palettes.size()]));
    return _prop;
  }

  /**
   * Appel� quand le repere a chang�. On reconstruit les lignes suivant la transformation.
   */
  public void changeRepere() {
    if (dataMemory_!=null) {
      CtuluAnalyze log=new CtuluAnalyze();
      restoreData(log, dataMemory_);
    }
  }
  
  private void restoreData(final CtuluAnalyze log, final EfTrajectoireParameters restore) {
    log.clear();
    final EfTrajectoireActivity ac = new EfTrajectoireActivity(src_.getInterpolator(), null, src_.getTimes());
//    final String title = PivResource.getS("Calcul");
    final List<EfTrajectoireResultBuilder> computeLigneCourant = ac.computeLigneCourant(restore, null, log);
    final PivComputeAndDisplayParticlesActivity activity = new PivComputeAndDisplayParticlesActivity(vue2d_,this,null);
    activity.reloadLinesIn(PivParticlesLayer.this, restore, computeLigneCourant, log);
//    if (!log.isEmpty()) {
//      final CtuluAnalyzeGroup gr = new CtuluAnalyzeGroup(null);
//      gr.addAnalyzer(log);
//      CtuluAnalyzeGUI.showDialog(gr, vue2d_.getCtuluUI(), title);
//    }
  }

  void initPaletteMap() {
    if (namePalette_ == null) {
      namePalette_ = new HashMap<String, BPalettePlage>(attList_.size());
    }
  }

  @Override
  public boolean isDonneesBoiteAvailable() {
    return true;
  }

  @Override
  public boolean isPaletteModifiable() {
    return true;
  }

  @Override
  public void paintDonnees(final Graphics2D _g, final GrMorphisme _versEcran, final GrMorphisme _versReel, final GrBoite _clipReel) {
    if ((modele_ == null) || (modele_.getNombre() <= 0)) {
      return;
    }
    final GrBoite clip = _clipReel;
    final GrMorphisme versEcran = _versEcran;
    final int nombre = modele_.getNombre();
    final TraceLigne tl = ligneModel_.buildCopy();

    final TraceIconModel iconeModel = iconModel_ == null ? null : new TraceIconModel(iconModel_);
    final TraceIcon icone = iconeModel == null ? null : new TraceIcon(iconeModel);
    final TraceIcon iconeMark = new TraceIcon(new TraceIconModel(iconeModelOuvert_));
    final GrBoite bPoly = new GrBoite();
    bPoly.o_ = new GrPoint();
    bPoly.e_ = new GrPoint();
    // on part de la fin : comme ca la premiere ligne apparait au-dessus
    for (int i = nombre - 1; i >= 0; i--) {
      // il n'y a pas de points pour cette ligne
      if (modele_.getNbPointForGeometry(i) <= 0) {
        continue;
      }
      modele_.getDomaineForGeometry(i, bPoly);
      // Si la boite du polygone n'est pas dans la boite d'affichage on passe
      if (bPoly.intersectionXY(clip) == null) {
        continue;
      }
      final int nbPoints = modele_.getNbPointForGeometry(i);
      if (nbPoints <= 0) {
        continue;
      }

      final GrPoint ptOri = new GrPoint();
      modele_.point(ptOri, i, nbPoints - 1);
      ptOri.autoApplique(versEcran);
      // initTrace(ligneModel, i); on laisse le trace de la ligne constante
      final GrPoint ptDest = new GrPoint();
      for (int j = nbPoints - 2; j >= 0; j--) {
        // le point de dest est initialise
        modele_.point(ptDest, i, j);
        ptDest.autoApplique(versEcran);
        tl.dessineTrait(_g, ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_);
        ptOri.initialiseAvec(ptDest);
      }
      if (modele_.isGeometryFermee(i)) {
        modele_.point(ptOri, i, nbPoints - 1);
        ptOri.autoApplique(versEcran);
        tl.dessineTrait(_g, ptOri.x_, ptOri.y_, ptDest.x_, ptDest.y_);
      }
      modele_.point(ptOri, i, nbPoints - 1);
      ptOri.autoApplique(versEcran);
    }
    // on trace les icones apres pour qu'ils soient dessin�s au-dessus des lignes.
    final GISZoneCollection geomData = super.modeleDonnees().getGeomData();
    final int idxMark = geomData.getIndiceOf(PivParticlesGISDataModel.ATT_MARK);
//    final int idxAttSelected = geomData.getIndiceOf(attSel_);
    // si idxSelected est positif on utilise une palette
//    final int idxSelected = isPaletteCouleurUsed_ && idxAttSelected >= 0 ? geomData.getIndiceOf(attList_.get(idxAttSelected)) : -1;
    final int idxSelected = isPaletteCouleurUsed_ ? geomData.getIndiceOf(attSel_):-1;

    if (icone != null) {
      for (int i = nombre - 1; i >= 0; i--) {
        // il n'y a pas de points pour cette ligne
        if (modele_.getNbPointForGeometry(i) <= 0) {
          continue;
        }
        modele_.getDomaineForGeometry(i, bPoly);
        // Si la boite du polygone n'est pas dans la boite d'affichage on passe
        if (bPoly.intersectionXY(clip) == null) {
          continue;
        }
        final int nbPoints = modele_.getNbPointForGeometry(i);
        if (nbPoints <= 0) {
          continue;
        }
        // initTrace(iconeModel, i);
        final GrPoint ptDest = new GrPoint();
        final GISAttributeModel[] subModel = geomData.getAtomicAttributeSubModel(i);
        final GISAttributeModelBooleanInterface markValues = (idxMark < 0 || CtuluLibArray.isEmpty(subModel)) ? null
            : (GISAttributeModelBooleanInterface) subModel[idxMark];
        for (int j = nbPoints - 1; j >= 0; j--) {
          // le point de dest est initialise
          modele_.point(ptDest, i, j);
          ptDest.autoApplique(versEcran);
          TraceIcon ic = icone;
          if (markValues != null && markValues.getObjectValueAt(j) == Boolean.TRUE) {
            ic = iconeMark;
          }
          if (idxSelected >= 0) {
            ic.setCouleur(((BPalettePlage) paletteCouleur_).getColorFor(((GISAttributeModelDoubleInterface) subModel[idxSelected]).getValue(j)));
          }
          ic.paintIconCentre(this, _g, ptDest.x_, ptDest.y_);
        }
      }
    }
  }

  @Override
  public boolean setIconModel(final int idx, final TraceIconModel model) {
    Color old = null;
    TraceIconModel iconModel = getIconModel(idx);
    if (iconModel != null) {
      old = iconModel.getCouleur();
    }
    final boolean modified = super.setIconModel(idx, model);
    Color newColor = null;
    iconModel = getIconModel(idx);
    if (iconModel != null) {
      newColor = iconModel.getCouleur();
    }
    if (!CtuluLib.isEquals(old, newColor)) {
      removeUsedPalette();
    }
    return modified;
  }

  @Override
  public void putCalqueInfo(final String _s) {
    putClientProperty("CalqueInfo", _s);
    firePropertyChange("title", true, false);
  }

  @Override
  public EbliUIProperties saveUIProperties() {
    // c'est pour que la variable en cours soit enregistr�e
    updateLegende(false);
    final EbliUIProperties res = super.saveUIProperties();
    savePalettes(namePalette_, res);
    res.put("att.selected", getSelectedAttributeName());
//    res.put("mark.attribute", attrMark_ == null ? CtuluLibString.EMPTY_STRING : attrMark_.getID());
    if (dataMemory_!=null)
      res.put(PivParticlesLayerPersistData.COMPUTE_DATA, new PivParticlesLayerPersistData(dataMemory_));
//    res.put(FSigTrPostTrajectoireLineLayerPersistence.INIT_DATA, dataMemory_);
    return res;
  }

  protected void updateCalqueInfo() {
    final String var = getSelectedAttributeName();
    putCalqueInfo(var);
  }

  protected void updateLegende() {
    updateLegende(false);
  }

  protected void updateLegende(final boolean _forceUpdate) {
    if (attList_.size() != 0 && attSel_ != null) {
      initPaletteMap();
      final String v=getSelectedAttributeName();
      BPalettePlage s=namePalette_.get(v);
      if (s == null || _forceUpdate) {
        s=createPlageForSelectedVar();
        namePalette_.put(v, s);
      }
      else {
        updateSavedPalBeforeSet(s);
      }
      setPaletteCouleur(s);
    }
    else {
      setPaletteCouleur(null);
    }
    construitLegende();
  }

  /**
   * @param _s
   */
  private void updateSavedPalBeforeSet(final BPalettePlage _s) {
  }

//  @Override
//  public void valueChanged(final ListSelectionEvent _e) {
//    if (_e.getValueIsAdjusting()) {
//      return;
//    }
//    // getIsoModelAbstract().setVar(this.mainVariableSelectionModel_.getMinSelectionIndex());
//    varUpdated();
//  }

  public final void varUpdated() {
    updateLegende();
    updateCalqueInfo();
    fireSelectionEvent();
    repaint();
  }

  /**
   * Surcharge des menus specifics, ajout d'un menu de rejoue de donn�es pour les trajectoires/lignes de courant.
   */
  @Override
  protected void buildSpecificMenuItemsForAction(final List _l) {
//    super.buildSpecificMenuItemsForAction(_l);
//
//    // -- ajour de l'action de rejouer les donn�es --//
//    final EbliActionSimple replayData = new EbliActionSimple(PivResource.getS("Rejouer les donn�es"), EbliResource.EBLI.getIcon("restore"), "REPLAYDATA") {
//      @Override
//      public void actionPerformed(final ActionEvent _e) {
//
//        new FudaaPanelTask(vue2d_.getCtuluUI(), new FSigTrPostTrajectoireTaskModel(vue2d_, dataMemory_, FSigTrPostTrajectoireLineLayer.this)).afficheDialog();
//      }
//    };
//    final JMenuItem menuReplayTrajectoires = new JMenuItem(replayData);
//
//    _l.add(menuReplayTrajectoires);
//
  }

}

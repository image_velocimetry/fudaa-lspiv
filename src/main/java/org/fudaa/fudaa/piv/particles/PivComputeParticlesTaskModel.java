package org.fudaa.fudaa.piv.particles;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluAnalyzeGroup;
import org.fudaa.ctulu.CtuluDurationFormatter;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;
import org.fudaa.ctulu.editor.CtuluValueEditorInteger;
import org.fudaa.ctulu.editor.CtuluValueEditorTime;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gui.CtuluComboBoxModelAdapter;
import org.fudaa.dodico.ef.operation.EfIndexHelper;
import org.fudaa.dodico.ef.operation.EfTrajectoireParameters;
import org.fudaa.dodico.ef.operation.EfTrajectoireParametersMarqueur;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.fudaa.commun.impl.FudaaPanelTaskModel;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.PivVisuPanel;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.utils.PivUtils;
import org.fudaa.fudaa.sig.layer.FSigTempLineInLayer;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuValueValidator;
import com.memoire.bu.BuVerticalLayout;

/**
 * La tache permettant de calculer les trajectoires ou lignes de courant. Repris de FudaaPrepro
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class PivComputeParticlesTaskModel implements FudaaPanelTaskModel {

  class LineChooser implements PivLineChooserI {
    FSigTempLineInLayer tmp_;
    
    @Override
    public FSigTempLineInLayer getTmpLine() {
      return tmp_;
    }

    @Override
    public void setTmpLine(FSigTempLineInLayer tmpLine) {
      tmp_=tmpLine;
    }

    @Override
    public void close() {
      if (tmp_ != null) {
        tmp_.close();
      }
    }

    // Pas de ligne pr�selectionn�e
    @Override
    public LineString getInitSelected() {
      return null;
    }

    @Override
    public void restaurer() {
      if (tmp_ != null) {
        tmp_.restaurer();
      }
    }

    @Override
    public void update(LineString _s, boolean _zoom) {
      if (tmp_ != null) {
        tmp_.display(_s, _zoom);
      }
    }

    @Override
    public void zoomInitial() {
      if (tmp_ != null) {
        tmp_.zoomInitial();
      }
    }
    
  }
  
  private static final double MAXDIST = 1E-3;
  
  PivComputeAndDisplayParticlesActivity algoCalcul_;
  PivVisuPanel pnCalques_;
  PivProject prj_;
  // choix mode
  JComboBox<String> choixMode_ = new JComboBox<>(new String[] { PivResource.getS("Lignes de courant"), PivResource.getS("Trajectoires") });
  JComponent duree_;
  JComponent finesse_;
  final PivLineChooserI lineChooser_;
  JComboBox cbFirstTime_;
  JList<CtuluVariable> listVar_;
  DefaultTableModel modelResult_;

  CtuluValueEditorDouble doubleEditor_ = new CtuluValueEditorDouble(false);
  CtuluValueEditorDouble doubleMarkEditor_ = new CtuluValueEditorDouble(false);
  CtuluValueEditorInteger integerEditor_ = new CtuluValueEditorInteger(false);
  JComboBox cbVitesse_;
  JComponent nbPoints_;

  // choix segment
  JComponent x_;
  JComponent x2_;
  JComponent y_;
  JComponent y2_;
  CtuluValueEditorTime dureeEditor_;
  /** reference du layer destination dans le cas ou l'on rejoue les donn�es. Null si utilisation classique. */
  PivParticlesLayer destlayer_ = null;

  /**
   * Constructeur reserv� au calques
   * 
   * @param _builderParams
   * @param _calque
   * @param _modelGraphe
   * @param _panelVariables
   * @param _source
   * @param _destLayer Le calque de destination. Peut �tre null.
   */
  public PivComputeParticlesTaskModel(final PivVisuPanel _pnCalques, PivParticlesLayer _destLayer) {
    destlayer_=_destLayer;
    pnCalques_ = _pnCalques;
    prj_=pnCalques_.getProject();
    int nbTimeStep = destlayer_.getSource().getNbTimeStep();
    if (nbTimeStep < 2)
      choixMode_.setEnabled(false);
    doubleEditor_.setFormatter(CtuluNumberFormatDefault.buildNoneFormatter(3, true));
    x_ = doubleEditor_.createEditorComponent();
    x2_ = doubleEditor_.createEditorComponent();
    y_ = doubleEditor_.createEditorComponent();
    y2_ = doubleEditor_.createEditorComponent();

    dureeEditor_ = new CtuluValueEditorTime();
    dureeEditor_.setFmt(new CtuluDurationFormatter(true, false));
    duree_ = dureeEditor_.createEditorComponent();
    double defaut = 60;
    if (nbTimeStep > 2) {
      defaut = (destlayer_.getSource().getTimeStep(nbTimeStep - 1) - destlayer_.getSource().getTimeStep(0)) / 2;
    }
    dureeEditor_.setValue(defaut, duree_);
    integerEditor_.setVal(BuValueValidator.MIN(1));
    integerEditor_.setEditable(true);
    nbPoints_ = integerEditor_.createEditorComponent();
    finesse_ = integerEditor_.createEditorComponent();
    integerEditor_.setValue(Integer.valueOf(1), nbPoints_);
    integerEditor_.setValue(Integer.valueOf(1), finesse_);
    lineChooser_=new LineChooser();
//    lineChooser_ = new MvLineChooser(((ZCalqueGeometry)calque.getCalqueActif()).getSelectedLine(), _src.getGrid(), calque.getCtuluUI());
    FSigTempLineInLayer tmpLine = new FSigTempLineInLayer(_pnCalques);
    tmpLine.setLineModel(PivUtils.TMP_LINE_MODEL);
    lineChooser_.setTmpLine(tmpLine);
//    modelPdt_ = source_.getTimeListModel();
    
    if (destlayer_!=null && destlayer_.getTrajectoireParams()!=null)
      restoreParams(destlayer_.getTrajectoireParams());
  }

  /**
   * Constructeur appel� pour rejouer les donn�es. Passe en parametre la structure efTrajectoire qui contient toutes les
   * infos de pr� remplissage
   * 
   * @param calque
   * @param dataReplay qui contient toutes les infos de pr� remplissage
   * @param _destLayer calque a ecraser en recopiant le r�sultat par dessus
   */
//  public PivComputeStreamLinesTaskModel(final ZEbliCalquesPanel calque, 
//      FSigTrPostSource _src, EfTrajectoireParameters dataReplay, FSigTrPostTrajectoireLineLayer _destLayer) {
//    this(calque, _src, _destLayer);
//    restoreParams(dataReplay);
//  }
  
  private void restoreParams(EfTrajectoireParameters dataReplay) {

    // -- on pr�remplit les donn�es avec les infos du dataReplay --//
//    layerAEcraser_ = calqueAEcraser;

    // -- on reinitialise les infos avec les donn�es des trajectoires/lignes de courant. --//
    // -- on initialise les params graphiques --//
    getPanel();

    // -- selection du vecteur vitesse --//
//    FuLog.warning("A modifier");
//    for (int k = 0; k < cbVitesse_.getItemCount(); k++)
//      if (((TrPostFlecheContent) cbVitesse_.getItemAt(k)).getVx() == dataReplay.vx)
//        if (((TrPostFlecheContent) cbVitesse_.getSelectedItem()).getVy() == dataReplay.vy)
//          cbVitesse_.setSelectedIndex(k);

    // -- duree itegration --//
    dureeEditor_.setValue(dataReplay.dureeIntegration_, duree_);

    // -- finesse --//
    integerEditor_.setValue(dataReplay.finesse_, finesse_);

    // -- pas de temps initial --//
    cbFirstTime_.setSelectedIndex(Math.min(cbFirstTime_.getItemCount()-1,dataReplay.firstTimeStepIdx_));

    // -- type traj ou ligne de courant --//
    if (dataReplay.isLigneDeCourant)
      choixMode_.setSelectedIndex(0);
    else choixMode_.setSelectedIndex(1);

    // -- choix des variables --//
    if (dataReplay.varsASuivre_.size() >= 1) {

      int[] indicesToselect = new int[dataReplay.varsASuivre_.size()];
      for (int i = 0; i < dataReplay.varsASuivre_.size(); i++) {

        for (int k = 0; k < listVar_.getModel().getSize(); k++) {
          if ((listVar_.getModel().getElementAt(k)) == dataReplay.varsASuivre_.get(i))
            indicesToselect[i] = k;
        }
      }
      listVar_.setSelectedIndices(indicesToselect);
    }

    // -- premier point --//
    GrPoint ptFirst=new GrPoint(dataReplay.segment_.get(0).x,dataReplay.segment_.get(0).y,0);
    ptFirst.autoApplique(prj_.getTransformationParameters().getToReal());
    doubleEditor_.setValue(ptFirst.x_, x_);
    doubleEditor_.setValue(ptFirst.y_, y_);
    integerEditor_.setValue(dataReplay.nbPointsInitiaux_, nbPoints_);
    // -- dernier point --//
    GrPoint ptLast=new GrPoint(dataReplay.segment_.get(dataReplay.segment_.size() - 1).x,dataReplay.segment_.get(dataReplay.segment_.size() - 1).y,0);
    ptLast.autoApplique(prj_.getTransformationParameters().getToReal());
    doubleEditor_.setValue(ptLast.x_, x2_);
    doubleEditor_.setValue(ptLast.y_, y2_);

    // -- marqueurs --//
    if (dataReplay.marqueur_ != null) {
      if (dataReplay.marqueur_.timeStep_)
        cbMarks_.setSelectedIndex(1);
      else cbMarks_.setSelectedIndex(2);

      doubleMarkEditor_.setValue(dataReplay.marqueur_.deltaMax_, markValue_);
    } else cbMarks_.setSelectedIndex(0);

  }

  private List<Coordinate> getPoints() {
    Coordinate c = getFirstCoordinate();
    Coordinate cEnd = getEndCoordinate();
    int nbLigne = (Integer) integerEditor_.getValue(nbPoints_);
    return getPointsFromSegment(c, cEnd, nbLigne);

  }

  public static List<Coordinate> getPointsFromSegment(Coordinate c, Coordinate cEnd, int nbLigne) {
    List<Coordinate> res = new ArrayList<Coordinate>(nbLigne);
    if (c.distance(cEnd) < MAXDIST) {
      res.add(c);
      return res;
    }
    if (nbLigne == 1) {
      c.x = (c.x + cEnd.x) / 2D;
      c.y = (c.y + cEnd.y) / 2D;
      res.add(c);
      return res;
    }
    int nbIteration = nbLigne - 1;
    double deltaX = (cEnd.x - c.x) / nbIteration;
    double deltaY = (cEnd.y - c.y) / nbIteration;
    res.add(c);
    for (int i = 1; i < nbIteration; i++) {
      Coordinate ci = new Coordinate(c.x + i * deltaX, c.y + i * deltaY);
      res.add(ci);
    }
    res.add(cEnd);
    return res;
  }

  /**
   * @return Le dernier point, dans l'espace r�el de calcul.
   */
  private Coordinate getEndCoordinate() {
    GrPoint pt=new GrPoint(getDoubleValue(x2_), getDoubleValue(y2_),0).applique(prj_.getTransformationParameters().getToData());
    return new Coordinate(pt.x_, pt.y_);
  }

  /**
   * @return Le premier point, dans l'espace r�el de calcul.
   */
  private Coordinate getFirstCoordinate() {
    GrPoint pt=new GrPoint(getDoubleValue(x_), getDoubleValue(y_),0).applique(prj_.getTransformationParameters().getToData());
    return new Coordinate(pt.x_, pt.y_);
  }

  @Override
  public void actTask(final ProgressionInterface _prog, final CtuluAnalyzeGroup analyzeGroup, final String[] _messages) {
    CtuluAnalyze log = new CtuluAnalyze();
    log.setDesc(getTitre());
    
    saveContextParams();
    analyzeGroup.addAnalyzer(log);
    // on recupere les points
    List<Coordinate> points = getPoints();
    _prog.setDesc(PivResource.getS("Construction des points"));
    int idxRemove = 0;
    // on enleve les points en dehors
    for (Iterator<Coordinate> it = points.iterator(); it.hasNext();) {
      Coordinate c = it.next();
      int idx = EfIndexHelper.getElementEnglobant(destlayer_.getSource().getGrid(), c.x, c.y, _prog);
      if (idx < 0) {
        idxRemove++;
        it.remove();
      }
    }
    // si pas de point pas de calcul
    if (points.size() == 0) {
      log.addFatalError(PivResource.getS("Aucun point de d�part n'appartient au maillage"));
      return;
    }
    if (idxRemove > 0) {
      String txt = null;
      if (idxRemove == 1) {
        txt = PivResource.getS("1 point de d�part a �t� ignor� car il est en dehors du maillage");
      } else {
        txt = PivResource.getS("{0} points de d�part ont �t� ignor�s car ils sont en dehors du maillage", CtuluLibString.getString(idxRemove));
      }
      _messages[0] = txt;
    }

    // creation de l algo
    _prog.setDesc(PivResource.getS("Calcul"));
    algoCalcul_ = new PivComputeAndDisplayParticlesActivity(pnCalques_, destlayer_, null);

    final PivVectorDefinitionI vectDefinition = getSelectedVector();
    final EfTrajectoireParameters data = new EfTrajectoireParameters();
    data.segment_ = new ArrayList<Coordinate>();
    data.segment_.add(getFirstCoordinate());
    data.segment_.add(getEndCoordinate());
    data.nbPointsInitiaux_ = (Integer) integerEditor_.getValue(nbPoints_);

    data.vx = vectDefinition.getVx();
    data.vy = vectDefinition.getVy();
    data.dureeIntegration_ = getDuree();
    data.finesse_ = (Integer) integerEditor_.getValue(finesse_);
    data.firstTimeStepIdx_ = getSelectedTimeStep();
    data.firstTimeStep_ = destlayer_.getSource().getTimeStep(data.firstTimeStepIdx_);
    data.isLigneDeCourant = isLigneCourant();
    if (!listVar_.isSelectionEmpty()) {
      data.varsASuivre_=listVar_.getSelectedValuesList();
//      data.varsASuivre_ = new ArrayList<CtuluVariable>(selectedValues.length);
//      for (int i = 0; i < selectedValues.length; i++) {
//        data.varsASuivre_.add((CtuluVariable) selectedValues[i]);
//      }
    }

    data.points_ = points;
    if (isMarqueurAvailable()) {
      data.marqueur_ = new EfTrajectoireParametersMarqueur();
      data.marqueur_.timeStep_ = isMarqueurTimeStep();
      data.marqueur_.deltaMax_ = (Double) doubleMarkEditor_.getValue(markValue_);

    }
    algoCalcul_.computeLigneCourant(pnCalques_, data, destlayer_.getSource(), log, _prog, _messages);

    // -- mise a jour du tableau de res --//
    // modelResult_.fireTableDataChanged();

  }

  
  /**
   * Methode qui enregistre les param�tres saisies pour les recharger � la prochaine ouverture.
   * @author Adrien Hadoux
   */
  private void saveContextParams() {
//    FuLog.warning("A implementer: "+new Exception().getStackTrace()[0].toString());
/*
    if(impl_ == null)
		return;
	  CommonProperties properties = impl_.getProject().getCommonProperties();
	  if(properties == null){
		  properties = new CommonProperties();
		  impl_.getProject().setCommonProperties(properties);
	  }
	  properties.trajectoireProperties = new CommonProperties.TrajectoireProperties();
	  CommonProperties.TrajectoireProperties prop = properties.trajectoireProperties;
	  prop.mode = choixMode_.getSelectedIndex();
	  prop.vecteur = cbVitesse_.getSelectedIndex();
	  prop.pdt = this.cbFirstTime_.getSelectedIndex();
	  prop.duree = ""+this.getDuree();
	  prop.precision =  integerEditor_.getStringValue(finesse_);
	  prop.p1x = doubleEditor_.getStringValue(x_);
	  prop.p1y = doubleEditor_.getStringValue(y_);
	  prop.p2x = doubleEditor_.getStringValue(x2_);
	  prop.p2y = doubleEditor_.getStringValue(y2_);
	  prop.nbTrajectoires = integerEditor_.getStringValue(nbPoints_);
	  prop.choixVariables = listVar_.getSelectedIndices();
	  prop.type = cbMarks_.getSelectedIndex();
	  prop.delta = doubleMarkEditor_.getStringValue(markValue_);
	  */
  }
  /**
   * Methode qui enregistre les param�tres saisies pour les recharger � la prochaine ouverture.
   * @author Adrien Hadoux
   */
  private void loadContextParams() {
//    FuLog.warning("A implementer: "+new Exception().getStackTrace()[0].toString());
/*	  if(impl_ == null || impl_.getProject().getCommonProperties() == null || impl_.getProject().getCommonProperties().trajectoireProperties == null)
			return;
	  CommonProperties properties = impl_.getProject().getCommonProperties();
	  CommonProperties.TrajectoireProperties prop = properties.trajectoireProperties;
	  
	  if(prop.mode< choixMode_.getItemCount())
		  choixMode_.setSelectedIndex(prop.mode);
	  
	  if(prop.vecteur < cbVitesse_.getItemCount())
			cbVitesse_.setSelectedIndex(prop.vecteur);
	  
	  if(prop.pdt < cbFirstTime_.getItemCount())
		  cbFirstTime_.setSelectedIndex(prop.pdt);
	  
	  dureeEditor_.setValue(prop.duree,duree_);
	  integerEditor_.setValue(prop.precision, finesse_);
	  doubleEditor_.setValue( prop.p1x, x_);
	  doubleEditor_.setValue( prop.p1y, y_);
	  doubleEditor_.setValue( prop.p2x, x2_);
	  doubleEditor_.setValue( prop.p2y, y2_);
	  integerEditor_.setValue(prop.nbTrajectoires,nbPoints_);
	  try{
	  if(prop.choixVariables != null)
		  listVar_.setSelectedIndices(prop.choixVariables);
	  }catch(Exception e) {
		  System.err.print(e);
	  }
	  
	  if(prop.type< cbMarks_.getItemCount())
		  cbMarks_.setSelectedIndex(prop.type);
	  doubleMarkEditor_.setValue(prop.delta,markValue_);
	  */
  }

  JPanel content_;
  JComponent markValue_;
  JComboBox<String> cbMarks_ = new JComboBox<>(new String[] { PivResource.getS("Aucun"), PivResource.getS("A pas de temps constant"),
      PivResource.getS("A distance constante") });

  public boolean isMarqueurAvailable() {
    return cbMarks_.getSelectedIndex() != 0;
  }

  public boolean isMarqueurTimeStep() {
    return cbMarks_.getSelectedIndex() == 1;
  }

  private JPanel buildMarqueur() {
    doubleMarkEditor_.setVal(BuValueValidator.MIN(1E-4));
    final JPanel conteneur = new JPanel(new BuGridLayout(2, 3, 3));
    conteneur.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Marqueurs")));
    conteneur.add(new JLabel(PivResource.getS("Type")));
    conteneur.add(cbMarks_);
    conteneur.add(new JLabel(PivResource.getS("Delta (valeur absolue)")));
    markValue_ = doubleMarkEditor_.createEditorComponent();
    conteneur.add(markValue_);
    markValue_.setEnabled(false);
    cbMarks_.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(ItemEvent _e) {
        markValue_.setEnabled(isMarqueurAvailable());

      }
    });

    return conteneur;
  }

  public boolean isSegmentValid() {
    final boolean segValid = !doubleEditor_.isEmpty(x_) && !doubleEditor_.isEmpty(x2_) && !doubleEditor_.isEmpty(y2_) && !doubleEditor_.isEmpty(y_);
    return segValid && integerEditor_.getValue(nbPoints_) != null;
  }

  public double getDoubleValue(final JComponent c) {
    return ((Double) doubleEditor_.getValue(c)).doubleValue();
  }

  protected void updateView(final boolean _zoom) {
    if (isSegmentValid()) {
      lineChooser_.update(
          GISGeometryFactory.INSTANCE.createSegment(getDoubleValue(x_), getDoubleValue(y_), getDoubleValue(x2_), getDoubleValue(y2_)), _zoom);
    }
  }

  private JPanel buildSegments() {

    final JPanel conteneur = new JPanel(new BuGridLayout(3, 5, 5, false, false, false, false, false));
    conteneur.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Choix du segment")));
    conteneur.add(new JLabel(""));
    conteneur.add(new JLabel("x:"));
    conteneur.add(new JLabel("y:"));
    conteneur.add(new JLabel("Point 1:"));
    conteneur.add(x_);
    conteneur.add(y_);
    conteneur.add(new JLabel("Point 2:"));
    conteneur.add(x2_);
    conteneur.add(y2_);
    conteneur.add(new JLabel(PivResource.getS("Nombre de trajectoire/lignes de courant")));
    conteneur.add(nbPoints_);
    final LineString initSelected = lineChooser_.getInitSelected();
    if (initSelected != null) {
      final int nbCoordinate = initSelected.getNumPoints();
      doubleEditor_.setValue(initSelected.getCoordinateSequence().getX(0), x_);
      doubleEditor_.setValue(initSelected.getCoordinateSequence().getY(0), y_);
      doubleEditor_.setValue(initSelected.getCoordinateSequence().getX(nbCoordinate - 1), x2_);
      doubleEditor_.setValue(initSelected.getCoordinateSequence().getY(nbCoordinate - 1), y2_);
      updateView(false);
    }
    JPanel mainSegment = new JPanel(new BuVerticalLayout(5, false, true));
    mainSegment.add(conteneur);
    JPanel pn = new JPanel(new BuGridLayout(3, 2, 0));
    JButton bt = new JButton(PivResource.getS("Zoomer sur le segment"));
    bt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        updateView(true);

      }
    });
    pn.add(bt);
    bt = new JButton(PivResource.getS("Zoom initial"));
    bt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        lineChooser_.zoomInitial();

      }
    });
    pn.add(bt);
    bt = new JButton(EbliLib.getS("Restaurer"));
    bt.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        lineChooser_.restaurer();

      }
    });
    pn.add(bt);
    mainSegment.add(pn);
    return mainSegment;
  }

  private JPanel buildVariables() {
    final JPanel conteneur = new JPanel(new BorderLayout());
    listVar_ = new JList<>(destlayer_.getSource().getVarListModel());
    listVar_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    conteneur.add(new JScrollPane(listVar_));
    conteneur.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Choix de la variable")));
    return conteneur;
  }

  @Override
  public void decoreBtApply(final JButton _bt) {
    _bt.setText(PivResource.getS("Calculer"));
  }

  @Override
  public int getNbMessageMax() {
    return 1;
  }

  @Override
  public JComponent getPanel() {
    if (content_ != null)
      return content_;
    content_ = new BuPanel(new BuVerticalLayout(5));
    content_.add(buildMainProperties());

    // -- panel des segements --//
    content_.add(buildSegments());
    // -- panel des variables --//
    content_.add(buildVariables());
    content_.add(buildMarqueur());
    
    loadContextParams();
    
    return content_;
  }

  private JPanel buildMainProperties() {
    JPanel pn = new JPanel(new BuGridLayout(2, 5, 5));
    pn.add(new JLabel(PivResource.getS("Mode")));
    pn.add(choixMode_);
    pn.add(new JLabel(PivResource.getS("Vecteur")));
    ListModel<PivVectorDefinitionI> flecheListModel = destlayer_.getSource().getFlecheListModel();
    CtuluComboBoxModelAdapter cb = new CtuluComboBoxModelAdapter(flecheListModel);
    cbVitesse_ = new BuComboBox(cb);
    cb.setSelectedItem(flecheListModel.getElementAt(0));

    pn.add(cbVitesse_);
    pn.add(new JLabel(PivResource.getS("Pas de temps initial")));
    cbFirstTime_ = new BuComboBox(new CtuluComboBoxModelAdapter(destlayer_.getSource().getTimeListModel()));
    if (cbFirstTime_.getItemCount()>0)
      cbFirstTime_.setSelectedIndex(0);
    pn.add(cbFirstTime_);
    pn.add(new JLabel(PivResource.getS("Dur�e d'int�gration:")));
    pn.add(duree_);
    pn.add(new JLabel(PivResource.getS("Pr�cision du trac�:")));
    pn.add(finesse_);
    return pn;
  }

  /**
   * retourne l indice du pas de temps.
   * 
   * @return
   */
  protected int getSelectedTimeStep() {
    return cbFirstTime_.getSelectedIndex();
  }

  /**
   * retourne la variable selectionnee
   * 
   * @return
   */
  protected PivVectorDefinitionI getSelectedVector() {

    return (PivVectorDefinitionI) cbVitesse_.getSelectedItem();
  }

  @Override
  public String getTitre() {
    return PivResource.getS("Lignes de courant") + "/" + PivResource.getS("Trajectoires");
  }

  protected boolean isLigneCourant() {
    return choixMode_.getSelectedIndex() == 0;
  }

  protected boolean isTrajectoire() {
    return choixMode_.getSelectedIndex() == 1;
  }

  @Override
  public String isTakDataValid() {
    if (!this.integerEditor_.isValueValidFromComponent(finesse_)) {
      return PivResource.getS("La pr�cision du trac� doit �tre d�finie par un entier sup�rieur � 1");
    }
    if (!isSegmentValid()) {
      return PivResource.getS("Le segment n'est pas d�fini");
    }
    if (integerEditor_.getValue(nbPoints_) == null) {
      return PivResource.getS("Pr�ciser le nombre de trajectoires");
    }
    if (dureeEditor_.isEmpty(duree_)) {
      return PivResource.getS("La dur�e doit �tre d�finie par un r�el en secondes");
    }
    if (isMarqueurAvailable() && !doubleMarkEditor_.isValueValidFromComponent(markValue_)) {
      return PivResource.getS("Le delta du marqueur doit �tre renseign�");
    }
    // on doit verifier que la duree est bien comprise dans les pas de temps du projet
    if (isTrajectoire()) {
      final int duree = getDuree();
      final double timeSelected = destlayer_.getSource().getTimeStep(cbFirstTime_.getSelectedIndex());
      final double timeEnd = timeSelected + duree;
      final double lastTimeStep = destlayer_.getSource().getTimeStep(destlayer_.getSource().getNbTimeStep() - 1);
      if (timeEnd < destlayer_.getSource().getTimeStep(0) || timeEnd > lastTimeStep) {
        final String min = "-".concat(destlayer_.getSource().getTimeFormatter().format(timeSelected - destlayer_.getSource().getTimeStep(0)));
        final String max = destlayer_.getSource().getTimeFormatter().format(lastTimeStep - timeSelected);
        return PivResource.getS("la dur�e du calcul doit �tre comprise entre {0} et {1}", min, max);
      }

    }
    if (getFirstCoordinate().distance(getEndCoordinate()) < MAXDIST) {
      boolean res = pnCalques_.getCtuluUI().question(PivResource.getS("Calculer une seule trajectoire"),
          PivResource.getS("Les extremit�s du segment sont confondues. Une seule trajectoire sera calcul�e.\nVoulez-vous continuer ?"));
      if (!res) {
        return PivResource.getS("Choisir 2 points diff�rents");
      }

    }

    return null;
  }

  private Integer getDuree() {
    return ((Integer) dureeEditor_.getValue(duree_));
  }

  // EfLigneDeCourantActivity activity_;

  @Override
  public void stopTask() {
    if (algoCalcul_ != null)
      algoCalcul_.stop();

  }

  @Override
  public void dialogClosed() {
    lineChooser_.close();

  }
}

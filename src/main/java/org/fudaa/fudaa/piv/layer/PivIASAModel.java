/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fudaa.fudaa.piv.layer;

import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeInteger;
import org.fudaa.ctulu.gis.GISCoordinateSequence;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ctulu.gis.GISPolyligne;
import org.fudaa.ctulu.gis.GISZoneCollectionLigneBrisee;
import org.fudaa.ebli.calque.edition.ZModeleLigneBriseeDefault;
import org.fudaa.fudaa.piv.metier.PivComputeParameters;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.locationtech.jts.geom.CoordinateSequence;

/**
 * Un modele pour les IA/SA bas� sur le modele de ligne bris�es.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivIASAModel extends ZModeleLigneBriseeDefault {
  /** L'attribut utilis� pour l'affichage des lignes suivant une palette de couleurs. */
  public static final GISAttributeInteger ATT_IND_COL=new GISAttributeInteger("color",false);
  // Le projet.
  PivProject prj_;

  /**
   * Construction du mod�le.
   */
  public PivIASAModel() {
    super(new GISZoneCollectionLigneBrisee());
    GISAttribute[] attrs = new GISAttribute[]{ATT_IND_COL};
    getGeomData().setAttributes(attrs, null);
  }

  /**
   * Redefinit le projet, et remet a jour le modele.
   * @param _prj Le projet courant.
   */
  public void setProjet(PivProject _prj) {
    prj_=_prj;
    update();
  }

  /**
   * Remise a jour du mod�le depuis le projet.
   */
  public void update() {
    computeIASA();
  }

  private void computeIASA() {
    GISZoneCollectionLigneBrisee zl=(GISZoneCollectionLigneBrisee)getGeomData();
    zl.removeAll(null);

    PivComputeParameters params=prj_.getComputeParameters();
    if (params==null) return;

    int xc;
    int yc;
    if (params.getIACenterPosition()==null) {
      xc=prj_.getTransfImageSize().width/2;
      yc=prj_.getTransfImageSize().height/2;
    }
    else {
      xc=(int)params.getIACenterPosition().x_;
      yc=(int)params.getIACenterPosition().y_;
    }
    
    int iasize=0;
    int sim=0;
    int sip=0;
    int sjm=0;
    int sjp=0;
    if (params.isFilledForComputing()) {
      iasize=params.getIASize();
      sim=params.getSim();
      sip=params.getSip();
      sjm=params.getSjm();
      sjp=params.getSjp();
    }

    // IA
    GISPolygone pg=GISGeometryFactory.INSTANCE.createLinearRing(
            xc-iasize/2, xc+iasize/2, yc-iasize/2, yc+iasize/2);
    zl.addPolygone(pg,new Object[]{0},null);
    
    // Centre IA
    CoordinateSequence seq=new GISCoordinateSequence(5);
    int size=3;
    seq.setOrdinate(0,0,xc-size);
    seq.setOrdinate(0,1,yc-size);
    seq.setOrdinate(1,0,xc+size);
    seq.setOrdinate(1,1,yc+size);
    seq.setOrdinate(2,0,xc);
    seq.setOrdinate(2,1,yc);
    seq.setOrdinate(3,0,xc+size);
    seq.setOrdinate(3,1,yc-size);
    seq.setOrdinate(4,0,xc-size);
    seq.setOrdinate(4,1,yc+size);
    GISPolyligne pl=GISGeometryFactory.INSTANCE.createLineString(seq);
    zl.addPolyligne(pl,new Object[]{0},null);
    
    // SA
    pg=GISGeometryFactory.INSTANCE.createLinearRing(xc-sjm, xc+sjp, yc-sim, yc+sip);
    zl.addPolygone(pg,new Object[]{1},null);
  }
}

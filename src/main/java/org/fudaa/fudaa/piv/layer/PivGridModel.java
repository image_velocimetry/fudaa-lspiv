/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fudaa.fudaa.piv.layer;

import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ebli.calque.edition.ZModelePointEditable;
import org.fudaa.fudaa.piv.metier.PivProject;

/**
 * Un mod�le pour les points de grille. Ce mod�le est �ditable, certains points
 * peuvent �tre supprim�s.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivGridModel extends ZModelePointEditable implements PivEditableModel {
  // Le projet.
  PivProject prj_;

  /**
   * Constructeur.
   */
  public PivGridModel() {
    super();
  }
  
  public PivGridModel(GISZoneCollectionPoint _zone) {
    super(_zone);
  }

//  /**
//   * Redefinit le projet, et remet a jour le modele
//   * @param _prj Le projet courant.
//   */
//  public void setProjet(PivProject _prj) {
//    prj_=_prj;
//    ((GISZoneCollectionPoint)getGeomData()).removeAll(null);
//  }

  // Ne fait rien, le projet n'a pas besoin d'etre remis a jour.
  @Override
  public void geometryChanged(int... _sel) {}
}

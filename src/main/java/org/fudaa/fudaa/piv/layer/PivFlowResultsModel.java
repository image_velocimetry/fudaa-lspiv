package org.fudaa.fudaa.piv.layer;

import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.calque.ZCalqueAffichageDonneesInterface;
import org.fudaa.ebli.calque.ZCalqueFleche;
import org.fudaa.ebli.calque.ZModeleFleche;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.palette.BPaletteInfo.InfoData;
import org.fudaa.fudaa.piv.PivResource;
import org.fudaa.fudaa.piv.metier.PivFlowResults;
import org.fudaa.fudaa.piv.metier.PivGlobalFlowResults;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivResultsI;
import org.fudaa.fudaa.piv.metier.PivResultsTransformationAdapter;

import com.memoire.bu.BuTable;

/**
 * Un modele pour le trac� des d�bits sous forme de vecteurs vitesses.
 * 
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivFlowResultsModel implements ZModeleFleche {
  /** Le projet. **/
  PivProject prj_;
  /** Le point, toujours le m�me, pour retourner les coordonn�es 3D d'un r�sultat. */
  GrPoint ptCache_=new GrPoint();
  /** Les resultats globaux */
  PivGlobalFlowResults res_=new PivGlobalFlowResults(new PivFlowResults[0]);

  /**
   * Le constructeur.
   */
  public PivFlowResultsModel() {
  }

  /**
   * Redefinit le projet, et remet a jour le modele.
   * @param _prj Le projet courant.
   */
  public void setProjet(PivProject _prj) {
    prj_=_prj;
    update();
  }

  /**
   * Met a jour le mod�le
   */
  public void update() {
    PivResultsI[] res=prj_.getFlowResults();
    PivResultsI[] adapters=new PivResultsI[res==null ? 0:res.length];
    for (int i=0; i<adapters.length; i++)
      adapters[i]=new PivResultsTransformationAdapter(res[i], prj_.getTransformationParameters().getToReal());    
    res_=new PivGlobalFlowResults(adapters);
  }

  /** Non implement� */
  @Override
  public boolean interpolate(GrSegment _seg, double _x, double _y) {
    return false;
  }

  /**
   * Initialise le segment depuis les points d'indice _i et _i+1
   * @param _s Le segement.
   * @param _i L'indice du premier point.
   * @param _force Inutilis�.
   * @return Fix� � true.
   */
  @Override
  public boolean segment(GrSegment _s, int _i, boolean _force) {
    _s.o_.x_=getX(_i);
    _s.o_.y_=getY(_i);
    _s.o_.z_=0;
    _s.e_.x_=getX(_i)+getVx(_i);
    _s.e_.y_=getY(_i)+getVy(_i);
    _s.e_.z_=0;

    return true;
  }

  /**
   * Retourne la norme de la vitesse au point d'indice _i
   * @param _i L'indice du point.
   * @return La norme.
   */
  @Override
  public double getNorme(int _i) {
    double vx=getVx(_i);
    double vy=getVy(_i);
    return Math.sqrt(vx*vx+vy*vy);
  }

  /**
   * Retourne la vitesse suivant X au point d'indice _i
   * @param _i L'indice du point.
   * @return La vitesse.
   */
  @Override
  public double getVx(int _i) {
    if (res_==null) return 0;
    return res_.getVx(_i);
  }

  /**
   * Retourne la vitesse suivant Y au point d'indice _i
   * @param _i L'indice du point.
   * @return La vitesse.
   */
  @Override
  public double getVy(int _i) {
    if (res_==null) return 0;
    return res_.getVy(_i);
  }

  /**
   * Retourne la coordonn�e X au point d'indice _i
   * @param _i L'indice du point.
   * @return La coordonn�e.
   */
  @Override
  public double getX(int _i) {
    if (res_==null) return 0;
    return res_.getX(_i);
  }

  /**
   * Retourne la coordonn�e Y au point d'indice _i
   * @param _i L'indice du point.
   * @return La coordonn�e.
   */
  @Override
  public double getY(int _i) {
    if (res_==null) return 0;
    return res_.getY(_i);
  }

  /** Non implement� */
  @Override
  public double getZ1(int _i) {
    return 0;
  }

  /** Non implement� */
  @Override
  public double getZ2(int _i) {
    return 0;
  }
  
  public double getComputeMode(int _i) {
    return res_.getComputeMode(_i);
  }

  @Override
  public BuTable createValuesTable(ZCalqueAffichageDonneesInterface _layer) {
    return new CtuluTable(new ZCalqueFleche.ValueTableModel(this));
  }

  @Override
  public void fillWithInfo(InfoData _d, ZCalqueAffichageDonneesInterface _layer) {
    final ZCalqueFleche.StringInfo info = new ZCalqueFleche.StringInfo();
    info.titleIfOne_ = PivResource.getS("R�sultats: point n�");
    info.title_ = PivResource.getS("R�sultats");
    
    // Infos de r�sultats de calcul
//    if (prj_.getFlowResults()!=null) {
//      _d.put(PivResource.getS("Niveau d'eau (m)"), ""+prj_.getFlowResults().getWaterElevation());
//      _d.put(PivResource.getS("D�bit total (m�/s)"), ""+prj_.getFlowResults().getDischarge());
//      _d.put(PivResource.getS("Aire mouill�e (m�)"), ""+prj_.getFlowResults().getWettedArea());
//      _d.put(PivResource.getS("Vitesse moyenne sur la section (m/s)"), ""+prj_.getFlowResults().getMeanVelocity());
//    }
    
    ZCalqueFleche.fillWithInfo(_d, _layer.getLayerSelection(), this, info);
  }

  /** Non implement� */
  @Override
  public boolean isValuesTableAvailable() {
    return true;
  }

  /**
   * Retourne la boite englobante des points du mod�le.
   * @return La boite.
   */
  @Override
  public GrBoite getDomaine() {
    if (res_==null) return null;

    GrBoite bt=new GrBoite();
    for (int i=0; i<res_.getNombre(); i++) {
      bt.ajuste(res_.getX(i),res_.getY(i),0);
    }
    return bt;
  }

  /**
   * Retourne le nombre de points du mod�le.
   * @return Le nombre de points.
   */
  @Override
  public int getNombre() {
    if (res_==null) return 0;
    return res_.getNombre();
  }

  /** Non implement� */
  @Override
  public Object getObject(int _ind) {
    return null;
  }

  public GrPoint getVertexForObject(int _ind, int _idVertex) {
    if (res_==null) return null;
    if (_ind!=0) return null;
    
    ptCache_.x_=getX(_idVertex);
    ptCache_.y_=getY(_idVertex);
    return ptCache_;
  }

  @Override
  public void prepare() {
  }
}

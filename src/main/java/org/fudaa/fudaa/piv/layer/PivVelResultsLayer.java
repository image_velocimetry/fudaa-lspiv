package org.fudaa.fudaa.piv.layer;

import java.awt.Graphics2D;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.calque.ZCalqueFleche;
import org.fudaa.ebli.calque.ZCalqueGeometry.SelectionMode;
import org.fudaa.ebli.calque.ZModeleSegment;
import org.fudaa.ebli.calque.edition.ZCalqueEditable;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.calque.edition.ZEditorInterface;
import org.fudaa.ebli.calque.edition.ZModeleEditable;
import org.fudaa.ebli.geometrie.GrObjet;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.palette.BPalettePlage;
import org.fudaa.ebli.trace.TraceIcon;

import com.memoire.fu.FuLog;

/**
 * Un calque pour le trac� des vitesses.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivVelResultsLayer extends ZCalqueFleche implements ZCalqueEditable {
  
  /** Defini le caractere editable du calque */
  private boolean isEditable_=false;
  /**
   * Construction du calque.
   */
  public PivVelResultsLayer() {
    super();

    // Definition de la palette
    BPalettePlage palette = new BPalettePlage();
    palette.setReduit(true);
    palette.initPlages(30, 0, 1);
    setPaletteCouleurPlages(palette);
  }
  
  @Override
  protected String getFlecheUnit() {
    return "m/s";
  }

  /**
   * True : Les bornes de la palette peuvent �tre d�finies de mani�re automatique pour tous
   * les temps.
   */
  @Override
  public boolean isDonneesBoiteAvailable() {
    return true;
  }

  /**
   * True : Les bornes de la palette peuvent �tre d�finies de mani�re automatique pour le temps
   * temps courant.
   */
  @Override
  public boolean isDonneesBoiteTimeAvailable() {
    return true;
  }
  
  /**
   * Definition des bornes de la palette pour tous les pas de temps.
   */
  @Override
  public boolean getRange(final CtuluRange _b) {
    PivVelResultsModel mdl=modeleDonnees();
    if (mdl==null) return false;
    
    // on initialise le min/max;
    _b.setToNill();
    for (int tidx = mdl.getNbTime() - 1; tidx >= 0; tidx--) {
      for (int i = mdl.getNombre() - 1; i >= 0; i--) {
        _b.expandTo(mdl.getNorme(tidx, i));
      }
    }
    return true;
  }
  
  /**
   * Definition des bornes de la palette pour le temps courant.
   */
  @Override
  public boolean getTimeRange(final CtuluRange _b) {
    PivVelResultsModel mdl=modeleDonnees();
    if (mdl==null) return false;
    
    // on initialise le min/max;
    _b.setToNill();
    for (int i = mdl.getNombre() - 1; i >= 0; i--) {
      _b.expandTo(mdl.getNorme(i));
    }
    return true;
  }
  
  @Override
  public PivVelResultsModel modeleDonnees() {
    return (PivVelResultsModel )modele_;
  }

  @Override
  public void setModele(ZModeleSegment _modele) {
    if (_modele!=null && !(_modele instanceof PivVelResultsModel))
      throw new IllegalArgumentException("Bad type argument");
    
    super.setModele(_modele);

    adjustPalette();
  }
  
  /**
   * Ajuste la palette en fonction des bornes min/max des valeurs pour tous les
   * pas de temps.
   */
  private void adjustPalette() {
    BPalettePlage palette=(BPalettePlage)getPaletteCouleur();
//    // Pas de plages : On consid�re que la palette n'est pas utilis�e.
//    if (palette==null || palette.getNbPlages()==0) return;
//    if (palette == null) {
//      palette=(BPalettePlage) this.createPaletteCouleur();
//      palette.setReduit(true);
//      palette.initPlages(30, 0, 1);
//    }
//    else {
      CtuluRange range=new CtuluRange();
      this.getRange(range);
      palette.initPlages(palette.getNbPlages(), range.min_, range.max_);
//    }
    this.setPaletteCouleurPlages(palette);
  }

  // Not used
  @Override
  public void paintDeplacement(Graphics2D _g2d, int _dx, int _dy, TraceIcon _ic) {
  }

  @Override
  public boolean removeSelectedObjects(CtuluCommandContainer _cmd, CtuluUI _ui) {
    if (isEditable_) {
      FuLog.debug("Remove object");
      if (!isSelectionEmpty()) {
        final boolean r=modeleDonnees().removeVelocity(getSelectedIndex(), _cmd);
        if (r) {
          // Sinon, le calque ne se repaint pas toujours...
          repaint();
        }
      }
    }
    return false;
  }
  
  /**
   * La methode parent n'a aucun sens => On la surcharge.
   */
  @Override
  public boolean isEditable() {
    return isEditable_;
  }
  
  /**
   * Definit si le calque est editable. Si c'est le cas, les methode d'editions peuvent �tre appel�es.
   * @param _b True : Le calque est editable.
   */
  public void setEditable(boolean _b) {
    isEditable_=_b;
  }

  // Not used
  @Override
  public boolean moveSelectedObjects(double _reelDx, double _reelDy, double _reelDz, CtuluCommandContainer _cmd, CtuluUI _ui) {
    return false;
  }

  // Not used
  @Override
  public boolean rotateSelectedObjects(double _angRad, double _xreel0, double _yreel0, CtuluCommandContainer _cmd, CtuluUI _ui) {
    return false;
  }

  // Not used
  @Override
  public boolean copySelectedObjects(CtuluCommandContainer _cmd, CtuluUI _ui) {
    return false;
  }

  // Not used
  @Override
  public boolean splitSelectedObject(CtuluCommandContainer _cmd, CtuluUI _ui) {
    return false;
  }

  // Not used
  @Override
  public boolean joinSelectedObjects(CtuluCommandContainer _cmd, CtuluUI _ui) {
    return false;
  }

  // Not used
  @Override
  public boolean canAddForme(int _typeForme) {
    return false;
  }

  // Not used
  @Override
  public boolean addForme(GrObjet _o, int _deforme, CtuluCommandContainer _cmd, CtuluUI _ui, ZEditionAttributesDataI _data) {
    return false;
  }

  // Not used
  @Override
  public boolean addAtome(GrPoint _ptReel, CtuluCommandContainer _cmd, CtuluUI _ui) {
    return false;
  }

  // Not used
  @Override
  public boolean addAtomeProjectedOnSelectedGeometry(GrPoint grPoint, CtuluCommandComposite cmp, CtuluUI _ui) {
    return false;
  }

  // Not used
  @Override
  public boolean canUseAtomicMode() {
    return false;
  }

  // Not used
  @Override
  public boolean canUseSegmentMode() {
    return false;
  }

  @Override
  public SelectionMode getSelectionMode() {
    return SelectionMode.NORMAL;
  }

  // Not used
  @Override
  public boolean setSelectionMode(SelectionMode mode) {
    return false;
  }

  // Not used
  @Override
  public boolean isAtomicMode() {
    return false;
  }

  // Not used
  @Override
  public ZModeleEditable getModelEditable() {
    return null;
  }

  // Not used
  @Override
  public ZEditorInterface getEditor() {
    return null;
  }

  // Not used
  @Override
  public void setEditor(ZEditorInterface _editor) {
  }
}

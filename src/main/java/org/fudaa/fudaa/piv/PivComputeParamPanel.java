package org.fudaa.fudaa.piv;

import java.awt.Dimension;

import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.calque.edition.ZCalqueEditionInteractionTargetI;
import org.fudaa.ebli.calque.edition.ZEditionAttributesDataI;
import org.fudaa.ebli.commun.EbliSelectedChangeListener;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.fudaa.piv.action.PivDefineAreaCenterAction;
import org.fudaa.fudaa.piv.io.PivExeLauncher;
import org.fudaa.fudaa.piv.metier.PivComputeParameters;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.utils.PivUtils;

/**
 * Un panneau de saisie des param�tres pour le calcul des vitesses.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivComputeParamPanel extends CtuluDialogPanel {
  
  /**
   * Un editeur qui r�agit � la saisie d'une forme pour la grille.
   *
   * @author Bertrand Marchand (marchand@deltacad.fr)
   * @version $Id$
   */
  class EditionController implements ZCalqueEditionInteractionTargetI {

    /** Non utilis� */
    public void atomicChanged() {
    }

    /** Non utilis� */
    public boolean addNewPolygone(GrPolygone _pg, ZEditionAttributesDataI _data) {
      return true;
    }

    /** 
     * Remet a jour la position du point centre.
     */
    public boolean addNewPoint(GrPoint _point, ZEditionAttributesDataI _data) {
      setCenterPosition(_point);
      actDefineAreaCenter_.changeAll();
      return true;
    }

    /** Non utilis� */
    public boolean addNewRectangle(GrPoint _pt, GrPoint _pt2, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public boolean addNewPolyligne(GrPolyligne _pt, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public boolean addNewMultiPoint(GrPolyligne _pt, ZEditionAttributesDataI _data) {
      return true;
    }

    /** Non utilis� */
    public void setMessage(String _s) {
    }

    /** Non utilis� */
    public void unsetMessage() {
    }

    /** Non utilis� */
    public void pointMove(double _x, double _y) {
    }
  }

  private PivImplementation impl_;
  private ZCalqueEditionInteractionTargetI ctrl_=new EditionController();
  private PivDefineAreaCenterAction actDefineAreaCenter_;
  double resolution_=0.1;
  double timeStep_;
  
  public final static int IA_MIN = 12;
  public final static int IA_MAX = 40;
  public final static int SA_MIN = 2;
  public final static int SA_MAX = 50;
  
  /**
   * Constructeur.
   */
  public PivComputeParamPanel(PivImplementation _impl) {
    impl_=_impl;
    
    initComponents();
    customize();
  }
  
  private void customize() {
    // Wrap, sinon, la fenetre est trop large.
    String text = PivUtils.wrapMessage("<html>"+
        PivResource.getS("<u>Aire d'interrogation</u><p>L'aire d'interrogation (IA) doit �tre suffisamment grande pour englober des traceurs de l'�coulement et d�finir des motifs reconnaissables, mais suffisamment petite pour d�crire les gradients de vitesses avec une r�solution suffisante et limiter la dur�e des calculs.<p>En pratique, entre {0} et {1} pixels.<p><p>", IA_MIN, IA_MAX) +
        PivResource.getS("<u>Aire de recherche</u><p>L'aire de recherche (SA) doit �tre suffisamment grande pour d�tecter les vrais d�placements des traceurs de l'�coulement, mais suffisamment petite pour emp�cher la d�tection de vitesses aberrantes (filtre) et limiter la dur�e des calculs. Il peut �tre utile de faire des calculs pr�liminaires avec de grandes SA sur quelques paires d'images pour �valuer les vitesses et pouvoir ensuite optimiser ses dimensions pour les calculs complets. Dans tous les cas, pr�voir une marge de s�curit�. Une SA trop petite produira des vitesses sous-estim�es, d'orientations aberrantes et avec des corr�lations faibles.<p>En pratique, entre {0} et {1} pixels.", SA_MIN, SA_MAX)+
        "</html>", 132);
    setHelpText(text);
    
    pnIASize.setBorder(javax.swing.BorderFactory.createTitledBorder(PivResource.getS("Aire d'interrogation")));
    lbIASize.setText(PivResource.getS("Taille de l'aire")+" (m):");
    lbIASizePix.setText(PivResource.getS("Taille de l'aire")+" (pix):");
    lbPosition.setText(PivResource.getS("Position du centre"));
    btPosition.setText(PivResource.getS("Position..."));
    
    pnSA.setBorder(javax.swing.BorderFactory.createTitledBorder(PivResource.getS("Aire de recherche")));
    
    Dimension lockprefsize = new Dimension(PivResource.PIV.getIcon("lie-horizontal").getIconWidth(),tfIASize.getPreferredSize().height);
    
//    lbLock1.setIcon(PivResource.PIV.getIcon("lie-horizontal"));
//    lbLock1.setPreferredSize(lockprefsize);
    lbLock2.setIcon(PivResource.PIV.getIcon("lie-horizontal"));
    lbLock2.setPreferredSize(lockprefsize);
    lbLock3.setIcon(PivResource.PIV.getIcon("lie-horizontal"));
    lbLock3.setPreferredSize(lockprefsize);
    lbLock4.setIcon(PivResource.PIV.getIcon("lie-horizontal"));
    lbLock4.setPreferredSize(lockprefsize);
    lbLock5.setIcon(PivResource.PIV.getIcon("lie-horizontal"));
    lbLock5.setPreferredSize(lockprefsize);
    lbLock6.setIcon(PivResource.PIV.getIcon("lie-horizontal"));
    lbLock6.setPreferredSize(lockprefsize);

    lbSchema.setIcon(PivResource.PIV.getIcon("schema_sa.png"));
    
    actDefineAreaCenter_=impl_.get2dFrame().getVisuPanel().getTransfView().getDefineAreaCenterAction();
    actDefineAreaCenter_.setEditionController(ctrl_);
    btPosition.setAction(actDefineAreaCenter_);
    actDefineAreaCenter_.addPropertyChangeListener(new EbliSelectedChangeListener(btPosition));
    
    btPreview.setText(PivResource.getS("Pr�visualiser..."));
    btPreview.setToolTipText(PivResource.getS("Pr�visualise les vitesses brutes"));
    btPreview.addActionListener((evt) -> previewResults());
    
    setPreferredSize(new Dimension(600, getPreferredSize().height));
    
    
//    new PivLinkNumericalTextFields<Double,Double>(tfNbImgSec, tfTemps) {
//
//      @Override
//      public Double calculateFromTF2() {
//        double val = Double.parseDouble(tf2_.getText());
//        if (val==0)
//          return 0.;
//        
//        return 1./val;
//      }
//
//      @Override
//      public Double calculateFromTF1() {
//        double val = Double.parseDouble(tf1_.getText());
//        if (val==0)
//          return 0.;
//        
//        return 1./val;
//      }
//    };
    
    new PivLinkNumericalTextFields<Double,Integer>(tfIASize, tfIASizePix) {

      @Override
      public Double calculateFromTF2() {
        return resolution_*Integer.parseInt(tf2_.getText());
      }

      @Override
      public Integer calculateFromTF1() {
        if (resolution_==0)
          return 0;
        
        // Arrondi, en entier pair.
        int iaSize = (int)(Double.parseDouble(tf1_.getText())/resolution_);
        if (iaSize%2 != 0)
          iaSize++;
        
        return iaSize;
      }
    };
    
    new PivLinkNumericalTextFields<Double,Integer>(tfSim, tfSimPix) {

      @Override
      public Double calculateFromTF2() {
        double val = timeStep_;
        if (val==0)
          return 0.;
        
        return resolution_*Integer.parseInt(tf2_.getText())/val;
      }

      @Override
      public Integer calculateFromTF1() {
        if (resolution_==0)
          return 0;
        
        return (int)(0.5+timeStep_*Double.parseDouble(tf1_.getText())/resolution_);
      }
    };
    
    new PivLinkNumericalTextFields<Double,Integer>(tfSip, tfSipPix) {

      @Override
      public Double calculateFromTF2() {
        double val = timeStep_;
        if (val==0)
          return 0.;
        
        return resolution_*Integer.parseInt(tf2_.getText())/val;
      }

      @Override
      public Integer calculateFromTF1() {
        if (resolution_==0)
          return 0;
        
        return (int)(0.5+timeStep_*Double.parseDouble(tf1_.getText())/resolution_);
      }
    };
    
    new PivLinkNumericalTextFields<Double,Integer>(tfSjm, tfSjmPix) {

      @Override
      public Double calculateFromTF2() {
        double val = timeStep_;
        if (val==0)
          return 0.;
        
        return resolution_*Integer.parseInt(tf2_.getText())/val;
      }

      @Override
      public Integer calculateFromTF1() {
        if (resolution_==0)
          return 0;
        
        return (int)(0.5+timeStep_*Double.parseDouble(tf1_.getText())/resolution_);
      }
    };
    
    new PivLinkNumericalTextFields<Double,Integer>(tfSjp, tfSjpPix) {

      @Override
      public Double calculateFromTF2() {
        double val = timeStep_;
        if (val==0)
          return 0.;
        
        return resolution_*Integer.parseInt(tf2_.getText())/val;
      }

      @Override
      public Integer calculateFromTF1() {
        if (resolution_==0)
          return 0;
        
        return (int)(0.5+timeStep_*Double.parseDouble(tf1_.getText())/resolution_);
      }
    };
  }

  /**
   * Rempli le panneau depuis les donn�es du projet.
   * @param _params L'objet m�tier param�tres de calcul.
   */
  public void setComputeParams(PivComputeParameters _params) {
    timeStep_ = _params.getTimeInterval()!=null ? _params.getTimeInterval():0;
    tfIASizePix.setText("" + (_params.getIASize()!=null ? _params.getIASize():""));
    tfSimPix.setText("" + (_params.getSim()!=null ? _params.getSim():""));
    tfSipPix.setText("" + (_params.getSip()!=null ? _params.getSip():""));
    tfSjmPix.setText("" + (_params.getSjm()!=null ? _params.getSjm():""));
    tfSjpPix.setText("" + (_params.getSjp()!=null ? _params.getSjp():""));
    if (_params.getIACenterPosition()!=null) {
      setCenterPosition(_params.getIACenterPosition());
    }
  }
  
  public void setResolution(double _resolution) {
    resolution_ = _resolution;
  }
  
  public void setCenterPosition(GrPoint _pt) {
    tfPosX.setText(""+(int)_pt.x_);
    tfPosY.setText(""+(int)_pt.y_);
  }

  /**
   * Retourne les param�tres saisis par l'utilisateur. Ces param�tres sont
   * retourn�s dans _params.
   *
   * @param _params L'objet m�tier param�tres de calcul servant � transf�rer les
   * valeurs mises � jour.
   */
  public void retrieveComputeParams(PivComputeParameters _params) {
    GrPoint ptCenter=null;
    boolean bpos=!tfPosX.getText().trim().isEmpty();
    if (bpos) {
      ptCenter=new GrPoint(Integer.parseInt(tfPosX.getText().trim()),Integer.parseInt(tfPosY.getText().trim()),0);
    }
    
    _params.setIASize(Integer.parseInt(tfIASizePix.getText().trim()));
    _params.setIACenterPosition(ptCenter);
    _params.setSim(Integer.parseInt(tfSimPix.getText().trim()));
    _params.setSjm(Integer.parseInt(tfSjmPix.getText().trim()));
    _params.setSip(Integer.parseInt(tfSipPix.getText().trim()));
    _params.setSjp(Integer.parseInt(tfSjpPix.getText().trim()));
//    _params.setTimeInterval(Double.parseDouble(tfTemps.getText().trim()));
  }

  public boolean previewResults() {
    if (!isDataValid())
        return false;
    
    PivProject prj = impl_.getCurrentProject();
    
    if (!impl_.question(PivResource.getS("Pr�visualiser les r�sultats"), 
        PivResource.getS("Pr�visualiser lancera le calcul, supprimera les r�sultats existants,\net affichera les r�sultats de vitesse brute sur la premi�re paire d'images.\nVoulez-vous continuer ?"))) {
      return false;
    }
    else {
      prj.setInstantRawResults(null);
    }
    
    // Pr� visualisation.
    CtuluLog ana=new CtuluLog();
    ana.setDesc(PivResource.getS("Pr�visualisation des vitesses"));

    PivExeLauncher.instance().computeRawInstantResults(ana, impl_.getCurrentProject(), true, null);
    if (ana.containsErrorOrSevereError()) {
      impl_.error(ana.getResume());
      return false;
    }
    
    impl_.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_REAL_VIEW);
    impl_.get2dFrame().getVisuPanel().setRawVelocitiesLayerVisible(true);
    
    // Affichage des statistiques en fin de calcul
    PivStatisticsInfoPanel pnInfo=new PivStatisticsInfoPanel(impl_);
    pnInfo.afficheModale(impl_.getFrame(), PivResource.getS("Statistiques r�sultats"), CtuluDialog.OK_OPTION);
    
    return true;
  }
  
  @Override
  public boolean apply() {
    
    // On se replace dans la vue transform�e
    impl_.get2dFrame().getVisuPanel().setViewMode(PivVisuPanel.MODE_TRANSF_VIEW);
    
    PivProject prj = impl_.getCurrentProject();
    PivComputeParameters params=new PivComputeParameters(prj.getComputeParameters());
    retrieveComputeParams(params);
    
    if (!params.equals(prj.getComputeParameters())) {
      
      if (!params.solverParametersEquals(prj.getComputeParameters())) {
        // Controle des valeurs, non bloquantes.
        if (params.getIASize() < IA_MIN || params.getIASize() > IA_MAX) {
          if (!impl_.question(PivResource.getS("Taille inadapt�e"), PivResource.getS("Attention : La taille de l'IA ne semble pas bien adapt�e. Elle devrait �tre entre {0} et {1} pixels.\n", IA_MIN, IA_MAX) + PivResource.getS("Voulez-vous continuer ?"))) {
            return false;
          }
        }
        if (params.getSim() < SA_MIN && params.getSip() < SA_MIN && params.getSjm() < SA_MIN && params.getSjp() < SA_MIN) {
          if (!impl_.question(PivResource.getS("Taille inadapt�e"), PivResource.getS("Attention : La taille de la SA ne semble pas bien adapt�e. Au moins une des dimensions devrait �tre sup�rieure ou �gale � {0} pixels.\n", SA_MIN) + PivResource.getS("Voulez-vous continuer ?"))) {
            return false;
          }
        }
        if (params.getSim() > SA_MAX ||  params.getSip() > SA_MAX || params.getSjm() > SA_MAX || params.getSjp() > SA_MAX) {
          if (!impl_.question(PivResource.getS("Taille inadapt�e"), PivResource.getS("Attention : La taille de la SA ne semble pas bien adapt�e. Aucune dimension ne devraient �tre sup�rieure � {0} pixels.\n", SA_MAX) + PivResource.getS("Voulez-vous continuer ?"))) {
            return false;
          }
        }
        
        if (prj.hasInstantRawResults()
            && !impl_.question(PivResource.getS("Suppression des r�sultats"), PivResource.getS("Attention : des r�sultats existent et seront supprim�s si vous modifiez les param�tres.\nVoulez-vous continuer ?"))) {
          return false;
        }
        else {
          prj.setInstantRawResults(null);
        }
        
        prj.setComputeParameters(params);
      }
      
      // Si les parametres modifi�s sont uniquemlent visuels, on ne fait rien de sp�cial.
      else {
        prj.setComputeParameters(params);
      }
    }
    
    return true;
  }

  @Override
  public boolean isDataValid() {
    boolean bposx=!tfPosX.getText().trim().isEmpty();
    boolean bposy=!tfPosY.getText().trim().isEmpty();
    
    if (bposx ^ bposy) {
      setErrorText(PivResource.getS("Position J et I doivent �tre d�finis simultan�ment ou vides"));
      return false;
    }
    
    boolean bok=
//        PivUtils.isStrictPositiveReal(this,tfNbImgSec.getText(),PivResource.getS("Nombre d'images par secondes")) &&
//        PivUtils.isStrictPositiveReal(this,tfTemps.getText(),PivResource.getS("Intervalle de temps")) &&
        PivUtils.isStrictPositiveReal(this,tfIASize.getText(),PivResource.getS("Taille de l'aire")+" (m)") &&
        PivUtils.isStrictPositiveInteger(this,tfIASizePix.getText(),PivResource.getS("Taille de l'aire")+" (pix)") &&
        (!bposx || PivUtils.isInteger(this,tfPosX.getText(), PivResource.getS("Position J"))) &&
        (!bposy || PivUtils.isInteger(this,tfPosY.getText(), PivResource.getS("Position I"))) &&
        PivUtils.isPositiveReal(this,tfSim.getText(),PivResource.getS("S1")+" (m/s)") &&
        PivUtils.isPositiveReal(this,tfSip.getText(),PivResource.getS("S2")+" (m/s)") &&
        PivUtils.isPositiveReal(this,tfSjm.getText(),PivResource.getS("S3")+" (m/s)") &&
        PivUtils.isPositiveReal(this,tfSjp.getText(),PivResource.getS("S4")+" (m/s)") &&
        PivUtils.isPositiveInteger(this,tfSimPix.getText(),PivResource.getS("S1")+" (pix)") &&
        PivUtils.isPositiveInteger(this,tfSipPix.getText(),PivResource.getS("S2")+" (pix)") &&
        PivUtils.isPositiveInteger(this,tfSjmPix.getText(),PivResource.getS("S3")+" (pix)") &&
        PivUtils.isPositiveInteger(this,tfSjpPix.getText(),PivResource.getS("S4")+" (pix)")
        ;
    if (!bok) return false;

    // Cas sp�cial de l'IA paire. A cette etape, l'IA est bien un entier.
    if (Integer.parseInt(tfIASizePix.getText().trim())%2!=0) {
      setErrorText(PivResource.getS("Taille de l'aire (pix): Doit �tre un entier positif et pair"));
      return false;
    }
    
    setErrorText("");
    return bok;
  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnIASize = new javax.swing.JPanel();
        lbIASize = new javax.swing.JLabel();
        tfIASize = new javax.swing.JTextField();
        lbLock2 = new javax.swing.JLabel();
        tfIASizePix = new javax.swing.JTextField();
        lbIASizePix = new javax.swing.JLabel();
        lbPosition = new javax.swing.JLabel();
        lbPosJ = new javax.swing.JLabel();
        tfPosX = new javax.swing.JTextField();
        lbPosI = new javax.swing.JLabel();
        tfPosY = new javax.swing.JTextField();
        btPosition = new javax.swing.JButton();
        pnSA = new javax.swing.JPanel();
        lbSchema = new javax.swing.JLabel();
        pnSAVals = new javax.swing.JPanel();
        tfSjm = new javax.swing.JTextField();
        lbLock3 = new javax.swing.JLabel();
        lbLock4 = new javax.swing.JLabel();
        lbLock5 = new javax.swing.JLabel();
        lbLock6 = new javax.swing.JLabel();
        lbSim = new javax.swing.JLabel();
        tfSim = new javax.swing.JTextField();
        lbSip = new javax.swing.JLabel();
        tfSip = new javax.swing.JTextField();
        lbSjm = new javax.swing.JLabel();
        tfSjp = new javax.swing.JTextField();
        lbSjp = new javax.swing.JLabel();
        tfSimPix = new javax.swing.JTextField();
        lbSimPix = new javax.swing.JLabel();
        tfSipPix = new javax.swing.JTextField();
        lbSipPix = new javax.swing.JLabel();
        lbSjmPix = new javax.swing.JLabel();
        tfSjmPix = new javax.swing.JTextField();
        tfSjpPix = new javax.swing.JTextField();
        lbSjpPix = new javax.swing.JLabel();
        btPreview = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();

        pnIASize.setBorder(javax.swing.BorderFactory.createTitledBorder("Aire d'interrogation"));

        lbIASize.setText("Taille de l'aire (m) :");

        lbLock2.setPreferredSize(new java.awt.Dimension(0, 20));

        lbIASizePix.setText("Taille de l'aire (pix) :");

        lbPosition.setText("Position du centre");

        lbPosJ.setText("J :");

        lbPosI.setText("I :");

        btPosition.setText("Position...");

        javax.swing.GroupLayout pnIASizeLayout = new javax.swing.GroupLayout(pnIASize);
        pnIASize.setLayout(pnIASizeLayout);
        pnIASizeLayout.setHorizontalGroup(
            pnIASizeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnIASizeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnIASizeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnIASizeLayout.createSequentialGroup()
                        .addComponent(lbPosition)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbPosJ))
                    .addComponent(lbIASize))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnIASizeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnIASizeLayout.createSequentialGroup()
                        .addComponent(tfPosX)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbPosI)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(pnIASizeLayout.createSequentialGroup()
                        .addComponent(tfIASize, javax.swing.GroupLayout.DEFAULT_SIZE, 56, Short.MAX_VALUE)
                        .addGap(35, 35, 35)
                        .addComponent(lbLock2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)))
                .addGroup(pnIASizeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnIASizeLayout.createSequentialGroup()
                        .addComponent(tfPosY)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btPosition))
                    .addGroup(pnIASizeLayout.createSequentialGroup()
                        .addComponent(lbIASizePix)
                        .addGap(43, 43, 43)
                        .addComponent(tfIASizePix, javax.swing.GroupLayout.DEFAULT_SIZE, 56, Short.MAX_VALUE))))
        );
        pnIASizeLayout.setVerticalGroup(
            pnIASizeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnIASizeLayout.createSequentialGroup()
                .addGroup(pnIASizeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbLock2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnIASizeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbIASize)
                        .addComponent(tfIASize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbIASizePix)
                        .addComponent(tfIASizePix, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnIASizeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbPosition)
                    .addComponent(lbPosJ)
                    .addComponent(tfPosX, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbPosI)
                    .addComponent(tfPosY, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btPosition)))
        );

        pnSA.setBorder(javax.swing.BorderFactory.createTitledBorder("Aire de recherche"));

        lbLock3.setPreferredSize(new java.awt.Dimension(0, 20));

        lbLock4.setPreferredSize(new java.awt.Dimension(0, 20));

        lbLock5.setPreferredSize(new java.awt.Dimension(0, 20));

        lbLock6.setPreferredSize(new java.awt.Dimension(0, 20));

        lbSim.setText("S1 (m/s) :");

        lbSip.setText("S2 (m/s) :");

        lbSjm.setText("S3 (m/s) :");

        lbSjp.setText("S4 (m/s) :");

        lbSimPix.setText("S1 (pix) :");

        lbSipPix.setText("S2 (pix) :");

        lbSjmPix.setText("S3 (pix) :");

        lbSjpPix.setText("S4 (pix) :");

        javax.swing.GroupLayout pnSAValsLayout = new javax.swing.GroupLayout(pnSAVals);
        pnSAVals.setLayout(pnSAValsLayout);
        pnSAValsLayout.setHorizontalGroup(
            pnSAValsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnSAValsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnSAValsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbSim)
                    .addComponent(lbSip)
                    .addComponent(lbSjm)
                    .addComponent(lbSjp))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnSAValsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(tfSip, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
                    .addComponent(tfSjm, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tfSjp, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tfSim))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnSAValsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbLock3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbLock4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbLock5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbLock6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnSAValsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbSimPix)
                    .addComponent(lbSipPix)
                    .addComponent(lbSjmPix)
                    .addComponent(lbSjpPix))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnSAValsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(tfSipPix, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
                    .addComponent(tfSjmPix, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tfSjpPix, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tfSimPix))
                .addContainerGap())
        );
        pnSAValsLayout.setVerticalGroup(
            pnSAValsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnSAValsLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(pnSAValsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnSAValsLayout.createSequentialGroup()
                        .addGroup(pnSAValsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbSimPix)
                            .addComponent(tfSimPix, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnSAValsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbSipPix)
                            .addComponent(tfSipPix, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnSAValsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbSjmPix)
                            .addComponent(tfSjmPix, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnSAValsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbSjpPix)
                            .addComponent(tfSjpPix, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnSAValsLayout.createSequentialGroup()
                        .addGroup(pnSAValsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbSim)
                            .addComponent(tfSim, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbLock3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnSAValsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbSip)
                            .addComponent(tfSip, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbLock4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnSAValsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbSjm)
                            .addComponent(tfSjm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbLock5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnSAValsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbSjp)
                            .addComponent(tfSjp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbLock6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pnSALayout = new javax.swing.GroupLayout(pnSA);
        pnSA.setLayout(pnSALayout);
        pnSALayout.setHorizontalGroup(
            pnSALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnSALayout.createSequentialGroup()
                .addComponent(pnSAVals, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbSchema)
                .addGap(2, 2, 2))
        );
        pnSALayout.setVerticalGroup(
            pnSALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnSALayout.createSequentialGroup()
                .addGroup(pnSALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(pnSAVals, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbSchema, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 8, Short.MAX_VALUE))
        );

        btPreview.setText("Pr�visualisation");
        btPreview.setToolTipText("Pr�visualisation des r�sultats");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnIASize, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnSA, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btPreview))
                    .addComponent(jSeparator1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnIASize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pnSA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btPreview)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btPosition;
    private javax.swing.JButton btPreview;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lbIASize;
    private javax.swing.JLabel lbIASizePix;
    private javax.swing.JLabel lbLock2;
    private javax.swing.JLabel lbLock3;
    private javax.swing.JLabel lbLock4;
    private javax.swing.JLabel lbLock5;
    private javax.swing.JLabel lbLock6;
    private javax.swing.JLabel lbPosI;
    private javax.swing.JLabel lbPosJ;
    private javax.swing.JLabel lbPosition;
    private javax.swing.JLabel lbSchema;
    private javax.swing.JLabel lbSim;
    private javax.swing.JLabel lbSimPix;
    private javax.swing.JLabel lbSip;
    private javax.swing.JLabel lbSipPix;
    private javax.swing.JLabel lbSjm;
    private javax.swing.JLabel lbSjmPix;
    private javax.swing.JLabel lbSjp;
    private javax.swing.JLabel lbSjpPix;
    private javax.swing.JPanel pnIASize;
    private javax.swing.JPanel pnSA;
    private javax.swing.JPanel pnSAVals;
    private javax.swing.JTextField tfIASize;
    private javax.swing.JTextField tfIASizePix;
    private javax.swing.JTextField tfPosX;
    private javax.swing.JTextField tfPosY;
    private javax.swing.JTextField tfSim;
    private javax.swing.JTextField tfSimPix;
    private javax.swing.JTextField tfSip;
    private javax.swing.JTextField tfSipPix;
    private javax.swing.JTextField tfSjm;
    private javax.swing.JTextField tfSjmPix;
    private javax.swing.JTextField tfSjp;
    private javax.swing.JTextField tfSjpPix;
    // End of variables declaration//GEN-END:variables
}

package org.fudaa.fudaa.piv;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.graphe3D.EG3dGraph;
import org.fudaa.ebli.graphe3D.data.EG3dLineModelAbstract;
import org.fudaa.ebli.graphe3D.renderer.EG3dDefaultDataRenderer;
import org.fudaa.ebli.graphe3D.ui.panel.EG3dGraphPanel;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.fudaa.piv.metier.PivFlowResults;
import org.fudaa.fudaa.piv.metier.PivResultsI;
import org.fudaa.fudaa.piv.metier.PivResultsI.ResultType;
import org.fudaa.fudaa.piv.metier.PivResultsTransformationAdapter;
import org.fudaa.fudaa.piv.metier.PivTransect;

/**
 * Un panneau d'affichage 3D des transects avec leurs vitesses calcul�es.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 * @version $Id$
 */
public class PivShow3DTransectPanel extends CtuluDialogPanel {
  PivImplementation impl_;
  EG3dGraph pnGraph_;
  /** Le morphisme pour passer des donn�es data vers le repere courant */
  GrMorphisme toReal_;
  
  class TransectModel extends EG3dLineModelAbstract {
    GrPolyligne pl_;
    
    public TransectModel(GrPolyligne _pl) {
      pl_=_pl;
    }

    @Override
    public int getNbPoints() {
      return pl_.nombre();
    }

    @Override
    public float getX(int _idx) {
      return (float)pl_.sommet(_idx).x_;
    }

    @Override
    public float getY(int _idx) {
      return (float)pl_.sommet(_idx).y_;
    }

    @Override
    public float getZ(int _idx) {
      return (float)pl_.sommet(_idx).z_;
    }
  }
  
  class DischargeModel extends EG3dLineModelAbstract {
    PivResultsI res_;
    GrPolyligne pl_;
    double zniv_;
    double ratio_;
    
    public DischargeModel(GrPolyligne _pl, double _zniv, PivResultsI _res) {
      res_=_res;
      pl_=_pl;
      zniv_=_zniv;
      
      double lgArrowMax=Double.NEGATIVE_INFINITY;
      double vx;
      double vy;
      for (int i=0; i<_res.getNbPoints(); i++) {
        vx=_res.getValue(i,ResultType.VX);
        vy=_res.getValue(i,ResultType.VY);
        lgArrowMax=Math.max(lgArrowMax,Math.sqrt(vx*vx+vy*vy));
      }
      ratio_=pl_.longueurXY()/6./lgArrowMax;
    }

    // Le nombre est modifi� pour afficher les fleches + une ligne horizontale
    @Override
    public int getNbPoints() {
      return res_.getNbPoints()*5+2;
    }

    @Override
    public float getX(int _idx) {
      // 1er point ligne horizontale
      if (_idx==0) {
        return (float)pl_.sommet(0).x_;
      }
      // 2eme point ligne horizontale
      if (_idx==1) {
        return (float)pl_.sommet(pl_.nombre()-1).x_;
      }
      _idx-=2;
      
      double vx;
      double vy;
      double lgFleche;
      
      switch (_idx%5) {
      // Point d'origine
      case 0:
      default:
        return (float)res_.getX(_idx/5);
      // Point extremit�
      case 1:
      case 3:
        return (float)(res_.getX(_idx/5)+res_.getValue(_idx/5,ResultType.VX)*ratio_);
      // Point extremite 1 fleche
      case 2:
        vx=res_.getValue(_idx/5,ResultType.VX)*ratio_;
        vy=res_.getValue(_idx/5,ResultType.VY)*ratio_;
        lgFleche=Math.sqrt(vx*vx+vy*vy)/10.;
        double angVit=Math.atan2(vy, vx);
        double angFleche=angVit+5./6.*Math.PI;
        return (float)(lgFleche*Math.cos(angFleche)+res_.getX(_idx/5)+vx);
        // Point extremite 2 fleche
      case 4:
        vx=res_.getValue(_idx/5,ResultType.VX)*ratio_;
        vy=res_.getValue(_idx/5,ResultType.VY)*ratio_;
        lgFleche=Math.sqrt(vx*vx+vy*vy)/10.;
        angVit=Math.atan2(vy,vx);
        angFleche=angVit-5./6.*Math.PI;
        return (float)(lgFleche*Math.cos(angFleche)+res_.getX(_idx/5)+vx);
      }
    }

    @Override
    public float getY(int _idx) {
      // 1er point ligne horizontale
      if (_idx==0) {
        return (float)pl_.sommet(0).y_;
      }
      // 2eme point ligne horizontale
      else if (_idx==1) {
        return (float)pl_.sommet(pl_.nombre()-1).y_;
      }
      _idx-=2;

      double vx;
      double vy;
      double lgFleche;

      switch (_idx % 5) {
      // Point d'origine
      case 0:
      default:
        return (float) res_.getY(_idx / 5);
      // Point extremit�
      case 1:
      case 3:
        return (float) (res_.getY(_idx / 5) + res_.getValue(_idx / 5, ResultType.VY) * ratio_);
      // Point extremite 1 fleche
      case 2:
        vx=res_.getValue(_idx / 5, ResultType.VX) * ratio_;
        vy=res_.getValue(_idx / 5, ResultType.VY) * ratio_;
        lgFleche=Math.sqrt(vx * vx + vy * vy) / 10.;
        double angVit=Math.atan2(vy, vx);
        double angFleche=angVit + 5. / 6. * Math.PI;
        return (float) (lgFleche * Math.sin(angFleche) + res_.getY(_idx / 5) + vy);
      // Point extremite 2 fleche
      case 4:
        vx=res_.getValue(_idx / 5, ResultType.VX) * ratio_;
        vy=res_.getValue(_idx / 5, ResultType.VY) * ratio_;
        lgFleche=Math.sqrt(vx * vx + vy * vy) / 10.;
        angVit=Math.atan2(vy, vx);
        angFleche=angVit - 5. / 6. * Math.PI;
        return (float) (lgFleche * Math.sin(angFleche) + res_.getY(_idx / 5) + vy);
      }
    }

    @Override
    public float getZ(int _idx) {
      return (float)zniv_;
    }
    
    /**
     * @return True, si le trac� entre le point pr�c�dent et celui ci correspondant � une vitesse extrapol�e.
     */
    public boolean isExtrapolated(int _idx) {
      // 1er point ligne horizontale
      if (_idx<2) {
        return false;
      }
      _idx-=2;
      
      return res_.getValue(_idx/5, ResultType.COMPUTE_MODE)!=0;
    }
  }
  
  class DischargeRenderer extends EG3dDefaultDataRenderer {
    DischargeModel mdl_;
    
    public DischargeRenderer(DischargeModel _mdl) {
      mdl_=_mdl;
      
      setLineColor(0, Color.ORANGE.darker());
      setLineThickness(0, 1.f);
    }
    
    @Override
    public int getLineStyle(int _idx) {
      if (_idx==0)
        return TraceLigne.POINTILLE;
      if (_idx==1)
        return TraceLigne.INVISIBLE;
      
      if ((_idx-2)%5==4)
        return TraceLigne.INVISIBLE;
      else
        return lm.getTypeTrait();
    }

    /* (non-Javadoc)
     * @see org.fudaa.ebli.graphe3D.renderer.EG3dDefaultDataRenderer#getLineColor(int)
     */
    @Override
    public Color getLineColor(int _idx) {
      if (mdl_.isExtrapolated(_idx))
        return new Color(200,120,0);
      else
        return Color.ORANGE;
    }
  }
  
  class TransectRenderer extends EG3dDefaultDataRenderer {
    public TransectRenderer() {
      
      setLineColor(0, Color.BLUE);
      setLineThickness(0, 1.f);
    }
  }
  
  /**
   * Constructeur.
   */
  public PivShow3DTransectPanel(PivImplementation _impl) {
    impl_=_impl;
    customize();
  }

  private void customize() {
    setLayout(new BorderLayout());
    
    EG3dGraphPanel pn=new EG3dGraphPanel();
    pn.setPreferredSize(new Dimension(700,500));
    
    pn.getBtnPanel().showSurfaceButton(false);
    
    pnGraph_=pn.getView();
//    pnGraph_.getViewRenderer().setDisplayGrids(true);
    pnGraph_.getViewRenderer().getProjector().setDistance(100000.f);
    pnGraph_.getViewRenderer().getProjector().setRotationAngle(160);
    pnGraph_.getViewRenderer().getProjector().setElevationAngle(20);
    
    this.add(pn,BorderLayout.CENTER);
    setHelpText(PivResource.getS("Clic gauche : Rotation de la vue\nCtrl+clic gauche : D�placement de la vue\nShift+clic gauche : Zoom de la vue"));
  }
  
  public EG3dGraph get3dGraph() {
    return pnGraph_;
  }
  
  public void setSelectedTransects(int... _isels) {
    pnGraph_.removeAllDatas();
    
    toReal_=impl_.getCurrentProject().getTransformationParameters().getToReal();
    
    PivTransect[] transects=impl_.getCurrentProject().getTransects();
    
    for (int i=0; i<_isels.length; i++) {
      TransectModel mdl=new TransectModel(transects[_isels[i]].getStraight().applique(toReal_));
      pnGraph_.addData(mdl);
      pnGraph_.setDataRenderer(i, new TransectRenderer());
    }
    
    PivFlowResults[] res=impl_.getCurrentProject().getFlowResults();
    if (res!=null) {
      for (int i=0; i<_isels.length; i++) {
        PivResultsI resAdapter=new PivResultsTransformationAdapter(res[_isels[i]], toReal_);
        GrPolyligne pl=transects[_isels[i]].getStraight().applique(toReal_);
        GrPoint ptZ=new GrPoint(0,0,res[_isels[i]].getWaterElevation()).applique(toReal_);
        
        DischargeModel mdl=new DischargeModel(pl,ptZ.z_,resAdapter);
        
        pnGraph_.addData(mdl);
        pnGraph_.setDataRenderer(i+_isels.length, new DischargeRenderer(mdl));
      }
    }
    
    // Retaillage des limites de boite pour un ratio identique sur X et Y
    float[] xrange=pnGraph_.getRangeX();
    float[] yrange=pnGraph_.getRangeY();
    float delta=(xrange[1]-xrange[0])-(yrange[1]-yrange[0]);
    if (delta>0) {
      yrange[0]-=delta/2.;
      yrange[1]+=delta/2.;
      pnGraph_.setRangeY(yrange[0], yrange[1]);
    }
    else {
      xrange[0]+=delta/2.;
      xrange[1]-=delta/2.;
      pnGraph_.setRangeX(xrange[0], xrange[1]);
    }
  }
}

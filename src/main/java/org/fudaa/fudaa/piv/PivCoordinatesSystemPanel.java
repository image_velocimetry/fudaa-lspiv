/*
 *  @creation     5 avr. 2005
 *  @modification $Date: 2008-05-13 12:10:31 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.piv;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.fudaa.ebli.commun.EbliFormatterInterface;
import org.fudaa.fudaa.piv.metier.PivProject;
import org.fudaa.fudaa.piv.metier.PivTransformationParameters;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuToolButton;

/**
 * Un panneau pour saisir la matrice de transformation rep�re initial vers rep�re de calcul.
 *
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class PivCoordinatesSystemPanel extends JPanel {

  protected JButton btApply_;
  protected EbliFormatterInterface formatter_;
  protected BuTextField tfTx_;
  protected BuTextField tfTy_;
  protected BuTextField tfTz_;
  protected BuTextField tfXCenter_;
  protected BuTextField tfYCenter_;
  protected BuTextField tfRz_;
  protected PivProject prj_;

  /**
   * @param _deplacement le calque de deplacement
   * @param _formatter
   */
  public PivCoordinatesSystemPanel(EbliFormatterInterface _formatter) {
    super();

    formatter_ = _formatter;

    tfTx_ = BuTextField.createDoubleField();
    tfTx_.setPreferredSize(new Dimension(90, tfTx_.getPreferredSize().height));
    tfTy_ = BuTextField.createDoubleField();
    tfTz_ = BuTextField.createDoubleField();
    tfXCenter_ = BuTextField.createDoubleField();
    tfYCenter_ = BuTextField.createDoubleField();
    tfRz_ = BuTextField.createDoubleField();
    tfRz_.setToolTipText(PivResource.getS("Angle en degr�s"));

    btApply_ = new BuToolButton(BuResource.BU.getToolIcon("appliquer"));
    btApply_.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent _e) {
        if (btApply_.isEnabled() && btApply_ == _e.getSource()) {
          Double tx = (Double) tfTx_.getValue();
          Double ty = (Double) tfTy_.getValue();
          Double tz = (Double) tfTz_.getValue();
          Double rz = (Double)tfRz_.getValue();
          Double xc = (Double)tfXCenter_.getValue();
          Double yc = (Double)tfYCenter_.getValue();
          
          if (tx != null && ty != null && tz != null && rz != null && xc != null && yc != null && prj_!=null) {
            PivTransformationParameters params=prj_.getTransformationParameters();
            params.setTranslationX(tx);
            params.setTranslationY(ty);
            params.setTranslationZ(tz);
            params.setRotationZ(rz);
            params.setXCenter(xc);
            params.setYCenter(yc);
            prj_.setTransformationParameters(params);
          }
        }
      }
    });

    buildComponent();
    updateComponent();
  }
  
  public void setProject(PivProject _prj) {
    prj_=_prj;
    updateComponent();
  }
  
  public void updateComponent() {
    if (prj_==null)
      return;
    
    tfTx_.setValue(formatter_.getXYFormatter().format(prj_.getTransformationParameters().getTranslationX()));
    tfTy_.setValue(formatter_.getXYFormatter().format(prj_.getTransformationParameters().getTranslationY()));
    tfTz_.setValue(formatter_.getXYFormatter().format(prj_.getTransformationParameters().getTranslationZ()));
    tfRz_.setValue(formatter_.getXYFormatter().format(prj_.getTransformationParameters().getRotationZ()));
    tfXCenter_.setValue(formatter_.getXYFormatter().format(prj_.getTransformationParameters().getXCenter()));
    tfYCenter_.setValue(formatter_.getXYFormatter().format(prj_.getTransformationParameters().getYCenter()));
  }

  protected void buildComponent() {
    JLabel lbTx = new JLabel("Tx:");
    JLabel lbXCenter = new BuLabel(PivResource.getS("X centre:"));
    BuLib.giveSameWidth(lbTx,lbXCenter);
    
    JPanel pnTrans = new JPanel();
    pnTrans.setLayout(new BuGridLayout(2, 2, 2));
    pnTrans.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Translation")));
    pnTrans.add(lbTx);
    pnTrans.add(tfTx_);
    pnTrans.add(new BuLabel("Ty:"));
    pnTrans.add(tfTy_);
    pnTrans.add(new BuLabel("Tz:"));
    pnTrans.add(tfTz_);
    
    JPanel pnRot = new JPanel();
    pnRot.setLayout(new BuGridLayout(2, 2, 2));
    pnRot.setBorder(BorderFactory.createTitledBorder(PivResource.getS("Rotation")));
    pnRot.add(lbXCenter);
    pnRot.add(tfXCenter_);
    pnRot.add(new BuLabel(PivResource.getS("Y centre:")));
    pnRot.add(tfYCenter_);
    pnRot.add(new BuLabel("Rz:"));
    pnRot.add(tfRz_);
    
    JPanel pnMain = new JPanel();
    pnMain.setLayout(new BuGridLayout(1, 2, 2));
    pnMain.add(pnTrans);
    pnMain.add(pnRot);

    JPanel pnButtons = new JPanel();
    pnButtons.setLayout(new BorderLayout(5, 5));
    pnButtons.add(btApply_, BorderLayout.SOUTH);

    setLayout(new BorderLayout(5, 5));
    setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
    add(new JLabel(PivResource.getS("<html><p>Param�tres de transformation du rep�re<br>initial vers le rep�re de calcul</html>")), BorderLayout.NORTH);
    add(pnMain, BorderLayout.CENTER);
    add(pnButtons, BorderLayout.EAST);


  }
}
